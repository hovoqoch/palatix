<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Practicedevices extends Model
{
    protected $table = 'device_practice';
    use SoftDeletes;

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }
    public function elearning()
    {
        return $this->hasOne(Elearning::class,'device_id');
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function device()
    {
        return $this->belongsTo(Device::class);
    }

    public function practice()
    {
        return $this->belongsTo(Practice::class);
    }

    public static function getPracticeDeviceById($id)
    {
        return PracticeDevices::find($id);
    }

    public static function approve($id)
    {
        $device = Practicedevices::find($id);
        $device->status = 1;
        $device->user_id = 0;
        return $device->update();
    }

    public static function keepPrivate($id)
    {
        $device = Practicedevices::find($id);
        $device->status = 1;
        return $device->update();
    }

    public static function getPracticedevices()
    {
        return PracticeDevices::with('practice', 'device', 'user')->get();
    }

    public static function storePracticedevice($params)
    {
        return self::createOrUpdate(null, $params);
    }

    public static function updatePracticedevice($qualification, $params)
    {
        return self::createOrUpdate($qualification, $params);
    }

    public static function deletePracticedevices($device)
    {
        $device->delete();
    }

    private static function createOrUpdate($device, $params)
    {
        if (!isset($device) || (isset($device) && !$device)) {
            $device = new Practicedevices;
        }
        $actualDevice = Device::find($params['device_id']);
        $val_next = (float)$actualDevice->val_duration * 365;

        $device->practice_id = $params['practice_id'];
        $device->user_id = 1;
        $device->device_id = $params['device_id'];
        $device->place_in_practice = $params['place_in_practice'];
        $device->serial_number = $params['serial_number'];
        $device->software_version = $params['software_version'];
        $device->parameter_version = $params['parameter_version'];
        $device->maintenance_report_valid = $params['maintenance_report_valid'];
        $device->setup_approval = $params['setup_approval'];
        $device->manufacturing_year = $params['manufacturing_year'];
        $device->remarks = $params['remarks'];
        $device->maintenance_report_date = strtotime($params['maintenance_report_date']);
        $device->first_setup_date = strtotime($params['first_setup_date']);
        $device->last_validation_date = strtotime($params['last_validation_date']);
        $device->next_validation_date = strtotime($params['last_validation_date'].' + ' .$val_next.' day');
        $device->last_maintenance_date = strtotime($params['last_maintenance_date']);
        $device->save();

        return $device->save();
    }
}

