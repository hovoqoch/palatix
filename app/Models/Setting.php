<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use SoftDeletes;

    public static function getSetting()
    {
        return Setting::all();
    }
    public static function getSettingById($id)
    {
        return Setting::find($id);
    }

    public static function updateSetting($params)
    {
        foreach ($params['settings'] as $key=>$val) {
            Setting::where('key', $key)->update(['val' => $val]);
        }
    }


}
