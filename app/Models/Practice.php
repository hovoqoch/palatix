<?php

namespace App\Models;

use App\Mail\NewPracticeMail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class Practice extends Model
{
    use SoftDeletes;

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function devices()
    {
        return $this->belongsToMany(Device::class)->withPivot('id','deleted_at','next_validation_date');
    }

    public function practicedevices()
    {
        return $this->hasMany(Practicedevices::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
    public function employee()
    {
        return $this->hasMany(User::class)->where('role_id',3);
    }
    public function usersMany()
    {
        return $this->belongsToMany(User::class);
    }


    public static function getPractice($region_id=0)
    {
        if($region_id){
            return Practice::with('region')->where('region_id',$region_id)->get();
        }
        return Practice::with('region')->get();
    }

    public static function storePractice($params)
    {
        return self::createOrUpdate(null, $params);
    }

    public static function updatePractice($practice, $params)
    {
        return self::createOrUpdate($practice, $params);
    }

    public static function deletePractice($practice)
    {
        $practice->delete();
    }

    private static function createOrUpdate($practice, $params):bool
    {
        $isNull = false;
        if (!$practice) {
            $isNull = true;
            $practice = new Practice;
        }
        $practice->name = $params['name_of_the_practice'];
        $practice->specialization = $params['specialization'];
        $practice->street_number = $params['street_and_number'];
        $practice->postal_code = $params['postel_code'];
        $practice->phone = $params['practice_phone_number'];
        $practice->city = $params['city'];
        if (Auth::user()->region_id) {
            $practice->region_id = Auth::user()->region_id;
        } else {
            $practice->region_id = $params['region_id'];
        }
        $practice->site = $params['site'];
        $practice->invoice_name = $params['invoice_name'];
        $practice->invoice_street = $params['invoice_street_and_number'];
        $practice->invoice_postal = $params['invoice_postal_code'];
        $practice->invoice_city = $params['invoice_city'];
        $practice->practice_email = $params['practice_email'];
        $bool = $practice->save();
        if ($isNull) {
            $settings = Setting::where('key', 'admin_email')->first();
            Mail::to($settings->val)->send(new NewPracticeMail($practice));
        }
        return $bool;
    }

    public static function getPracticeById($id, $region_id = 0)
    {
        if($region_id){
            return Practice::where('id',$id)->where('region_id',$region_id)->first();
        }else{
            return Practice::find($id);
        }

    }

    public static function createPracticeWithDoctor($params)
    {
        $pract = new self();
        $pract->name;

        if ($pract->save()) {
            $doctor = new Doctor;
            $doctor->prac_id = $pract->id;
            if ($doctor->save()) {

            } else {
                $pract->delete();
            }
        }
    }
}
