<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'practice_id'];

    public function practice()
    {
        return $this->hasOne(Practice::class, 'id', 'practice_id');
    }

    public static function getSiteById($id)
    {
        return Site::find($id);
    }

    public static function updateSite($site, $params)
    {
        return self::createOrUpdate($site, $params);
    }

    private static function createOrUpdate($site, $params)
    {
        if (!isset($site) || (isset($site) && !$site)) {
            $site = new Site;
        }
        $site->name = $params['name'];
        $site->practice_id = $params['practice_id'];

        return $site->save();
    }

    public static function deleteSite($site)
    {
        $site->delete();
    }
}
