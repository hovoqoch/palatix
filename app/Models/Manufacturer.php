<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manufacturer extends Model
{

    use SoftDeletes;

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public static function getManufacturer()
    {
        return Manufacturer::all();
    }
    public static function deleteManufacturer($type)
    {
        $type->delete();
    }
    public static function storeManufacturer($params)
    {
        return self::createOrUpdate(null, $params);
    }

    public static function updateManufacturer($type, $params)
    {

        return self::createOrUpdate($type, $params);
    }

    public static function getManufacturerById($id)
    {
        return Manufacturer::find($id);
    }

    private static function createOrUpdate($type, $params)
    {
        if (!isset($type) || (isset($type) && !$type)) {
            $type = new Manufacturer;
        }
        $type->name = $params['name'];

        return $type->save();
    }
}
