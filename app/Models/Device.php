<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use SoftDeletes;
    protected $with = ['user'];

    public function manufacturer(){
        return $this->belongsTo(Manufacturer::class);
    }



    public function type(){
        return $this->belongsTo(Type::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }


    public function practices()
    {
        return $this->belongsToMany(Practice::class)->withPivot('id','deleted_at','next_validation_date');
    }

    public static function getDeviceById($id)
    {
        return Device::find($id);
    }

    public static function getDevice()
    {
        return Device::with('manufacturer', 'type','user')->get();
    }

    public static function approveCustomDevice($id,$valDate){
        $device = Device::find($id);
        $device->status = 1;
        $device->user_id = 1;
        $device->val_duration = $valDate;

        $device->save();
        session()->forget('approve_id');
    }
    public static function privateCustomDevice($id,$valDate){
        $device = Device::find($id);
        $device->status = 1;
        $device->val_duration = $valDate;

        $device->save();
        session()->forget('approve_id');
    }
    public static function approve($id)
    {


        $device = Device::find($id);

        if($device->val_duration){
            $device->status = 1;
            $device->user_id = 1;
            $device->save();
            return 'ok';
        }else{
            // device validation period is not set
            return $device;
        }

    }
    public static function keepPrivate($id)
    {
        $device = Device::find($id);
        if($device->val_duration){
            $device->status = 1;
            $device->save();
            return 'ok';
        }else{
            // device validation period is not set
            return $device;
        }
    }

    public static function storeDevice($params)
    {
        return self::createOrUpdate(null, $params);
    }

    public static function updateDevice($device, $params)
    {
        return self::createOrUpdate($device, $params);
    }

    public static function deleteDevice($device)
    {
        $device->delete();
    }

    private static function createOrUpdate($device, $params)
    {
        if (!isset($device) || (isset($device) && !$device)) {
            $device = new Device;
            $device->user_id = auth()->id();
        }

        $device->manufacturer_id = $params['manufacturer_id'] ;
        $device->type_id = $params['type_id'] ;
        $device->val_duration = $params['val_duration'] ;
        $device->name = $params['name'] ;

        return $device->save();
    }
}
