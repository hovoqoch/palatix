<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Response;

class Elearning extends Model
{
    protected $fillable = ['name', 'summary', 'youtube_link', 'vimeo_link', 'attach_file_name', 'device_id', 'practice_id','deadline_date'];

    public static function getElearning()
    {
        return Elearning::with('devices')->get();
    }

    public function answers()
    {
        return $this->hasMany(ElearningQuestionsAnswer::class);
    }

    public function practicedevices()
    {
        return $this->belongsTo(Practicedevices::class,'id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withPivot('id','certification_report','status','admin_notified')->withTimestamps();
    }

    public static function getElearningById($id)
    {
        return Elearning::find($id);
    }

    public static function deleteElearning($elearning)
    {
        $elearning->delete();
    }

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
    public function questions()
    {
        return $this->hasMany(ElearningQuestion::class);
    }
    public function practice()
    {
        return $this->belongsTo(Practice::class);
    }

    public static function testsByPractice($id)
    {
        return self::where('practice_id',$id)->get();
    }
}
