<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    use SoftDeletes;

    public function practices()
    {
        return $this->hasMany(Practice::class);
    }

    public static function getRegions()
    {
        return Region::all();
    }
}
