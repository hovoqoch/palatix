<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use SoftDeletes;

    public static function getType()
    {
        return Type::all();
    }
    public static function deleteType($type)
    {
        $type->delete();
    }
    public static function storeType($params)
    {
        return self::createOrUpdate(null, $params);
    }

    public static function updateType($type, $params)
    {

        return self::createOrUpdate($type, $params);
    }

    public static function getTypeById($id)
    {
        return Type::find($id);
    }

    private static function createOrUpdate($type, $params)
    {
        if (!isset($type) || (isset($type) && !$type)) {
            $type = new Type;
        }
        $type->name = $params['type'];

        return $type->save();
    }
}
