<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ElearningQuestion extends Model
{
    protected $fillable = ['question','elearning_id'];
    public function answers()
    {
        return $this->hasMany(ElearningQuestionsAnswer::class);
    }
    public function right_answers()
    {
        return $this->hasMany(ElearningQuestionsAnswer::class)->where('type', 1);
    }
    public function elearning()
    {
        return $this->belongsTo(Elearning::class);
    }

    public static function getQuestionByID($id)
    {
        return ElearningQuestion::with('answers')->find($id);
    }

    public function getQuestions()
    {
        return ElearningQuestion::with('answers')->get();
    }
}
