<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class Translation extends Model
{
    use SoftDeletes;

    public static function getTranslations()
    {
        return Translation::all();
    }

    public static function storeTranslation($params)
    {
        /*$translation = new User;

        $translation->name = $params['name'];
        $translation->email = $params['email'];
        $translation->role_id = User::DOCTOR;
        $translation->password = Hash::make($params['password']);
        return $translation->save();
        */

        return self::createOrUpdate(null, $params);
    }

    public static function updateTranslation($translation, $params)
    {
        /*$translation->name = $params['name'];
        $translation->email = $params['email'];
        $translation->role_id = $params['role_id'];
        return $translation->save();
        */

        return self::createOrUpdate($translation, $params);
    }

    public static function deleteTranslation($translation)
    {
        $translation->delete();
    }

    private static function createOrUpdate($translation, $params)
    {
        if (!isset($translation) || (isset($translation) && !$translation)) {
            $translation = new Translation;
            $translation->short_code = $params['short_code'];
        }

        $translation->en = $params['en'];
        $translation->de = $params['de'];

        return $translation->save();
    }

    public static function getTranslationById($id)
    {
        return Translation::find($id);
    }
}
