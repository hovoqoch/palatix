<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorsTests extends Model
{
    protected $fillable = ['name', 'summary', 'youtube_link', 'vimeo_link', 'attach_file_name'];
    public function device()
    {
        return $this->belongsTo(Device::class);
    }

    public static function getDoctorTestById($id)
    {
        return DoctorsTests::find($id);
    }
}
