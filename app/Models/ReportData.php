<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportData extends Model
{

    protected $fillable = ['device_practice_id', 'site_practice_id', 'data', 'report_id'];

    public function getData()
    {
        return !empty($this->data) ? json_decode($this->data, true) : [];
    }

    public function devicePractice()
    {
        return $this->hasOne(Practicedevices::class, 'id', 'device_practice_id');
    }

    public function sitePractice()
    {
        return $this->hasOne(Site::class, 'id', 'site_practice_id');
    }

    public function devices()
    {
        return is_null($this->devicePractice) ? $this->sitePractice : $this->devicePractice['device'];
    }

    public function practices()
    {
        return is_null($this->devicePractice) ? $this->sitePractice['practice'] : $this->devicePractice['practice'];
    }

    public function deviceType(): string
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        return is_null($this->devicePractice) ? $translations['site'] ?? 'Site' : $translations['device'] ?? 'Device';
    }
}
