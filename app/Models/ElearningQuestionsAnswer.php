<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ElearningQuestionsAnswer extends Model
{
    protected $fillable = ['answer','type','elearning_question_id'];
    public function question()
    {
        return $this->belongsTo(ElearningQuestion::class);
    }
}
