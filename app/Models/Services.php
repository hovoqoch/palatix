<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class services extends Model
{

    use SoftDeletes;

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public static function getServices()
    {
        return Services::all();
    }

    public static function deleteService($type)
    {
        $type->delete();
    }

    public static function storeServices($params)
    {
        return self::createOrUpdate(null, $params);
    }

    public static function updateService($type, $params)
    {

        return self::createOrUpdate($type, $params);
    }

    public static function getServiceById($id)
    {
        return Services::find($id);
    }

    private static function createOrUpdate($type, $params)
    {
        if (!isset($type) || (isset($type) && !$type)) {
            $type = new Services;
        }
        $type->name = $params['name'];

        return $type->save();
    }

    public static function addDoctorsServices($data, $doctor_id)
    {
        DB::table('services_user')->where('user_id', $doctor_id)->delete();

        foreach ($data as $datum) {

            $array = array(
                'user_id' => $doctor_id,
                'services_id' => $datum,
            );
            DB::table('services_user')->insert($array);
        }
    }
}
