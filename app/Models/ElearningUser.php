<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ElearningUser extends Model
{
    protected $table = "elearning_user";

    public function not_null_elearnings(){
        // return $this->hasMany(Elearning::class, 'id')->with('device')->with('questions');
        return $this->hasMany(ElearningQuestion::class, 'elearning_id')->whereNotNull('question');
    }

    public function elearnings(){
        return $this->hasMany(Elearning::class, 'id')->with('device')->with('questions');
    }

    public function elearnings_belongs(){
        return $this->belongsTo(Elearning::class, 'id')->with('device')->with('questions');
    }

}
