<?php

namespace App\Models;

use Auth;
use App\Mail\DoctorsPasswordMail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const ADMIN = 1;
    const DOCTOR = 2;
    const EMPLOYEE = 3;

    public function role()
    {
        return $this->belongsTo(Role::class);
    }


    public function elearnings()
    {
        return $this->belongsToMany(Elearning::class)
            ->withPivot('id', 'certification_report','status','admin_notified')->withTimestamps();
    }

    public function qualification()
    {
        return $this->belongsTo(Qualification::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function practice()
    {

        return $this->belongsTo(Practice::class);
    }

    public function practicesMany()
    {

        return $this->belongsToMany(Practice::class);
    }

    public function doctor()
    {

        return $this->belongsTo(User::class);
    }

    public static function getUsersById($id = 0)
    {
        if (!$id) {
            $id = Auth::user()->id;
        }
        return User::find($id);
    }

    public static function getDoctors($region_id = 0)
    {
        if (!$region_id) {
            return User::where('role_id', self::DOCTOR)->with('practicesMany', 'qualification', 'doctor')->get();
        }
        return DB::table('users')
            ->join('practice_user', 'users.id', '=', 'practice_user.user_id')
            ->join('practices', 'practices.id', '=', 'practice_user.practice_id')
                ->where('practices.region_id', '=', $region_id)
                ->where('role_id', self::DOCTOR)
            ->select('users.*', 'practices.name AS p_name')
            ->get();

    }

    public static function getAdmins()
    {
        return User::where('role_id', self::ADMIN)->where('region_id', '!=', 0)->with('practice', 'qualification', 'doctor', 'region')->get();
    }

    public static function getEmployees($region_id = 0)
    {
        if (!$region_id) {
            return User::where('role_id', self::EMPLOYEE)->with('qualification','doctor','practice')->get();
        }
        return DB::table('users')
            ->join('practices', 'practices.id', '=', 'users.practice_id')
            ->join('qualifications', 'qualifications.id', '=', 'users.qualification_id')
            ->join('users as doctor', 'users.doctor_id', '=', 'doctor.id')
            ->where('practices.region_id', '=', $region_id)
            ->where('users.role_id', self::EMPLOYEE)
            ->select('users.*', 'practices.name AS p_name', 'qualifications.name AS q_name', 'doctor.name AS d_name')
            ->get();

    }

    public static function getDoctorEmployees($id)
    {
        return User::query()->where('role_id', self::EMPLOYEE)->where('doctor_id', $id)->with('qualification','practice')->get();
    }

    public static function deleteUser($user)
    {
        $user->delete();
    }

    public static function storeDoctor($params)
    {
        return self::createOrUpdate(null, $params);

    }

    public static function storeAdmin($params)
    {
        return self::createOrUpdateAdmin(null, $params);

    }

    public static function updateDoctor($doctor, $params)
    {
        return self::createOrUpdate($doctor, $params);
    }

    private static function createOrUpdate($doctor, $params)
    {
        if (!$doctor) {
            $doctor = new User;
            $doctor->role_id = self::DOCTOR;
            $doctor->password = Hash::make(now());
            $link = $doctor->remember_token = md5(microtime());
            Mail::to($params['email'])->send(new DoctorsPasswordMail($params, $link));
        }

        $doctor->title = $params['title'];
        $doctor->name = $params['name'];
        $doctor->last_name = $params['last_name'];
        $doctor->phone = $params['phone'];
        $doctor->email = $params['email'];

        $doctor->save();

        return $doctor->id;
    }

    /////////////////////////////////////////////////////

    public static function storeEmployee($params)
    {
        return self::createOrUpdateEmployee(null, $params);
    }

    public static function updateEmployee($employee, $params)
    {
        return self::createOrUpdateEmployee($employee, $params);
    }

    public static function updateAdmin($admin, $params)
    {
        return self::createOrUpdateAdmin($admin, $params);
    }

    private static function createOrUpdateEmployee($employee, $params)
    {
        if (!isset($employee) || (isset($employee) && !$employee)) {
            $employee = new User;
            $employee->role_id = self::EMPLOYEE;
            $employee->password = Hash::make(now());
            $link = $employee->remember_token = md5(microtime());
            Mail::to($params['email'])->send(new DoctorsPasswordMail($params, $link));
        }

        $employee->practice_id = $params['practice_id'];
        $employee->title = $params['job_title'];
        $employee->name = $params['name'];
        $employee->last_name = $params['last_name'];
        $employee->phone = $params['phone'];
        $employee->email = $params['email'];
        $employee->qualification_id = $params['qualification_id'];

        $employee->save();

        return $employee->id;
    }

    private static function createOrUpdateAdmin($admin, $params)
    {
        if (!isset($admin) || (isset($admin) && !$admin)) {
            $admin = new User;
            $admin->role_id = self::ADMIN;
            $admin->password = Hash::make(now());
            $link = $admin->remember_token = md5(microtime());
            $params['phone'] = '';
            Mail::to($params['email'])->send(new DoctorsPasswordMail($params, $link));
        }

        $admin->region_id = $params['region_id'];
        $admin->name = $params['name'];
        $admin->last_name = $params['last_name'];
        $admin->email = $params['email'];

        $admin->save();

        return $admin->id;
    }

    public function services()
    {
        return $this->belongsToMany(Services::class);
    }

}
