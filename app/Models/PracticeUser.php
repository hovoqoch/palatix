<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PracticeUser extends Model
{
    protected $table = 'practice_user';

    public function device_practice(){
        return $this->belongsTo(PracticeDevices::class, 'practice_id')->with('device');
    }

    public static function getPractices($user_id)
    {
        return PracticeUser::where('user_id', $user_id)->with('practice')->get();
    }

    public function practice(){
        return $this->hasOne(Practice::class , 'id', 'practice_id');
    }
}
