<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class Qualification extends Model
{
    use SoftDeletes;

    public static function getQualification()
    {
        return Qualification::all();
    }
    public static function getQualificationById($id)
    {
        return Qualification::find($id);
    }

    public static function storeQualification($params)
    {
        return self::createOrUpdate(null, $params);
    }

    public static function updateQualification($qualification, $params)
    {
        return self::createOrUpdate($qualification, $params);
    }

    public static function deleteQualification($qualification)
    {
        $qualification->delete();
    }

    private static function createOrUpdate($date, $params)
    {
        if (!isset($date) || (isset($date) && !$date)) {
            $date = new Qualification;
        }

        $date->name = $params['name'];

        return $date->save();
    }


}
