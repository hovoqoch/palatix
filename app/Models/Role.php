<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    public static function getRoles()
    {
       return self::orderBy('role', 'ASC')->get();
    }
}
