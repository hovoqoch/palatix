<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Training extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','file','device_id'];


    public function device()
    {
        return $this->belongsTo(Device::class);
    }
}
