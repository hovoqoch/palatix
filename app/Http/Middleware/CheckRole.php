<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string $role
     * @return mixed
     */

    public function handle(Request $request, Closure $next, $role)
    {


        $user = $request->user();
        if (is_null($user) || $user->role->name != $role) {
            return abort(404);
        }

        return $next($request);
    }

}
