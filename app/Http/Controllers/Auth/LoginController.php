<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated($request, $user)
    {
        $user->last_login_at = now();
        $user->save();
        if ($user->role->name == 'superadmin') {
            return redirect()->route('admin.home', ['locale' => app()->getLocale()]);
        } elseif ($user->role->name == 'doctor') {
            return redirect()->route('doctor.home', ['locale' => app()->getLocale()]);
        } else {
            return redirect()->route('user.home', ['locale' => app()->getLocale()]);

        }
    }
}
