<?php

namespace App\Http\Controllers\User;

use App\Models\Translation;
use Auth;
use App\Models\Device;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Elearning;
use App\Models\ElearningUser;
use App\Models\ElearningQuestion;
use App\Http\Requests\Admin\StoreElearningRequest;
use App\Http\Requests\Admin\UpdateElearningRequest;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Mpdf\Mpdf;

class UserElearningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|\Illuminate\View\View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['certificates'] ?? 'Certificates';
        /*$elearnings = DB::table('elearning_user')
            ->join('elearnings', 'elearnings.id', '=', 'elearning_user.elearning_id')
            ->join('device_practice', 'device_practice.id', '=', 'elearnings.device_id')
            ->join('devices', 'devices.id', '=', 'device_practice.device_id')
            ->join('elearning_questions', 'elearning_questions.elearning_id', '=', 'elearnings.id', 'left')
            ->select('devices.name as device_name', 'elearning_user.status as test_status',
                'elearnings.id as e_id', 'elearnings.deadline_date as valid_due',
                'elearning_user.certification_report', 'elearning_questions.question')
            ->get();*/
        $user_id = auth()->id();

        $elearnings_user_not_null_questions = ElearningUser::with('not_null_elearnings')->where('user_id', $user_id )->get();
        $elearning_ids = [];
        foreach ($elearnings_user_not_null_questions as $item) {
            if(count($item->not_null_elearnings) > 0){
                $elearning_ids[] = $item->id;
            }
        }
        $elearnings = ElearningUser::with('elearnings_belongs')->where('user_id', $user_id )->whereIn('elearning_id', $elearning_ids)->get();
        if ($request->ajax()) return datatables()->of($elearnings)->make(true);
        $data = [
            'meta_title' => $meta_title,
            'elearnings' => [],
            'parent_link' => '/' . app()->getLocale() . '/user/elearning',
        ];

        return view('user.elearning.home', $data);
    }

    public function passVideo(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['certificates'] ?? 'Certificates';
        $elearning = Elearning::find($id);

        $youtube = substr($elearning->youtube_link, strpos($elearning->youtube_link, "?v=") + 3);
        $elearning->youtube = $youtube;
        $vimeo = substr($elearning->vimeo_link, strrpos($elearning->vimeo_link, '/') + 1);
        $elearning->vimeo = $vimeo;
        $data = [
            'meta_title' => $meta_title,
            'elearning' => $elearning,
            'parent_link' => '/' . app()->getLocale() . '/user/elearning',
        ];
        //dd($elearning);

        return view('user.elearning.single', $data);
    }

    public function passTest(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['certificates'] ?? 'Certificates';
        $elearning = Elearning::with('questions.answers')->find($id);
        //dd($elearning);
        $data = [
            'meta_title' => $meta_title,
            'elearning' => $elearning,
            'parent_link' => '/' . app()->getLocale() . '/user/elearning',
        ];

        return view('user.elearning.test', $data);
    }

    public function checkAnswers(Request $request, $lang, $id)
    {
        $elearning = Elearning::with('questions.answers')->find($id);

        return view('user.elearning.home');
    }

    public function submitTest(Request $request, $lang, $id)
    {
        //not working ????
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $results = [];
        $elearning = Elearning::with('questions.right_answers')->find($id);
        //dd($elearning);
        $questions = $elearning->questions;
        if ($req_answers = $request->get('answers')){
            foreach ($req_answers as $question_id => $answer) {
                $question_with_right_answer = ElearningQuestion::with('right_answers')->find($question_id);
                $question_with_right_answer_id = $question_with_right_answer->right_answers->pluck('id')->toArray();
                foreach ($answer as $anser_id => $item) {
                    if (!in_array($anser_id, $question_with_right_answer_id)) {
                        $results[$question_id] = false;
                        break;
                    }
                    $results[$question_id] = true;
                }
            }
        }


        $true_answers_count = count(array_filter($results));
        $false_answers_count = count($results) - count(array_filter($results));
        $percentage = 0;

        if (count(array_filter($results)) > 0) {
            $percentage = (count(array_filter($results)) / count($results)) * 100;
            if (!is_int($percentage)) {
                $percentage = floatval(number_format($percentage, 2, '.', ''));
            }
        } else {
            $percentage = 0;
        }
        $user = auth()->user();
        $user_id = auth()->id();

        if ($percentage > 70) {
            $status = 1;
        } else {
            $certification_name = '';
            $status = 0;
        }
        $elearning_users = ElearningUser::where('elearning_id', $id)->where('user_id', $user_id)->first();
        $elearning_users->percent_right_answers = $percentage;
        $elearning_users->true_answers_count = $true_answers_count;
        $elearning_users->false_answers_count = $false_answers_count;
        $elearning_users->status = $status;
        $elearning_users->save();

        return redirect(app()->getLocale() . '/user/score/' . $elearning_users->id);
    }

    public function score($lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['score'] ?? 'Score';
        $elearning_users = ElearningUser::find($id);
        $data = [
            'meta_title' => $meta_title,
            'elearning_users' => $elearning_users,
            'parent_link' => '/' . app()->getLocale() . '/user/elearning',
        ];
        return view('user.elearning.after_test', $data);
    }

    public function create_certification($lang, $elearning_id)
    {

        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = auth()->user();
        $user_id = auth()->id();
        $elearning_users = ElearningUser::with('elearnings_belongs')->where('elearning_id', $elearning_id)->where('user_id', $user_id)->first();
        if (isset($elearning_users->status) && $elearning_users->status) {
            $path = storage_path() . '/app/public/user/certifications/';
            /*Begin: Creating PDF*/
            $name = 'certification_report_' . $elearning_id . '_' . $user->id . '.pdf';
            $path_with_name = $path . $name;
            if (!file_exists($path_with_name)) {
                $mpdf = new Mpdf();
                $view = view::make('user.elearning.certification_report', compact(['user', 'elearning_users']))->render();
                $mpdf->WriteHTML($view);
                $mpdf->Output();
            } else {
                unlink($path_with_name); // Delete old pdf
                $mpdf = new Mpdf();
                $view = view::make('user.elearning.certification_report', compact(['user', 'elearning_users']))->render();;
                $mpdf->WriteHTML($view);
                $mpdf->Output();
            }
            /*End: Creating PDF*/

        } else {
            return redirect()->back();
        }
    }


    public function downloadCertificate(Request $request, $lang, $name)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $file_path = storage_path() . '/app/public/user/certifications/' . $name;
        return Response::download($file_path, $name);

    }

    public function downloadAttached(Request $request, $lang, $name)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $file_path = storage_path() . '/app/public/trainings/' . $name;
        return Response::download($file_path, $name);

    }


}
