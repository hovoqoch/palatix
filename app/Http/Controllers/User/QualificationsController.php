<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Translation;
use Illuminate\Http\Request;
use App\Models\Qualification;
use Illuminate\Support\Facades\Validator;

class QualificationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['qualif'] ?? 'Qualifications',
            'parent_link' => route('qualifications.index', app()->getLocale()),
            'date' => Qualification::getQualification(),
        ];
        return view('qualifications.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['qualif'] ?? 'Qualification',
            'meta_parent' => $translations['add_qualification'] ?? 'Add qualification',
            'parent_link' => route('qualifications.index', app()->getLocale()),
        ];
        return view('qualifications.create',$data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);

        Qualification::storeQualification($request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['qual_succ_created'] ?? 'Qualification successfully created');
        return redirect()->route('qualifications.index',[app()->getLocale()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $date = Qualification::getQualificationById($id);
        if (!$date) {
            return redirect()->route('qualifications.index',[app()->getLocale()])->withErrors(['error' => $translations['qual_not_found'] ?? 'Qualification not found']);
        }

        $data = [
            'meta_title' => $translations['qualif'] ?? 'Qualifications',
            'meta_parent' => $translations['edit_qual'] ?? 'Edit qualification',
            'parent_link' => route('qualifications.index', app()->getLocale()),
            'date' => $date,
        ];
        return view('qualifications.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);
        $date = Qualification::getQualificationById($id);
        if (!$date) {
            return redirect()->route('qualifications.index',[app()->getLocale()])->withErrors(['error' => $translations['qual_not_found'] ?? 'Qualification not found']);
        }

        Qualification::updateQualification($date,$request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['qual_succ_update'] ?? 'Qualification successfully updated');
        return redirect()->route('qualifications.index',[app()->getLocale()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $qualification = Qualification::getQualificationById($id);
        if (!$qualification) {
            return redirect()->route('doctors.index',[app()->getLocale()])->withErrors(['error' => $translations['doc_not_found'] ?? 'Doctor not found']);
        }

        Qualification::deleteQualification($qualification);

        session()->flash('type', 'success');
        session()->flash('message', $translations['qual_del'] ?? 'Qualification Deleted');
        return redirect()->route('qualifications.index',[app()->getLocale()]);

    }

}
