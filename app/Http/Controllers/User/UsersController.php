<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Mail\UserInvitationMail;
use App\Models\Role;
use App\Models\Translation;
use App\Models\User;
use App\Models\Practice;
use App\Models\Qualification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => array('createPassword', 'updatePassword')]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $id = auth()->id();
        $employees = User::getDoctorEmployees($id);

        $data = [
            'meta_title' => $translations['employees_list'] ?? 'Employees list',
            'parent_link' => route('types.index', app()->getLocale()),
            'employees' => $employees,
            'type' => 'employee',

        ];
        return view('doctor.employees.home', $data);
    }

    public function createPassword($id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'user' => User::where('remember_token',$id)->first(),
            'meta_title' => $translations['user'] ?? 'User',
            'parent_link' => url(app()->getLocale(),'users'),
            'meta_parent' => $translations['create_password'] ?? 'Create password',
        ];
        return view('user.users.create_password',$data);
    }

    public function updatePassword(Request $request, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::where('remember_token',$id)->first();
        $request->validate([

            'password' => ['required', 'string', 'min:8'],
        ]);
        $user->password = Hash::make($request->password);
        //$user->remember_token = null;
        $user->save();

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['pass_saved'] ?? 'Your password is saved.');

        return redirect('/login');



    }

    /**
     * Display a listing of the doctors.
     *
     * @return \Illuminate\Http\Response
     */
    public function doctors()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $users = User::getDoctors();
        $data = [
            'meta_title' => $translations['doc_list'] ?? 'Dashboard',
            'parent_link' => route('types.index', app()->getLocale()),
            'users' => $users,
            'type' => 'doctor',

        ];
        return view('users.home', $data);
    }

    /**
     * Display a listing of the employees.
     *
     * @return \Illuminate\Http\Response
     */
    public function employees()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $users = User::getEmployees();

        $data = [
            'meta_title' => $translations['employees_list'] ?? 'Employees list',
            'parent_link' => route('types.index', app()->getLocale()),
            'users' => $users,
            'type' => 'employee',

        ];
        return view('users.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['employee'] ?? 'Employee',
            'meta_parent' => $translations['add_employee'] ?? 'Add new employee',
            'parent_link' => url(app()->getLocale(), 'employees'),
            'practices' => Practice::all(),
            'qualifications' => Qualification::all(),
        ];
        return view('doctor.employees.create', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addDoctor()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['doctors'] ?? 'Doctors',
            'meta_parent' => $translations['add_new_doctor'] ?? 'Add new doctor',
            'parent_link' => url(app()->getLocale(), 'doctors'),
            'practices' => Practice::all(),
        ];
        return view('doctors.create', $data);
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'role_id' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::orderBy('role', 'ASC')->get();
        $data = [
            'user' => $user,
            'roles' => $roles,
        ];
        return view('doctor.edit', $data);
    }

    public function profileEdit($locale)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = auth()->user();
        $data = [
            'meta_title' => $translations['profile'] ?? 'Profile',
            'user' => $user,
            'parent_link' => url(app()->getLocale(), 'doctors')
        ];
        return view('user.profile.edit', $data);
    }

    public function profileUpdate(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = auth()->user();


        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],

        ]);

        if ($request->get('email') != $user->email) {
            $request->validate([
                'email' => ['required', 'unique:users', 'email'],
            ]);
        }


        $user->title = $request->title;
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->email = $request->email;

        $user->save();
        return redirect()->back()->with([
            'type' => 'success',
            'message' => $translations['user_succ_update'] ?? 'User was successfully updated'
        ]);
    }

    public function profileUpdatePassword(Request $request, $locale)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = auth()->user();
        // dd($user);
        $hashedPassword = $user->password;
        dd($user->password);

            $request->validate([
                'old_password' => ['required'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);

        if (Hash::check($request->old_password, $hashedPassword)) {
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            return redirect()->back()->with([
                'type' => 'success',
                'message' => $translations['pass_succ_change'] ?? 'Password was successfully changed'
            ]);
        } else {
            return redirect()
                ->back()
                ->with([
                    'type' => 'danger',
                    'message' => $translations['old_pass_incor'] ?? 'Old password was incorrect'
                ]);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::find($id);

        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->title = $request->title;



        $user->save();
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['user_succ_update'] ?? 'User successfully updated');
        return redirect(app()->getLocale() . 'account/employees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::findOrfail($id);
        $user->delete();

        return redirect()->back()->with([
            'type' => 'success',
            'message' => $translations['use_succ_del'] ?? 'User was successfully deleted'
        ]);
    }

}
