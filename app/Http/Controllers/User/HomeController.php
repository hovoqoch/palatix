<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Practice;
use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\Device;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $practice = auth()->user()->practice;
        $devices = $practice->devices->where('pivot.deleted_at', null)->sortBy('pivot.next_validation_date');
        if ($request->ajax()) return datatables()->of($devices)->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['dashboard'] ?? 'Dashboard';
        $data = [
            'meta_title' => $meta_title,
            'devices' => [],
            'parent_link' => '/' . app()->getLocale() . '/account',
        ];
        return view('user.home', $data);
    }
}
