<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\Practicedevices;
use App\Models\Device;
use App\Models\Manufacturer;
use App\Models\Type;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class PracticeDevicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $practice = auth()->user()->practice;
        $all = DB::table('device_practice')
            ->where('device_practice.practice_id','=',$practice->id)
            ->join('devices', 'device_practice.device_id', '=', 'devices.id')
            ->join('manufacturers', 'devices.manufacturer_id', '=', 'manufacturers.id')
            ->join('types', 'devices.type_id', '=', 'types.id')
            ->join('elearnings', 'device_practice.device_id', '=', 'elearnings.device_id','left')
            ->join('elearning_user', 'elearnings.id', '=', 'elearning_user.elearning_id','left')
            ->select('device_practice.*',
                'devices.name AS device_name',
                'devices.id AS device_id',
                'manufacturers.name AS manufacturer',
                'types.name AS type',
                'elearnings.name AS description',
                'elearnings.id AS e_id',
                'elearning_user.user_id AS elearning_user_id',
                'elearning_user.certification_report AS report'
            )
            ->get();
        if ($request->ajax())return datatables()->of($all)->make(true);
        $meta_title = $translations['devices'] ?? 'Devices';
        //$devices = $practice->practicedevices->where('deleted_at', null)->load('device','device.manufacturer', 'device.type','elearning','elearning.users');

        $data = [
            'meta_title' => $meta_title,
            'devices' => [],
            'parent_link' => '/' . app()->getLocale() . '/user/devices',
        ];
        return view('user.practice_devices.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['devices'] ?? 'Devices',
            'meta_parent' => $translations['add_new_device'] ?? 'Add new device',
            'parent_link' => route('user.devices.index', app()->getLocale()),
            'manufactorers' => Manufacturer::getManufacturer(),
            'types' => Type::getType(),
            'devices' => Device::getDevice(),
        ];
        return view('user.practice_devices.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{
            DB::beginTransaction();
            $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
            $practiceDevice = new Practicedevices;
            $data = [
                $practiceDevice->practice_id = auth()->user()->practice_id,
                $practiceDevice->user_id = auth()->id(),
                $practiceDevice->place_in_practice = $request['place_in_practice'],
                $practiceDevice->serial_number = $request['serial_number'],
                $practiceDevice->software_version = $request['software_version'],
                $practiceDevice->parameter_version = $request['parameter_version'],
                $practiceDevice->maintenance_report_valid = $request['maintenance_report_valid'],
                $practiceDevice->setup_approval = $request['setup_approval'],
                $practiceDevice->manufacturing_year = $request['manufacturing_year'],
                $practiceDevice->remarks = $request['remarks'],
                $practiceDevice->maintenance_report_date = strtotime($request['maintenance_report_date']),
                $practiceDevice->next_validation_date = strtotime($request['maintenance_report_date']),
                $practiceDevice->first_setup_date = strtotime($request['first_setup_date']),
                $practiceDevice->last_validation_date = strtotime($request['last_validation_date']),
                $practiceDevice->last_maintenance_date = strtotime($request['last_maintenance_date'])
            ];
            if ($request->device_id == 0) {
                $device = new Device();

                $device->manufacturer_id = $request->manufacturer_id;
                $device->type_id = $request->type_id;
                $device->name = $request->device_name;
                $device->user_id = auth()->id();
                $device->status = 0;
                $device->save();


                $practiceDevice->save([
                    $data,

                    $practiceDevice->device_id = $device->id
                ]);

            } else {

                $device = Device::find($request->device_id);
                $val_next = (float)$device->val_duration * 365;
                $practiceDevice->save([
                    $data,
                    $practiceDevice->next_validation_date = strtotime($request['last_validation_date'] . ' + ' . $val_next . ' day'),
                    $practiceDevice->device_id = $request->device_id
                ]);
            }
            DB::commit();
            $request->session()->flash('type', 'success');
            $request->session()->flash('message', $translations['dev_created'] ?? 'Device successfully created');
        }catch (Exception $exception){
            DB::rollBack();
        }


        return redirect(app()->getLocale() . '/user/devices');
    }

    public function filterDevices(Request $request)
    {
        $devices = Device::all();
        if ($request->manufacturer_id) {
            $devices = $devices->where('manufacturer_id', $request->manufacturer_id);
        }
        if ($request->type_id) {
            $devices = $devices->where('type_id', $request->type_id);
        }
        return response()->json([
            'devices' => array_values($devices->toArray())
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $device = Practicedevices::find($id);
        $meta_title = $translations['devices'] ?? 'Devices';
        $meta_parent = $translations['edit_devices'] ?? 'Edit devices';

        $data = [
            'meta_title' => $meta_title,
            'meta_parent' => $meta_parent,
            'manufactorers' => Manufacturer::getManufacturer(),
            'types' => Type::getType(),
            'parent_link' => route('devices.index', app()->getLocale()),
            'device' => $device,
        ];

        return view('user.practice_devices.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lang ,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $device = Practicedevices::find($id);

        $mainDevice = Device::find($device->id);
        $val_next = (float)$mainDevice->val_duration * 365;

        $device->place_in_practice = $request->place_in_practice;
        $device->serial_number = $request->serial_number;
        $device->software_version = $request->software_version;
        $device->parameter_version = $request->parameter_version;
        $device->maintenance_report_valid = $request->maintenance_report_valid;
        $device->setup_approval = $request->setup_approval;
        $device->remarks = $request->remarks;
        $device->maintenance_report_date = strtotime($request->maintenance_report_date);
        $device->first_setup_date = strtotime($request->first_setup_date);
        $device->last_validation_date = strtotime($request->last_validation_date);
        $device->last_maintenance_date = strtotime($request->last_maintenance_date);
        $device->next_validation_date = strtotime($request->last_validation_date.' + ' .$val_next.' day');

        $device->save();

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['dev_updated'] ?? 'Device successfully updated');
        return redirect(app()->getLocale().'/user/devices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $id = $request->segment(3);
        $device = Device::find($id);

        $device->delete();
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['dev_deleted'] ?? 'Device successfully deleted');
        return redirect()->route('devices.index', [app()->getLocale()]);

    }

}
