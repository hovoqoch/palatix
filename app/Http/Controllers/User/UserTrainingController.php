<?php

namespace App\Http\Controllers\User;

use App\Models\Training;
use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;

class UserTrainingController extends Controller
{
    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if ($request->ajax()) return datatables()->of(Training::with('device')->get())->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['training_documents'] ?? 'Training documents';
        $data = [
            'meta_title' => $meta_title,
            'trainings' => [],
            'parent_link' => '/' . app()->getLocale().'/user/training',
        ];
        return view('user.training.home', $data);
    }



        public function downloadFile($lang,$name)
    {
        $file_path = storage_path() .'/app/public/trainings/'. $name;
        return Response::download($file_path, $name);
    }



}
