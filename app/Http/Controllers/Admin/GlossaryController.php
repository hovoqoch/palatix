<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GlossaryController extends Controller
{
    public function getTranslate($short_code,$lang){
        $get_glossary = DB::table('translations')->select("$lang as translate")->where('short_code','=', $short_code)->first();
        return $get_glossary->translate;
    }
}
