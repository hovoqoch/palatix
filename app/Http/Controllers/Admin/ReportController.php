<?php

namespace App\Http\Controllers\Admin;

use App\Models\Manufacturer;
use App\Models\Practice;
use App\Models\ReportData;
use App\Models\Reports;
use App\Models\Translation;
use App\Models\Type;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;
use Mpdf\HTMLParserMode;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use mysql_xdevapi\Session;

class ReportController extends Controller
{
    public function index()
    {
        $data = [
            'meta_title' => $translations['generate_report'] ?? 'Generate Report',
            'parent_link' => route('practice-devices.index', app()->getLocale()),
            'practices' => Practice::getPractice(auth()->user()->region_id),
            'manufacturers' => Manufacturer::all(),
            'types' => Type::all(),
        ];
        return view('admin.reports.home', $data);

    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function show(Request $request)
    {
        if ($request->ajax()) {
            try {
                $reportData = ReportData::all()->filter(function ($query){
                    $query->setAttribute('type', $query->deviceType());
                    $query->setAttribute('device', $query->devices());
                    $query->setAttribute('practice', $query->practices());
                    return $query;
                });
                return datatables()->of($reportData)->make(true);
            } catch (\Exception $e) {
                dd($e->getMessage());
            }
        }
        $data = [
            'meta_title' => $translations['show_report'] ?? 'Show report',
            'parent_link' => route('practice-devices.index', app()->getLocale()),
            'practices' => Practice::getPractice(auth()->user()->region_id),
            'reports' => []
        ];
        return view('admin.reports.show', $data);

    }

    /**
     * @param string $lang
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(string $lang, int $id, Request $request)
    {
        try {
            $data = ReportData::query()->findOrFail($id);
            $array = $data->getData();
            $keys = ['photo_1', 'photo_2', 'photo_3'];
            foreach ($keys as $key) {
                $result = isset($array[$key]) ? $array[$key] : null;
                if ($result) {
                    $oldPhoto = public_path('images/upload/') . $result;
                    if (file_exists($oldPhoto)) unlink($oldPhoto);
                }
            }
            $data->delete();

            $request->session()->flash('type', 'success');
            $request->session()->flash('message', $translations['success'] ?? 'Action completed successfully');
        } catch (\Exception $e) {
            $request->session()->flash('type', 'danger');
            $request->session()->flash('message', $translations['error'] ?? 'Error, try again');
        }
        return redirect()->back();
    }

    public function generate()
    {

        $data = [
            'meta_title' => $translations['generate_report'] ?? 'Generate Report',
            'parent_link' => route('practice-devices.index', app()->getLocale()),
            'practices' => Practice::getPractice(auth()->user()->region_id),
            'manufacturers' => Manufacturer::all(),
            'types' => Type::all(),
        ];

        return view('admin.reports.home', $data);
    }

    public function dataCreate(Request $request, $lang, int $id = 0)
    {
        $update = [];
        $edit = $request->segment(4) && $request->segment(4) === 'edit' ? true : false;
        $request->session()->put('admin_id', auth()->id());
        if (!$edit) {
            $request->session()->put('device_ids', $request->device_id);
            $request->session()->put('site_ids', $request->site_id);
        } else {
            $update = ReportData::query()->findOrFail($id);
            $devicePracticeID = is_null($update->device_practice_id) ? null : [(string)$update->device_practice_id];
            $sitePracticeID = is_null($update->site_practice_id) ? null : [(string)$update->site_practice_id];
            $request->session()->put('device_ids', $devicePracticeID);
            $request->session()->put('site_ids', $sitePracticeID);
        }
        $data = [
            'meta_title' => $translations['generate_report'] ?? 'Generate Report',
            'parent_link' => route('practice-devices.index', app()->getLocale()),
            'manufacturers' => Manufacturer::all(),
            'types' => Type::all(),
            'update' => $update
        ];
        return view('admin.reports.generate_report', $data);
    }

    public function store(Request $request)
    {
        $photos = [];
        $reportDataRepeatMerge = [];
        $translations = Translation::query()->get()->pluck(app()->getLocale(), 'short_code');
        $data = $request->all();
//        $request->validate([
//            'report_data.*.*'=>'required',
//            'report_data.*.photo_*'=>'mimes:jpeg,jpg,png,gif|sometimes|required|max:2048',
//            'report_data_repeat.*.*'=>'sometimes|required'
//        ]);
        if (key_exists('report_data_repeat', $data)) {
            foreach (array_keys($data['report_data']) as $key) {
                $reportDataRepeatMerge[$key] = array_merge($data['report_data'][$key], $data['report_data_repeat'][$key]);
            }
            $data['report_data'] = $reportDataRepeatMerge;
        }
        $path = public_path('images/upload');
        $session_report_data = $request->session()->all();
        if (!array_key_exists('admin_id', $session_report_data)) return redirect()->back();
        $updateID = $request['update'] === 'false' ? false : (int)$request['update'];
        $files = $request->allFiles();
        if ($fileBool = !empty($files)) {
            foreach ($files['report_data'] as $item => $images) {
                foreach ($images as $key => $image) {
                    if ($image->isValid()) {
                        $ImageUpload = Image::make($image);
                        if (!is_dir($path)) mkdir($path, 0777);
                        $ImageUpload->resize(750, 380);
                        $imageName = time() . '_' . str_replace(' ', '_', mb_strtolower($image->getClientOriginalName()));
                        $ImageUpload->save($path . '/' . $imageName);
                        $data["report_data"][$item][$key] = $imageName;
                        array_push($photos, $key);
                    }
                }
            }
        }
        if ($updateID) {
            $key = array_key_first($data['report_data']);
            $value = $data['report_data'][$key];
            if ($value) {
                $reportData = ReportData::query()->find($updateID);
                foreach ($photos as $photo) {
                    if (key_exists($photo, $reportData->getData())) {
                        $oldPhoto = $path . '/' . $reportData->getData()[$photo];
                        if (file_exists($oldPhoto)) unlink($oldPhoto);
                    }
                }
                $value = array_replace($reportData->getData(), $value);
                $reportData->update([
                    'data' => json_encode($value)
                ]);
            }
        } else {
            $report = Reports::create([
                'admin_id' => $session_report_data['admin_id'],
            ]);
            $report_data = [];

            if ($session_report_data['device_ids']) {
                foreach ($session_report_data['device_ids'] as $item) {
                    $reportData = ReportData::create([
                        'device_practice_id' => $item,
                        'report_id' => $report->id,
                    ]);
                    array_push($report_data, $reportData);
                }
            } else {
                foreach ($session_report_data['site_ids'] as $item) {
                    $reportData = ReportData::create([
                        'site_practice_id' => $item,
                        'report_id' => $report->id,
                    ]);
                    array_push($report_data, $reportData);
                }
            }
            $i = 0;
            foreach ($data['report_data'] as $key => $item) {
                DB::table('report_data')->where('id', $report_data[$i]->id)->update([
                    'data' => json_encode($item)
                ]);
                $i++;
            }
        }


        $request->session()->forget(['admin_id', 'device_ids', 'site_ids']);
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['success'] ?? 'Action completed successfully');
        return redirect()->route('report.show', [app()->getLocale()]);
    }

    /**
     * @param string $lang
     * @param int $id
     * @return RedirectResponse
     */
    public function downloadPDF(string $lang, int $id)
    {
        $data = [
            'meta_title' => $translations['show_report'] ?? 'Show report',
            'parent_link' => route('practice-devices.index', app()->getLocale()),
            'practices' => Practice::getPractice(auth()->user()->region_id),
            'report' => ReportData::with('devicePractice', 'sitePractice')->findOrFail($id)
        ];
        try {
            $mPdf = new Mpdf();
            $view = view('admin.reports.pdf', $data)->render();
            $mPdf->WriteHTML($view);
            $mPdf->Output('report.pdf', 'D');
        } catch (MpdfException $e) {
            request()->session()->flash('type', 'danger');
            request()->session()->flash('message', $translations['error'] ?? $e->getMessage());
        } catch (\Throwable $e) {
            request()->session()->flash('type', 'danger');
            request()->session()->flash('message', $translations['error'] ?? $e->getMessage());
        }
        return redirect()->back();
    }
}
