<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\Qualification;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class QualificationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            try {
                return datatables()->of(Qualification::getQualification())->make(true);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
        }
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['qualif'] ?? 'Qualifications',
            'parent_link' => route('qualifications.index', app()->getLocale()),
            'date' => [],
        ];
        return view('admin.qualifications.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['qualif'] ?? 'Qualifications',
            'meta_parent' => $translations['add_qualification'] ?? 'Add qualification',
            'parent_link' => route('qualifications.index', app()->getLocale()),
        ];
        return view('admin.qualifications.create',$data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
        ]);

        Qualification::storeQualification($request->all());
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['qual_succ_created'] ?? 'Qualification successfully created');
        return redirect()->route('qualifications.index',[app()->getLocale()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $lang
     * @param int $id
     * @return Response
     */
    public function edit(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $date = Qualification::getQualificationById($id);
        if (!$date) {
            return redirect()->route('qualifications.index',[app()->getLocale()])->withErrors(['error' => $translations['no_qualifications'] ?? 'No qualifications']);
        }

        $data = [
            'meta_title' => $translations['qualif'] ?? 'Qualifications',
            'meta_parent' => $translations['edit_qual'] ?? 'Devices',
            'parent_link' => route('qualifications.index', app()->getLocale()),
            'date' => $date,
        ];
        return view('admin.qualifications.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $lang
     * @param int $id
     * @return Response
     */
    public function update(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'name' => ['required', 'string'],
        ]);
        $date = Qualification::getQualificationById($id);
        if (!$date) {
            return redirect()->route('qualifications.index',[app()->getLocale()])->withErrors(['error' => $translations['no_qualifications'] ?? 'No qualifications']);
        }

        Qualification::updateQualification($date,$request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['qual_succ_update'] ?? 'Qualification successfully updated');
        return redirect()->route('qualifications.index',[app()->getLocale()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $qualification = Qualification::getQualificationById($id);
        if (!$qualification) {
            return redirect()->route('doctors.index',[app()->getLocale()])->withErrors(['error' => $translations['no_qualifications'] ?? 'No qualifications']);
        }

        Qualification::deleteQualification($qualification);

        session()->flash('type', 'success');
        session()->flash('message', $translations['qual_del'] ?? 'Qualification Deleted');
        return redirect()->route('qualifications.index',[app()->getLocale()]);

    }

}
