<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguageController extends Controller
{

    public function language_switcher($code){
        $url = $_SERVER['REQUEST_URI'];
        $url_array = explode("/",$url);
        $return = url('/').'/'.$code."/";
        $skizb = 2;
        for($i=$skizb;$i<count($url_array);$i++){
            $return .= $url_array[$i]."/";
        }

        return substr($return,0,-1);
    }

}
