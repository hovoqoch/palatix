<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Device;
use App\Models\Manufacturer;
use App\Models\Translation;
use App\Models\Type;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;


class DevicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View|string
     */
    public function index(Request $request)
    {
        if ($request->ajax()){
            try {
                return datatables()
                    ->of(Device::with('manufacturer', 'type', 'user')->get())->make(true);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'devices' => [],
            'parent_link' => route('devices.index', app()->getLocale()),
            'meta_title' => $translations['devices'] ?? 'Devices',
        ];
        return view('admin.devices.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['devices'] ?? 'Devices',
            'meta_parent' => $translations['add_new_device'] ?? 'Add new device',
            'parent_link' => route('devices.index', app()->getLocale()),
            'manufactorers' => Manufacturer::getManufacturer(),
            'types' => Type::getType(),
        ];
        return view('admin.devices.create', $data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'manufacturer_id' => ['required', 'numeric'],
            'type_id' => ['required', 'numeric'],
            'val_duration' => ['required', 'numeric'],
            'name' => ['required', 'string'],
        ]);

        Device::storeDevice($request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['dev_created'] ?? 'Device successfully created');
        return redirect()->route('devices.index', [app()->getLocale()]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $device = Device::getDeviceById($id);
        if (!$device) {
            return redirect()->route('translations.index', [app()->getLocale()])->withErrors(['error' => $translations['dev_not_found'] ?? 'Device not found']);
        }

        $data = [
            'meta_title' => $translations['devices'] ?? 'Devices',
            'meta_parent' => $translations['edit_devices'] ?? 'Edit devices',
            'manufactorers' => Manufacturer::getManufacturer(),
            'types' => Type::getType(),
            'parent_link' => route('devices.index', app()->getLocale()),
            'device' => $device,
        ];

        return view('admin.devices.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'manufacturer_id' => ['required', 'numeric'],
            'type_id' => ['required', 'numeric'],
            'name' => ['required', 'string'],
        ]);
        $device = Device::getDeviceById($id);
        if (!$device) {
            return redirect()->route('translations.index', [app()->getLocale()])->withErrors(['error' => $translations['dev_not_found'] ?? 'Device not found']);
        }

        if (session()->get('approve_id') && (int)session()->get('approve_id') == (int)$id || session()->get('private_id') && (int)session()->get('private_id') == (int)$id) {
            Device::approveCustomDevice($id, $request->val_duration);
        } else {
            Device::updateDevice($device, $request->all());
        }


        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['dev_updated'] ?? 'Device successfully updated');
        return redirect()->route('devices.index', [app()->getLocale()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $device = Device::getDeviceById($id);
        if (!$device) {
            return redirect()->route('translations.index', [app()->getLocale()])->withErrors(['error' => $translations['dev_not_found'] ?? 'Device not found']);
        }

        Device::deleteDevice($device);
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['dev_deleted'] ?? 'Device successfully deleted');
        return redirect()->route('devices.index', [app()->getLocale()]);

    }

    /**
     * Approve the specified resource in storage.
     *
     * @param int $id
     * @return redirect()->back();
     */
    public function approve($id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        if ($device = Device::approve($id)) {
            if ($device === 'ok') {
                session()->flash('type', 'success');
                session()->flash('message', $translations['dev_approved'] ?? 'Device is successfully approved');
            } else {
                session()->flash('type', 'danger');
                session()->flash('message', $translations['please_add_val_date'] ?? 'Please update validation duration info');
                session()->put('approve_id', $device->id);
                return redirect(app()->getLocale() . '/devices/' . $device->id . '/edit');
            }

        } else {
            session()->flash('type', 'danger');
            session()->flash('message', $translations['error'] ?? 'Error, try again');
        }
        return redirect()->back();
    }

    /**
     * Keeps private the specified resource in storage.
     *
     * @param int $id
     * @return redirect()->back();
     */
    public function keepPrivate($id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        if ($device = Device::keepPrivate($id)) {
            if ($device === 'ok') {
                session()->flash('type', 'success');
                session()->flash('message', $translations['dev_updated'] ?? 'Device is successfully updated');
            } else {
                session()->flash('type', 'danger');
                session()->flash('message', $translations['please_add_val_date'] ?? 'Please update validation duration info');
                session()->put('private_id', $id);
                return redirect(app()->getLocale() . '/devices/' . $id . '/edit');
            }
        } else {
            session()->flash('type', 'danger');
            session()->flash('message', $translations['error'] ?? 'Error, try again');
        }
        return redirect()->back();
    }

    private function methodToGetMembers($start, $length,$search,$sort)
    {
        $data = Device::with('manufacturer', 'type', 'user')
            ->orderBy('devices.name',$sort)
            ->skip($start)->take($length)
            ->get();
        if ($search) {
            $data = Device::with('manufacturer', 'type', 'user')
                ->orderBy('devices.name',$sort)
                ->where('name', 'like', '%' . $search . '%')
                ->get();
        }
        return $data;
    }
}
