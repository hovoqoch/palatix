<?php

namespace App\Http\Controllers\Admin;

use App\Mail\NewDeviceMail;
use App\Models\Device;
use App\Models\Practice;
use App\Models\Practicedevices;
use App\Models\Translation;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Elearning;
use App\Http\Requests\Admin\StoreElearningRequest;
use App\Http\Requests\Admin\UpdateElearningRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class ElearningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if($request->ajax()) return datatables()->of(Elearning::with('device','practice')->get())->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'tests' => [],
            'parent_link' => route('elearning.index', app()->getLocale()),
            'meta_title' => $translations['elearning'] ?? 'E-learning',
        ];
        return view('admin.elearning.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $practices = Practice::all();
        $data = [
            'meta_title' => $translations['elearning'] ?? 'E-learning',
            'meta_parent' => $translations['elearning_test'] ?? 'Add E-learning Test',
            'parent_link' => route('elearning.index', app()->getLocale()),
            'devices'=> Practicedevices::getPracticedevices(),
            'practices'=> Practice::getPractice(Auth::user()->region_id),
        ];
        return view('admin.elearning.create', $data);
    }


    /**
     * Store a newly created Elearning in storage.
     *
     * @param  \App\Http\Requests\StoreElearningRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreElearningRequest $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        list($elearning,$requestdata) = $this->elearning($request);
        foreach ($request['user_id'] as $user){
            $elearning->users()->attach($user, ['status' => 0]);
            $user_single = User::where('id',$user)->first();
            Mail::to($user_single['email'])->send(new NewDeviceMail($requestdata));
        }

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['test_created'] ?? 'Test successfully created');
        return redirect()->route('questions.index',[app()->getLocale(),$elearning->id]);
    }

    /**
     * @param Request $request
     * @param string $lang
     * @param int $id
     * @return Factory|View
     */
    public function edit(Request $request, string $lang, int $id)
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $elearning = Elearning::with('users')->find($id);
        $practices = Practice::all();
        $users = $elearning->practice->employee;
        $devices = Practicedevices::with( 'device')->get();

        $data = [
            'meta_title' => $translations['elearning'] ?? 'E-learning',
            'meta_parent' => $translations['edit_elaerning'] ?? 'Edit Elearning',
            'elearning' => $elearning,
            'practices' => $practices,
            'devices' => $devices,
            'users' => $users,
            'parent_link' => route('elearning.index', app()->getLocale()),
        ];
        return view('admin.elearning.edit',$data);

    }

    public function download_pdf($name,$file)
    {
        $file_path = storage_path() .'/app/public/elearning_files/'. $file;
        return Response::download($file_path, $file);

    }

    public function update(UpdateElearningRequest $request,$lang, $id)
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $elearning = Elearning::query()->find($id);
        $requestdata = $request->all();
        if($request->hasFile('attach_file_name') != '') {
            // Get filename with extension
            $filenameWithExt = $request->file('attach_file_name')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('attach_file_name')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('attach_file_name')->storeAs('public/elearning_files', $fileNameToStore);
            $requestdata['attach_file_name'] = $fileNameToStore;
        }


        if ($request['deadline_date']){
            $requestdata['deadline_date'] = Carbon::parse($request['deadline_date'])->toDateString();
        }
        $elearning->users()->detach();
        $elearning->update($requestdata);
        if ($request->has('user_id')){
            foreach ($request['user_id'] as $user){
                $elearning->users()->attach($user, ['status' => 0]);
            }
        }



        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['test_update'] ?? 'Test successfully updated');
        return redirect()->route('elearning.index',[app()->getLocale()]);
    }

    public function destroy(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');


        $elearning = Elearning::find($id);
        $elearning->users()->detach();

        $elearning->delete();

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['test_deleted'] ?? 'Test successfully deleted');
        return redirect()->route('elearning.index',[app()->getLocale()]);

    }

    public function viewFile($locale,$name)
    {
        return response()->file(storage_path() .'/app/public/elearning_files/'. $name) ;
    }

    public function deleteFile(Request $request,$locale,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $elearning = Elearning::find($id);
        $file = $elearning->attach_file_name;
        Storage::delete('app/public/elearning_files/'. $file);
        $elearning->update([
            'attach_file_name' => null
        ]);
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['file_deleted'] ?? 'File successfully deleted');
        return redirect()->back();

    }

    public function filterDevices(Request $request)
    {
        $devices = Practicedevices::with('practice', 'device', 'user')->get();
        $practice = Practice::find($request->practice_id);
        if ($request->practice_id) {
            $devices = $devices->where('practice_id', $request->practice_id);
            $users = $practice->users->where('role_id', 3);

        }
        return response()->json([
            'devices' => array_values($devices->toArray()),
            'users' => array_values($users->toArray())
        ]);
    }
    public function elearning(StoreElearningRequest $request){
        $requestdata = $request->all();

        if($request->hasFile('attach_file_name')) {
            // Get filename with extension
            $filenameWithExt = $request->file('attach_file_name')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('attach_file_name')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('attach_file_name')->storeAs('public/elearning_files', $fileNameToStore);
        } else {
            $fileNameToStore = null;
        }

        if ($request['deadline_date']){

            $requestdata['deadline_date'] = Carbon::parse($request['deadline_date'])->toDateString();
        }
        $requestdata['attach_file_name'] = $fileNameToStore;

        $elearning = Elearning::create($requestdata);
        return [$elearning,$requestdata];
    }
}
