<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Translation;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\Practicedevices;
use App\Models\Device;
use App\Models\Practice;
use App\Models\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class PracticeDevicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
       if ($request->ajax()){
           try {
               return datatables()->of(Practicedevices::getPracticedevices())->make(true);
           } catch (\Exception $e) {
               dd($e->getMessage());
           }
       }
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'devices' => [],
            'parent_link' => route('practice-devices.index', app()->getLocale()),
            'meta_title' => $translations['pr_device'] ?? 'Practice Devices',
        ];
        return view('admin.practice_devices.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['pr_device'] ?? 'Practice Devices',
            'meta_parent' => $translations['add_pr_div'] ?? 'Add Practice Devices',
            'parent_link' => route('practice-devices.index', app()->getLocale()),
            'practices'=> Practice::getPractice(Auth::user()->region_id),
            'devices'=> Device::getDevice(),
        ];
        return view('admin.practice_devices.create', $data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $translations = $this->newValideate($request);
        $device = Device::find($request->device_id);
        $val_next = (float)$device->val_duration * 365;
        $request->next_validation_date = strtotime($request->last_validation_date.' + ' .$val_next.' day');

        Practicedevices::storePracticedevice($request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['dev_add_to_prac'] ?? 'Device successfully added to Practice');
        return redirect()->route('practice-devices.index',[app()->getLocale()]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $device = Practicedevices::getPracticeDeviceById($id);
        if (!$device) {
            return redirect()->route('practice_devices.index',[app()->getLocale()])->withErrors(['error' => $translations['pr_dev_not_found'] ?? 'Practice device not found']);
        }

        $data = [
            'meta_title' => $translations['pr_device'] ?? 'Practice Devices',
            'meta_parent' => $translations['edit_pr_dev'] ?? 'Edit Practive Device',
            'parent_link' => route('practice-devices.index', app()->getLocale()),
            'practices'=> Practice::getPractice(),
            'devices'=> Device::getDevice(),
            'practiceDevices'=> $device,
        ];

        return view('admin.practice_devices.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {
        $translations = $this->newValideate($request);
        $device = Practicedevices::getPracticeDeviceById($id);
        if (!$device) {
            return redirect()->route('practice_devices.index',[app()->getLocale()])->withErrors(['error' => $translations['pr_dev_not_found'] ?? 'Practice device not found']);
        }

        Practicedevices::updatePracticedevice($device,$request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['pr_dev_succ_upd'] ?? 'Practice Device successfully updated');
        return redirect()->route('practice-devices.index',[app()->getLocale()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $device = Practicedevices::getPracticeDeviceById($id);
        if (!$device) {
            return redirect()->route('practice_devices.index',[app()->getLocale()])->withErrors(['error' => $translations['pr_dev_not_found'] ?? 'Practice device not found']);
        }

        Practicedevices::deletePracticedevices($device);
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['pr_dev_succ_del'] ?? 'Practice Device successfully deleted');
        return redirect()->route('practice-devices.index',[app()->getLocale()]);

    }

    /**
     * Approve the specified resource in storage.
     *
     * @param int $id
     * @return redirect()->back();
     */
    public function approve($id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        if(Device::approve($id)){
            session()->flash('type', 'success');
            session()->flash('message', $translations['dev_succ_appr'] ?? 'Device is successfully approved');
        }else{
            session()->flash('type', 'danger');
            session()->flash('message', $translations['error'] ?? 'Error, try again');
        }
        return redirect()->back();
    }

    /**
     * Keeps private the specified resource in storage.
     *
     * @param int $id
     * @return redirect()->back();
     */
    public function keepPrivate($id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        if(Device::keepPrivate($id)){
            session()->flash('type', 'success');
            session()->flash('message', $translations['dev_updated'] ?? 'Device is successfully updated');
        }else{
            session()->flash('type', 'danger');
            session()->flash('message', $translations['error'] ?? 'Error, try again');
        }
        return redirect()->back();
    }

    private function newValideate($request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'practice_id' => ['required', 'string'],
            'device_id' => ['required', 'numeric'],
            'place_in_practice' => ['required', 'string'],
            'serial_number' => ['required', 'string'],
            'software_version' => ['required', 'string'],
            'parameter_version' => ['required', 'string'],
            'maintenance_report_valid' => ['required', 'string'],
            'maintenance_report_date' => ['required', 'date'],
            'first_setup_date' => ['required', 'date'],
            'setup_approval' => ['required', 'string'],
            'manufacturing_year' => ['required', 'numeric'],
            'last_validation_date' => ['required', 'date'],
            'last_maintenance_date' => ['required', 'date'],
            'remarks' => ['required', 'string'],
        ]);
        return $translations;
    }
}
