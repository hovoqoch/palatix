<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Setting;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->region_id){
            abort(404);die();
        }
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['settings'] ?? 'Settings',
            'parent_link' => route('settings.index', app()->getLocale()),
            'settings' => Setting::getSetting(),
        ];
        return view('admin.settings.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function edit(Request $request,$lang,$id)
    {
        if (auth()->user()->region_id){
            abort(404);die();
        }
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $date = Setting::getSettingById($id);
        if (!$date) {
            return redirect()->route('settings.index',[app()->getLocale()])->withErrors(['error' => $translations['setting_not_found'] ?? 'Setting not found']);
        }

        $data = [
            'meta_title' => $translations['settings'] ?? 'Settings',
            'meta_parent' => $translations['edit_setting'] ?? 'Edit Setting',
            'parent_link' => route('settings.index', app()->getLocale()),
            'date' => $date,
        ];

        return view('admin.settings.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (auth()->user()->region_id){
            abort(404);die();
        }
//        foreach ($request->all()['settings'] as $key=>$val) {
//            Setting::where('key', $key)->update(['val' => $val]);
//        }
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        Setting::updateSetting($request->all());
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['setting_update'] ?? 'Settings successfully updated');
        return redirect()->route('settings.index',[app()->getLocale()]);
    }

    /**
     * Update batch the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateBatch(Request $request)
    {
        if (auth()->user()->region_id){
            abort(404);die();
        }
        $settingInstance = new Setting;
        $value = array();
        foreach ($request->all()['settings'] as $key=>$val) {
            array_push($value,array(
                'key'=>$key,
                'val'=>$val,
            ));
        }

        $index = 'key';

        echo '<pre>'; var_dump($value); die;
        Batch::update($settingInstance, $value, $index);

        return redirect()->route('settings.index',[app()->getLocale()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

    }
}
