<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\DB;
use App\Models\Role;
use App\Models\User;
use App\Models\Practice;
use App\Models\Qualification;
use App\Models\Services;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $users = User::all();

        $data = [
            'meta_title' => $translations['doctor'] ?? 'dev_types',
            'parent_link' => route('types.index', app()->getLocale()),
            'users' => $users,
        ];
        return view('admin.users.home', $data);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function doctors(Request $request)
    {
        if ($request->ajax()){
            try {
                return datatables()->of(User::getDoctors(Auth::user()->region_id))->make(true);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
        }
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['doctor'] ?? 'Doctors',
            'parent_link' => route('doctors.home', app()->getLocale()),
            'doctors' => [],
            'type' => 'doctor',

        ];
        return view('admin.doctors.home', $data);
    }

    public function addDoctor()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');

        $data = [
            'meta_title' => $translations['doctor'] ?? 'Doctors',
            'meta_parent' => $translations['add_new_doctor'] ?? 'Add new doctor',
            'parent_link' => url(app()->getLocale(), 'doctors'),
            'practices' => Practice::getPractice(Auth::user()->region_id),
            'services' => Services::getServices(),
        ];
        return view('admin.doctors.create', $data);
    }

    public function storeDoctor(Request $request)
    {

        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'practice_id' => ['required'],
            'title' => ['required', 'string'],
            'name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'phone' => ['required', 'numeric'],
            'email' => ['required', 'string', 'email', 'unique:users'],
        ]);


        $data = $request->all();
        $practice_ids = $data['practice_id'];
        $data['practice_id'] = NULL;

        if($request->services){
            $doctors_data = $data;
            unset($doctors_data['services']);
            $doctor_id = User::storeDoctor($doctors_data);
            $services_data = $data['services'];
            Services::addDoctorsServices($services_data, $doctor_id);
        }else{
            $doctor_id = User::storeDoctor($data);
        }

        foreach ($practice_ids as $practice_id){
            DB::table('practice_user')->insert([
                'practice_id' => $practice_id,
                'user_id' => $doctor_id
            ]);
        }


        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['doc_succ_created'] ?? 'Doctor successfully created');
        return redirect(app()->getLocale() . '/doctors');
    }

    public function editDoctor($lang, $id)
    {

        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');

        $user = User::with('services', 'practicesMany')->find($id);
        if (Auth::user()->region_id && Auth::user()->region_id != $user->practice->region_id){
            return abort(404);
            die();
        }
        $data = [
            'user' => $user,
            'meta_title' => $translations['doctor'] ?? 'Doctors',
            'meta_parent' => $translations['edit_doc'] ?? 'Edit doctor',
            'parent_link' => url(app()->getLocale(), 'doctors'),
            'practices' => Practice::getPractice(Auth::user()->region_id),
            'services' => Services::getServices(),
        ];
        return view('admin.doctors.edit', $data);
    }

    public function updateDoctor(Request $request, $id)
    {

        // getting translations
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        // validation
        $request->validate([
            'practice_id' => ['required'],
            'title' => ['required', 'string'],
            'name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'phone' => ['required', 'numeric'],
            'email' => ['required', 'string', 'email', "unique:users,email,$id"],
        ]);

        // check if user exists
        $user = User::getUsersById($id);
        if (!$user) {
            return redirect()->route('translations.index', [app()->getLocale()])->withErrors(['error' => $translations['doc_not_found'] ?? 'Doctor not found.']);
        }

        // getting request data
        $data = $request->all();
        // getting practice data
        $practice_ids = $data['practice_id'];
        // unseting practice from user data
        unset($data['practice_id']);

        // Updateing doctor services
        if(isset($data['services'])){
            Services::addDoctorsServices($data['services'], $id);
            // unsetting services from user data
            unset($data['services']);
        }
        // updating user
        User::updateDoctor($user,$data);

        // Updating doctor practices
        DB::table('practice_user')->where('user_id', $id)->delete();
        foreach ($practice_ids as $practice_id){
            DB::table('practice_user')->insert([
                'practice_id' => $practice_id,
                'user_id' => $id
            ]);
        }
        //
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['doc_succ_updated'] ?? 'Doctor successfully updated');
        return redirect(app()->getLocale() . '/doctors');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function employees(Request $request)
    {
        if ($request->ajax()){
            try {
                return datatables()->of(User::getEmployees(Auth::user()->region_id))->make(true);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
        }
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['employees'] ?? 'Employees',
            'parent_link' => route('employees.index', app()->getLocale()),
            'users' => [],
            'type' => 'employee',

        ];
        return view('admin.users.home', $data);
    }

    public function addEmployee()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['employees'] ?? 'Employees',
            'meta_parent' => $translations['add_new_employee'] ?? 'Add new employee',
            'parent_link' => url(app()->getLocale(), 'employees'),
            'practices' => Practice::getPractice(Auth::user()->region_id),
            'qualifications' => Qualification::getQualification(),
            'services' => Services::getServices(),
        ];
        return view('admin.employees.create', $data);
    }

    public function storeEmployee(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'practice_id' => ['required', 'numeric'],
            'name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'phone' => ['required', 'numeric'],
            'job_title' => ['required', 'string'],
            'qualification_id' => ['required', 'numeric'],
            'email' => ['required', 'string', 'email', 'unique:users'],
        ]);

        $employee = $request->all();

        User::storeEmployee($employee);

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['empl_succ_created'] ?? 'Employee successfully created');
        return redirect(app()->getLocale() . '/employees');
    }

    public function editEmployee($lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::with('services', 'practice')->find($id);
        if (Auth::user()->region_id && Auth::user()->region_id != $user->practice->region_id){
            return abort(404);
        }
        $data = [
            'meta_title' => $translations['employees'] ?? 'Employees',
            'meta_parent' => $translations['edit_empl'] ?? 'Edit employee',
            'parent_link' => url(app()->getLocale(), 'employees'),
            'employee' => User::getUsersById($id),
            'qualifications' => Qualification::getQualification(),
            'practices' => Practice::getPractice(Auth::user()->region_id),
            'services' => Services::getServices(),
        ];
        return view('admin.employees.edit', $data);
    }

    public function updateEmployee(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::getUsersById($id);
        if (!$user) {
            return redirect()->route('employees.index', [app()->getLocale()])->withErrors(['error' => $translations['empl_not_found'] ?? 'Employee not found.']);
        }

        User::updateEmployee($user, $request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['empl_succ_updated'] ?? 'Employee successfully updated');
        return redirect(app()->getLocale() . '/employees');
    }

    // Regional admins part
    /**
     * @param Request $request
     * @return Factory|View|void
     * @throws Exception
     */
    public function admins(Request $request)
    {
        if(Auth::user()->region_id){
            return abort(404);
        }
        if($request->ajax()){
            try {
                return datatables()->of(User::getAdmins())->make(true);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
        }
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['regional_admins'] ?? 'Regional Admins',
            'parent_link' => route('admins.index', app()->getLocale()),
            'users' => [],
            'type' => 'admin',

        ];
        return view('admin.admins.home', $data);
    }

    public function addAdmin()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['admins'] ?? 'Admin',
            'meta_parent' => $translations['add_new_admin'] ?? 'Add new admin',
            'parent_link' => url(app()->getLocale(), 'admins'),
            'practices' => Practice::getPractice(),
            'qualifications' => Qualification::getQualification(),
            'services' => Services::getServices(),
            'regions' => Region::all(),
        ];
        return view('admin.admins.create', $data);
    }

    public function storeAdmin(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'region_id' => ['required', 'numeric'],
            'name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'unique:users'],
        ]);

        $admins = $request->all();

        User::storeAdmin($admins);

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['admin_succ_created'] ?? 'Admin successfully created');
        return redirect(app()->getLocale() . '/admins');
    }

    public function editAdmin($lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['admins'] ?? 'Admins',
            'meta_parent' => $translations['edit_empl'] ?? 'Edit employee',
            'parent_link' => url(app()->getLocale(), 'admins'),
            'admin' => User::getUsersById($id),
            'qualifications' => Qualification::getQualification(),
            'practices' => Practice::getPractice(),
            'services' => Services::getServices(),
            'regions' => Region::all(),
        ];
        return view('admin.admins.edit', $data);
    }

    public function updateAdmin(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $admin = User::getUsersById($id);
        if (!$admin) {
            return redirect()->route('admins.index', [app()->getLocale()])->withErrors(['error' => $translations['admin_not_found'] ?? 'admin_not_found']);
        }

        User::updateAdmin($admin, $request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['admin_succ_updated'] ?? 'Admin successfully updated');
        return redirect(app()->getLocale() . '/admins');
    }
    // end regional admins part

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::getUsersById($id);
        if (!$user) {
            return redirect()->route('translations.index', [app()->getLocale()])->withErrors(['error' => $translations['not_found'] ?? 'Not found']);
        }
        User::deleteUser($user);
        DB::table('practice_user')->where('user_id', $id)->delete();

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['succ_del'] ?? 'Successfully deleted');

        return redirect()->back();
    }

    public function create()
    {
        $roles = Role::orderBy('role', 'ASC')->get();
        $data = [
            'roles' => $roles,
        ];
        return view('admin.users.create', $data);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = $request->role_id;
        $user->password = Hash::make($request->password);

        $user->save();
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', 'User successfully created');
        return redirect(app()->getLocale() . '/users');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::orderBy('role', 'ASC')->get();
        $data = [
            'user' => $user,
            'roles' => $roles,
        ];
        return view('admin.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = $request->role_id;

        $user->save();

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', 'User successfully updated');
        return redirect(app()->getLocale() . '/users');
    }

    /**
     * Update User login page.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function changelogin()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['change_login_or_password'] ?? 'Change login or password',
            'parent_link' => url(app()->getLocale(), 'employees'),
            'user' => User::getUsersById(),
        ];
        return view('admin.users.changelogin', $data);
    }

    public function updatepassword(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::getUsersById();

        if($request->password != ''){
            $request->validate([
                'new_password' => 'confirmed|min:6|different:password',
            ]);
            if (Hash::check($request->password, $user->password)) {
                $user->fill([
                    'password' => Hash::make($request->new_password)
                ])->save();

                $request->session()->flash('type', 'success');
                $request->session()->flash('message', $translations['pass_succ_updated'] ?? 'Password successfully updated');
                return redirect(app()->getLocale().'/changelogin');

            } else {
                $request->session()->flash('type', 'danger');
                $request->session()->flash('message', $translations['old_pass_wrong'] ?? 'Error, old password is wrong!');
                return redirect(app()->getLocale().'/changelogin');
            }
        }
        $request->validate([
            'email' => 'required|email|unique:users,email,'.$user->id.',id'
        ]);
        $user->email = $request->email;

        $user->save();
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['pass_succ_updated'] ?? 'Password successfully updated');
        return redirect(app()->getLocale().'/changelogin');
    }
}
