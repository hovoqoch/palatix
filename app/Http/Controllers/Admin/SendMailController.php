<?php

namespace App\Http\Controllers\Admin;


use App\Models\Translation;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class SendMailController extends Controller
{
    public function createpassword($lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'user' => User::where('remember_token',$id)->first(),
            'meta_title' => $translations['doctor'] ?? 'Doctors',
            'parent_link' => url(app()->getLocale(),'doctors'),
            'meta_parent' => $translations['create_password'] ?? 'Create password',
        ];
        return view('admin.doctors.createpassword',$data);
    }

    public function updatepassword(Request $request,$lang, $id)
    {
        $request->validate([
            'password' => 'confirmed|min:6',
        ]);
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::where('remember_token',$id)->first();

        $user->password = Hash::make($request->password);

        $user->remember_token = null;

        $user->save();

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['pass_saved'] ?? 'Your password is saved.');

        return redirect('/login');

    }

}
