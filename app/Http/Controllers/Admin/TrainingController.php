<?php

namespace App\Http\Controllers\Admin;

use App\Models\Device;
use App\Models\Training;

use App\Models\Translation;
use App\Http\Requests\Admin\StoreElearningRequest;
use App\Http\Requests\Admin\UpdateElearningRequest;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class TrainingController extends Controller
{

    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if ($request->ajax()) return datatables()->of(Training::with('device')->get())->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['training'] ?? 'Training';
        $data = [
            'meta_title' => $meta_title,
            'trainings' => [],
            'parent_link' => '/' . app()->getLocale(),
        ];
        return view('admin.training.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $devices = Device::all();
        $data = [
            'meta_title' => $translations['training'] ?? 'Training',
            'meta_parent' => $translations['add_training_mat'] ?? 'Add Training Materials',
            'devices' => $devices,
            'parent_link' => route('training.index', app()->getLocale()),
        ];
        return view('admin.training.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'name' => ['required', 'string'],
            'file' => ['required']
        ]);
        $requestdata = $request->all();
        if ($request->hasFile('file')) {
            $path = 'public/trainings';
            $fileNameToStore = $this->fileUpload($request, $path);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }
        $requestdata['file'] = $fileNameToStore;

        $data = Training::create($requestdata);

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['training_succ_created'] ?? 'Training successfully created');
        return redirect()->route('training.index', [app()->getLocale()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $training = Training::find($id);
        $devices = Device::all();

        $data = [
            'meta_title' => $translations['training'] ?? 'Training',
            'meta_parent' => $translations['edit_train'] ?? 'Edit Training',
            'training' => $training,
            'devices' => $devices,
            'parent_link' => route('training.index', app()->getLocale()),
        ];
        return view('admin.training.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $training = Training::find($id);

        $requestdata = $request->all();


        if($request->hasFile('file') != '') {
            $path = 'public/trainings';
            $fileNameToStore = $this->fileUpload($request, $path);
            $requestdata['file'] = $fileNameToStore;
        }



        $training->update($requestdata);

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['train_succ_updated'] ?? 'Training successfully updated');
        return redirect()->route('training.index',[app()->getLocale()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $training = Training::find($id);
        $training->delete();

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['train_succ_del'] ?? 'Training successfully deleted');
        return redirect()->route('training.index', [app()->getLocale()]);

    }

    public function viewFile($locale,$name)
    {
        $file_path = storage_path() .'/app/public/trainings/'. $name;
        return Response::download($file_path, $name);
    }

    public function deleteFile(Request $request,$locale,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $training = Training::find($id);
        $file = $training->file;
        Storage::delete('app/public/trainings/'. $file);
        $training->update([
            'file' => null
        ]);
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['file_deleted'] ?? 'File successfully deleted');
        return redirect()->back();

    }

    private function fileUpload(Request $request, string $path)
    {
        // Get filename with extension
        $filenameWithExt = $request->file('file')->getClientOriginalName();
        // Get just filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get just ext
        $extension = $request->file('file')->getClientOriginalExtension();
        //Filename to store
        $fileNameToStore = $filename.'_'.time().'.'.$extension;
        // Upload Image
        $request->file('file')->storeAs($path, $fileNameToStore);
        return $fileNameToStore;
    }
}
