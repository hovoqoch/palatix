<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Manufacturer;
use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ManufacturersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if($request->ajax()) return datatables()->of(Manufacturer::getManufacturer())->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['manufacturer'] ?? 'Manufacturer',
            'parent_link' => route('manufacturers.index', app()->getLocale()),
            'types' => [],
        ];
        return view('admin.manufacturers.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['manufacturer'] ?? 'Manufacturer',
            'parent_link' => route('manufacturers.index', app()->getLocale()),
            'meta_parent' => $translations['add_manufacturer'] ?? 'Add Manufacturerg',
        ];
        return view('admin.manufacturers.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'name' => ['required', 'string'],
        ]);

        Manufacturer::storeManufacturer($request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['manufacturer_created'] ?? 'Manufacturer successfully created');
        return redirect(app()->getLocale().'/manufacturers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $type = Manufacturer::getManufacturerById($id);
        if (!$type) {
            return redirect()->route('manufacturers.index',[app()->getLocale()])->withErrors(['error' => $translations['man_not_found'] ?? 'Manufacturer not found']);
        }

        $data = [
            'meta_title' => $translations['manufacturer'] ?? 'Manufacturer',
            'meta_parent' => $translations['edit_manuf'] ?? 'Edit Manufacturer',
            'parent_link' => route('manufacturers.index', app()->getLocale()),
            'type' => $type,
        ];
        return view('admin.manufacturers.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'name' => ['required', 'string'],
        ]);
        $type = Manufacturer::getManufacturerById($id);
        if (!$type) {
            return redirect()->route('manufacturers.index',[app()->getLocale()])->withErrors(['error' => $translations['man_not_found'] ?? 'Manufacturer not found']);
        }

        Manufacturer::updateManufacturer($type,$request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['man_update'] ?? 'Manufacturer successfully updated');
        return redirect(app()->getLocale().'/manufacturers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        DB::table('devices')->where('manufacturer_id',$id)->delete();
        $type = Manufacturer::getManufacturerById($id);
        if (!$type) {
            return redirect()->route('manufacturers.index',[app()->getLocale()])->withErrors(['error' => $translations['man_not_found'] ?? 'Manufacturer not found']);
        }

        Manufacturer::deleteManufacturer($type);

        session()->flash('type', 'success');
        session()->flash('message', $translations['man_deleted'] ?? 'Manufacturer Deleted');
        return redirect(app()->getLocale().'/manufacturers');
    }
}
