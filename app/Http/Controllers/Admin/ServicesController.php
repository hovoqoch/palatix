<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Services;
use App\Models\Translation;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        if ($request->ajax()){
            try {
                return datatables()->of(Services::all())->make(true);
            } catch (\Exception $e) {
                dd($e->getMessage());
            }
        }
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['services'] ?? 'Services',
            'parent_link' => route('services.index', app()->getLocale()),
            'services' => [],
        ];
        return view('admin.services.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['services'] ?? 'Services',
            'parent_link' => route('services.index', app()->getLocale()),
            'meta_parent' => $translations['add_service'] ?? 'Add Service',
        ];
        return view('admin.services.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'name' => ['required', 'string'],
        ]);

        Services::storeServices($request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['service_succ_created'] ?? 'Service successfully created');
        return redirect(app()->getLocale().'/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $type = Services::getServiceById($id);
        if (!$type) {
            return redirect()->route('services.index',[app()->getLocale()])->withErrors(['error' => $translations['no_service'] ?? 'No services']);
        }

        $data = [
            'meta_title' => "services",
            'meta_parent' => $translations['edit_serv'] ?? 'Edit Service',
            'parent_link' => route('services.index', app()->getLocale()),
            'type' => $type,
        ];
        return view('admin.services.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'name' => ['required', 'string'],
        ]);
        $type = Services::getServiceById($id);
        if (!$type) {
            return redirect()->route('services.index',[app()->getLocale()])->withErrors(['error' => $translations['no_service'] ?? 'No services']);
        }

        Services::updateService($type,$request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['serv_succ_updated'] ?? 'Service successfully updated');
        return redirect(app()->getLocale().'/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $type = Services::getServiceById($id);
        if (!$type) {
            return redirect()->route('services.index',[app()->getLocale()])->withErrors(['error' => $translations['no_service'] ?? 'No services']);
        }

        Services::deleteService($type);

        session()->flash('type', 'success');
        session()->flash('message', $translations['serv_del'] ?? 'Service Deleted');
        return redirect(app()->getLocale().'/services');
    }
}
