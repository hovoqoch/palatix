<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Translation;
use App\Models\Site;
use App\Models\Practice;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\StoreSiteRequest;
use Illuminate\View\View;


class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if ($request->ajax()) return datatables()->of(Site::with('practice'))->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['Sites'] ?? 'Sites',
            'parent_link' => route('sites.index', app()->getLocale()),
            'sites' => [],
        ];
        return view('admin.sites.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['sites'] ?? 'Sites',
            'parent_link' => route('services.index', app()->getLocale()),
            'meta_parent' => $translations['add_site'] ?? 'Add Site',
            'practices' => Practice::all()
        ];
        return view('admin.sites.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSiteRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSiteRequest $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = Site::create($request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['site_succ_created'] ?? 'Site successfully created');
        return redirect(app()->getLocale().'/sites');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $site = Site::getSiteById($id);
        if (!$site) {
            return redirect()->route('sites.index',[app()->getLocale()])->withErrors(['error' => $translations['no_sites'] ?? 'No sites']);
        }

        $data = [
            'meta_title' => "sites",
            'meta_parent' => $translations['edit_site'] ?? 'Edit Site',
            'parent_link' => route('sites.index', app()->getLocale()),
            'practices' => Practice::all(),
            'site' => $site,
        ];
        return view('admin.sites.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'name' => ['required', 'string'],
            'practice_id' => ['required'],
        ]);
        $site = Site::getSiteById($id);
        if (!$site) {
            return redirect()->route('sites.index',[app()->getLocale()])->withErrors(['error' => $translations['no_sites'] ?? 'No sites']);
        }

        Site::updateSite($site,$request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['site_succ_updated'] ?? 'Site successfully updated');
        return redirect(app()->getLocale().'/sites');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $site = Site::getSiteById($id);
        if (!$site) {
            return redirect()->route('sites.index',[app()->getLocale()])->withErrors(['error' => $translations['no_sites'] ?? 'No sites']);
        }

        Site::deleteSite($site);

        session()->flash('type', 'success');
        session()->flash('message', $translations['site_del'] ?? 'Site Deleted');
        return redirect(app()->getLocale().'/sites');
    }

    public function getSitesByPractice(Request $request)
    {
        $sites = Site::where('practice_id', $request->practice_id)->get();
        return response()->json([
            'sites' => array_values($sites->toArray())
        ]);
    }
}
