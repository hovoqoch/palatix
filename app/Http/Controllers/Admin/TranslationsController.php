<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;


class TranslationsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if (auth()->user()->region_id){
            abort(404);
        }
        if ($request->ajax()) return datatables()->of(Translation::getTranslations())->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['translations'] ?? 'Translations',
            'parent_link' => route('translations.index', app()->getLocale()),
            'translates' => [],
        ];
        return view('admin.translations.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->region_id){
            abort(404);die();
        }
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['translations'] ?? 'Translations',
            'meta_parent' => $translations['add_translation'] ?? 'Add Translation',
            'parent_link' => route('translations.index', app()->getLocale()),
        ];
        return view('admin.translations.create',$data);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
//    protected function validator(array $data)
//    {
//        return Validator::make($data, [
//            'short_code' => ['required', 'string'],
//            'en' => ['required', 'string'],
//            'de' => ['required', 'string'],
//        ]);
//    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->region_id){
            abort(404);die();
        }
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'short_code' => ['required', 'string', 'unique:translations'],
            'en' => ['required', 'string'],
            'de' => ['required', 'string'],
        ]);

        Translation::storeTranslation($request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['trans_succ_create'] ?? 'Translation successfully created');
        return redirect()->route('translations.index',[app()->getLocale()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$lang,$id)
    {
        if (auth()->user()->region_id){
            return abort(404);
        }
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $translation = Translation::getTranslationById($id);
        if (!$translation) {
            return redirect()->route('translations.index',[app()->getLocale()])->withErrors(['error' => $translations['trans_not_found'] ?? 'Translation not found']);
        }
        $data = [
            'meta_title' => $translations['translations'] ?? 'Translations',
            'meta_parent' => $translations['edit_trans'] ?? 'Edit Translation',
            'parent_link' => route('translations.index', app()->getLocale()),
            'translation' => $translation,
        ];
        return view('admin.translations.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {
        if (auth()->user()->region_id){
            abort(404);die();
        }
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'en' => ['required', 'string'],
            'de' => ['required', 'string'],
        ]);
        $translation = Translation::getTranslationById($id);
        if (!$translation) {
            return redirect()->route('translations.index',[app()->getLocale()])->withErrors(['error' => $translations['trans_not_found'] ?? 'Translation not found']);
        }

        Translation::updateTranslation($translation,$request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['trnas_succ_upd'] ?? 'Translation successfully updated');
        return redirect()->route('translations.index',[app()->getLocale()]);
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$lang,$id)
    {
        if (auth()->user()->region_id){
            return abort(404);
        }
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $translation = Translation::getTranslationById($id);
        if (!$translation) {
            return redirect()->route('translations.index',[app()->getLocale()])->withErrors(['error' => $translations['trans_not_found'] ?? 'Translation not found']);
        }
        Translation::deleteTranslation($translation);

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['trans_succ_del'] ?? 'Translation successfully deleted');
        return redirect()->route('translations.index',[app()->getLocale()]);
    }
}
