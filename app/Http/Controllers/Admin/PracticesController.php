<?php

namespace App\Http\Controllers\Admin;


use App\Models\Device;
use App\Models\Manufacturer;
use App\Models\Practice;
use App\Models\Region;
use App\Models\Translation;
use App\Models\Type;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class PracticesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        if ($request->ajax()){
            try {
                return datatables()->of(Practice::getPractice(Auth::user()->region_id))->make(true);
            } catch (\Exception $e) {
                dd($e->getMessage());
            }
        }
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['practice'] ?? 'Practice',
            'practices' => [],
            'parent_link' => route('practices.index', app()->getLocale()),
        ];

        return view('admin.practices.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['practice'] ?? 'Practice',
            'meta_parent' => $translations['add_practice'] ?? 'Add Practice',
            'parent_link' => route('practices.index', app()->getLocale()),
            'practice' => Region::getRegions(),
        ];
        return view('admin.practices.create',$data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        Practice::storePractice($request->all());
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['pract_succ_created'] ?? 'Practice successfully created');
        return redirect()->route('practices.index',[app()->getLocale()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request,$lang,$id)
    {
        if(!$practice = Practice::getPracticeById($id,Auth::user()->region_id)){
           return abort('404');
        }
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['practice'] ?? 'Practice',
            'meta_parent' => $translations['edit_practice'] ?? 'Edit Practice',
            'regions' => Region::getRegions(),
            'practice' => $practice,
            'parent_link' => route('practices.index', app()->getLocale()),
        ];
        return view('admin.practices.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$lang, $id)
    {

        //        $request->validate([
//            'name_of_the_practice' => ['required', 'string'],
//            'specialization' => ['required', 'string'],
//            'street_and_number' => ['required', 'string'],
//            'postel_code' => ['required', 'numeric'],
//            'practice_phone_number' => ['required', 'numeric'],
//            'city' => ['required', 'string'],
//            'site' => ['required', 'string'],
//            'invoice_name' => ['required', 'string'],
//            'invoice_street_and_number' => ['required', 'string'],
//            'invoice_postal_code' => ['required', 'numeric'],
//            'invoice_city' => ['required', 'string'],
//            'practice_email' => ['required', 'string', 'email',],
//            'password' => ['required', 'string', 'min:8', 'confirmed'],
//        ]);
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $practice = Practice::getPracticeById($id);
        if (!$practice) {
            return redirect()->route('practices.index',[app()->getLocale()])->withErrors(['error' => $translations['no_practice'] ?? 'No practice']);
        }

        Practice::updatePractice($practice,$request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['pract_succ_updated'] ?? 'Practice successfully updated');
        return redirect()->route('practices.index',[app()->getLocale()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $lang
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $practice = Practice::getPracticeById($id);
        if (!$practice) {
            return redirect()->route('practices.index',[app()->getLocale()])->withErrors(['error' => $translations['no_practice'] ?? 'No practice']);
        }
        Practice::deletePractice($practice);

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['pract_succ_del'] ?? 'Practice successfully deleted');
        return redirect()->route('practices.index',[app()->getLocale()]);
    }

    public function storePracticWithDoctor(Request $request)
    {
        //todo add validation
       $params = Practice::createPracticeWithDoctor($request->all());

       return redirect()->route('practices.index',[app()->getLocale()]);
    }
}
