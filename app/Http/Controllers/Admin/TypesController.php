<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\Type;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class TypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if ($request->ajax()){
            return datatables()->of(Type::getType())->make(true);
        }
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['dev_types'] ?? 'Devices Types',
            'parent_link' => route('types.index', app()->getLocale()),
            'types' => [],
        ];
        return view('admin.types.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['dev_types'] ?? 'Devices Types',
            'parent_link' => route('types.index', app()->getLocale()),
            'meta_parent' => $translations['add_dev_type'] ?? 'Add Devices Types',
        ];
        return view('admin.types.create',$data);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'type' => ['required', 'string'],
        ]);

        Type::storeType($request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['type_succ_created'] ?? 'Type successfully created');
        return redirect()->route('types.index',[app()->getLocale()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $type = Type::getTypeById($id);
        if (!$type) {
            return redirect()->route('types.index',[app()->getLocale()])->withErrors(['error' => $translations['dev_type_not'] ?? 'Device Type not found']);
        }

        $data = [
            'meta_title' => $translations['dev_types'] ?? 'Devices Types',
            'meta_parent' => $translations['edit_dev_type'] ?? 'Edit Devices Types',
            'parent_link' => route('types.index', app()->getLocale()),
            'type' => $type,
        ];
        return view('admin.types.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'type' => ['required', 'string'],
        ]);
        $type = Type::getTypeById($id);
        if (!$type) {
            return redirect()->route('types.index',[app()->getLocale()])->withErrors(['error' => $translations['dev_type_not'] ?? 'Device Type not found']);
        }

        Type::updatetype($type,$request->all());

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['dev_type_succ_update'] ?? 'Devices Type successfully updated');
        return redirect()->route('types.index',[app()->getLocale()]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        DB::table('devices')->where('type_id',$id)->delete();
        $type = Type::getTypeById($id);
        if (!$type) {
            return redirect()->route('doctors.index',[app()->getLocale()])->withErrors(['error' => $translations['dev_type_not'] ?? 'Device Type not found']);
        }

        Type::deleteType($type);

        session()->flash('type', 'success');
        session()->flash('message', $translations['dev_type_del'] ?? 'Devices Type Deleted');
        return redirect()->route('types.index',[app()->getLocale()]);
    }
}
