<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Doctor\DoctorElearningQuestionController;
use App\Models\Elearning;
use App\Models\ElearningQuestion;
use App\Models\ElearningQuestionsAnswer;
use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class ElearningQuestionController extends Controller
{

    /**
     * @param Request $request
     * @param string $lang
     * @param int $id
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request, string $lang, int $id)
    {
        $elearning = Elearning::query()->findOrFail($id, ['id']);
        if ($request->ajax()) return datatables()->of(Elearning::query()->with(['questions' => function ($query) {
            $query->with('answers');
        }])->find($elearning->id)->questions)->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'elearning' => $elearning,
            'parent_link' => route('questions.index', ['locale' => app()->getLocale(), 'id' => $id]),
            'meta_title' => $translations['question'] ?? 'Questions',
        ];
        return view('admin.elearning_question.home', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @param string $lang
     * @param int $id
     * @return RedirectResponse
     */
    public function store(Request $request, string $lang, int $id)
    {
        $duplicate = new DoctorElearningQuestionController;
        $duplicate->store($request,$lang,$id);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, $elearning_id, $question_id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['question'] ?? 'Devices',
            'meta_parent' => $translations['edit_quest'] ?? 'Edit Question',
            'question' => ElearningQuestion::getQuestionByID($question_id),
            'parent_link' => route('elearning.index', app()->getLocale()),
        ];
        return view('admin.elearning_question.edit',$data);
    }

    /**
     * @param Request $request
     * @param string $lang
     * @param int $elearning_id
     * @param int $question_id
     * @return RedirectResponse
     */
    public function update(Request $request, string $lang, int $elearning_id, int $question_id)
    {
        $duplicate = new DoctorElearningQuestionController;
        $translations = $duplicate->updateFuncFromAdmin($request,$question_id);
        $request->session()->flash('message', $translations['quest_with_answer_update'] ?? 'Question with answers were successfully updateed');
        $url = url("/") . "/{$lang}/elearning/{$elearning_id}/questions";
        return redirect($url);
    }


    /**
     * @param string $lang
     * @param int $id
     * @param int $questionID
     * @return RedirectResponse
     */
    public function destroy(string $lang, int $id, int $questionID)
    {
        $duplicate = new DoctorElearningQuestionController;
        try {
            $duplicate->destroy($lang, $id, $questionID);
        } catch (Exception $e) {
            request()->session()->flash('type', 'danger');
            request()->session()->flash('message', $e->getMessage());
            return redirect()->back();
        }
        request()->session()->flash('type', 'success');
        request()->session()->flash('message','Action completed successfully');
        return redirect()->back();
    }
}
