<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Jobs\NotifyUserAboutTest;
use App\Models\Translation;
use App\Models\Device;
use App\Models\PracticeDevices;
use Composer\DependencyResolver\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['dashboard'] ?? 'Dashboard';
        $devices = Device::with('user', 'user.practice')->where('status', 0)->take(10)->get();
        $practiceDevices = PracticeDevices::with('practice', 'device', 'user')->orderBy('next_validation_date', 'desc')->take(10)->get();
        $data = [
            'meta_title' => $meta_title,
            'devices' => $devices,
            'practiceDevices' => $practiceDevices,
            'parent_link' => '/' . app()->getLocale(),
        ];
        return view('admin.home', $data);
    }


}
