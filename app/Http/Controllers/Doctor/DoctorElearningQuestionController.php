<?php

namespace App\Http\Controllers\Doctor;

use App\Models\Elearning;
use App\Models\ElearningQuestion;
use App\Models\ElearningQuestionsAnswer;
use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Collection;
use Illuminate\View\View;

class DoctorElearningQuestionController extends Controller
{


    /**
     * @param Request $request
     * @param string $lang
     * @param int $id
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request, string $lang, int $id)
    {
        $elearning = Elearning::query()->findOrFail($id, ['id']);
        if ($request->ajax()) return datatables()->of(Elearning::query()->with(['questions' => function ($query) {
            $query->with('answers');
        }])->find($elearning->id)->questions)->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'elearning' => $elearning,
            'parent_link' => route('doctor.elearning.index', ['locale' => app()->getLocale()]),
            'meta_title' => $translations['elearning'] ?? 'Elearning',
        ];
        return view('doctor.elearning_question.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * @param Request $request
     * @param string $lang
     * @param int $id
     * @return RedirectResponse
     */
    public function store(Request $request, string $lang, int $id)
    {
        $translations = Translation::all()->pluck($lang, 'short_code');
        $question = ElearningQuestion::query()->create([
            'question' => $request->question,
            'elearning_id' => $id
        ]);
        foreach ($request->answer as $key => $value){
            ElearningQuestionsAnswer::query()->create([
                'answer' => $value,
                'type' => $request->type[$key],
                'elearning_question_id' => $question->id
            ]);

        }
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['quest_with_answer'] ?? 'Question with answers were successfully created');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param string $lang
     * @param int $elearning_id
     * @param int $question_id
     * @return Factory|View
     */
    public function edit(string $lang, int $elearning_id, int $question_id)
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['question'] ?? 'Devices',
            'meta_parent' => $translations['edit_quest'] ?? 'Edit Question',
            'question' => ElearningQuestion::getQuestionByID($question_id),
            'parent_link' => url("/")."/{$lang}/account/elearning/{$elearning_id}/questions",
        ];
        return view('doctor.elearning_question.edit',$data);
    }

    /**
     * @param Request $request
     * @param string $lang
     * @param int $elearning_id
     * @param int $question_id
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, string $lang, int $elearning_id, int $question_id)
    {
        $translations = $this->updateFuncFromAdmin($request, $question_id);
        $request->session()->flash('message', $translations['quest_with_answer_update'] ?? $translations['quest_with_answer_update'] ?? 'Question with answers were successfully updateed');
        $url = url("/") . "/{$lang}/account/elearning/{$elearning_id}/questions";
        return redirect($url);
    }


    /**
     * @param string $lang
     * @param int $id
     * @param int $questionID
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(string $lang, int $id, int $questionID)
    {
        try {
            ElearningQuestion::with(['answers' => function ($query) {
                $query->delete();
            }])->findOrFail($questionID)->delete();
        } catch (Exception $e) {
            request()->session()->flash('type', 'danger');
            request()->session()->flash('message', $e->getMessage());
            return redirect()->back();
        }
        request()->session()->flash('type', 'success');
        request()->session()->flash('message', $translations['success'] ?? 'Action completed successfully');
        return redirect()->back();
    }


    /**
     * @param object $request
     * @param int $question_id
     * @return Collection
     */
    public function updateFuncFromAdmin($request, int $question_id)
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        try {
            ElearningQuestion::query()->findOrFail($question_id)->update([
                'question' => $request->question,
            ]);
            ElearningQuestionsAnswer::query()->where('elearning_question_id', $question_id)->delete();

            foreach ($request->answer as $key => $value){
                ElearningQuestionsAnswer::query()->create([
                    'answer' => $value,
                    'type' => $request->type[$key],
                    'elearning_question_id' => $question_id
                ]);
            }
        } catch (Exception $e) {
            $request->session()->flash('type', 'danger');
            return $translations;
        }
        //dd(count($request->answer));
        $request->session()->flash('type', 'success');
        return $translations;
    }
}
