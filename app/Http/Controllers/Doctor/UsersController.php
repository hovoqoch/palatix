<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Mail\UserInvitationMail;
use App\Models\Role;
use App\Models\Translation;
use App\Models\User;
use App\Models\Practice;
use App\Models\PracticeUser;
use App\Models\Qualification;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $id = auth()->id();
        $employees = User::getDoctorEmployees($id);
        if ($request->ajax()) return datatables()->of($employees)->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['employees_list'] ?? 'Employees list',
            'parent_link' => route('doctor.employees.index', app()->getLocale()),
            'employees' => [],
            'type' => 'employee',

        ];
        return view('doctor.employees.home', $data);
    }

    /**
     * Display a listing of the doctors.
     *
     * @return \Illuminate\Http\Response
     */
    public function doctors()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $users = User::getDoctors();
        $data = [
            'meta_title' => $translations['doc_list'] ?? 'Doctors list',
            'parent_link' => route('types.index', app()->getLocale()),
            'users' => $users,
            'type' => 'doctor',

        ];
        return view('users.home', $data);
    }

    /**
     * Display a listing of the employees.
     *
     * @return \Illuminate\Http\Response
     */
    public function employees()
    {
        $users = User::getEmployees();
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['employees_list'] ?? 'Employees lis',
            'parent_link' => route('types.index', app()->getLocale()),
            'users' => $users,
            'type' => 'employee',

        ];
        return view('users.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['employee'] ?? 'Employee',
            'meta_parent' => $translations['add_new_employee'] ?? 'Add new employee',
            'parent_link' => route("doctor.employees.index",["local"=>app()->getLocale()]),
            'practices' => PracticeUser::getPractices($user->id),
            'qualifications' => Qualification::all(),
        ];
        return view('doctor.employees.create', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addDoctor()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['doctors'] ?? 'Doctors',
            'meta_parent' => $translations['add_new_doctor'] ?? 'Add new doctor',
            'parent_link' => url(app()->getLocale(), 'doctors'),
            'practices' => Practice::all(),
        ];
        return view('doctors.create', $data);
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'role_id' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = 3;
        $user->password = Hash::make($request->password);

        $user->save();
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['user_succ_create'] ?? 'User successfully created');
        return redirect(app()->getLocale() . '/users');
    }

    public function storeEmployee(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            //'role_id' => ['required'],
            'job_title' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'first_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'unique:users', 'email'],
            'qualification_id' => ['required'],
        ]);
        $user = new User;


        $user->practice_id = $request->practice_id;
        $user->role_id = 3;
        $user->title = $request->job_title;
        $user->name = $request->name;
        $user->last_name = $request->first_name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->doctor_id = auth()->id();
        $user->qualification_id = $request->qualification_id;
        $user->status = 1;
        $user->password = Hash::make(now());
        $link = $user->remember_token = md5(microtime());
        $user->save();
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['user_succ_create'] ?? 'User successfully created');

        Mail::to($request->email)->send(new UserInvitationMail($user, $link));
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $current_user = auth()->user();
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::find($id);
        $qualifications = Qualification::all();

        $data = [
            'employee' => $user,
            'practices' => PracticeUser::getPractices($current_user->id),
            'qualifications' => $qualifications,
            'meta_title' => $translations['user'] ?? 'User',
            'parent_link' => route('doctor.employees.index', app()->getLocale()),

        ];
        return view('doctor.employees.edit', $data);
    }

    public function profileEdit($locale)
    {

        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = auth()->user();
        $data = [
            'meta_title' => $translations['profile'] ?? 'Profile',
            'user' => $user,
            'parent_link' => url(app()->getLocale(), 'doctors')
        ];
        return view('doctor.profile.edit', $data);
    }

    public function profileUpdate(Request $request)
    {

        $user = auth()->user();


        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],

        ]);

        if ($request->get('email') != $user->email) {
            $request->validate([
                'email' => ['required', 'unique:users', 'email'],
            ]);
        }

        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user->title = $request->title;
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->email = $request->email;

        $user->save();
        return redirect()->back()->with([
            'type' => 'success',
            'message' => $translations['user_succ_update'] ?? 'User was successfully updated'
        ]);
    }

    public function profileUpdatePassword(Request $request, $locale)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = auth()->user();
        // dd($user);
        $hashedPassword = $user->password;
        $request->validate([
            'old_password' => ['required'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if (Hash::check($request->old_password, $hashedPassword)) {
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            return redirect()->back()->with([
                'type' => 'success',
                'message' => $translations['pass_succ_change'] ?? 'Password was successfully changed'
            ]);
        } else {
            return redirect()
                ->back()
                ->with([
                    'type' => 'danger',
                    'message' => $translations['old_pass_incor'] ?? 'Old password was incorrect'
                ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {

        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::find($id);
        $user->practice_id = $request->practice_id;
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->title = $request->title;
        $user->qualification_id = $request->qualification_id;


        $user->save();
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['user_succ_update'] ?? 'User successfully updated');
        return redirect(app()->getLocale() . '/account/employees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = User::findOrfail($id);
        $user->delete();

        return redirect()->back()->with([
            'type' => 'success',
            'message' => $translations['use_succ_del'] ?? 'User was successfully deleted'
        ]);
    }
}
