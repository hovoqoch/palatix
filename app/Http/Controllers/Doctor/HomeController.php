<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\PracticeDevices;
use App\Models\PracticeUser;
use App\Models\Translation;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\Device;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['dashboard'] ?? 'Dashboard';
        $user = auth()->user();
        //$practice = PracticeUser::with('device_practice')->where('user_id', $user->id)->get();
        $practice = PracticeUser::query()->where('user_id', $user->id)->pluck('practice_id');
        $devices = PracticeDevices::query()->whereIn('practice_id', $practice)->with('device')->get();

        // $practice_and_devices = PracticeUser::with('device_practice')->where('user_id', $user->id)->get();
        // $devices = $practice->practicedevices->where('pivot.deleted_at',null)->sortBy('next_validation_date');
        if ($request->ajax()) return datatables()->of($devices)->make(true);
        $data = [
            'meta_title' => $meta_title,
            'devices' => [],
            'parent_link' => '/'.app()->getLocale().'/account',
        ];
        return view('doctor.home',$data);
    }
}
