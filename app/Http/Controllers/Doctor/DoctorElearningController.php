<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Admin\ElearningController;
use App\Http\Requests\Doctor\UpdateDoctorElearningRequest;
use App\Models\Device;
use App\Models\DoctorsTests;
use App\Models\ElearningQuestion;
use App\Models\ElearningQuestionsAnswer;
use App\Models\ElearningUser;
use App\Models\Practice;
use App\Models\PracticeUser;
use App\Models\Practicedevices;
use App\Models\Translation;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Elearning;
use App\Http\Requests\Admin\StoreElearningRequest;
use App\Http\Requests\Admin\UpdateElearningRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class DoctorElearningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $user_practice = PracticeUser::query()->where('user_id', Auth::id())->pluck('practice_id');
        if ($request->ajax()) return datatables()->of(Elearning::query()->whereIn('practice_id', $user_practice)->with('device')->get())->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'tests' => [],
            'parent_link' => route('doctor.elearning.index', app()->getLocale()),
            'meta_title' => $translations['elearning'] ?? 'E-learning',
        ];
        return view('doctor.elearning.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $practices = auth()->user()->practicesMany;


        $data = [
            'meta_title' => $translations['elearning'] ?? 'E-learning',
            'meta_parent' => $translations['elearning_test'] ?? 'Add E-learning Test',
            'parent_link' => route('doctor.elearning.index', app()->getLocale()),
            'practices'=> $practices,
        ];
        return view('doctor.elearning.create', $data);
    }


    /**
     * Store a newly created Elearning in storage.
     *
     * @param StoreElearningRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreElearningRequest $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $_this = new ElearningController;
        list($elearning) = $_this->elearning($request);
        foreach ($request['user_id'] as $user){
            $elearning->users()->attach($user, ['status' => 0]);
        }

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['test_created'] ?? 'Test successfully created');
        return redirect()->route('doctor.questions.index',[app()->getLocale(),$elearning->id]);
    }

    /**
     * Show the form for editing Elearning.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * */
    public function edit(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $elearning = Elearning::with('users')->find($id);
        $practices = auth()->user()->practicesMany;
        $users = $elearning->practice->employee;
        $devices = Practicedevices::where('practice_id', $elearning->practice_id)->with( 'device')->get();

        $data = [
            'meta_title' => $translations['elearning'] ?? 'E-learning',
            'meta_parent' => $translations['edit_elaerning'] ?? 'Edit Elearning',
            'elearning' => $elearning,
            'practices' => $practices,
            'devices' => $devices,
            'users' => $users,
            'parent_link' => route('doctor.elearning.index', app()->getLocale()),
        ];
        return view('doctor.elearning.edit',$data);

    }

    public function download_pdf($name,$file)
    {
        $file_path = storage_path() .'/app/public/elearning_files/'. $file;
        return Response::download($file_path, $file);

    }

    public function update(UpdateDoctorElearningRequest $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $elearning = Elearning::find($id);
        $requestdata = $request->all();
        if($request->hasFile('attach_file_name') != '') {
            // Get filename with extension
            $filenameWithExt = $request->file('attach_file_name')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('attach_file_name')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('attach_file_name')->storeAs('public/elearning_files', $fileNameToStore);
            $requestdata['attach_file_name'] = $fileNameToStore;
        }


        if ($request['deadline_date']){

            $requestdata['deadline_date'] = Carbon::parse($request['deadline_date'])->toDateString();
        }

        $elearning->users()->detach();
        $elearning->update($requestdata);
        foreach ($request['user_id'] as $user){
            $elearning->users()->attach($user, ['status' => 0]);
        }
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['test_update'] ?? 'Test successfully updated');
        return redirect()->route('doctor.elearning.index',[app()->getLocale()]);
    }

    public function destroy(Request $request,$lang, $id)
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $elearning = Elearning::query()->findOrFail($id);
        try {
            DB::beginTransaction();
            $elearningQuestions = ElearningQuestion::query()->where('elearning_id', $id);
            if ($elearningQuestions->count()) {
                ElearningQuestionsAnswer::query()->whereIn('elearning_question_id', $elearningQuestions->pluck('id'))->delete();
                $elearningQuestions->delete();
            }
            $elearning->users()->detach();
            $elearning->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $request->session()->flash('type', 'danger');
            $request->session()->flash('message', 'error');
            return redirect()->back();
        }
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['test_deleted'] ?? 'Test successfully deleted');
        return redirect()->route('doctor.elearning.index', [app()->getLocale()]);
    }

    public function viewFile($locale,$name)
    {
        return response()->file(storage_path() .'/app/public/elearning_files/'. $name) ;
    }

    public function deleteFile(Request $request,$locale,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $elearning = Elearning::find($id);
        $file = $elearning->attach_file_name;
        Storage::delete('app/public/elearning_files/'. $file);
        $elearning->update([
            'attach_file_name' => null
        ]);
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['file_deleted'] ?? 'File successfully deleted');
        return redirect()->back();

    }

    public function filterDevices(Request $request)
    {
        $devices = Practicedevices::with('practice', 'device', 'user')->get();
        if ($request->practice_id) {
            $devices = $devices->where('practice_id', $request->practice_id);
        }
        return response()->json([
            'devices' => array_values($devices->toArray())
        ]);
    }
}
