<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Mail\NewDeviceMail;
use App\Models\DoctorsTests;
use App\Models\Practice;
use App\Models\Setting;
use App\Models\Translation;
use App\Models\User;
use Exception;
use hisorange\BrowserDetect\Stages\DeviceDetector;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\Practicedevices;
use App\Models\PracticeUser;
use App\Models\Device;
use App\Models\Manufacturer;
use App\Models\Type;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;


class PracticeDevicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['pr_device'] ?? 'Practice Devices';
        //$devices = $practice->practicedevices->where('deleted_at',null)->sortBy('next_validation_date');
        $user = auth()->user();
        $practice = PracticeUser::query()->where('user_id', $user->id)->pluck('practice_id');
        $devices = PracticeDevices::query()->whereIn('practice_id', $practice)->with(['device' => function ($query) {
            $query->with('manufacturer');
            $query->with('type');
        }])->get();
        if ($request->ajax()) return datatables()->of($devices)->make(true);
        $data = [
            'meta_title' => $meta_title,
            'devices' => [],
            'parent_link' => '/'.app()->getLocale().'/account',
        ];
        return view('doctor.practice_devices.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $data = [
            'meta_title' => $translations['pr_device'] ?? 'Practice Devices',
            'meta_parent' => $translations['add_pr_div'] ?? 'Add Practice Devices',
            'parent_link' => route('doctor.devices.index', app()->getLocale()),
            'manufactorers' => Manufacturer::getManufacturer(),
            'practices' => PracticeUser::getPractices($user->id),
            'types' => Type::getType(),
            'devices' => Device::getDevice(),
        ];
        return view('doctor.practice_devices.create', $data);
    }

    public function filterDevices(Request $request)
    {
        $devices = Device::all();
        if ($request->manufacturer_id) {
            $devices = $devices->where('manufacturer_id', $request->manufacturer_id);
        }
        if ($request->type_id) {
            $devices = $devices->where('type_id', $request->type_id);
        }
        return response()->json([
            'devices' => array_values($devices->toArray())
        ]);
    }

    public function getDevicesByPractice(Request $request)
    {
        $device_ids = Practicedevices::with('devicesByPractice')->where('practice_id', $request->practice_id)->pluck('device_id');
        $devices = Device::whereIn('id', $device_ids)->get()->toArray();
        $users = User::where('practice_id', $request->practice_id)->get()->toArray();
        return response()->json([
            'devices' => array_values($devices),
            'users' => array_values($users)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'practice_id' => ['required']
            ]
        );
        $practiceDevice = new Practicedevices;

        $settings = Setting::where('key','admin_email')->first();
        $data = [
            $practiceDevice->practice_id = $request['practice_id'],
            $practiceDevice->user_id = auth()->id(),
            $practiceDevice->place_in_practice = $request['place_in_practice'],
            $practiceDevice->serial_number = $request['serial_number'],
            $practiceDevice->software_version = $request['software_version'],
            $practiceDevice->parameter_version = $request['parameter_version'],
            $practiceDevice->maintenance_report_valid = $request['maintenance_report_valid'],
            $practiceDevice->setup_approval = $request['setup_approval'],
            $practiceDevice->manufacturing_year = $request['manufacturing_year'],
            $practiceDevice->remarks = $request['remarks'],
            $practiceDevice->maintenance_report_date = strtotime($request['maintenance_report_date']),
            $practiceDevice->first_setup_date = strtotime($request['first_setup_date']),
            $practiceDevice->last_validation_date = strtotime($request['last_validation_date']),
            $practiceDevice->last_maintenance_date = strtotime($request['last_maintenance_date'])

        ];
        if ($request->device_id == 0) {
            $device = new Device();

            $device->manufacturer_id = $request->manufacturer_id;
            $device->type_id = $request->type_id;
            $device->name = $request->device_name;
            $device->user_id = auth()->id();
            $device->status = 0;


            Mail::to($settings->val)->send(new NewDeviceMail($device));


            $device->save();

            $val_next = (float)$device->val_duration * 365;
            $practiceDevice->save([
                $data,
                $practiceDevice->next_validation_date = strtotime($request['last_validation_date'].' + ' .$val_next.' day'),
                $practiceDevice->device_id = $device->id
            ]);

        }else{

            $device = Device::find($request->device_id);
            $val_next = (float)$device->val_duration * 365;
            Mail::to($settings->val)->send(new NewDeviceMail($device));
            $practiceDevice->save([
                $data,
                $practiceDevice->next_validation_date = strtotime($request['last_validation_date'].' + ' .$val_next.' day'),
                $practiceDevice->device_id = $request->device_id
            ]) ;
        }

        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['dev_created'] ?? 'Device successfully created');
        return redirect(app()->getLocale().'/account/devices');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $device = Practicedevices::find($id);
        $meta_title = $translations['devices'] ?? 'Devices';
        $meta_parent = $translations['edit_devices'] ?? 'Edit devices';
        $user = auth()->user();
        $data = [
            'meta_title' => $meta_title,
            'meta_parent' => $meta_parent,
            'parent_link' => route('doctor.devices.index', app()->getLocale()),
            'device' => $device,
            'practices' => PracticeUser::getPractices($user->id),
        ];

        return view('doctor.practice_devices.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {

        $device = Practicedevices::find($id);

        $mainDevice = Device::find($device->id);
        $val_next = (float)$mainDevice->val_duration * 365;

        $device->practice_id = $request->practice_id;
        $device->place_in_practice = $request->place_in_practice;
        $device->serial_number = $request->serial_number;
        $device->software_version = $request->software_version;
        $device->parameter_version = $request->parameter_version;
        $device->maintenance_report_valid = $request->maintenance_report_valid;
        $device->setup_approval = $request->setup_approval;
        $device->remarks = $request->remarks;
        $device->maintenance_report_date = strtotime($request->maintenance_report_date);
        $device->first_setup_date = strtotime($request->first_setup_date);
        $device->last_validation_date = strtotime($request->last_validation_date);
        $device->last_maintenance_date = strtotime($request->last_maintenance_date);
        $device->next_validation_date = strtotime($request->last_validation_date.' + ' .$val_next.' day');

        $device->save();
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['dev_updated'] ?? 'Device successfully updated');
        return redirect(app()->getLocale().'/account/devices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $lang ,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $device = Practicedevices::find($id);

        $device->delete();
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['dev_deleted'] ?? 'Device successfully deleted');
        return redirect()->route('doctor.devices.index', [app()->getLocale()]);

    }





    public function createtest(Request $request,$lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['dashboard'] ?? 'Dashboard';
        $device = Device::getDeviceById($id);

        $data = [
            'meta_title' => $meta_title,
            'device' => $device,
            'parent_link' => '/'.app()->getLocale().'/account',
        ];
        return view('doctor.practice_devices.createtest',$data);
    }
    public function storetest(Request $request)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $practice = auth()->user()->practicesMany->pluck('id');

        $meta_title = $translations['dashboard'] ?? 'Dashboard';

        $devices = PracticeDevices::whereIn('practice_id', $practice)->with('device')->get();;
        $data = [
            'meta_title' => $meta_title,
            'devices' => $devices,
            'parent_link' => '/'.app()->getLocale().'/account',
        ];

        $practiceDevice = new DoctorsTests;

        $practiceDevice->device_id = $request->device_id;
        $practiceDevice->user_id = auth()->user()->id;
        $practiceDevice->name = $request->name;
        $practiceDevice->summary = $request->summary;
        $practiceDevice->youtube_link = $request->youtube_link;
        $practiceDevice->vimeo_link = $request->vimeo_link;


        if($request->hasFile('attach_file_name')) {
            // Get filename with extension
            $filenameWithExt = $request->file('attach_file_name')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('attach_file_name')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('attach_file_name')->storeAs('public/elearning_files', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $practiceDevice->attach_file_name = $fileNameToStore;

        $practiceDevice->save();


        return view('doctor.practice_devices.home',$data);
    }

    public function report()
    {
        $data = [
            'meta_title' => 'Report',
            'parent_link' => '/'.app()->getLocale().'/account/devices',
        ];
        return view('doctor.practice_devices.report',$data);
    }
}
