<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\Device;
use App\Models\Manufacturer;
use App\Models\Practice;
use App\Models\Region;
use App\Models\Translation;
use App\Models\Type;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PracticesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['practice'] ?? 'Practice';
        $practices = Practice::with('region')->get();
        $data = [
            'meta_title' => $meta_title,
            'practices' => $practices,
            'parent_link' => route('practices.index', app()->getLocale()),
        ];
        return view('practices.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['practice'] ?? 'Practice';
        $meta_parent = $translations['add_practice'] ?? 'Add Practice';
        $practice = Region::orderBy('name', 'ASC')->get();
        $manufacturers = Manufacturer::orderBy('name', 'ASC')->get();
        $types = Type::orderBy('name', 'ASC')->get();
        $devices = Device::where('manufacturer_id','=', '1')
            ->where('type_id', '=', '2')
            ->get();
        $data = [
            'meta_title' => $meta_title,
            'meta_parent' => $meta_parent,
            'parent_link' => route('practices.index', app()->getLocale()),
            'practice' => $practice,
            'devices' => $devices,
            'manufacturers' => $manufacturers,
            'types' => $types,
        ];
        return view('practices.create',$data);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'specialization' => ['required'],
            'name_of_the_practice' => ['required'],
            'street_and_number' => ['required'],
            'postel_code' => ['required'],
            'city' => ['required'],
            'region_id' => ['required'],
            'site' => ['required'],
            'invoice_name' => ['required'],
            'invoice_street_and_number' => ['required'],
            'invoice_postal_code' => ['required'],
            'invoice_city' => ['required'],
            'phone_number' => ['required'],
            'email_address' => ['required'],
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $request->validate([
//            'name_of_the_practice' => ['required', 'string', 'max:255'],
//            'specialization' => ['required', 'string', 'max:255'],
//            'street_and_number' => ['required', 'string', 'max:255'],
//            'postel_code' => ['required', 'numeric', 'max:255'],
//            'practice_phone_number' => ['required', 'numeric', 'max:255'],
//            'city' => ['required', 'string', 'max:255'],
//            'site' => ['required', 'string', 'max:255'],
//            'invoice_name' => ['required', 'string', 'max:255'],
//            'invoice_street_and_number' => ['required', 'string', 'max:255'],
//            'invoice_postal_code' => ['required', 'numeric', 'max:255'],
//            'invoice_city' => ['required', 'string', 'max:255'],
//            'practice_email' => ['required', 'string', 'email', 'max:255',],
//            'password' => ['required', 'string', 'min:8', 'confirmed'],
//        ]);
        $practice = new Practice;
        $translations = $this->updateFunc($practice, $request);
        $request->session()->flash('message', $translations['pract_succ_created'] ?? 'Practice successfully created');

        return redirect()->route('practices.index',[app()->getLocale()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['practice'] ?? 'Practice';
        $meta_parent = $translations['edit_practice'] ?? 'Edit Practice';
        $practice = Practice::find($id);
        $user = User::find($practice->user_id);
        $regions = Region::orderBy('name', 'ASC')->get();
        $manufacturers = Manufacturer::orderBy('name', 'ASC')->get();
        $types = Type::orderBy('name', 'ASC')->get();
        $devices = Device::where('manufacturer_id','=', '1')
            ->where('type_id', '=', '2')
            ->get();
        $data = [
            'meta_title' => $meta_title,
            'meta_parent' => $meta_parent,
            'regions' => $regions,
            'practice' => $practice,
            'devices' => $devices,
            'user' => $user,
            'manufacturers' => $manufacturers,
            'types' => $types,
            'parent_link' => route('practices.index', app()->getLocale()),
        ];
        return view('practices.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {
        $practice = Practice::find($id);
        $translations = $this->updateFunc($practice,$request);
        $request->session()->flash('message', $translations['pract_succ_updated'] ?? 'Practice successfully updated');
        return redirect()->route('practices.index',[app()->getLocale()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$lang,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $practice = Practice::find($id);
        $practice->delete();
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['pract_succ_del'] ?? 'Practice successfully delete');
        return redirect()->route('practices.index',[app()->getLocale()]);
    }

    public function storePracticWithDoctor(Request $request)
    {
        //todo add validation
       $params = Practice::createPracticeWithDoctor($request->all());

       return redirect()->route('practices.index',[app()->getLocale()]);
    }

    /**
     * @param Practice $practice
     * @param Request $request
     * @return mixed
     */
    public function updateFunc(Practice $practice , Request $request)
    {
        $practice->name = $request->name_of_the_practice;
        $practice->specialization = $request->specialization;
        $practice->street_number = $request->street_and_number;
        $practice->postal_code = $request->postel_code;
        $practice->phone = $request->practice_phone_number;
        $practice->city = $request->city;
        $practice->region_id = $request->region_id;
        $practice->site = $request->site;
        $practice->invoice_name = $request->invoice_name;
        $practice->invoice_street = $request->invoice_street_and_number;
        $practice->invoice_postal = $request->invoice_postal_code;
        $practice->invoice_city = $request->invoice_city;
        $practice->practice_email = $request->practice_email;
        $practice->save();
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->session()->flash('type', 'success');
        return $translations;
    }
}
