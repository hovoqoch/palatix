<?php

namespace App\Http\Controllers\Doctor;

use App\Models\Device;
use App\Models\Practicedevices;
use App\Models\Training;
use App\Models\Translation;
use App\Models\PracticeUser;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;

class DoctorTrainingController extends Controller
{
    /**
     * @param Request $request
     * @return Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $trainings = Training::with('device')->get();
        if($request->ajax()) return datatables()->of($trainings)->make(true);
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $meta_title = $translations['training_documents'] ?? 'Training documents';
        $data = [
            'meta_title' => $meta_title,
            'trainings' => [],
            'parent_link' => '/' . app()->getLocale().'/account',
        ];
        return view('doctor.training.home', $data);
    }

    public function create()
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $user = auth()->user();
        $practice = PracticeUser::where('user_id', $user->id)->with('practice')->get();
        $devices = PracticeDevices::whereIn('practice_id', $practice->pluck('practice_id'))->with('device')->get();
        $data = [
            'meta_title' => $translations['training'] ?? 'Training',
            'meta_parent' => $translations['add_training_mat'] ?? 'Add Training Materials',
            'practices' => $practice,
            'devices' => $devices,
            'parent_link' => route('doctor.training.index', app()->getLocale()),
        ];
        return view('doctor.training.create', $data);
    }

    public function store(Request $request)
    {

        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $request->validate([
            'name' => ['required', 'string'],
            'file' => ['required']
        ]);
        $requestdata = $request->all();
        if ($request->hasFile('file')) {
            // Get filename with extension
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('file')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('file')->storeAs('public/trainings/', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }
        $requestdata['file'] = $fileNameToStore;

        $data = Training::create($requestdata);

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['training_succ_created'] ?? 'Training successfully created');
        return redirect()->route('doctor.training.index', [app()->getLocale()]);
    }

    public function edit($lang,$id)
    {
        $translations = Translation::all()->pluck(app()->getLocale(), 'short_code');
        $training = Training::query()->findOrFail($id);

        $user = auth()->user();
        $practice = PracticeUser::query()->where('user_id', $user->id)->with('practice')->get();

        $chosen_prectice_id = PracticeDevices::query()->where('device_id', $training->device_id)->first('practice_id');
        $devices = [];
        if ($chosen_prectice_id) {
            $devices = PracticeDevices::query()->where('practice_id', $chosen_prectice_id->practice_id)->with('device')->get();
        }
        $data = [
            'meta_title' => $translations['training'] ?? 'Training',
            'meta_parent' => $translations['edit_train'] ?? 'Edit Training',
            'training' => $training,
            'practices' => $practice,
            'devices' => $devices,
            'chosen_prectice_id' => $chosen_prectice_id,
            'parent_link' => route('doctor.training.index', app()->getLocale()),
        ];
        return view('doctor.training.edit', $data);
    }
    public function update(Request $request,$lang, $id)
    {

        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $training = Training::find($id);

        $requestdata = $request->all();


        if ($request->hasFile('file') != '') {
            // Get filename with extension
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('file')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('file')->storeAs('public/trainings/', $fileNameToStore);
            $requestdata['file'] = $fileNameToStore;
        }



        $training->update($requestdata);

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['train_succ_updated'] ?? 'Training successfully updated');
        return redirect()->route('doctor.training.index',[app()->getLocale()]);
    }

    public function destroy(Request $request, $lang, $id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $training = Training::find($id);
        $training->delete();

        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['train_succ_del'] ?? 'Training successfully deleted');
        return redirect()->route('doctor.training.index', [app()->getLocale()]);

    }

    public function viewFile($locale,$name)
    {
        return response()->file(storage_path() .'/app/public/trainings/'. $name) ;
    }
    public function deleteFile(Request $request,$locale,$id)
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        $training = Training::find($id);
        $file = $training->file;
        Storage::delete('app/public/trainings/'. $file);
        $training->update([
            'file' => null
        ]);
        $request->session()->flash('type', 'success');
        $request->session()->flash('message', $translations['file_deleted'] ?? 'File successfully deleted');
        return redirect()->back();

    }

    public function downloadFile($lang,$name)
    {
        $file_path = storage_path() .'/app/public/trainings/'. $name;
        return Response::download($file_path, $name);
    }
}
