<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->role->name == 'superadmin') {
            return redirect()->route('admin.home', ['locale' => app()->getLocale()]);
        } elseif (auth()->user()->role->name == 'doctor') {
            return redirect()->route('doctor.home', ['locale' => app()->getLocale()]);
        } else {
            return redirect()->route('user.home', ['locale' => app()->getLocale()]);

        }
    }
}
