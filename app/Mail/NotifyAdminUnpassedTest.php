<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdminUnpassedTest extends Mailable
{
    use Queueable, SerializesModels;

    public $users;
    public $admin;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($users,$admin)
    {
        $this->users = $users;
        $this->admin = $admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Notification about outdated tests')
            ->view('emails.adminNotifyUnpassedTests');
    }
}
