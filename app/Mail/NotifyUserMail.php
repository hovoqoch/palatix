<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUserMail extends Mailable
{
    use Queueable, SerializesModels;

    public $device;
    public $diff;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($device,$diff)
    {
        $this->device = $device;
        $this->diff = $diff;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Device Validation Date Notification')
            ->view('emails.deviceNextValidationNotification');
    }
}
