<?php

namespace App\Mail;

use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DoctorsPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $doctor;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($doctor,$link)
    {
        $this->doctor = $doctor;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $settings = Setting::where('key','reg_email_subj')->first();
        return $this->subject($settings['val'])
            ->view('emails.password');
    }
}
