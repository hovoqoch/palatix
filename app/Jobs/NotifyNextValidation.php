<?php

namespace App\Jobs;

use App\Mail\NotifyUserMail;
use App\Models\Device;
use App\Models\Practicedevices;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class NotifyNextValidation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notifications = DB::table('notification_logs')->select('notification_logs.*')->get();
        $notification_log = [];
        foreach ($notifications as $notification) {
            $notification_log[$notification->device_practice_id] = [$notification->recall_1, $notification->recall_2, $notification->recall_3];
        }
        $settings = Setting::all();
        $recall_1 = (int)$settings->where('key', 'recall_1')->first()->val;
        $recall_2 = (int)$settings->where('key', 'recall_2')->first()->val;
        $recall_3 = (int)$settings->where('key', 'recall_3')->first()->val;
        $devices = Practicedevices::all();
        //dd(date('Y/m/d',1576707200));
        $group_1 = [];
        $group_2 = [];
        $group_3 = [];


        foreach ($devices as $device) {
            $date = Carbon::parse($device->next_validation_date);
            $now = Carbon::now();
            $diff = $date->diffInDays($now);

            switch ($diff) {
                case ($diff <= $recall_3 && $diff > $recall_2):
                    if (key_exists($device->id, $notification_log)) {
                        if (!$notification_log[$device->id][0]) {
                            // send mail and update log
                            $this->notifyUser($device, 1, $diff);

                        }
                    } else {
                        DB::table('notification_logs')->insert([
                            'device_practice_id' => $device->id,

                        ]);
                        // send mail and update log
                        $this->notifyUser($device, 1, $diff);

                    }
                    break;
                case ($diff <= $recall_2 && $diff > $recall_1) :
                    //
                    if (key_exists($device->id, $notification_log)) {
                        if (!$notification_log[$device->id][1]) {
                            // send mail and update log
                            $this->notifyUser($device, 2, $diff);

                        }
                    } else {
                        DB::table('notification_logs')->insert([
                            'device_practice_id' => $device->id,
                            'recall_1' => 1,

                        ]);
                        // send mail and update log
                        $this->notifyUser($device, 2, $diff);

                    }
                    break;
                case ($diff <= $recall_1) :
                    //
                    if (key_exists($device->id, $notification_log)) {
                        if (!$notification_log[$device->id][2]) {
                            // send mail and update log
                            $this->notifyUser($device, 3, $diff);

                        }
                    } else {
                        DB::table('notification_logs')->insert([
                            'device_practice_id' => $device->id,
                            'recall_1' => 1,
                            'recall_2' => 1,

                        ]);
                        // send mail and update log
                        $this->notifyUser($device, 3, $diff);

                    }
                    break;
                default :
                    // Do Nothing
                    break;
            }

        }

    }

    public function notifyUser($device = null,  $recall = 1,$diff = 60)
    {
        if ($device) {
            // get email template
            if (isset($device->user) && isset($device->user->email) && $device->user->email){
                $email = $device->user->email;

                if ($email)
                    // send mail to user
                    Mail::to($email)->send(new NotifyUserMail($device, $diff));
                // update notification log
                $recall_data = [
                    'device_practice_id' => $device->id,
                    'recall_'.$recall => 1,
                ];
                DB::table('notification_logs')->insert($recall_data);
            }
        }
    }
}
