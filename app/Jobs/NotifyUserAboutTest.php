<?php

namespace App\Jobs;

use App\Mail\NotifyAdminUnpassedTest;
use App\Models\Elearning;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class NotifyUserAboutTest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $admin = User::find(1);
        $adminEmail = $admin->email;
        $outdated = [];
        $elearnings = Elearning::with('users')->get();
        foreach ($elearnings as $elearning) {
            $date = Carbon::parse($elearning->deadline_date);
            $now = Carbon::now();
            foreach ($elearning->users as $user) {
                if ($now > $date && $user->pivot->status === 0 && $user->pivot->admin_notified === 0) {
                    $outdated[] = $user;
                    DB::table('elearning_user')->where('id',$user->pivot->id)->update([
                        'admin_notified' => 1
                    ]);

                }
            }
        }
        Mail::to($adminEmail)->send(new NotifyAdminUnpassedTest($outdated,$admin));

    }
}
