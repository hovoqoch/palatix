<?php

namespace App\Providers;

use App\Models\Setting;
use App\Models\Translation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer("*", function () {
            View::share('translations', Translation::get()->pluck(app()->getLocale(), 'short_code'));
            View::share('website_settings', Setting::get()->pluck('val', 'key'));
        });
    }


}
