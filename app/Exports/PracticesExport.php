<?php

namespace App\Exports;

use App\Models\Practice;
use App\Models\Translation;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PracticesExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Practice::with('region', 'devices','practicedevices','users')->get();
    }

    public function map($practice) : array {
        return [
            $practice->id,
            $practice->name,
            $practice->specialization,
            $practice->street_number,
            $practice->postal_code,
            $practice->phone,
            $practice->practice_email,
            $practice->city,
            $practice->region->name,
            $practice->site,
            $practice->invoice_name,
            $practice->invoice_street,
            $practice->invoice_postal,
            $practice->invoice_city,
            Carbon::parse($practice->created_at)->toFormattedDateString(),
            Carbon::parse($practice->updated_at)->toFormattedDateString()
        ] ;


    }
    /**
     * @var Practice $invoice
     */
    public function headings(): array
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        return [
            'Id',
            $translations['name'] ?? 'Name',
            $translations['specialization'] ?? 'Specialization',
            $translations['street_and_number'] ?? 'Street and number',
            $translations['postal_code'] ?? 'Postal code',
            $translations['practice_phone'] ?? 'Practice Phone number',
            $translations['practice_email'] ?? 'Practice email',
            $translations['city'] ?? 'City',
            $translations['region'] ?? 'Region',
            $translations['site'] ?? 'Site',
            $translations['invoice_name'] ?? 'Invoice name',
            $translations['invoice_street'] ?? 'Invoice street and number',
            $translations['invoice_postal'] ?? 'Invoice postal code',
            $translations['invoice_city'] ?? 'Invoice city',
            $translations['created'] ?? 'Created',
            $translations['updated'] ?? 'Updated',
        ];
    }
}
