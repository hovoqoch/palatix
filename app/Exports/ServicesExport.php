<?php

namespace App\Exports;

use App\Models\Services;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

class ServicesExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Services::all();
    }
    public function map($services) : array {
        return [
            $services->id,
            $services->name,
            Carbon::parse($services->created_at)->toFormattedDateString(),
            Carbon::parse($services->updated_at)->toFormattedDateString()
        ] ;


    }
    /**
     * @var Services $invoice
     */
    public function headings(): array
    {
        return [
            'Id',
            $translations['name'] ?? 'Name',
            $translations['created'] ?? 'Created',
            $translations['updated'] ?? 'Updated',
        ];
    }
}
