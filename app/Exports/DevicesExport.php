<?php

namespace App\Exports;

use App\Models\Device;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DevicesExport implements  FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Device::with('manufacturer', 'type','user')->get();
    }

    public function map($device) : array {
        return [
            $device->id,
            $device->manufacturer->name,
            $device->type->name,
            $device->name,
            $device->val_duration.' year',
            $device->user->name,
            $device->status,
            Carbon::parse($device->created_at)->toFormattedDateString(),
            Carbon::parse($device->updated_at)->toFormattedDateString()
        ] ;


    }
    /**
     * @var Device $invoice
     */
    public function headings(): array
    {
        return [
            'Id',
            $translations['manufacturer'] ?? 'Manufacture',
            $translations['type'] ?? 'Type',
            $translations['name'] ?? 'Name',
            $translations['validation_duration'] ?? 'Validation duration',
            $translations['user'] ?? 'User',
            $translations['status'] ?? 'Status',
            $translations['created'] ?? 'Created',
            $translations['updated'] ?? 'Updated',
        ];
    }
}
