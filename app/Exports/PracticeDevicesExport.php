<?php

namespace App\Exports;

use App\Models\Practicedevices;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PracticeDevicesExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Practicedevices::with('manufacturer', 'type','user','device','Practice')->get();
    }

    public function map($practicedevices) : array {
    return [
        $practicedevices->id,
        $practicedevices->Practice->name,
        $practicedevices->user->name,
        $practicedevices->device->name,
        $practicedevices->place_in_practice,
        $practicedevices->serial_number,
        $practicedevices->software_version,
        $practicedevices->parameter_version,
        Carbon::createFromTimestamp($practicedevices->maintenance_report_valid)->toFormattedDateString(),
        Carbon::createFromTimestamp($practicedevices->maintenance_report_date)->toFormattedDateString(),
        $practicedevices->setup_approval,
        Carbon::createFromTimestamp($practicedevices->first_setup_date)->toFormattedDateString(),
        $practicedevices->manufacturing_year,
        Carbon::createFromTimestamp($practicedevices->last_validation_date)->toFormattedDateString(),
        Carbon::createFromTimestamp($practicedevices->next_validation_date)->toFormattedDateString(),
        Carbon::createFromTimestamp($practicedevices->last_maintenance_date)->toFormattedDateString(),
        $practicedevices->remarks,
        Carbon::parse($practicedevices->created_at)->toFormattedDateString(),
        Carbon::parse($practicedevices->updated_at)->toFormattedDateString()
    ] ;


}
    /**
     * @var Practicedevices $invoice
     */
    public function headings(): array
    {
        return [
            'Id',
            $translations['practice'] ?? 'Practice',
            $translations['user'] ?? 'User',
            $translations['devices'] ?? 'Device',
            $translations['place_in_practice'] ?? 'Place in practice',
            $translations['serial_number'] ?? 'Serial number',
            $translations['software_version'] ?? 'Software version',
            $translations['parameter_version'] ?? 'Parameter version',
            $translations['maintenance_report_valid'] ?? 'Maintenance report valid',
            $translations['maintenance_report_date'] ?? 'Maintenance report date',
            $translations['setup_approval'] ?? 'Setup approval',
            $translations['first_setup_date'] ?? 'First setup date',
            $translations['manufacturing_year'] ?? 'Manufacturing year',
            $translations['last_validation_year'] ?? 'Last validation date',
            $translations['next_validation_date'] ?? 'Next validation date',
            $translations['last_maintenance_date'] ?? 'Last maintenance date',
            $translations['remarks'] ?? 'Remarks',
            $translations['created'] ?? 'Created',
            $translations['updated'] ?? 'Updated',
        ];
    }
}
