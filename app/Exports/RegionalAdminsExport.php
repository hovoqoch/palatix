<?php

namespace App\Exports;

use App\Models\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RegionalAdminsExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::where('role_id',1)->with('region')->get();
    }

    public function map($regionaladmins) : array {
        return [
            $regionaladmins->id,
            $regionaladmins->region['name'],
            $regionaladmins->name,
            $regionaladmins->last_name,
            $regionaladmins->email,
            Carbon::parse($regionaladmins->created_at)->toFormattedDateString(),
            Carbon::parse($regionaladmins->updated_at)->toFormattedDateString()
        ] ;


    }
    /**
     * @var User $invoice
     */
    public function headings(): array
    {
        return [
            'Id',
            $translations['region'] ?? 'Region',
            $translations['name'] ?? 'Name',
            $translations['last_name'] ?? 'Last Name',
            $translations['email'] ?? 'Email',
            $translations['created'] ?? 'Created',
            $translations['updated'] ?? 'Updated',
        ];
    }
}
