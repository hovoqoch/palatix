<?php

namespace App\Exports;

use App\Models\Manufacturer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

class ManufacturersExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Manufacturer::all();
    }

    public function map($manufacturer) : array {
        return [
            $manufacturer->id,
            $manufacturer->name,
            Carbon::parse($manufacturer->created_at)->toFormattedDateString(),
            Carbon::parse($manufacturer->updated_at)->toFormattedDateString()
        ] ;


    }
    /**
     * @var Manufacturer $invoice
     */
    public function headings(): array
    {
        return [
            'Id',
            $translations['name'] ?? 'Name',
            $translations['created'] ?? 'Created',
            $translations['updated'] ?? 'Updated',
        ];
    }
}
