<?php

namespace App\Exports;

use App\Models\Type;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

class DeviceTypesExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Type::all();
    }

    public function map($type) : array {
        return [
            $type->id,
            $type->name,
            Carbon::parse($type->created_at)->toFormattedDateString(),
            Carbon::parse($type->updated_at)->toFormattedDateString()
        ] ;


    }
    /**
     * @var Type $invoice
     */
    public function headings(): array
    {
        return [
            'Id',
            $translations['name'] ?? 'Name',
            $translations['created'] ?? 'Created',
            $translations['updated'] ?? 'Updated',
        ];
    }
}
