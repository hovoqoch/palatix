<?php

namespace App\Exports;

use App\Models\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class EmployeesExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::where('role_id',3)->with('qualification','practice')->get();
    }

    public function map($employees) : array {
        return [
            $employees->id,
            $employees->practice->name,
            $employees->title,
            $employees->name,
            $employees->last_name,
            $employees->phone,
            $employees->email,
            $employees->qualification->name,
            Carbon::parse($employees->created_at)->toFormattedDateString(),
            Carbon::parse($employees->updated_at)->toFormattedDateString()
        ] ;


    }
    /**
     * @var User $invoice
     */
    public function headings(): array
    {
        return [
            'Id',
            $translations['practice'] ?? 'Practice',
            $translations['job_title'] ?? 'Job title',
            $translations['name'] ?? 'Name',
            $translations['last_name'] ?? 'Last Name',
            $translations['practice_phone'] ?? 'Phone number',
            $translations['email'] ?? 'Email',
            $translations['qualif'] ?? 'Qualification',
            $translations['created'] ?? 'Created',
            $translations['updated'] ?? 'Updated',
        ];
    }
}
