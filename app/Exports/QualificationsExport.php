<?php

namespace App\Exports;

use App\Models\Qualification;
use App\Models\Translation;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

class QualificationsExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Qualification::all();
    }
    public function map($qualification) : array {
        return [
            $qualification->id,
            $qualification->name,
            Carbon::parse($qualification->created_at)->toFormattedDateString(),
            Carbon::parse($qualification->updated_at)->toFormattedDateString()
        ] ;


    }
    /**
     * @var Qualification $invoice
     */
    public function headings(): array
    {
        $translations = Translation::get()->pluck(app()->getLocale(), 'short_code');
        return [
            'Id',
            $translations['name'] ?? 'Name',
            $translations['created'] ?? 'Created',
            $translations['updated'] ?? 'Updated',
        ];
    }
}
