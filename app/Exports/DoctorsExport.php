<?php

namespace App\Exports;

use App\Models\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DoctorsExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::where('role_id',2)->with('practice','services')->get();
    }
    public function map($doctors) : array {
        $serv = [];
        foreach ($doctors->services as $service){
            $serv[] = $service->name;
        }
        return [
            $doctors->id,
            $doctors->practice->name,
            $serv,
            $doctors->title,
            $doctors->name,
            $doctors->last_name,
            $doctors->phone,
            $doctors->email,
            Carbon::parse($doctors->created_at)->toFormattedDateString(),
            Carbon::parse($doctors->updated_at)->toFormattedDateString()
        ] ;


    }
    /**
     * @var User $invoice
     */
    public function headings(): array
    {
        return [
            'Id',
            $translations['practice'] ?? 'Practice',
            $translations['services'] ?? 'Services',
            $translations['doctors_title'] ?? 'Doctor\'s title',
            $translations['name'] ?? 'Name',
            $translations['last_name'] ?? 'Last Name',
            $translations['practice_phone'] ?? 'Phone number',
            $translations['email'] ?? 'Email',
            $translations['created'] ?? 'Created',
            $translations['updated'] ?? 'Updated',
        ];
    }
}
