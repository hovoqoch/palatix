<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Exports\DeviceTypesExport;
use App\Exports\DoctorsExport;
use App\Exports\EmployeesExport;
use App\Exports\PracticesExport;
use App\Exports\RegionalAdminsExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\QualificationsExport;
use App\Exports\ServicesExport;
use App\Exports\DevicesExport;
use App\Exports\PracticeDevicesExport;
use App\Exports\ManufacturersExport;

Route::get('/', 'HomeController@index')->name('home');

Route::group([
    'prefix' => '{locale}',
    'where' => ['locale' => '[a-zA-Z]{2}'],
    'middleware' => 'setlocale',
], function () {
    Auth::routes();
});

Route::group([
    'prefix' => '{locale}',
    'where' => ['locale' => '[a-zA-Z]{2}'],
    'middleware' => 'setlocale',
], function () {

    Route::namespace('Admin')->group(function () {
        Route::group(['middleware' => 'role:superadmin'], function () {
            Route::get('/', 'HomeController@index')->name('admin.home');
            Route::resource('/users', 'UsersController');
            Route::post('/elearning/filter', 'ElearningController@filterDevices')->name('admin.filter');
            Route::get('/doctors', 'UsersController@doctors')->name('doctors.home');
            Route::get('/doctors/createdoctor', 'UsersController@addDoctor');
            Route::get('/doctors/{id}/editDoctor', 'UsersController@editDoctor');
            Route::post('/doctors/storedoctor', 'UsersController@storeDoctor');
            Route::get('/reports/generate', 'ReportController@generate')->name('report.generate');
            Route::post('/reports/generate', 'ReportController@dataCreate');

            Route::post('/reports/store', 'ReportController@store')->name('report.store');

            //Route::post('/reports/store', 'ReportController@dataStore')->name('report.store');
            Route::get('/reports', 'ReportController@index');
            Route::get('/show/reports', 'ReportController@show')->name('report.show');
            Route::post('/report/{id}/pdf-download', 'ReportController@downloadPDF')->name('report.download');
            Route::delete('/report/{id}/delete', 'ReportController@delete')->name('report.delete');
            Route::get('/report/{id}/edit', 'ReportController@dataCreate')->name('report.edit');


            Route::resource('/sites', 'SiteController');
            Route::post('/elearning/getsites', 'SiteController@getSitesByPractice')->name('admin.getSitesByPractice');


            Route::group(['prefix' => 'download'], function () {
                Route::get('/devices', function () {
                    return Excel::download(new DevicesExport, 'Devices.xlsx');
                })->middleware('auth');
                Route::get('/practicedevices', function () {
                    return Excel::download(new PracticeDevicesExport, 'PracticeDevices.xlsx');
                })->middleware('auth');
                Route::get('/services', function () {
                    return Excel::download(new ServicesExport, 'Services.xlsx');
                })->middleware('auth');
                Route::get('/qualifications', function () {
                    return Excel::download(new QualificationsExport, 'Qualifications.xlsx');
                })->middleware('auth');
                Route::get('/manufacturers', function () {
                    return Excel::download(new ManufacturersExport, 'Manufacturer.xlsx');
                })->middleware('auth');
                Route::get('/types', function () {
                    return Excel::download(new DeviceTypesExport, 'DeviceTypes.xlsx');
                })->middleware('auth');
                Route::get('/practice', function () {
                    return Excel::download(new PracticesExport, 'Practice.xlsx');
                })->middleware('auth');
                Route::get('/employee', function () {
                    return Excel::download(new EmployeesExport, 'Employees.xlsx');
                })->middleware('auth');
                Route::get('/doctor', function () {
                    return Excel::download(new DoctorsExport, 'Doctors.xlsx');
                })->middleware('auth');
                Route::get('/admin', function () {
                    return Excel::download(new RegionalAdminsExport, 'RegionalAdmins.xlsx');
                })->middleware('auth');
            });


            Route::get('/changelogin/', 'UsersController@changelogin');
            Route::put('/changelogin/store', 'UsersController@updatepassword');


            Route::get('/doctors/createpassword/{token}', 'SendMailController@createpassword');
            Route::put('/doctors/createpassword/{token}/store', 'SendMailController@updatepassword');
            Route::get('/doctor/{id}/edit', 'UsersController@editDoctor');
            Route::get('/employees', 'UsersController@employees')->name('employees.index');
            Route::get('/admins', 'UsersController@admins')->name('admins.index');
            Route::get('/createemployee', 'UsersController@addEmployee');
            Route::get('/createadmin', 'UsersController@addAdmin');
            Route::post('/employees/storeemployee', 'UsersController@storeEmployee');
            Route::post('/admins/storeadmin', 'UsersController@storeAdmin');
            Route::put('/employees/{id}', 'UsersController@updateEmployee');
            Route::put('/admin/{id}', 'UsersController@updateAdmin');
            Route::get('/employee/{id}/edit', 'UsersController@editEmployee');
            Route::get('/admin/{id}/edit', 'UsersController@editAdmin');
            Route::resource('/settings', 'SettingsController');
            Route::resource('/devices', 'DevicesController');
            Route::resource('/qualifications', 'QualificationsController');
            Route::resource('/types', 'TypesController');
            Route::resource('/practices', 'PracticesController');
            Route::resource('/translations', 'TranslationsController');
            Route::resource('/manufacturers', 'ManufacturersController');
            Route::resource('/services', 'ServicesController');
            Route::resource('/practice-devices', 'PracticeDevicesController');
            Route::resource('/elearning', 'ElearningController');
            Route::resource('/training', 'TrainingController');
            Route::resource('/elearning/{id}/questions', 'ElearningQuestionController');
            Route::get('/elearning/download/{name}', 'ElearningController@download_pdf')->name('download_pdf');
            Route::get('/elearning/file/{name}', 'ElearningController@viewFile')->name('elearning.file.view');
            Route::get('/elearning/{id}/file', 'ElearningController@deleteFile')->name('elearning.file.delete');
            Route::get('/training/download/{name}', 'TrainingController@viewFile')->name('training.download');
            Route::get('/training/{id}/file', 'TrainingController@deleteFile')->name('training.file.delete');
        });
    });


    // Routes for Doctor role
    Route::group(['middleware' => 'role:doctor', 'prefix' => 'account', 'namespace' => 'Doctor'], function () {
        Route::name('doctor.')->group(function () {

            Route::get('/', 'HomeController@index')->name('home');
            Route::resource('/devices', 'PracticeDevicesController');
            Route::get('/report/{id}', 'PracticeDevicesController@report');
            Route::get('/createtest/{id}', 'PracticeDevicesController@createtest');
            Route::post('/createtest/store', 'PracticeDevicesController@storetest');
            Route::post('/devices/filter', 'PracticeDevicesController@filterDevices');
            Route::post('/get-devices-by-practice', 'PracticeDevicesController@getDevicesByPractice')->name('getDevicesByPractice');
            Route::post('/employee/add', 'UsersController@storeEmployee');
            Route::resource('/employees', 'UsersController');
            Route::get('/profile/edit', 'UsersController@profileEdit')->name('profile.edit');
            Route::resource('/training', 'DoctorTrainingController');
            Route::get('/training/download/{name}', 'DoctorTrainingController@downloadFile');
            Route::post('/profile/update', 'UsersController@profileUpdate')->name('profile.update');
            Route::post('/profile/password', 'UsersController@profileUpdatePassword')->name('profile.update.password');
            Route::resource('/elearning', 'DoctorElearningController');
            Route::resource('/elearning/{id}/questions', 'DoctorElearningQuestionController');
            Route::get('/elearning/download/{name}', 'DoctorElearningController@download_pdf')->name('download_pdf');
            Route::get('/elearning/file/{name}', 'ElearningController@viewFile')->name('elearning.file.view');
            Route::get('/elearning/{id}/file', 'ElearningController@deleteFile')->name('elearning.file.delete');
            Route::get('/training/file/{name}', 'DoctorTrainingController@viewFile')->name('training.file.view');
            Route::get('/training/{id}/file', 'DoctorTrainingController@deleteFile')->name('training.file.delete');

        });
    });


    Route::group(['middleware' => 'role:employee', 'prefix' => 'user', 'namespace' => 'User'], function () {
        Route::name('user.')->group(function () {

            Route::get('/', 'HomeController@index')->name('home');
            Route::get('/training', 'UserTrainingController@index');
            Route::get('/training/{name}', 'UserTrainingController@downloadFile');
            Route::resource('/devices', 'PracticeDevicesController');
            Route::post('/devices/filter', 'PracticeDevicesController@filterDevices')->name('devices.filter');
            Route::resource('/elearning', 'UserElearningController');
            Route::get('/elearning/{id}/test', 'UserElearningController@passVideo')->name('test.video');
            Route::get('/elearning/{id}/test/questions', 'UserElearningController@passTest')->name('test.questions');
            Route::get('/score/{id}', 'UserElearningController@score')->name('score');
            Route::get('/generate-certificate/{id}', 'UserElearningController@create_certification')->name('generate_certificate');
            Route::post('/elearning/{id}/submit-test', 'UserElearningController@submitTest');
            Route::get('/profile/edit', 'UsersController@profileEdit')->name('profile.edit');
            Route::get('/elearning/certificate/{name}', 'UserElearningController@downloadCertificate')->name('certificate');
            Route::get('/elearning/attached/{name}', 'UserElearningController@downloadAttached')->name('attached');
            Route::post('/profile/update', 'UsersController@profileUpdate')->name('profile.update');
            Route::post('/profile/password', 'UsersController@profileUpdatePassword')->name('profile.update.password');

        });
    });
});
Route::get(app()->getLocale() . '/user/createpassword/{token}', 'User\UsersController@createPassword')->name('employee.verify');
Route::put(app()->getLocale() . '/user/createpassword/{token}/store', 'User\UsersController@updatePassword');
Route::get('/changeStatus', 'Admin\UsersController@changeStatus');
Route::get('/approveDevice/{id}', 'Admin\DevicesController@approve')->name('approveDevice');
Route::get('/keepPrivet/{id}', 'Admin\DevicesController@keepPrivate')->name('keepPrivateDevice');
Route::put('/doctors/{id}', 'Admin\UsersController@updateDoctor');
Route::get('/doctors/{id}', 'Admin\UsersController@updateDoctor');
