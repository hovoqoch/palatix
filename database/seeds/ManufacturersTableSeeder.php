<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ManufacturersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('manufacturers')->insert([
            ['name' => 'Mocom Sterilization First',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'MELAG Medizintechnik oHG',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Getinge Group',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'EURONDA Deutschland GmbH',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Miele Deutschland',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'SciCan GmbH',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'W&H DEUTSCHLAND GMBH',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'IC Medical GmbH',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Dentsply Sirona Deutschland GmbH',
                'created_at' => now(),
                'updated_at' => now()],
        ]);
    }
}
