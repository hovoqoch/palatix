<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('translations')->insert([
             [
                'id'=>1,
                'short_code'=>'short_code',
                'en'=>'Short code',
                'de'=>'Short code',
                'created_at'=>'2019-10-29 08:08:09',
                'updated_at'=>'2019-10-29 08:08:09',
                'deleted_at'=>NULL
            ] , [
            'id'=>5,
            'short_code'=>'update',
            'en'=>'Update',
            'de'=>'Update',
            'created_at'=>'2019-10-29 08:08:09',
            'updated_at'=>'2019-10-29 08:08:09',
            'deleted_at'=>NULL
        ] , [
            'id'=>8,
            'short_code'=>'practice',
            'en'=>'Practice',
            'de'=>'Practice',
            'created_at'=>'2019-10-29 08:08:09',
            'updated_at'=>'2019-10-29 08:08:09',
            'deleted_at'=>NULL
        ] , [
            'id'=>10,
            'short_code'=>'devices',
            'en'=>'Devices',
            'de'=>'Devices',
            'created_at'=>'2019-10-29 08:08:09',
            'updated_at'=>'2019-10-29 08:08:09',
            'deleted_at'=>NULL
        ] , [
            'id'=>11,
            'short_code'=>'dev_types',
            'en'=>'Devices Types',
            'de'=>'Devices Types',
            'created_at'=>'2019-10-29 08:08:09',
            'updated_at'=>'2019-10-29 08:08:09',
            'deleted_at'=>NULL
        ] , [
            'id'=>13,
            'short_code'=>'translations',
            'en'=>'Translations',
            'de'=>'Translations',
            'created_at'=>'2019-10-29 08:08:09',
            'updated_at'=>'2019-10-29 08:08:09',
            'deleted_at'=>NULL
        ] , [
            'id'=>14,
            'short_code'=>'settings',
            'en'=>'Settings',
            'de'=>'Settings',
            'created_at'=>'2019-10-29 08:08:09',
            'updated_at'=>'2019-10-29 08:08:09',
            'deleted_at'=>NULL
        ] , [
            'id'=>16,
            'short_code'=>'services',
            'en'=>'Services',
            'de'=>'Services',
            'created_at'=>'2019-10-29 08:08:09',
            'updated_at'=>'2019-10-29 08:08:09',
            'deleted_at'=>NULL
        ] , [
            'id'=>21,
            'short_code'=>'your_device_lists',
            'en'=>'Your Device List',
            'de'=>'Your Device List',
            'created_at'=>'2019-10-29 15:49:46',
            'updated_at'=>'2019-10-29 15:49:46',
            'deleted_at'=>NULL
        ] , [
            'id'=>22,
            'short_code'=>'name',
            'en'=>'Name',
            'de'=>'Name',
            'created_at'=>'2019-10-29 15:50:20',
            'updated_at'=>'2019-11-01 18:35:46',
            'deleted_at'=>NULL
        ] , [
            'id'=>23,
            'short_code'=>'serial_number',
            'en'=>'Serial Number',
            'de'=>'Serial Number',
            'created_at'=>'2019-10-29 15:51:01',
            'updated_at'=>'2019-10-29 15:51:01',
            'deleted_at'=>NULL
        ] , [
            'id'=>24,
            'short_code'=>'software_version',
            'en'=>'Software Version',
            'de'=>'Software Version',
            'created_at'=>'2019-10-29 15:51:31',
            'updated_at'=>'2019-10-29 15:51:31',
            'deleted_at'=>NULL
        ] , [
            'id'=>25,
            'short_code'=>'parameter_version',
            'en'=>'Parameter Version',
            'de'=>'Parameter Version',
            'created_at'=>'2019-10-29 15:51:54',
            'updated_at'=>'2019-10-29 15:51:54',
            'deleted_at'=>NULL
        ] , [
            'id'=>26,
            'short_code'=>'last_validation',
            'en'=>'Last validation',
            'de'=>'Last validation',
            'created_at'=>'2019-10-29 15:52:21',
            'updated_at'=>'2019-10-29 15:52:21',
            'deleted_at'=>NULL
        ] , [
            'id'=>27,
            'short_code'=>'next_validation',
            'en'=>'Next validation',
            'de'=>'Next validation',
            'created_at'=>'2019-10-29 15:52:42',
            'updated_at'=>'2019-10-29 15:52:42',
            'deleted_at'=>NULL
        ] , [
            'id'=>28,
            'short_code'=>'no_devices',
            'en'=>'No devices',
            'de'=>'No devices',
            'created_at'=>'2019-10-29 15:53:19',
            'updated_at'=>'2019-10-29 15:53:19',
            'deleted_at'=>NULL
        ] , [
            'id'=>29,
            'short_code'=>'first_name',
            'en'=>'First name',
            'de'=>'First name',
            'created_at'=>'2019-10-29 15:59:25',
            'updated_at'=>'2019-10-29 15:59:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>30,
            'short_code'=>'last_name',
            'en'=>'Last name',
            'de'=>'Last name',
            'created_at'=>'2019-10-29 15:59:47',
            'updated_at'=>'2019-10-29 15:59:47',
            'deleted_at'=>NULL
        ] , [
            'id'=>31,
            'short_code'=>'email',
            'en'=>'Email',
            'de'=>'Email',
            'created_at'=>'2019-10-29 16:00:12',
            'updated_at'=>'2019-10-29 16:00:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>32,
            'short_code'=>'title',
            'en'=>'Title',
            'de'=>'Title',
            'created_at'=>'2019-10-29 16:00:34',
            'updated_at'=>'2019-10-29 16:00:34',
            'deleted_at'=>NULL
        ] , [
            'id'=>33,
            'short_code'=>'phone',
            'en'=>'Phone',
            'de'=>'Phone',
            'created_at'=>'2019-10-29 16:00:51',
            'updated_at'=>'2019-10-29 16:00:51',
            'deleted_at'=>NULL
        ] , [
            'id'=>34,
            'short_code'=>'old_password',
            'en'=>'Old Password',
            'de'=>'Old Password',
            'created_at'=>'2019-10-29 16:01:32',
            'updated_at'=>'2019-10-29 16:01:32',
            'deleted_at'=>NULL
        ] , [
            'id'=>35,
            'short_code'=>'new_password',
            'en'=>'New password',
            'de'=>'New password',
            'created_at'=>'2019-10-29 16:01:56',
            'updated_at'=>'2019-10-29 16:01:56',
            'deleted_at'=>NULL
        ] , [
            'id'=>36,
            'short_code'=>'confirm_new_password',
            'en'=>'Confirm new password',
            'de'=>'Confirm new password',
            'created_at'=>'2019-10-29 16:02:25',
            'updated_at'=>'2019-10-29 16:02:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>37,
            'short_code'=>'file_name',
            'en'=>'File name',
            'de'=>'File name',
            'created_at'=>'2019-10-29 16:04:00',
            'updated_at'=>'2019-10-29 16:04:00',
            'deleted_at'=>NULL
        ] , [
            'id'=>38,
            'short_code'=>'device_name',
            'en'=>'Device name',
            'de'=>'Device name',
            'created_at'=>'2019-10-29 16:04:16',
            'updated_at'=>'2019-10-29 16:04:16',
            'deleted_at'=>NULL
        ] , [
            'id'=>39,
            'short_code'=>'download_file',
            'en'=>'Download file',
            'de'=>'Download file',
            'created_at'=>'2019-10-29 16:04:50',
            'updated_at'=>'2019-10-29 16:04:50',
            'deleted_at'=>NULL
        ] , [
            'id'=>40,
            'short_code'=>'no_training_materials',
            'en'=>'No training materials',
            'de'=>'No training materials',
            'created_at'=>'2019-10-29 16:05:15',
            'updated_at'=>'2019-10-29 16:05:15',
            'deleted_at'=>NULL
        ] , [
            'id'=>41,
            'short_code'=>'add_new_employee',
            'en'=>'Add new employee',
            'de'=>'Add new employee',
            'created_at'=>'2019-10-29 16:12:08',
            'updated_at'=>'2019-10-29 16:12:08',
            'deleted_at'=>NULL
        ] , [
            'id'=>42,
            'short_code'=>'practice_name',
            'en'=>'Practice name',
            'de'=>'Practice name',
            'created_at'=>'2019-10-29 16:12:32',
            'updated_at'=>'2019-10-29 16:12:32',
            'deleted_at'=>NULL
        ] , [
            'id'=>43,
            'short_code'=>'job_title',
            'en'=>'Job title',
            'de'=>'Job title',
            'created_at'=>'2019-10-29 16:13:24',
            'updated_at'=>'2019-10-29 16:13:24',
            'deleted_at'=>NULL
        ] , [
            'id'=>44,
            'short_code'=>'status',
            'en'=>'Status',
            'de'=>'Status',
            'created_at'=>'2019-10-29 16:14:05',
            'updated_at'=>'2019-10-29 16:14:05',
            'deleted_at'=>NULL
        ] , [
            'id'=>45,
            'short_code'=>'no_employees_registered',
            'en'=>'No Employees registered',
            'de'=>'No Employees registered',
            'created_at'=>'2019-10-29 16:14:54',
            'updated_at'=>'2019-10-29 16:14:54',
            'deleted_at'=>NULL
        ] , [
            'id'=>46,
            'short_code'=>'are_you_sure',
            'en'=>'Are you sure?',
            'de'=>'Are you sure?',
            'created_at'=>'2019-10-29 16:15:37',
            'updated_at'=>'2019-10-29 16:15:37',
            'deleted_at'=>NULL
        ] , [
            'id'=>47,
            'short_code'=>'details',
            'en'=>'Details',
            'de'=>'Details',
            'created_at'=>'2019-10-29 16:16:23',
            'updated_at'=>'2019-10-29 16:16:23',
            'deleted_at'=>NULL
        ] , [
            'id'=>48,
            'short_code'=>'add_new_device',
            'en'=>'Add new device',
            'de'=>'Add new device',
            'created_at'=>'2019-10-29 16:21:35',
            'updated_at'=>'2019-10-29 16:21:35',
            'deleted_at'=>NULL
        ] , [
            'id'=>49,
            'short_code'=>'type',
            'en'=>'Type',
            'de'=>'Type',
            'created_at'=>'2019-10-29 16:22:19',
            'updated_at'=>'2019-10-29 16:22:19',
            'deleted_at'=>NULL
        ] , [
            'id'=>50,
            'short_code'=>'approve',
            'en'=>'Approve',
            'de'=>'Approve',
            'created_at'=>'2019-10-29 16:24:25',
            'updated_at'=>'2019-10-29 16:24:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>51,
            'short_code'=>'keep_private',
            'en'=>'Keep private',
            'de'=>'Keep private',
            'created_at'=>'2019-10-29 16:24:49',
            'updated_at'=>'2019-10-29 16:24:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>52,
            'short_code'=>'validation_duration',
            'en'=>'Validation duration',
            'de'=>'Validation duration',
            'created_at'=>'2019-10-29 16:27:01',
            'updated_at'=>'2019-10-29 16:27:01',
            'deleted_at'=>NULL
        ] , [
            'id'=>53,
            'short_code'=>'year',
            'en'=>'year',
            'de'=>'year',
            'created_at'=>'2019-10-29 16:27:15',
            'updated_at'=>'2019-10-29 16:27:15',
            'deleted_at'=>NULL
        ] , [
            'id'=>54,
            'short_code'=>'years',
            'en'=>'years',
            'de'=>'years',
            'created_at'=>'2019-10-29 16:27:22',
            'updated_at'=>'2019-10-29 16:27:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>55,
            'short_code'=>'sign_in',
            'en'=>'Sign In',
            'de'=>'Sign In',
            'created_at'=>'2019-10-29 16:28:26',
            'updated_at'=>'2019-10-29 16:28:26',
            'deleted_at'=>NULL
        ] , [
            'id'=>56,
            'short_code'=>'confirm_password',
            'en'=>'Confirm password',
            'de'=>'Confirm password',
            'created_at'=>'2019-10-29 16:29:17',
            'updated_at'=>'2019-10-29 16:29:17',
            'deleted_at'=>NULL
        ] , [
            'id'=>57,
            'short_code'=>'reset_password',
            'en'=>'Reset password',
            'de'=>'Reset password',
            'created_at'=>'2019-10-29 16:29:51',
            'updated_at'=>'2019-10-29 16:29:51',
            'deleted_at'=>NULL
        ] , [
            'id'=>58,
            'short_code'=>'send_password_reset_link',
            'en'=>'Send Password Reset Link',
            'de'=>'Send Password Reset Link',
            'created_at'=>'2019-10-29 16:31:14',
            'updated_at'=>'2019-10-29 16:31:14',
            'deleted_at'=>NULL
        ] , [
            'id'=>59,
            'short_code'=>'verify_your_email_address',
            'en'=>'Verify Your Email Address',
            'de'=>'Verify Your Email Address',
            'created_at'=>'2019-10-29 16:31:59',
            'updated_at'=>'2019-10-29 16:31:59',
            'deleted_at'=>NULL
        ] , [
            'id'=>60,
            'short_code'=>'fresh_verification_link_is_sent',
            'en'=>'A fresh verification link has been sent to your email address.',
            'de'=>'A fresh verification link has been sent to your email address.',
            'created_at'=>'2019-10-29 16:32:42',
            'updated_at'=>'2019-10-29 16:32:42',
            'deleted_at'=>NULL
        ] , [
            'id'=>61,
            'short_code'=>'before_proceeding',
            'en'=>'Before proceeding, please check your email for a verification link.',
            'de'=>'Before proceeding, please check your email for a verification link.',
            'created_at'=>'2019-10-29 16:33:15',
            'updated_at'=>'2019-10-29 16:33:15',
            'deleted_at'=>NULL
        ] , [
            'id'=>62,
            'short_code'=>'if_not_receive',
            'en'=>'If you did not receive the email',
            'de'=>'If you did not receive the email',
            'created_at'=>'2019-10-29 16:33:42',
            'updated_at'=>'2019-10-29 16:33:42',
            'deleted_at'=>NULL
        ] , [
            'id'=>63,
            'short_code'=>'click_to_request_again',
            'en'=>'click here to request another',
            'de'=>'click here to request another',
            'created_at'=>'2019-10-29 16:34:23',
            'updated_at'=>'2019-10-29 16:34:23',
            'deleted_at'=>NULL
        ] , [
            'id'=>64,
            'short_code'=>'registration',
            'en'=>'Registration',
            'de'=>'Registration',
            'created_at'=>'2019-10-29 16:35:55',
            'updated_at'=>'2019-10-29 16:35:55',
            'deleted_at'=>NULL
        ] , [
            'id'=>65,
            'short_code'=>'role',
            'en'=>'Role',
            'de'=>'Role',
            'created_at'=>'2019-10-29 16:36:50',
            'updated_at'=>'2019-10-29 16:36:50',
            'deleted_at'=>NULL
        ] , [
            'id'=>66,
            'short_code'=>'doctor',
            'en'=>'Doctor',
            'de'=>'Doctor',
            'created_at'=>'2019-10-29 16:37:13',
            'updated_at'=>'2019-10-29 16:37:13',
            'deleted_at'=>NULL
        ] , [
            'id'=>67,
            'short_code'=>'employee',
            'en'=>'Employee',
            'de'=>'Employee',
            'created_at'=>'2019-10-29 16:37:27',
            'updated_at'=>'2019-10-29 16:37:27',
            'deleted_at'=>NULL
        ] , [
            'id'=>68,
            'short_code'=>'register',
            'en'=>'Register',
            'de'=>'Register',
            'created_at'=>'2019-10-29 16:38:16',
            'updated_at'=>'2019-10-29 16:38:16',
            'deleted_at'=>NULL
        ] , [
            'id'=>69,
            'short_code'=>'username',
            'en'=>'Username',
            'de'=>'Username',
            'created_at'=>'2019-10-29 16:38:59',
            'updated_at'=>'2019-10-29 16:38:59',
            'deleted_at'=>NULL
        ] , [
            'id'=>70,
            'short_code'=>'forgot_password',
            'en'=>'Forgot password?',
            'de'=>'Forgot password?',
            'created_at'=>'2019-10-29 16:39:58',
            'updated_at'=>'2019-10-29 16:40:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>71,
            'short_code'=>'training_documents',
            'en'=>'Training documents',
            'de'=>'Training documents',
            'created_at'=>'2019-10-29 17:48:09',
            'updated_at'=>'2019-10-29 17:48:09',
            'deleted_at'=>NULL
        ] , [
            'id'=>72,
            'short_code'=>'hi',
            'en'=>'Hi',
            'de'=>'Hi',
            'created_at'=>'2019-10-29 17:48:47',
            'updated_at'=>'2019-10-29 17:48:47',
            'deleted_at'=>NULL
        ] , [
            'id'=>73,
            'short_code'=>'logout',
            'en'=>'Log Out',
            'de'=>'Log Out',
            'created_at'=>'2019-10-29 17:49:35',
            'updated_at'=>'2019-10-29 17:49:35',
            'deleted_at'=>NULL
        ] , [
            'id'=>74,
            'short_code'=>'my_profile',
            'en'=>'My Profile',
            'de'=>'My Profile',
            'created_at'=>'2019-10-29 17:49:56',
            'updated_at'=>'2019-10-29 17:49:56',
            'deleted_at'=>NULL
        ] , [
            'id'=>75,
            'short_code'=>'account_settings_and_more',
            'en'=>'Account settings and more',
            'de'=>'Account settings and more',
            'created_at'=>'2019-10-29 17:50:25',
            'updated_at'=>'2019-10-29 17:50:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>76,
            'short_code'=>'impressum',
            'en'=>'Impressum',
            'de'=>'Impressum',
            'created_at'=>'2019-10-29 17:51:13',
            'updated_at'=>'2019-10-29 17:51:13',
            'deleted_at'=>NULL
        ] , [
            'id'=>77,
            'short_code'=>'datenschutzerklarung',
            'en'=>'Datenschutzerklärung',
            'de'=>'Datenschutzerklärung',
            'created_at'=>'2019-10-29 17:51:33',
            'updated_at'=>'2019-10-29 17:51:33',
            'deleted_at'=>NULL
        ] , [
            'id'=>78,
            'short_code'=>'kontakt',
            'en'=>'Kontakt',
            'de'=>'Kontakt',
            'created_at'=>'2019-10-29 17:51:52',
            'updated_at'=>'2019-10-29 17:51:52',
            'deleted_at'=>NULL
        ] , [
            'id'=>79,
            'short_code'=>'login_text_1',
            'en'=>'Willkommen bei PERLEASE MPV',
            'de'=>'Willkommen bei PERLEASE MPV',
            'created_at'=>'2019-10-29 17:53:17',
            'updated_at'=>'2019-10-29 17:53:17',
            'deleted_at'=>NULL
        ] , [
            'id'=>80,
            'short_code'=>'login_text_2',
            'en'=>'Bitte melden Sie sich mit lhren Zugangsdaten an',
            'de'=>'Bitte melden Sie sich mit lhren Zugangsdaten an',
            'created_at'=>'2019-10-29 17:53:34',
            'updated_at'=>'2019-10-29 17:53:34',
            'deleted_at'=>NULL
        ] , [
            'id'=>81,
            'short_code'=>'account_email_password',
            'en'=>'Account email and password',
            'de'=>'Account email and password',
            'created_at'=>'2019-10-29 18:03:17',
            'updated_at'=>'2019-10-29 18:03:17',
            'deleted_at'=>NULL
        ] , [
            'id'=>82,
            'short_code'=>'no',
            'en'=>'No',
            'de'=>'No',
            'created_at'=>'2019-10-29 18:06:38',
            'updated_at'=>'2019-10-29 18:06:38',
            'deleted_at'=>NULL
        ] , [
            'id'=>83,
            'short_code'=>'yes',
            'en'=>'Yes',
            'de'=>'Yes',
            'created_at'=>'2019-10-29 18:06:58',
            'updated_at'=>'2019-10-29 18:06:58',
            'deleted_at'=>NULL
        ] , [
            'id'=>84,
            'short_code'=>'add_new_device_to_practice',
            'en'=>'Add new device to practice',
            'de'=>'Add new device to practice',
            'created_at'=>'2019-10-29 19:44:08',
            'updated_at'=>'2019-10-29 19:44:08',
            'deleted_at'=>NULL
        ] , [
            'id'=>85,
            'short_code'=>'pass_test',
            'en'=>'Pass test',
            'de'=>'Pass test',
            'created_at'=>'2019-10-29 19:45:16',
            'updated_at'=>'2019-10-29 19:45:16',
            'deleted_at'=>NULL
        ] , [
            'id'=>86,
            'short_code'=>'download_certificate',
            'en'=>'Download Certificate',
            'de'=>'Download Certificate',
            'created_at'=>'2019-10-29 19:45:40',
            'updated_at'=>'2019-10-29 19:45:40',
            'deleted_at'=>NULL
        ] , [
            'id'=>87,
            'short_code'=>'no_device_added',
            'en'=>'No device added',
            'de'=>'No device added',
            'created_at'=>'2019-10-29 19:46:12',
            'updated_at'=>'2019-10-29 19:46:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>88,
            'short_code'=>'place_in_practice',
            'en'=>'Place in practice',
            'de'=>'Place in practice',
            'created_at'=>'2019-10-29 19:47:12',
            'updated_at'=>'2019-10-29 19:47:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>89,
            'short_code'=>'maintenance_report_valid',
            'en'=>'Maintenance report valid',
            'de'=>'Maintenance report valid',
            'created_at'=>'2019-10-29 19:48:23',
            'updated_at'=>'2019-10-29 19:48:23',
            'deleted_at'=>NULL
        ] , [
            'id'=>90,
            'short_code'=>'maintenance_report_date',
            'en'=>'Maintenance report date',
            'de'=>'Maintenance report date',
            'created_at'=>'2019-10-29 19:48:57',
            'updated_at'=>'2019-10-29 19:48:57',
            'deleted_at'=>NULL
        ] , [
            'id'=>91,
            'short_code'=>'first_setup_date',
            'en'=>'First setup date',
            'de'=>'First setup date',
            'created_at'=>'2019-10-29 19:49:28',
            'updated_at'=>'2019-10-29 19:49:28',
            'deleted_at'=>NULL
        ] , [
            'id'=>92,
            'short_code'=>'setup_approval',
            'en'=>'Setup approval',
            'de'=>'Setup approval',
            'created_at'=>'2019-10-29 19:49:56',
            'updated_at'=>'2019-10-29 19:49:56',
            'deleted_at'=>NULL
        ] , [
            'id'=>93,
            'short_code'=>'manufacturing_year',
            'en'=>'Manufacturing year',
            'de'=>'Manufacturing year',
            'created_at'=>'2019-10-29 19:50:26',
            'updated_at'=>'2019-10-29 19:50:26',
            'deleted_at'=>NULL
        ] , [
            'id'=>94,
            'short_code'=>'last_validation_year',
            'en'=>'Last validation date',
            'de'=>'Last validation date',
            'created_at'=>'2019-10-29 19:50:55',
            'updated_at'=>'2019-10-29 19:50:55',
            'deleted_at'=>NULL
        ] , [
            'id'=>95,
            'short_code'=>'last_maintenance_date',
            'en'=>'Last maintenance date',
            'de'=>'Last maintenance date',
            'created_at'=>'2019-10-29 19:51:39',
            'updated_at'=>'2019-10-29 19:51:39',
            'deleted_at'=>NULL
        ] , [
            'id'=>96,
            'short_code'=>'remarks',
            'en'=>'Remarks',
            'de'=>'Remarks',
            'created_at'=>'2019-10-29 19:51:55',
            'updated_at'=>'2019-10-29 19:51:55',
            'deleted_at'=>NULL
        ] , [
            'id'=>97,
            'short_code'=>'select_manufacturer',
            'en'=>'Select Manufacturer',
            'de'=>'Select Manufacturer',
            'created_at'=>'2019-10-29 19:53:06',
            'updated_at'=>'2019-10-29 19:53:06',
            'deleted_at'=>NULL
        ] , [
            'id'=>98,
            'short_code'=>'select_type',
            'en'=>'Select Type',
            'de'=>'Select Type',
            'created_at'=>'2019-10-29 19:53:33',
            'updated_at'=>'2019-10-29 19:53:33',
            'deleted_at'=>NULL
        ] , [
            'id'=>99,
            'short_code'=>'select_device',
            'en'=>'Select Device',
            'de'=>'Select Device',
            'created_at'=>'2019-10-29 19:53:57',
            'updated_at'=>'2019-10-29 19:53:57',
            'deleted_at'=>NULL
        ] , [
            'id'=>100,
            'short_code'=>'create_new_device',
            'en'=>'Create new device',
            'de'=>'Create new device',
            'created_at'=>'2019-10-29 19:54:25',
            'updated_at'=>'2019-10-29 19:54:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>101,
            'short_code'=>'create_password',
            'en'=>'Create password',
            'de'=>'Create password',
            'created_at'=>'2019-10-29 21:39:25',
            'updated_at'=>'2019-10-29 21:39:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>102,
            'short_code'=>'export',
            'en'=>'Export',
            'de'=>'Export',
            'created_at'=>'2019-10-29 21:46:58',
            'updated_at'=>'2019-10-29 21:46:58',
            'deleted_at'=>NULL
        ] , [
            'id'=>103,
            'short_code'=>'list',
            'en'=>'list',
            'de'=>'list',
            'created_at'=>'2019-10-29 21:47:22',
            'updated_at'=>'2019-10-29 21:47:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>104,
            'short_code'=>'add_new',
            'en'=>'Add new',
            'de'=>'Add new',
            'created_at'=>'2019-10-29 21:47:49',
            'updated_at'=>'2019-10-29 21:47:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>105,
            'short_code'=>'more_details',
            'en'=>'More details',
            'de'=>'More details',
            'created_at'=>'2019-10-29 21:48:49',
            'updated_at'=>'2019-10-29 21:48:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>106,
            'short_code'=>'active',
            'en'=>'Active',
            'de'=>'Active',
            'created_at'=>'2019-10-29 21:49:27',
            'updated_at'=>'2019-10-29 21:49:27',
            'deleted_at'=>NULL
        ] , [
            'id'=>107,
            'short_code'=>'passive',
            'en'=>'Passive',
            'de'=>'Passive',
            'created_at'=>'2019-10-29 21:49:35',
            'updated_at'=>'2019-10-29 21:49:35',
            'deleted_at'=>NULL
        ] , [
            'id'=>108,
            'short_code'=>'show_details',
            'en'=>'Show Details',
            'de'=>'Show Details',
            'created_at'=>'2019-10-29 21:50:31',
            'updated_at'=>'2019-10-29 21:50:31',
            'deleted_at'=>NULL
        ] , [
            'id'=>109,
            'short_code'=>'last_login',
            'en'=>'Last login',
            'de'=>'Last login',
            'created_at'=>'2019-10-29 21:51:27',
            'updated_at'=>'2019-10-29 21:51:27',
            'deleted_at'=>NULL
        ] , [
            'id'=>110,
            'short_code'=>'not_login_yet',
            'en'=>'Not login yet',
            'de'=>'Not login yet',
            'created_at'=>'2019-10-29 21:51:54',
            'updated_at'=>'2019-10-29 21:51:54',
            'deleted_at'=>NULL
        ] , [
            'id'=>111,
            'short_code'=>'change_login_or_password',
            'en'=>'Change login or password.',
            'de'=>'Change login or password.',
            'created_at'=>'2019-10-29 21:55:40',
            'updated_at'=>'2019-10-29 21:55:40',
            'deleted_at'=>NULL
        ] , [
            'id'=>112,
            'short_code'=>'login_email',
            'en'=>'Login/Email',
            'de'=>'Login/Email',
            'created_at'=>'2019-10-29 21:56:03',
            'updated_at'=>'2019-10-29 21:56:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>113,
            'short_code'=>'repeat_password',
            'en'=>'Repeat password',
            'de'=>'Repeat password',
            'created_at'=>'2019-10-29 21:57:05',
            'updated_at'=>'2019-10-29 21:57:05',
            'deleted_at'=>NULL
        ] , [
            'id'=>114,
            'short_code'=>'export_list',
            'en'=>'Export List',
            'de'=>'Export List',
            'created_at'=>'2019-10-29 21:57:45',
            'updated_at'=>'2019-10-29 21:57:45',
            'deleted_at'=>NULL
        ] , [
            'id'=>115,
            'short_code'=>'add_new_type',
            'en'=>'Add new type',
            'de'=>'Add new type',
            'created_at'=>'2019-10-29 21:58:17',
            'updated_at'=>'2019-10-29 21:58:17',
            'deleted_at'=>NULL
        ] , [
            'id'=>116,
            'short_code'=>'no_types',
            'en'=>'No types',
            'de'=>'No types',
            'created_at'=>'2019-10-29 21:59:13',
            'updated_at'=>'2019-10-29 21:59:13',
            'deleted_at'=>NULL
        ] , [
            'id'=>117,
            'short_code'=>'add_translation',
            'en'=>'Add translation',
            'de'=>'Add translation',
            'created_at'=>'2019-10-29 22:00:34',
            'updated_at'=>'2019-10-29 22:00:34',
            'deleted_at'=>NULL
        ] , [
            'id'=>118,
            'short_code'=>'no_translations',
            'en'=>'No translations',
            'de'=>'No translations',
            'created_at'=>'2019-10-29 22:01:36',
            'updated_at'=>'2019-10-29 22:01:36',
            'deleted_at'=>NULL
        ] , [
            'id'=>119,
            'short_code'=>'custom_devices_to_be_reviewed',
            'en'=>'Custom Device to be reviewed',
            'de'=>'Custom Device to be reviewed',
            'created_at'=>'2019-10-29 22:02:39',
            'updated_at'=>'2019-10-29 22:02:39',
            'deleted_at'=>NULL
        ] , [
            'id'=>120,
            'short_code'=>'user',
            'en'=>'User',
            'de'=>'User',
            'created_at'=>'2019-10-29 22:03:07',
            'updated_at'=>'2019-10-29 22:03:07',
            'deleted_at'=>NULL
        ] , [
            'id'=>121,
            'short_code'=>'date',
            'en'=>'Date',
            'de'=>'Date',
            'created_at'=>'2019-10-29 22:03:22',
            'updated_at'=>'2019-10-29 22:03:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>122,
            'short_code'=>'see_all',
            'en'=>'See all',
            'de'=>'See all',
            'created_at'=>'2019-10-29 22:04:40',
            'updated_at'=>'2019-10-29 22:04:40',
            'deleted_at'=>NULL
        ] , [
            'id'=>123,
            'short_code'=>'no_device_to_show',
            'en'=>'No device to show',
            'de'=>'No device to show',
            'created_at'=>'2019-10-29 22:05:12',
            'updated_at'=>'2019-10-29 22:05:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>124,
            'short_code'=>'practice_devices_for_validation',
            'en'=>'Practice Devices for validation',
            'de'=>'Practice Devices for validation',
            'created_at'=>'2019-10-29 22:05:44',
            'updated_at'=>'2019-10-29 22:05:44',
            'deleted_at'=>NULL
        ] , [
            'id'=>125,
            'short_code'=>'valid_until',
            'en'=>'Valid Until',
            'de'=>'Valid Until',
            'created_at'=>'2019-10-29 22:06:37',
            'updated_at'=>'2019-10-29 22:06:37',
            'deleted_at'=>NULL
        ] , [
            'id'=>126,
            'short_code'=>'not_set',
            'en'=>'Not set',
            'de'=>'Not set',
            'created_at'=>'2019-10-29 22:07:05',
            'updated_at'=>'2019-10-29 22:07:05',
            'deleted_at'=>NULL
        ] , [
            'id'=>127,
            'short_code'=>'add_training',
            'en'=>'Add training',
            'de'=>'Add training',
            'created_at'=>'2019-10-29 22:09:13',
            'updated_at'=>'2019-10-29 22:09:13',
            'deleted_at'=>NULL
        ] , [
            'id'=>128,
            'short_code'=>'no_training_material',
            'en'=>'No training material',
            'de'=>'No training material',
            'created_at'=>'2019-10-29 22:10:41',
            'updated_at'=>'2019-10-29 22:10:41',
            'deleted_at'=>NULL
        ] , [
            'id'=>129,
            'short_code'=>'test_name',
            'en'=>'Test name',
            'de'=>'Test name',
            'created_at'=>'2019-10-29 22:11:38',
            'updated_at'=>'2019-10-29 22:11:38',
            'deleted_at'=>NULL
        ] , [
            'id'=>130,
            'short_code'=>'attach_document',
            'en'=>'Attach document',
            'de'=>'Attach document',
            'created_at'=>'2019-10-29 22:12:02',
            'updated_at'=>'2019-10-29 22:12:02',
            'deleted_at'=>NULL
        ] , [
            'id'=>131,
            'short_code'=>'choose_file',
            'en'=>'Choose file',
            'de'=>'Choose file',
            'created_at'=>'2019-10-29 22:12:22',
            'updated_at'=>'2019-10-29 22:12:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>132,
            'short_code'=>'attached_document',
            'en'=>'Attached document',
            'de'=>'Attached document',
            'created_at'=>'2019-10-29 22:12:52',
            'updated_at'=>'2019-10-29 22:12:52',
            'deleted_at'=>NULL
        ] , [
            'id'=>133,
            'short_code'=>'view_file',
            'en'=>'View file',
            'de'=>'View file',
            'created_at'=>'2019-10-29 22:13:13',
            'updated_at'=>'2019-10-29 22:13:13',
            'deleted_at'=>NULL
        ] , [
            'id'=>134,
            'short_code'=>'delete_file',
            'en'=>'Delete file',
            'de'=>'Delete file',
            'created_at'=>'2019-10-29 22:13:38',
            'updated_at'=>'2019-10-29 22:13:38',
            'deleted_at'=>NULL
        ] , [
            'id'=>135,
            'short_code'=>'value',
            'en'=>'Value',
            'de'=>'Value',
            'created_at'=>'2019-10-29 22:15:22',
            'updated_at'=>'2019-10-29 22:15:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>136,
            'short_code'=>'no_settings',
            'en'=>'No settings',
            'de'=>'No settings',
            'created_at'=>'2019-10-29 22:15:49',
            'updated_at'=>'2019-10-29 22:15:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>137,
            'short_code'=>'add_service',
            'en'=>'Add service',
            'de'=>'Add service',
            'created_at'=>'2019-10-29 22:16:26',
            'updated_at'=>'2019-10-29 22:16:26',
            'deleted_at'=>NULL
        ] , [
            'id'=>138,
            'short_code'=>'no_service',
            'en'=>'No services',
            'de'=>'No services',
            'created_at'=>'2019-10-29 22:17:30',
            'updated_at'=>'2019-10-29 22:17:30',
            'deleted_at'=>NULL
        ] , [
            'id'=>139,
            'short_code'=>'add_qualification',
            'en'=>'Add qualification',
            'de'=>'Add qualification',
            'created_at'=>'2019-10-29 22:18:52',
            'updated_at'=>'2019-10-29 22:18:52',
            'deleted_at'=>NULL
        ] , [
            'id'=>140,
            'short_code'=>'no_qualifications',
            'en'=>'No qualifications',
            'de'=>'No qualifications',
            'created_at'=>'2019-10-29 22:20:59',
            'updated_at'=>'2019-10-29 22:20:59',
            'deleted_at'=>NULL
        ] , [
            'id'=>141,
            'short_code'=>'add_practice',
            'en'=>'Add  practice',
            'de'=>'Add  practice',
            'created_at'=>'2019-10-29 22:21:16',
            'updated_at'=>'2019-10-29 22:21:16',
            'deleted_at'=>NULL
        ] , [
            'id'=>142,
            'short_code'=>'fachrichtung',
            'en'=>'Fachrichtung',
            'de'=>'Fachrichtung',
            'created_at'=>'2019-10-29 22:21:38',
            'updated_at'=>'2019-10-29 22:21:38',
            'deleted_at'=>NULL
        ] , [
            'id'=>143,
            'short_code'=>'stabe_and_hausnummer',
            'en'=>'Straße und Hausnummer',
            'de'=>'Straße und Hausnummer',
            'created_at'=>'2019-10-29 22:22:12',
            'updated_at'=>'2019-10-29 22:22:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>144,
            'short_code'=>'plz',
            'en'=>'PLZ',
            'de'=>'PLZ',
            'created_at'=>'2019-10-29 22:22:25',
            'updated_at'=>'2019-10-29 22:22:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>145,
            'short_code'=>'ort',
            'en'=>'ORT',
            'de'=>'ORT',
            'created_at'=>'2019-10-29 22:22:42',
            'updated_at'=>'2019-10-29 22:22:42',
            'deleted_at'=>NULL
        ] , [
            'id'=>146,
            'short_code'=>'region',
            'en'=>'Region',
            'de'=>'Region',
            'created_at'=>'2019-10-29 22:23:04',
            'updated_at'=>'2019-10-29 22:23:04',
            'deleted_at'=>NULL
        ] , [
            'id'=>147,
            'short_code'=>'standort',
            'en'=>'Standort',
            'de'=>'Standort',
            'created_at'=>'2019-10-29 22:24:16',
            'updated_at'=>'2019-10-29 22:24:16',
            'deleted_at'=>NULL
        ] , [
            'id'=>148,
            'short_code'=>'rechung_name',
            'en'=>'Rechnung Name',
            'de'=>'Rechnung Name',
            'created_at'=>'2019-10-29 22:24:26',
            'updated_at'=>'2019-10-29 22:24:26',
            'deleted_at'=>NULL
        ] , [
            'id'=>149,
            'short_code'=>'rechnung_strabe_and_hausnummer',
            'en'=>'Rechnung Straße und Hausnummer',
            'de'=>'Rechnung Straße und Hausnummer',
            'created_at'=>'2019-10-29 22:25:02',
            'updated_at'=>'2019-10-29 22:25:02',
            'deleted_at'=>NULL
        ] , [
            'id'=>150,
            'short_code'=>'rechnung_plz',
            'en'=>'Rechnung PLZ',
            'de'=>'Rechnung PLZ',
            'created_at'=>'2019-10-29 22:25:23',
            'updated_at'=>'2019-10-29 22:25:23',
            'deleted_at'=>NULL
        ] , [
            'id'=>151,
            'short_code'=>'rechnung_ort',
            'en'=>'Rechnung Ort',
            'de'=>'Rechnung Ort',
            'created_at'=>'2019-10-29 22:25:43',
            'updated_at'=>'2019-10-29 22:25:43',
            'deleted_at'=>NULL
        ] , [
            'id'=>152,
            'short_code'=>'tel_ruckfragen',
            'en'=>'Tel. bei Rückfragen',
            'de'=>'Tel. bei Rückfragen',
            'created_at'=>'2019-10-29 22:26:08',
            'updated_at'=>'2019-10-29 22:26:08',
            'deleted_at'=>NULL
        ] , [
            'id'=>153,
            'short_code'=>'no_practice',
            'en'=>'No practice',
            'de'=>'No practice',
            'created_at'=>'2019-10-29 22:27:14',
            'updated_at'=>'2019-10-29 22:27:14',
            'deleted_at'=>NULL
        ] , [
            'id'=>154,
            'short_code'=>'setup_practice',
            'en'=>'Setup Practice',
            'de'=>'Setup Practice',
            'created_at'=>'2019-10-29 22:27:55',
            'updated_at'=>'2019-10-29 22:27:55',
            'deleted_at'=>NULL
        ] , [
            'id'=>155,
            'short_code'=>'invoice',
            'en'=>'Invoice',
            'de'=>'Invoice',
            'created_at'=>'2019-10-29 22:28:12',
            'updated_at'=>'2019-10-29 22:28:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>156,
            'short_code'=>'review_and_submit',
            'en'=>'Review and submit',
            'de'=>'Review and submit',
            'created_at'=>'2019-10-29 22:28:35',
            'updated_at'=>'2019-10-29 22:28:35',
            'deleted_at'=>NULL
        ] , [
            'id'=>157,
            'short_code'=>'setup_you_location',
            'en'=>'Setup Your Current Location',
            'de'=>'Setup Your Current Location',
            'created_at'=>'2019-10-29 22:29:02',
            'updated_at'=>'2019-10-29 22:29:02',
            'deleted_at'=>NULL
        ] , [
            'id'=>158,
            'short_code'=>'street_and_number',
            'en'=>'Street and number',
            'de'=>'Street and number',
            'created_at'=>'2019-10-29 22:29:29',
            'updated_at'=>'2019-10-29 22:29:29',
            'deleted_at'=>NULL
        ] , [
            'id'=>159,
            'short_code'=>'practice_phone',
            'en'=>'Practice Phone number',
            'de'=>'Practice Phone number',
            'created_at'=>'2019-10-29 22:29:54',
            'updated_at'=>'2019-10-29 22:29:54',
            'deleted_at'=>NULL
        ] , [
            'id'=>160,
            'short_code'=>'postal_code',
            'en'=>'Postal code',
            'de'=>'Postal code',
            'created_at'=>'2019-10-29 22:30:21',
            'updated_at'=>'2019-10-29 22:30:21',
            'deleted_at'=>NULL
        ] , [
            'id'=>161,
            'short_code'=>'city',
            'en'=>'City',
            'de'=>'City',
            'created_at'=>'2019-10-29 22:30:31',
            'updated_at'=>'2019-10-29 22:30:31',
            'deleted_at'=>NULL
        ] , [
            'id'=>162,
            'short_code'=>'specialization',
            'en'=>'Specialization',
            'de'=>'Specialization',
            'created_at'=>'2019-10-29 22:30:49',
            'updated_at'=>'2019-10-29 22:30:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>163,
            'short_code'=>'site',
            'en'=>'Site',
            'de'=>'Site',
            'created_at'=>'2019-10-29 22:31:29',
            'updated_at'=>'2019-10-29 22:31:29',
            'deleted_at'=>NULL
        ] , [
            'id'=>164,
            'short_code'=>'practice_email',
            'en'=>'Practice email',
            'de'=>'Practice email',
            'created_at'=>'2019-10-29 22:31:44',
            'updated_at'=>'2019-10-29 22:31:44',
            'deleted_at'=>NULL
        ] , [
            'id'=>165,
            'short_code'=>'select_service',
            'en'=>'Select Services',
            'de'=>'Select Services',
            'created_at'=>'2019-10-29 22:32:27',
            'updated_at'=>'2019-10-29 22:32:27',
            'deleted_at'=>NULL
        ] , [
            'id'=>166,
            'short_code'=>'same_as_in_practice',
            'en'=>'Same as in practice ?',
            'de'=>'Same as in practice ?',
            'created_at'=>'2019-10-29 22:32:55',
            'updated_at'=>'2019-10-29 22:32:55',
            'deleted_at'=>NULL
        ] , [
            'id'=>167,
            'short_code'=>'invoice_name',
            'en'=>'Invoice name',
            'de'=>'Invoice name',
            'created_at'=>'2019-10-29 22:33:16',
            'updated_at'=>'2019-10-29 22:33:16',
            'deleted_at'=>NULL
        ] , [
            'id'=>168,
            'short_code'=>'invoice_street',
            'en'=>'Invoice street and number',
            'de'=>'Invoice street and number',
            'created_at'=>'2019-10-29 22:33:49',
            'updated_at'=>'2019-10-29 22:33:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>169,
            'short_code'=>'invoice_postal',
            'en'=>'Invoice postal code',
            'de'=>'Invoice postal code',
            'created_at'=>'2019-10-29 22:34:07',
            'updated_at'=>'2019-10-29 22:34:07',
            'deleted_at'=>NULL
        ] , [
            'id'=>170,
            'short_code'=>'invoice_city',
            'en'=>'Invoice city',
            'de'=>'Invoice city',
            'created_at'=>'2019-10-29 22:34:28',
            'updated_at'=>'2019-10-29 22:34:28',
            'deleted_at'=>NULL
        ] , [
            'id'=>171,
            'short_code'=>'previous',
            'en'=>'Previous',
            'de'=>'Previous',
            'created_at'=>'2019-10-29 22:36:56',
            'updated_at'=>'2019-10-29 22:36:56',
            'deleted_at'=>NULL
        ] , [
            'id'=>172,
            'short_code'=>'next_step',
            'en'=>'Next Step',
            'de'=>'Next Step',
            'created_at'=>'2019-10-29 22:37:17',
            'updated_at'=>'2019-10-29 22:37:17',
            'deleted_at'=>NULL
        ] , [
            'id'=>173,
            'short_code'=>'enter_invoice_data',
            'en'=>'Enter Your invoice data',
            'de'=>'Enter Your invoice data',
            'created_at'=>'2019-10-29 22:40:37',
            'updated_at'=>'2019-10-29 22:40:37',
            'deleted_at'=>NULL
        ] , [
            'id'=>175,
            'short_code'=>'next_validation_date',
            'en'=>'Next validation date',
            'de'=>'Next validation date',
            'created_at'=>'2019-10-29 22:48:26',
            'updated_at'=>'2019-10-29 22:48:26',
            'deleted_at'=>NULL
        ] , [
            'id'=>176,
            'short_code'=>'select_practice',
            'en'=>'Select practice',
            'de'=>'Select practice',
            'created_at'=>'2019-10-29 22:49:48',
            'updated_at'=>'2019-10-29 22:49:48',
            'deleted_at'=>NULL
        ] , [
            'id'=>177,
            'short_code'=>'add_manufacturer',
            'en'=>'Add new manufacturer',
            'de'=>'Add new manufacturer',
            'created_at'=>'2019-10-29 23:03:40',
            'updated_at'=>'2019-10-29 23:03:40',
            'deleted_at'=>NULL
        ] , [
            'id'=>178,
            'short_code'=>'no_manufacturer',
            'en'=>'No manufacturer',
            'de'=>'No manufacturer',
            'created_at'=>'2019-10-29 23:04:26',
            'updated_at'=>'2019-10-29 23:04:26',
            'deleted_at'=>NULL
        ] , [
            'id'=>179,
            'short_code'=>'add_employee',
            'en'=>'Add employee',
            'de'=>'Add employee',
            'created_at'=>'2019-10-29 23:05:45',
            'updated_at'=>'2019-10-29 23:05:45',
            'deleted_at'=>NULL
        ] , [
            'id'=>180,
            'short_code'=>'enter_details',
            'en'=>'Enter details',
            'de'=>'Enter details',
            'created_at'=>'2019-10-29 23:07:44',
            'updated_at'=>'2019-10-29 23:07:44',
            'deleted_at'=>NULL
        ] , [
            'id'=>181,
            'short_code'=>'question',
            'en'=>'Question',
            'de'=>'Question',
            'created_at'=>'2019-10-29 23:10:52',
            'updated_at'=>'2019-10-29 23:10:52',
            'deleted_at'=>NULL
        ] , [
            'id'=>182,
            'short_code'=>'answer',
            'en'=>'Answer',
            'de'=>'Answer',
            'created_at'=>'2019-10-29 23:11:09',
            'updated_at'=>'2019-10-29 23:11:09',
            'deleted_at'=>NULL
        ] , [
            'id'=>183,
            'short_code'=>'enter_answer',
            'en'=>'Enter answer',
            'de'=>'Enter answer',
            'created_at'=>'2019-10-29 23:11:29',
            'updated_at'=>'2019-10-29 23:11:29',
            'deleted_at'=>NULL
        ] , [
            'id'=>184,
            'short_code'=>'correct_answer',
            'en'=>'Correct answer',
            'de'=>'Correct answer',
            'created_at'=>'2019-10-29 23:13:44',
            'updated_at'=>'2019-10-29 23:13:44',
            'deleted_at'=>NULL
        ] , [
            'id'=>185,
            'short_code'=>'wrong_answer',
            'en'=>'Wrong answer',
            'de'=>'Wrong answer',
            'created_at'=>'2019-10-29 23:13:55',
            'updated_at'=>'2019-10-29 23:13:55',
            'deleted_at'=>NULL
        ] , [
            'id'=>186,
            'short_code'=>'no_questions',
            'en'=>'No questions',
            'de'=>'No questions',
            'created_at'=>'2019-10-29 23:14:57',
            'updated_at'=>'2019-10-29 23:14:57',
            'deleted_at'=>NULL
        ] , [
            'id'=>187,
            'short_code'=>'enter_test_name',
            'en'=>'Enter test name',
            'de'=>'Enter test name',
            'created_at'=>'2019-10-29 23:17:25',
            'updated_at'=>'2019-10-29 23:17:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>188,
            'short_code'=>'test_summary',
            'en'=>'Test summary',
            'de'=>'Test summary',
            'created_at'=>'2019-10-29 23:17:47',
            'updated_at'=>'2019-10-29 23:17:47',
            'deleted_at'=>NULL
        ] , [
            'id'=>189,
            'short_code'=>'youtube_video',
            'en'=>'Youtube video',
            'de'=>'Youtube video',
            'created_at'=>'2019-10-29 23:18:19',
            'updated_at'=>'2019-10-29 23:18:19',
            'deleted_at'=>NULL
        ] , [
            'id'=>190,
            'short_code'=>'vimeo_video',
            'en'=>'Vimeo video',
            'de'=>'Vimeo video',
            'created_at'=>'2019-10-29 23:18:58',
            'updated_at'=>'2019-10-29 23:18:58',
            'deleted_at'=>NULL
        ] , [
            'id'=>191,
            'short_code'=>'add_new_doctor',
            'en'=>'Add new doctor',
            'de'=>'Add new doctor',
            'created_at'=>'2019-10-29 23:20:23',
            'updated_at'=>'2019-10-29 23:20:23',
            'deleted_at'=>NULL
        ] , [
            'id'=>192,
            'short_code'=>'no_doctors',
            'en'=>'No doctors',
            'de'=>'No doctors',
            'created_at'=>'2019-10-29 23:21:37',
            'updated_at'=>'2019-10-29 23:21:37',
            'deleted_at'=>NULL
        ] , [
            'id'=>193,
            'short_code'=>'doctors_title',
            'en'=>'Doctors title',
            'de'=>'Doctors title',
            'created_at'=>'2019-10-29 23:22:28',
            'updated_at'=>'2019-10-29 23:22:28',
            'deleted_at'=>NULL
        ] , [
            'id'=>194,
            'short_code'=>'public',
            'en'=>'Public',
            'de'=>'Public',
            'created_at'=>'2019-10-29 23:27:10',
            'updated_at'=>'2019-10-29 23:27:10',
            'deleted_at'=>NULL
        ] , [
            'id'=>196,
            'short_code'=>'edit',
            'en'=>'Edit',
            'de'=>'Edit',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>197,
            'short_code'=>'delete',
            'en'=>'Delete',
            'de'=>'Delete',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>198,
            'short_code'=>'action',
            'en'=>'Action',
            'de'=>'Action',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>199,
            'short_code'=>'update',
            'en'=>'Update',
            'de'=>'Update',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>200,
            'short_code'=>'create',
            'en'=>'Create',
            'de'=>'Create',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>201,
            'short_code'=>'dashboard',
            'en'=>'Dashboard',
            'de'=>'Dashboard',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>203,
            'short_code'=>'employees',
            'en'=>'Employees',
            'de'=>'Employees',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>206,
            'short_code'=>'qualif',
            'en'=>'Qualifications',
            'de'=>'Qualifications',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>209,
            'short_code'=>'manufacturer',
            'en'=>'Manufacturer',
            'de'=>'Manufacturer',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>211,
            'short_code'=>'elearning',
            'en'=>'E-learning',
            'de'=>'E-learning',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>212,
            'short_code'=>'elearning_tests',
            'en'=>'E-learning tests',
            'de'=>'E-learning tests',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>213,
            'short_code'=>'elearning_materials',
            'en'=>'E-learning materials',
            'de'=>'E-learning materials',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>214,
            'short_code'=>'elearning_certificates',
            'en'=>'E-learning certificates',
            'de'=>'E-learning certificates',
            'created_at'=>'2019-10-31 19:04:12',
            'updated_at'=>'2019-10-31 19:04:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>215,
            'short_code'=>'edit_devices',
            'en'=>'Edit devices',
            'de'=>'Edit devices',
            'created_at'=>'2019-10-31 19:43:06',
            'updated_at'=>'2019-10-31 19:43:06',
            'deleted_at'=>NULL
        ] , [
            'id'=>216,
            'short_code'=>'dev_created',
            'en'=>'Device successfully created',
            'de'=>'Device successfully created',
            'created_at'=>'2019-10-31 19:44:23',
            'updated_at'=>'2019-10-31 19:44:23',
            'deleted_at'=>NULL
        ] , [
            'id'=>217,
            'short_code'=>'dev_updated',
            'en'=>'Device successfully updated',
            'de'=>'Device successfully updated',
            'created_at'=>'2019-10-31 19:47:53',
            'updated_at'=>'2019-10-31 19:47:53',
            'deleted_at'=>NULL
        ] , [
            'id'=>218,
            'short_code'=>'error',
            'en'=>'Error, try again',
            'de'=>'Error, try again',
            'created_at'=>'2019-10-31 19:48:42',
            'updated_at'=>'2019-10-31 19:48:42',
            'deleted_at'=>NULL
        ] , [
            'id'=>219,
            'short_code'=>'dev_deleted',
            'en'=>'Device successfully deleted',
            'de'=>'Device successfully deleted',
            'created_at'=>'2019-10-31 19:50:04',
            'updated_at'=>'2019-10-31 19:50:04',
            'deleted_at'=>NULL
        ] , [
            'id'=>220,
            'short_code'=>'dev_approved',
            'en'=>'Device is successfully approved',
            'de'=>'Device is successfully approved',
            'created_at'=>'2019-10-31 19:50:49',
            'updated_at'=>'2019-10-31 19:50:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>221,
            'short_code'=>'dev_val_error',
            'en'=>'Error! Please select device validation period!',
            'de'=>'Error! Please select device validation period!',
            'created_at'=>'2019-10-31 19:51:23',
            'updated_at'=>'2019-10-31 19:51:23',
            'deleted_at'=>NULL
        ] , [
            'id'=>222,
            'short_code'=>'pass_saved',
            'en'=>'Your password is saved.',
            'de'=>'Your password is saved.',
            'created_at'=>'2019-10-31 19:59:29',
            'updated_at'=>'2019-10-31 19:59:29',
            'deleted_at'=>NULL
        ] , [
            'id'=>223,
            'short_code'=>'quest_with_answer',
            'en'=>'Question with answers were successfully created',
            'de'=>'Question with answers were successfully created',
            'created_at'=>'2019-10-31 21:18:40',
            'updated_at'=>'2019-10-31 21:18:40',
            'deleted_at'=>NULL
        ] , [
            'id'=>224,
            'short_code'=>'edit_quest',
            'en'=>'Edit Question',
            'de'=>'Edit Question',
            'created_at'=>'2019-10-31 21:19:39',
            'updated_at'=>'2019-10-31 21:19:39',
            'deleted_at'=>NULL
        ] , [
            'id'=>225,
            'short_code'=>'quest_with_answer_update',
            'en'=>'Question with answers were successfully updateed',
            'de'=>'Question with answers were successfully updateed',
            'created_at'=>'2019-10-31 21:20:36',
            'updated_at'=>'2019-10-31 21:20:36',
            'deleted_at'=>NULL
        ] , [
            'id'=>226,
            'short_code'=>'elearning_test',
            'en'=>'Add E-learning Test',
            'de'=>'Add E-learning Test',
            'created_at'=>'2019-10-31 21:23:16',
            'updated_at'=>'2019-10-31 21:23:16',
            'deleted_at'=>NULL
        ] , [
            'id'=>227,
            'short_code'=>'test_created',
            'en'=>'Test successfully created',
            'de'=>'Test successfully created',
            'created_at'=>'2019-10-31 21:23:45',
            'updated_at'=>'2019-10-31 21:23:45',
            'deleted_at'=>NULL
        ] , [
            'id'=>228,
            'short_code'=>'edit_elaerning',
            'en'=>'Edit Elearning',
            'de'=>'Edit Elearning',
            'created_at'=>'2019-10-31 21:24:13',
            'updated_at'=>'2019-10-31 21:24:13',
            'deleted_at'=>NULL
        ] , [
            'id'=>229,
            'short_code'=>'test_update',
            'en'=>'Test successfully updated',
            'de'=>'Test successfully updated',
            'created_at'=>'2019-10-31 21:28:48',
            'updated_at'=>'2019-10-31 21:28:48',
            'deleted_at'=>NULL
        ] , [
            'id'=>231,
            'short_code'=>'file_deleted',
            'en'=>'File successfully deleted',
            'de'=>'File successfully deleted',
            'created_at'=>'2019-10-31 21:29:58',
            'updated_at'=>'2019-10-31 21:29:58',
            'deleted_at'=>NULL
        ] , [
            'id'=>232,
            'short_code'=>'dev_not_found',
            'en'=>'Device not found',
            'de'=>'Device not found',
            'created_at'=>'2019-10-31 21:36:11',
            'updated_at'=>'2019-10-31 21:36:11',
            'deleted_at'=>NULL
        ] , [
            'id'=>233,
            'short_code'=>'manufacturer_created',
            'en'=>'Manufacturer successfully created',
            'de'=>'Manufacturer successfully created',
            'created_at'=>'2019-10-31 21:39:54',
            'updated_at'=>'2019-10-31 21:39:54',
            'deleted_at'=>NULL
        ] , [
            'id'=>234,
            'short_code'=>'man_not_found',
            'en'=>'Manufacturer not found',
            'de'=>'Manufacturer not found',
            'created_at'=>'2019-10-31 21:41:03',
            'updated_at'=>'2019-10-31 21:41:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>235,
            'short_code'=>'edit_manuf',
            'en'=>'Edit Manufacturer',
            'de'=>'Edit Manufacturer',
            'created_at'=>'2019-10-31 21:41:47',
            'updated_at'=>'2019-10-31 21:41:47',
            'deleted_at'=>NULL
        ] , [
            'id'=>236,
            'short_code'=>'man_update',
            'en'=>'Manufacturer successfully updated',
            'de'=>'Manufacturer successfully updated',
            'created_at'=>'2019-10-31 21:42:36',
            'updated_at'=>'2019-10-31 21:42:36',
            'deleted_at'=>NULL
        ] , [
            'id'=>237,
            'short_code'=>'man_deleted',
            'en'=>'Manufacturer Deleted',
            'de'=>'Manufacturer Deleted',
            'created_at'=>'2019-10-31 21:43:26',
            'updated_at'=>'2019-10-31 21:43:26',
            'deleted_at'=>NULL
        ] , [
            'id'=>238,
            'short_code'=>'edit_setting',
            'en'=>'Edit Setting',
            'de'=>'Edit Setting',
            'created_at'=>'2019-10-31 22:28:24',
            'updated_at'=>'2019-10-31 22:28:24',
            'deleted_at'=>NULL
        ] , [
            'id'=>239,
            'short_code'=>'setting_update',
            'en'=>'Settings successfully updated',
            'de'=>'Settings successfully updated',
            'created_at'=>'2019-10-31 22:28:54',
            'updated_at'=>'2019-10-31 22:28:54',
            'deleted_at'=>NULL
        ] , [
            'id'=>240,
            'short_code'=>'setting_not_found',
            'en'=>'Setting not found',
            'de'=>'Setting not found',
            'created_at'=>'2019-10-31 22:29:36',
            'updated_at'=>'2019-10-31 22:29:36',
            'deleted_at'=>NULL
        ] , [
            'id'=>241,
            'short_code'=>'qual_succ_created',
            'en'=>'Qualification successfully created',
            'de'=>'Qualification successfully created',
            'created_at'=>'2019-10-31 22:31:38',
            'updated_at'=>'2019-10-31 22:31:38',
            'deleted_at'=>NULL
        ] , [
            'id'=>242,
            'short_code'=>'edit_qual',
            'en'=>'Edit qualification',
            'de'=>'Edit qualification',
            'created_at'=>'2019-10-31 22:32:46',
            'updated_at'=>'2019-10-31 22:32:46',
            'deleted_at'=>NULL
        ] , [
            'id'=>243,
            'short_code'=>'qual_succ_update',
            'en'=>'Qualification successfully updated',
            'de'=>'Qualification successfully updated',
            'created_at'=>'2019-10-31 22:33:32',
            'updated_at'=>'2019-10-31 22:33:32',
            'deleted_at'=>NULL
        ] , [
            'id'=>244,
            'short_code'=>'qual_del',
            'en'=>'Qualification Deleted',
            'de'=>'Qualification Deleted',
            'created_at'=>'2019-10-31 22:34:16',
            'updated_at'=>'2019-10-31 22:34:16',
            'deleted_at'=>NULL
        ] , [
            'id'=>245,
            'short_code'=>'service_succ_created',
            'en'=>'Service successfully created',
            'de'=>'Service successfully created',
            'created_at'=>'2019-10-31 22:37:21',
            'updated_at'=>'2019-10-31 22:37:21',
            'deleted_at'=>NULL
        ] , [
            'id'=>246,
            'short_code'=>'edit_serv',
            'en'=>'Edit Service',
            'de'=>'Edit Service',
            'created_at'=>'2019-10-31 22:38:04',
            'updated_at'=>'2019-10-31 22:38:04',
            'deleted_at'=>NULL
        ] , [
            'id'=>247,
            'short_code'=>'serv_succ_updated',
            'en'=>'Service successfully updated',
            'de'=>'Service successfully updated',
            'created_at'=>'2019-10-31 22:38:33',
            'updated_at'=>'2019-10-31 22:38:33',
            'deleted_at'=>NULL
        ] , [
            'id'=>248,
            'short_code'=>'serv_del',
            'en'=>'Service Deleted',
            'de'=>'Service Deleted',
            'created_at'=>'2019-10-31 22:38:57',
            'updated_at'=>'2019-10-31 22:38:57',
            'deleted_at'=>NULL
        ] , [
            'id'=>249,
            'short_code'=>'add_dev_type',
            'en'=>'Add Devices Types',
            'de'=>'Add Devices Types',
            'created_at'=>'2019-10-31 22:40:58',
            'updated_at'=>'2019-10-31 22:40:58',
            'deleted_at'=>NULL
        ] , [
            'id'=>250,
            'short_code'=>'type_succ_created',
            'en'=>'Type successfully created',
            'de'=>'Type successfully created',
            'created_at'=>'2019-10-31 22:41:26',
            'updated_at'=>'2019-10-31 22:41:26',
            'deleted_at'=>NULL
        ] , [
            'id'=>251,
            'short_code'=>'edit_dev_type',
            'en'=>'Edit Devices Types',
            'de'=>'Edit Devices Types',
            'created_at'=>'2019-10-31 22:42:02',
            'updated_at'=>'2019-10-31 22:42:02',
            'deleted_at'=>NULL
        ] , [
            'id'=>252,
            'short_code'=>'dev_type_succ_update',
            'en'=>'Devices Type successfully updated',
            'de'=>'Devices Type successfully updated',
            'created_at'=>'2019-10-31 22:42:51',
            'updated_at'=>'2019-10-31 22:42:51',
            'deleted_at'=>NULL
        ] , [
            'id'=>253,
            'short_code'=>'dev_type_del',
            'en'=>'Devices Type Deleted',
            'de'=>'Devices Type Deleted',
            'created_at'=>'2019-10-31 22:43:17',
            'updated_at'=>'2019-10-31 22:43:17',
            'deleted_at'=>NULL
        ] , [
            'id'=>254,
            'short_code'=>'dev_type_not',
            'en'=>'Device Type not found',
            'de'=>'Device Type not found',
            'created_at'=>'2019-10-31 22:44:02',
            'updated_at'=>'2019-10-31 22:44:02',
            'deleted_at'=>NULL
        ] , [
            'id'=>255,
            'short_code'=>'training',
            'en'=>'Training',
            'de'=>'Training',
            'created_at'=>'2019-10-31 22:45:20',
            'updated_at'=>'2019-10-31 22:45:20',
            'deleted_at'=>NULL
        ] , [
            'id'=>256,
            'short_code'=>'add_training_mat',
            'en'=>'Add Training Materials',
            'de'=>'Add Training Materials',
            'created_at'=>'2019-10-31 22:46:12',
            'updated_at'=>'2019-10-31 22:46:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>257,
            'short_code'=>'training_succ_created',
            'en'=>'Training successfully created',
            'de'=>'Training successfully created',
            'created_at'=>'2019-10-31 22:46:47',
            'updated_at'=>'2019-10-31 22:46:47',
            'deleted_at'=>NULL
        ] , [
            'id'=>258,
            'short_code'=>'edit_train',
            'en'=>'Edit Training',
            'de'=>'Edit Training',
            'created_at'=>'2019-10-31 22:47:23',
            'updated_at'=>'2019-10-31 22:47:23',
            'deleted_at'=>NULL
        ] , [
            'id'=>259,
            'short_code'=>'train_succ_updated',
            'en'=>'Training successfully updated',
            'de'=>'Training successfully updated',
            'created_at'=>'2019-10-31 22:47:55',
            'updated_at'=>'2019-10-31 22:47:55',
            'deleted_at'=>NULL
        ] , [
            'id'=>260,
            'short_code'=>'train_succ_del',
            'en'=>'Training successfully deleted',
            'de'=>'Training successfully deleted',
            'created_at'=>'2019-10-31 22:48:22',
            'updated_at'=>'2019-10-31 22:48:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>261,
            'short_code'=>'pract_succ_created',
            'en'=>'Practice successfully created',
            'de'=>'Practice successfully created',
            'created_at'=>'2019-10-31 22:52:35',
            'updated_at'=>'2019-10-31 22:52:35',
            'deleted_at'=>NULL
        ] , [
            'id'=>262,
            'short_code'=>'edit_practice',
            'en'=>'Edit Practice',
            'de'=>'Edit Practice',
            'created_at'=>'2019-10-31 22:53:08',
            'updated_at'=>'2019-10-31 22:53:08',
            'deleted_at'=>NULL
        ] , [
            'id'=>263,
            'short_code'=>'pract_succ_updated',
            'en'=>'Practice successfully updated',
            'de'=>'Practice successfully updated',
            'created_at'=>'2019-10-31 22:53:40',
            'updated_at'=>'2019-10-31 22:53:40',
            'deleted_at'=>NULL
        ] , [
            'id'=>264,
            'short_code'=>'pract_succ_del',
            'en'=>'Practice successfully deleted',
            'de'=>'Practice successfully deleted',
            'created_at'=>'2019-10-31 22:54:07',
            'updated_at'=>'2019-10-31 22:54:07',
            'deleted_at'=>NULL
        ] , [
            'id'=>265,
            'short_code'=>'pr_device',
            'en'=>'Practice Devices',
            'de'=>'Practice Devices',
            'created_at'=>'2019-10-31 22:56:18',
            'updated_at'=>'2019-10-31 22:56:18',
            'deleted_at'=>NULL
        ] , [
            'id'=>266,
            'short_code'=>'add_pr_div',
            'en'=>'Add Practice Devices',
            'de'=>'Add Practice Devices',
            'created_at'=>'2019-10-31 22:57:08',
            'updated_at'=>'2019-10-31 22:57:08',
            'deleted_at'=>NULL
        ] , [
            'id'=>267,
            'short_code'=>'dev_add_to_prac',
            'en'=>'Device successfully added to Practice',
            'de'=>'Device successfully added to Practice',
            'created_at'=>'2019-10-31 22:57:42',
            'updated_at'=>'2019-10-31 22:57:42',
            'deleted_at'=>NULL
        ] , [
            'id'=>268,
            'short_code'=>'edit_pr_dev',
            'en'=>'Edit Practive Device',
            'de'=>'Edit Practive Device',
            'created_at'=>'2019-10-31 22:58:09',
            'updated_at'=>'2019-10-31 22:58:09',
            'deleted_at'=>NULL
        ] , [
            'id'=>269,
            'short_code'=>'pr_dev_succ_upd',
            'en'=>'Practice Device successfully updated',
            'de'=>'Practice Device successfully updated',
            'created_at'=>'2019-10-31 22:58:55',
            'updated_at'=>'2019-10-31 22:58:55',
            'deleted_at'=>NULL
        ] , [
            'id'=>270,
            'short_code'=>'pr_dev_succ_del',
            'en'=>'Practice Device successfully deleted',
            'de'=>'Practice Device successfully deleted',
            'created_at'=>'2019-10-31 22:59:22',
            'updated_at'=>'2019-10-31 22:59:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>271,
            'short_code'=>'dev_succ_appr',
            'en'=>'Device is successfully approved',
            'de'=>'Device is successfully approved',
            'created_at'=>'2019-10-31 22:59:59',
            'updated_at'=>'2019-10-31 22:59:59',
            'deleted_at'=>NULL
        ] , [
            'id'=>272,
            'short_code'=>'pr_dev_not_found',
            'en'=>'Practice device not found',
            'de'=>'Practice device not found',
            'created_at'=>'2019-10-31 23:01:46',
            'updated_at'=>'2019-10-31 23:01:46',
            'deleted_at'=>NULL
        ] , [
            'id'=>273,
            'short_code'=>'doc_succ_created',
            'en'=>'Doctor successfully created',
            'de'=>'Doctor successfully created',
            'created_at'=>'2019-10-31 23:07:40',
            'updated_at'=>'2019-10-31 23:07:40',
            'deleted_at'=>NULL
        ] , [
            'id'=>274,
            'short_code'=>'edit_doc',
            'en'=>'Edit doctor',
            'de'=>'Edit doctor',
            'created_at'=>'2019-10-31 23:08:03',
            'updated_at'=>'2019-10-31 23:08:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>275,
            'short_code'=>'doc_not_found',
            'en'=>'Doctor not found.',
            'de'=>'Doctor not found.',
            'created_at'=>'2019-10-31 23:09:22',
            'updated_at'=>'2019-10-31 23:09:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>276,
            'short_code'=>'succ_del',
            'en'=>'Successfully deleted',
            'de'=>'Successfully deleted',
            'created_at'=>'2019-10-31 23:11:38',
            'updated_at'=>'2019-10-31 23:11:38',
            'deleted_at'=>NULL
        ] , [
            'id'=>277,
            'short_code'=>'not_found',
            'en'=>'Not found',
            'de'=>'Not found',
            'created_at'=>'2019-10-31 23:12:20',
            'updated_at'=>'2019-10-31 23:12:20',
            'deleted_at'=>NULL
        ] , [
            'id'=>278,
            'short_code'=>'doc_succ_updated',
            'en'=>'Doctor successfully updated',
            'de'=>'Doctor successfully updated',
            'created_at'=>'2019-10-31 23:13:53',
            'updated_at'=>'2019-10-31 23:13:53',
            'deleted_at'=>NULL
        ] , [
            'id'=>279,
            'short_code'=>'empl_succ_created',
            'en'=>'Employee successfully created',
            'de'=>'Employee successfully created',
            'created_at'=>'2019-10-31 23:16:15',
            'updated_at'=>'2019-10-31 23:16:15',
            'deleted_at'=>NULL
        ] , [
            'id'=>280,
            'short_code'=>'edit_empl',
            'en'=>'Edit employee',
            'de'=>'Edit employee',
            'created_at'=>'2019-10-31 23:16:39',
            'updated_at'=>'2019-10-31 23:16:39',
            'deleted_at'=>NULL
        ] , [
            'id'=>281,
            'short_code'=>'empl_not_found',
            'en'=>'Employee not found.',
            'de'=>'Employee not found.',
            'created_at'=>'2019-10-31 23:17:22',
            'updated_at'=>'2019-10-31 23:17:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>282,
            'short_code'=>'empl_succ_updated',
            'en'=>'Employee successfully updated',
            'de'=>'Employee successfully updated',
            'created_at'=>'2019-10-31 23:17:51',
            'updated_at'=>'2019-10-31 23:17:51',
            'deleted_at'=>NULL
        ] , [
            'id'=>283,
            'short_code'=>'pass_succ_updated',
            'en'=>'Password successfully updated',
            'de'=>'Password successfully updated',
            'created_at'=>'2019-10-31 23:19:56',
            'updated_at'=>'2019-10-31 23:19:56',
            'deleted_at'=>NULL
        ] , [
            'id'=>284,
            'short_code'=>'old_pass_wrong',
            'en'=>'Error, old password is wrong!',
            'de'=>'Error, old password is wrong!',
            'created_at'=>'2019-10-31 23:20:27',
            'updated_at'=>'2019-10-31 23:20:27',
            'deleted_at'=>NULL
        ] , [
            'id'=>285,
            'short_code'=>'trans_succ_del',
            'en'=>'Translation successfully deleted',
            'de'=>'Translation successfully deleted',
            'created_at'=>'2019-11-01 19:59:17',
            'updated_at'=>'2019-11-01 19:59:17',
            'deleted_at'=>NULL
        ] , [
            'id'=>286,
            'short_code'=>'trans_not_found',
            'en'=>'Translation not found',
            'de'=>'Translation not found',
            'created_at'=>'2019-11-01 19:59:46',
            'updated_at'=>'2019-11-01 19:59:46',
            'deleted_at'=>NULL
        ] , [
            'id'=>287,
            'short_code'=>'trnas_succ_upd',
            'en'=>'Translation successfully updated',
            'de'=>'Translation successfully updated',
            'created_at'=>'2019-11-01 20:00:18',
            'updated_at'=>'2019-11-01 20:00:18',
            'deleted_at'=>NULL
        ] , [
            'id'=>288,
            'short_code'=>'edit_trans',
            'en'=>'Edit Translation',
            'de'=>'Edit Translation',
            'created_at'=>'2019-11-01 20:00:41',
            'updated_at'=>'2019-11-01 20:00:41',
            'deleted_at'=>NULL
        ] , [
            'id'=>289,
            'short_code'=>'trans_succ_create',
            'en'=>'Translation successfully created',
            'de'=>'Translation successfully created',
            'created_at'=>'2019-11-01 20:01:19',
            'updated_at'=>'2019-11-01 20:01:19',
            'deleted_at'=>NULL
        ] , [
            'id'=>290,
            'short_code'=>'created',
            'en'=>'Created',
            'de'=>'Created',
            'created_at'=>'2019-11-01 20:05:34',
            'updated_at'=>'2019-11-01 20:05:34',
            'deleted_at'=>NULL
        ] , [
            'id'=>291,
            'short_code'=>'updated',
            'en'=>'Updated',
            'de'=>'Updated',
            'created_at'=>'2019-11-01 20:06:11',
            'updated_at'=>'2019-11-01 20:06:11',
            'deleted_at'=>NULL
        ] , [
            'id'=>292,
            'short_code'=>'employees_list',
            'en'=>'Employees list',
            'de'=>'Employees list',
            'created_at'=>'2019-11-02 00:48:16',
            'updated_at'=>'2019-11-02 00:48:16',
            'deleted_at'=>NULL
        ] , [
            'id'=>293,
            'short_code'=>'doc_list',
            'en'=>'Doctors list',
            'de'=>'Doctors list',
            'created_at'=>'2019-11-02 00:49:42',
            'updated_at'=>'2019-11-02 00:49:42',
            'deleted_at'=>NULL
        ] , [
            'id'=>294,
            'short_code'=>'doctors',
            'en'=>'Doctors',
            'de'=>'Doctors',
            'created_at'=>'2019-11-02 00:51:11',
            'updated_at'=>'2019-11-02 00:51:11',
            'deleted_at'=>NULL
        ] , [
            'id'=>295,
            'short_code'=>'user_succ_create',
            'en'=>'User successfully created',
            'de'=>'User successfully created',
            'created_at'=>'2019-11-02 00:52:04',
            'updated_at'=>'2019-11-02 00:52:04',
            'deleted_at'=>NULL
        ] , [
            'id'=>296,
            'short_code'=>'profile',
            'en'=>'Profile',
            'de'=>'Profile',
            'created_at'=>'2019-11-02 00:52:29',
            'updated_at'=>'2019-11-02 00:52:29',
            'deleted_at'=>NULL
        ] , [
            'id'=>297,
            'short_code'=>'user_succ_update',
            'en'=>'User was successfully updated',
            'de'=>'User was successfully updated',
            'created_at'=>'2019-11-02 00:53:03',
            'updated_at'=>'2019-11-02 00:53:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>298,
            'short_code'=>'pass_succ_change',
            'en'=>'Password was successfully changed',
            'de'=>'Password was successfully changed',
            'created_at'=>'2019-11-02 00:53:29',
            'updated_at'=>'2019-11-02 00:53:29',
            'deleted_at'=>NULL
        ] , [
            'id'=>299,
            'short_code'=>'old_pass_incor',
            'en'=>'Old password was incorrect',
            'de'=>'Old password was incorrect',
            'created_at'=>'2019-11-02 00:54:07',
            'updated_at'=>'2019-11-02 00:54:07',
            'deleted_at'=>NULL
        ] , [
            'id'=>300,
            'short_code'=>'use_succ_del',
            'en'=>'User was successfully deleted',
            'de'=>'User was successfully deleted',
            'created_at'=>'2019-11-02 00:54:54',
            'updated_at'=>'2019-11-02 00:54:54',
            'deleted_at'=>NULL
        ] , [
            'id'=>301,
            'short_code'=>'qual_not_found',
            'en'=>'Qualification not found',
            'de'=>'Qualification not found',
            'created_at'=>'2019-11-02 01:01:36',
            'updated_at'=>'2019-11-02 01:01:36',
            'deleted_at'=>NULL
        ] , [
            'id'=>302,
            'short_code'=>'summary',
            'en'=>'Summary',
            'de'=>'Summary',
            'created_at'=>'2019-11-03 06:41:42',
            'updated_at'=>'2019-11-03 06:41:42',
            'deleted_at'=>NULL
        ] , [
            'id'=>303,
            'short_code'=>'no_test',
            'en'=>'No Tests Yet',
            'de'=>'No Tests Yet',
            'created_at'=>'2019-11-03 06:46:25',
            'updated_at'=>'2019-11-03 06:46:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>304,
            'short_code'=>'add_new_test',
            'en'=>'Add new Test',
            'de'=>'Add new Test',
            'created_at'=>'2019-11-03 06:47:35',
            'updated_at'=>'2019-11-03 06:47:35',
            'deleted_at'=>NULL
        ] , [
            'id'=>305,
            'short_code'=>'del_tes',
            'en'=>'Delete Test',
            'de'=>'Delete Test',
            'created_at'=>'2019-11-03 06:49:35',
            'updated_at'=>'2019-11-03 06:49:35',
            'deleted_at'=>NULL
        ] , [
            'id'=>306,
            'short_code'=>'questions',
            'en'=>'Questions',
            'de'=>'Questions',
            'created_at'=>'2019-11-03 06:50:47',
            'updated_at'=>'2019-11-03 06:50:47',
            'deleted_at'=>NULL
        ] , [
            'id'=>307,
            'short_code'=>'pass_not_ent',
            'en'=>'Password not entered',
            'de'=>'Password not entered',
            'created_at'=>'2019-11-03 07:05:49',
            'updated_at'=>'2019-11-03 07:05:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>308,
            'short_code'=>'strong_pass',
            'en'=>'Strong Password Tips',
            'de'=>'Strong Password Tips',
            'created_at'=>'2019-11-03 07:06:11',
            'updated_at'=>'2019-11-03 07:06:11',
            'deleted_at'=>NULL
        ] , [
            'id'=>309,
            'short_code'=>'min_six',
            'en'=>'At least 6 characters',
            'de'=>'At least 6 characters',
            'created_at'=>'2019-11-03 07:06:35',
            'updated_at'=>'2019-11-03 07:06:35',
            'deleted_at'=>NULL
        ] , [
            'id'=>310,
            'short_code'=>'one_num',
            'en'=>'At least one number',
            'de'=>'At least one number',
            'created_at'=>'2019-11-03 07:06:55',
            'updated_at'=>'2019-11-03 07:06:55',
            'deleted_at'=>NULL
        ] , [
            'id'=>311,
            'short_code'=>'one_low_one_up',
            'en'=>'At least one lowercase & one uppercase letter',
            'de'=>'At least one lowercase & one uppercase letter',
            'created_at'=>'2019-11-03 07:07:36',
            'updated_at'=>'2019-11-03 07:08:14',
            'deleted_at'=>NULL
        ] , [
            'id'=>312,
            'short_code'=>'min_one_char',
            'en'=>'At least one special character',
            'de'=>'At least one special character',
            'created_at'=>'2019-11-03 07:08:44',
            'updated_at'=>'2019-11-03 07:08:44',
            'deleted_at'=>NULL
        ] , [
            'id'=>313,
            'short_code'=>'confirm',
            'en'=>'Confirm',
            'de'=>'Confirm',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>314,
            'short_code'=>'certificates',
            'en'=>'Certificates',
            'de'=>'Certificates',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>315,
            'short_code'=>'score',
            'en'=>'Score',
            'de'=>'Score',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>316,
            'short_code'=>'continue',
            'en'=>'Continue',
            'de'=>'Continue',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>317,
            'short_code'=>'download_attached_file',
            'en'=>'Download attached file',
            'de'=>'Download attached file',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>318,
            'short_code'=>'no_file_attached',
            'en'=>'No file attached',
            'de'=>'No file attached',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>319,
            'short_code'=>'no_elearnings',
            'en'=>'No elearnings',
            'de'=>'No elearnings',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>320,
            'short_code'=>'well_done',
            'en'=>'Well done!',
            'de'=>'Well done!',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>321,
            'short_code'=>'true_ans',
            'en'=>'True answers',
            'de'=>'True answers',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>322,
            'short_code'=>'wrong_ans',
            'en'=>'Wrong answers',
            'de'=>'Wrong answers',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>323,
            'short_code'=>'can_download',
            'en'=>'Aww yeah, Now you can download certificate',
            'de'=>'Aww yeah, Now you can download certificate',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>324,
            'short_code'=>'got_issues',
            'en'=>'Got Issues',
            'de'=>'Got Issues',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>325,
            'short_code'=>'important_alert',
            'en'=>'Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.',
            'de'=>'Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>326,
            'short_code'=>'use_utilities',
            'en'=>'Whenever you need to, be sure to use margin utilities to keep things nice and tidy.',
            'de'=>'Whenever you need to, be sure to use margin utilities to keep things nice and tidy.',
            'created_at'=>'2019-11-03 07:09:03',
            'updated_at'=>'2019-11-03 07:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>327,
            'short_code'=>'generate_report',
            'en'=>'Generate report',
            'de'=>'Generate report',
            'created_at'=>'2019-11-06 10:30:58',
            'updated_at'=>'2019-11-06 10:30:58',
            'deleted_at'=>NULL
        ] , [
            'id'=>328,
            'short_code'=>'chose_practice_device',
            'en'=>'Chose practice device',
            'de'=>'Chose practice device',
            'created_at'=>'2019-11-06 10:33:40',
            'updated_at'=>'2019-11-06 10:33:40',
            'deleted_at'=>NULL
        ] , [
            'id'=>329,
            'short_code'=>'trans_home_desc',
            'en'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',
            'de'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',
            'created_at'=>'2019-11-06 13:30:49',
            'updated_at'=>'2019-11-06 13:30:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>330,
            'short_code'=>'trans_edit_desc',
            'en'=>'Translation edit desc',
            'de'=>'Translation edit desc',
            'created_at'=>'2019-11-06 13:50:04',
            'updated_at'=>'2019-11-12 12:19:01',
            'deleted_at'=>NULL
        ] , [
            'id'=>331,
            'short_code'=>'trans_create_desc',
            'en'=>'Translation create desc',
            'de'=>'Translation create desc',
            'created_at'=>'2019-11-06 13:50:22',
            'updated_at'=>'2019-11-06 13:50:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>332,
            'short_code'=>'changelog_desc',
            'en'=>'Changelog desc',
            'de'=>'Changelog desc',
            'created_at'=>'2019-11-06 13:52:35',
            'updated_at'=>'2019-11-06 13:52:35',
            'deleted_at'=>NULL
        ] , [
            'id'=>333,
            'short_code'=>'admin_creat_desc',
            'en'=>'Admin create desc',
            'de'=>'Admin create desc',
            'created_at'=>'2019-11-06 13:52:52',
            'updated_at'=>'2019-11-12 12:24:07',
            'deleted_at'=>NULL
        ] , [
            'id'=>334,
            'short_code'=>'admin_edit_desc',
            'en'=>'Admin edit desc',
            'de'=>'Admin edit desc',
            'created_at'=>'2019-11-06 13:53:03',
            'updated_at'=>'2019-11-12 12:13:37',
            'deleted_at'=>NULL
        ] , [
            'id'=>335,
            'short_code'=>'admin_home_desc',
            'en'=>'Admin home desc',
            'de'=>'Admin home desc',
            'created_at'=>'2019-11-06 13:53:14',
            'updated_at'=>'2019-11-12 12:04:45',
            'deleted_at'=>NULL
        ] , [
            'id'=>336,
            'short_code'=>'device_home_desc',
            'en'=>'Device home desc',
            'de'=>'Device home desc',
            'created_at'=>'2019-11-06 13:54:56',
            'updated_at'=>'2019-11-12 12:05:05',
            'deleted_at'=>NULL
        ] , [
            'id'=>337,
            'short_code'=>'users_home_desc',
            'en'=>'Users home desc',
            'de'=>'Users home desc',
            'created_at'=>'2019-11-06 13:56:11',
            'updated_at'=>'2019-11-12 12:11:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>338,
            'short_code'=>'settings_home_desc',
            'en'=>'Settings home desc',
            'de'=>'Settings home desc',
            'created_at'=>'2019-11-06 13:56:41',
            'updated_at'=>'2019-11-12 12:10:33',
            'deleted_at'=>NULL
        ] , [
            'id'=>339,
            'short_code'=>'trainings_home_desc',
            'en'=>'Trainings home desc',
            'de'=>'Trainings home desc',
            'created_at'=>'2019-11-06 13:57:39',
            'updated_at'=>'2019-11-12 12:10:43',
            'deleted_at'=>NULL
        ] , [
            'id'=>340,
            'short_code'=>'services_home_desc',
            'en'=>'Services home desc',
            'de'=>'Services home desc',
            'created_at'=>'2019-11-06 13:58:13',
            'updated_at'=>'2019-11-12 12:10:24',
            'deleted_at'=>NULL
        ] , [
            'id'=>341,
            'short_code'=>'qualifications_home_desc',
            'en'=>'Qualifications home desc',
            'de'=>'Qualifications home desc',
            'created_at'=>'2019-11-06 13:58:37',
            'updated_at'=>'2019-11-12 12:10:04',
            'deleted_at'=>NULL
        ] , [
            'id'=>342,
            'short_code'=>'practices_home_desc',
            'en'=>'Practices home desc',
            'de'=>'Practices home desc',
            'created_at'=>'2019-11-06 13:58:54',
            'updated_at'=>'2019-11-12 12:09:54',
            'deleted_at'=>NULL
        ] , [
            'id'=>343,
            'short_code'=>'practice_devices_home_desc',
            'en'=>'Practice devices home desc',
            'de'=>'Practice devices home desc',
            'created_at'=>'2019-11-06 13:59:14',
            'updated_at'=>'2019-11-12 12:09:15',
            'deleted_at'=>NULL
        ] , [
            'id'=>344,
            'short_code'=>'manufacturers_home_desc',
            'en'=>'Manufacturers home desc',
            'de'=>'Manufacturers home desc',
            'created_at'=>'2019-11-06 13:59:38',
            'updated_at'=>'2019-11-12 12:09:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>345,
            'short_code'=>'types_home_desc',
            'en'=>'Types home desc',
            'de'=>'Types home desc',
            'created_at'=>'2019-11-06 14:00:07',
            'updated_at'=>'2019-11-12 12:11:12',
            'deleted_at'=>NULL
        ] , [
            'id'=>346,
            'short_code'=>'employees_home_desc',
            'en'=>'Employees home desc',
            'de'=>'Employees home desc',
            'created_at'=>'2019-11-06 14:00:34',
            'updated_at'=>'2019-11-12 12:08:43',
            'deleted_at'=>NULL
        ] , [
            'id'=>347,
            'short_code'=>'home_desc',
            'en'=>'Home desc',
            'de'=>'Home desc',
            'created_at'=>'2019-11-06 14:01:23',
            'updated_at'=>'2019-11-12 12:08:52',
            'deleted_at'=>NULL
        ] , [
            'id'=>348,
            'short_code'=>'eleraning_quest_home_desc',
            'en'=>'Eleraning quest home desc',
            'de'=>'Eleraning quest home desc',
            'created_at'=>'2019-11-06 14:02:13',
            'updated_at'=>'2019-11-12 12:07:40',
            'deleted_at'=>NULL
        ] , [
            'id'=>349,
            'short_code'=>'eleraning_home_desc',
            'en'=>'Eleraning home desc',
            'de'=>'Eleraning home desc',
            'created_at'=>'2019-11-06 14:02:33',
            'updated_at'=>'2019-11-12 12:07:29',
            'deleted_at'=>NULL
        ] , [
            'id'=>350,
            'short_code'=>'doctors_home_desc',
            'en'=>'Doctors home desc',
            'de'=>'Doctors home desc',
            'created_at'=>'2019-11-06 14:02:49',
            'updated_at'=>'2019-11-12 12:07:19',
            'deleted_at'=>NULL
        ] , [
            'id'=>351,
            'short_code'=>'employees_creat_desc',
            'en'=>'Employees create desc',
            'de'=>'Employees create desc',
            'created_at'=>'2019-11-06 14:06:50',
            'updated_at'=>'2019-11-12 12:23:43',
            'deleted_at'=>NULL
        ] , [
            'id'=>352,
            'short_code'=>'manufacturers_creat_desc',
            'en'=>'Manufacturers create desc',
            'de'=>'Manufacturers create desc',
            'created_at'=>'2019-11-06 14:07:11',
            'updated_at'=>'2019-11-12 12:26:13',
            'deleted_at'=>NULL
        ] , [
            'id'=>353,
            'short_code'=>'practice_devices_creat_desc',
            'en'=>'Practice devices create desc',
            'de'=>'Practice devices create desc',
            'created_at'=>'2019-11-06 14:07:32',
            'updated_at'=>'2019-11-12 12:25:51',
            'deleted_at'=>NULL
        ] , [
            'id'=>354,
            'short_code'=>'practice_creat_desc',
            'en'=>'Practice create desc',
            'de'=>'Practice create desc',
            'created_at'=>'2019-11-06 14:07:57',
            'updated_at'=>'2019-11-12 12:26:02',
            'deleted_at'=>NULL
        ] , [
            'id'=>355,
            'short_code'=>'qualifications_creat_desc',
            'en'=>'Qualifications create desc',
            'de'=>'Qualifications create desc',
            'created_at'=>'2019-11-06 14:08:23',
            'updated_at'=>'2019-11-12 12:25:34',
            'deleted_at'=>NULL
        ] , [
            'id'=>356,
            'short_code'=>'services_creat_desc',
            'en'=>'Services create desc',
            'de'=>'Services create desc',
            'created_at'=>'2019-11-06 14:08:41',
            'updated_at'=>'2019-11-12 12:25:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>357,
            'short_code'=>'training_creat_desc',
            'en'=>'Training create desc',
            'de'=>'Training create desc',
            'created_at'=>'2019-11-06 14:08:55',
            'updated_at'=>'2019-11-12 12:25:10',
            'deleted_at'=>NULL
        ] , [
            'id'=>358,
            'short_code'=>'types_creat_desc',
            'en'=>'Types create desc',
            'de'=>'Types create desc',
            'created_at'=>'2019-11-06 14:09:09',
            'updated_at'=>'2019-11-12 12:24:51',
            'deleted_at'=>NULL
        ] , [
            'id'=>359,
            'short_code'=>'users_creat_desc',
            'en'=>'Users create desc',
            'de'=>'Users create desc',
            'created_at'=>'2019-11-06 14:09:34',
            'updated_at'=>'2019-11-12 12:24:40',
            'deleted_at'=>NULL
        ] , [
            'id'=>360,
            'short_code'=>'elearning_creat_desc',
            'en'=>'Elearning create desc',
            'de'=>'Elearning create desc',
            'created_at'=>'2019-11-06 14:10:40',
            'updated_at'=>'2019-11-12 12:24:19',
            'deleted_at'=>NULL
        ] , [
            'id'=>361,
            'short_code'=>'elearning_questions_creat_desc',
            'en'=>'Elearning questions create desc',
            'de'=>'Elearning questions create desc',
            'created_at'=>'2019-11-06 14:10:59',
            'updated_at'=>'2019-11-12 12:24:30',
            'deleted_at'=>NULL
        ] , [
            'id'=>362,
            'short_code'=>'doctors_creat_desc',
            'en'=>'Doctors create desc',
            'de'=>'Doctors create desc',
            'created_at'=>'2019-11-06 14:11:15',
            'updated_at'=>'2019-11-12 12:22:44',
            'deleted_at'=>NULL
        ] , [
            'id'=>363,
            'short_code'=>'doctors_edit_desc',
            'en'=>'Doctors edit desc',
            'de'=>'Doctors edit desc',
            'created_at'=>'2019-11-06 14:13:18',
            'updated_at'=>'2019-11-12 12:16:47',
            'deleted_at'=>NULL
        ] , [
            'id'=>364,
            'short_code'=>'elernings_edit_desc',
            'en'=>'Elernings edit desc',
            'de'=>'Elernings edit desc',
            'created_at'=>'2019-11-06 14:13:37',
            'updated_at'=>'2019-11-12 12:16:56',
            'deleted_at'=>NULL
        ] , [
            'id'=>365,
            'short_code'=>'elernings_questions_edit_desc',
            'en'=>'Elernings questions edit desc',
            'de'=>'Elernings questions edit desc',
            'created_at'=>'2019-11-06 14:14:00',
            'updated_at'=>'2019-11-12 12:17:08',
            'deleted_at'=>NULL
        ] , [
            'id'=>366,
            'short_code'=>'employees_edit_desc',
            'en'=>'Employees edit desc',
            'de'=>'Employees edit desc',
            'created_at'=>'2019-11-06 14:14:18',
            'updated_at'=>'2019-11-12 12:17:44',
            'deleted_at'=>NULL
        ] , [
            'id'=>367,
            'short_code'=>'manufacturers_edit_desc',
            'en'=>'Manufacturers edit desc',
            'de'=>'Manufacturers edit desc',
            'created_at'=>'2019-11-06 14:14:42',
            'updated_at'=>'2019-11-12 12:17:55',
            'deleted_at'=>NULL
        ] , [
            'id'=>368,
            'short_code'=>'practice_device_edit_desc',
            'en'=>'Practice device edit desc',
            'de'=>'Practice device edit desc',
            'created_at'=>'2019-11-06 14:14:59',
            'updated_at'=>'2019-11-12 12:18:06',
            'deleted_at'=>NULL
        ] , [
            'id'=>369,
            'short_code'=>'users_edit_desc',
            'en'=>'Users edit desc',
            'de'=>'Users edit desc',
            'created_at'=>'2019-11-06 14:16:28',
            'updated_at'=>'2019-11-12 12:19:27',
            'deleted_at'=>NULL
        ] , [
            'id'=>370,
            'short_code'=>'types_edit_desc',
            'en'=>'Types edit desc',
            'de'=>'Types edit desc',
            'created_at'=>'2019-11-06 14:16:56',
            'updated_at'=>'2019-11-12 12:19:14',
            'deleted_at'=>NULL
        ] , [
            'id'=>371,
            'short_code'=>'training_edit_desc',
            'en'=>'Training edit desc',
            'de'=>'Training edit desc',
            'created_at'=>'2019-11-06 14:17:15',
            'updated_at'=>'2019-11-12 12:18:44',
            'deleted_at'=>NULL
        ] , [
            'id'=>372,
            'short_code'=>'services_edit_desc',
            'en'=>'Services edit desc',
            'de'=>'Services edit desc',
            'created_at'=>'2019-11-06 14:17:33',
            'updated_at'=>'2019-11-12 12:18:34',
            'deleted_at'=>NULL
        ] , [
            'id'=>373,
            'short_code'=>'qualifications_edit_desc',
            'en'=>'Qualifications edit desc',
            'de'=>'Qualifications edit desc',
            'created_at'=>'2019-11-06 14:17:54',
            'updated_at'=>'2019-11-12 12:18:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>374,
            'short_code'=>'practices_edit_desc',
            'en'=>'Practices edit desc',
            'de'=>'Practices edit desc',
            'created_at'=>'2019-11-06 14:18:14',
            'updated_at'=>'2019-11-12 12:18:16',
            'deleted_at'=>NULL
        ] , [
            'id'=>375,
            'short_code'=>'change_login_desc',
            'en'=>'Change login desc',
            'de'=>'Change login desc',
            'created_at'=>'2019-11-06 14:19:05',
            'updated_at'=>'2019-11-06 14:19:05',
            'deleted_at'=>NULL
        ] , [
            'id'=>376,
            'short_code'=>'create_pass_desc',
            'en'=>'Create pass desc',
            'de'=>'Create pass desc',
            'created_at'=>'2019-11-06 14:19:28',
            'updated_at'=>'2019-11-06 14:19:28',
            'deleted_at'=>NULL
        ] , [
            'id'=>377,
            'short_code'=>'doc_home_desc',
            'en'=>'Doctor home desc',
            'de'=>'Doctor home desc',
            'created_at'=>'2019-11-06 14:34:02',
            'updated_at'=>'2019-11-12 12:06:30',
            'deleted_at'=>NULL
        ] , [
            'id'=>378,
            'short_code'=>'doc_elearning_home_desc',
            'en'=>'Doctor elearning home desc',
            'de'=>'Doctor elearning home desc',
            'created_at'=>'2019-11-06 14:36:14',
            'updated_at'=>'2019-11-12 12:05:25',
            'deleted_at'=>NULL
        ] , [
            'id'=>379,
            'short_code'=>'doc_elearning_questions_home_desc',
            'en'=>'Doctor elearning questions home desc',
            'de'=>'Doctor elearning questions home desc',
            'created_at'=>'2019-11-06 14:36:38',
            'updated_at'=>'2019-11-12 12:05:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>380,
            'short_code'=>'doc_employees_home_desc',
            'en'=>'Doctor employees home desc',
            'de'=>'Doctor employees home desc',
            'created_at'=>'2019-11-06 14:37:01',
            'updated_at'=>'2019-11-12 12:06:11',
            'deleted_at'=>NULL
        ] , [
            'id'=>381,
            'short_code'=>'doc_practice_devices_home_desc',
            'en'=>'Doctor practice devices home desc',
            'de'=>'Doctor practice devices home desc',
            'created_at'=>'2019-11-06 14:37:28',
            'updated_at'=>'2019-11-12 12:06:51',
            'deleted_at'=>NULL
        ] , [
            'id'=>382,
            'short_code'=>'doc_training_home_desc',
            'en'=>'Doctor training home desc',
            'de'=>'Doctor training home desc',
            'created_at'=>'2019-11-06 14:37:47',
            'updated_at'=>'2019-11-12 12:07:10',
            'deleted_at'=>NULL
        ] , [
            'id'=>383,
            'short_code'=>'doc_elerning_creat_desc',
            'en'=>'Doctor elerning create desc',
            'de'=>'Doctor elerning create desc',
            'created_at'=>'2019-11-06 14:39:27',
            'updated_at'=>'2019-11-12 12:24:02',
            'deleted_at'=>NULL
        ] , [
            'id'=>384,
            'short_code'=>'doc_training_creat_desc',
            'en'=>'Doctor training create desc',
            'de'=>'Doctor training create desc',
            'created_at'=>'2019-11-06 14:39:52',
            'updated_at'=>'2019-11-12 12:22:19',
            'deleted_at'=>NULL
        ] , [
            'id'=>385,
            'short_code'=>'doc_practice_devices_creat_desc',
            'en'=>'Doctor practice devices create desc',
            'de'=>'Doctor practice devices create desc',
            'created_at'=>'2019-11-06 14:40:22',
            'updated_at'=>'2019-11-12 12:21:50',
            'deleted_at'=>NULL
        ] , [
            'id'=>386,
            'short_code'=>'doc_employees_creat_desc',
            'en'=>'Doctor employees create desc',
            'de'=>'Doctor employees create desc',
            'created_at'=>'2019-11-06 14:40:41',
            'updated_at'=>'2019-11-12 12:23:48',
            'deleted_at'=>NULL
        ] , [
            'id'=>387,
            'short_code'=>'doc_elerning_questions_creat_desc',
            'en'=>'Doctor elerning questions create desc',
            'de'=>'Doctor elerning questions create desc',
            'created_at'=>'2019-11-06 14:41:01',
            'updated_at'=>'2019-11-12 12:23:54',
            'deleted_at'=>NULL
        ] , [
            'id'=>388,
            'short_code'=>'doc_test_creat_desc',
            'en'=>'Doctor test create desc',
            'de'=>'Doctor test create desc',
            'created_at'=>'2019-11-06 14:41:49',
            'updated_at'=>'2019-11-12 12:22:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>389,
            'short_code'=>'doc_report_desc',
            'en'=>'Doctor report desc',
            'de'=>'Doctor report desc',
            'created_at'=>'2019-11-06 14:42:39',
            'updated_at'=>'2019-11-06 14:42:39',
            'deleted_at'=>NULL
        ] , [
            'id'=>390,
            'short_code'=>'doc_profile_edit_desc',
            'en'=>'Doctor profile edit desc',
            'de'=>'Doctor profile edit desc',
            'created_at'=>'2019-11-06 14:43:10',
            'updated_at'=>'2019-11-12 12:16:24',
            'deleted_at'=>NULL
        ] , [
            'id'=>391,
            'short_code'=>'doc_elearning_edit_desc',
            'en'=>'Doctor elearning edit desc',
            'de'=>'Doctor elearning edit desc',
            'created_at'=>'2019-11-06 14:44:22',
            'updated_at'=>'2019-11-12 12:14:03',
            'deleted_at'=>NULL
        ] , [
            'id'=>392,
            'short_code'=>'doc_elearning_questions_edit_desc',
            'en'=>'Doctor elearning questions edit desc',
            'de'=>'Doctor elearning questions edit desc',
            'created_at'=>'2019-11-06 14:44:52',
            'updated_at'=>'2019-11-12 12:14:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>393,
            'short_code'=>'doc_employee_edit_desc',
            'en'=>'Doctor employee edit desc',
            'de'=>'Doctor employee edit desc',
            'created_at'=>'2019-11-06 14:45:10',
            'updated_at'=>'2019-11-12 12:14:35',
            'deleted_at'=>NULL
        ] , [
            'id'=>394,
            'short_code'=>'doc_practice_devices_edit_desc',
            'en'=>'Doctor practice devices edit desc',
            'de'=>'Doctor practice devices edit desc',
            'created_at'=>'2019-11-06 14:45:34',
            'updated_at'=>'2019-11-12 12:14:48',
            'deleted_at'=>NULL
        ] , [
            'id'=>395,
            'short_code'=>'doc_training_edit_desc',
            'en'=>'Doctor training edit desc',
            'de'=>'Doctor training edit desc',
            'created_at'=>'2019-11-06 14:45:48',
            'updated_at'=>'2019-11-12 12:16:37',
            'deleted_at'=>NULL
        ] , [
            'id'=>396,
            'short_code'=>'employee_home_desc',
            'en'=>'Employee home desc',
            'de'=>'Employee home desc',
            'created_at'=>'2019-11-06 14:47:45',
            'updated_at'=>'2019-11-12 12:07:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>397,
            'short_code'=>'employee_create_pass_desc',
            'en'=>'Employee create pass desc',
            'de'=>'Employee create pass desc',
            'created_at'=>'2019-11-06 14:48:27',
            'updated_at'=>'2019-11-06 14:48:27',
            'deleted_at'=>NULL
        ] , [
            'id'=>398,
            'short_code'=>'employee_training_home_desc',
            'en'=>'Employee training home desc',
            'de'=>'Employee training home desc',
            'created_at'=>'2019-11-06 14:48:53',
            'updated_at'=>'2019-11-12 12:08:22',
            'deleted_at'=>NULL
        ] , [
            'id'=>399,
            'short_code'=>'employee_profile_edit_desc',
            'en'=>'Employee profile edit desc',
            'de'=>'Employee profile edit desc',
            'created_at'=>'2019-11-06 14:49:22',
            'updated_at'=>'2019-11-12 12:17:37',
            'deleted_at'=>NULL
        ] , [
            'id'=>400,
            'short_code'=>'employee_pr_dev_edit_desc',
            'en'=>'Employee practice device edit desc',
            'de'=>'Employee practice device edit desc',
            'created_at'=>'2019-11-06 14:49:59',
            'updated_at'=>'2019-11-12 12:17:26',
            'deleted_at'=>NULL
        ] , [
            'id'=>401,
            'short_code'=>'employee_pr_dev_home_desc',
            'en'=>'Employee practice device home desc',
            'de'=>'Employee practice device home desc',
            'created_at'=>'2019-11-06 14:50:40',
            'updated_at'=>'2019-11-12 12:08:11',
            'deleted_at'=>NULL
        ] , [
            'id'=>402,
            'short_code'=>'employee_elearning_desc',
            'en'=>'Employee elearning desc',
            'de'=>'Employee elearning desc',
            'created_at'=>'2019-11-06 14:51:03',
            'updated_at'=>'2019-11-12 12:39:24',
            'deleted_at'=>NULL
        ] , [
            'id'=>403,
            'short_code'=>'employee_pr_dev_create_desc',
            'en'=>'Employee practice device create desc',
            'de'=>'Employee practice device create desc',
            'created_at'=>'2019-11-06 14:51:46',
            'updated_at'=>'2019-11-12 12:41:08',
            'deleted_at'=>NULL
        ] , [
            'id'=>404,
            'short_code'=>'employee_after_test_desc',
            'en'=>'Employee after test desc',
            'de'=>'Employee after test desc',
            'created_at'=>'2019-11-06 14:57:45',
            'updated_at'=>'2019-11-06 14:57:45',
            'deleted_at'=>NULL
        ] , [
            'id'=>405,
            'short_code'=>'employee_single_elearning_desc',
            'en'=>'Employee single elearning desc',
            'de'=>'Employee single elearning desc',
            'created_at'=>'2019-11-06 14:58:12',
            'updated_at'=>'2019-11-12 12:40:44',
            'deleted_at'=>NULL
        ] , [
            'id'=>406,
            'short_code'=>'employee_elearning_test_desc',
            'en'=>'Employee elearning test desc',
            'de'=>'Employee elearning test desc',
            'created_at'=>'2019-11-06 14:58:31',
            'updated_at'=>'2019-11-06 14:58:31',
            'deleted_at'=>NULL
        ] , [
            'id'=>407,
            'short_code'=>'generate_report_desc',
            'en'=>'Generate report desc',
            'de'=>'Generate report desc',
            'created_at'=>'2019-11-06 15:00:58',
            'updated_at'=>'2019-11-06 15:00:58',
            'deleted_at'=>NULL
        ] , [
            'id'=>408,
            'short_code'=>'reports_home_desc',
            'en'=>'Reports home desc',
            'de'=>'Reports home desc',
            'created_at'=>'2019-11-06 15:00:58',
            'updated_at'=>'2019-11-12 12:10:15',
            'deleted_at'=>NULL
        ] , [
            'id'=>409,
            'short_code'=>'device_create_desc',
            'en'=>'Device create desc',
            'de'=>'Device create desc',
            'created_at'=>'2019-11-06 15:00:58',
            'updated_at'=>'2019-11-12 12:31:32',
            'deleted_at'=>NULL
        ] , [
            'id'=>410,
            'short_code'=>'device_edit_desc',
            'en'=>'Device edit desc',
            'de'=>'Device edit desc',
            'created_at'=>'2019-11-06 15:00:58',
            'updated_at'=>'2019-11-12 12:13:49',
            'deleted_at'=>NULL
        ] , [
            'id'=>411,
            'short_code'=>'no_attachment',
            'en'=>'No attachment',
            'de'=>'No attachment',
            'created_at'=>'2019-11-12 12:32:07',
            'updated_at'=>'2019-11-12 12:32:07',
            'deleted_at'=>NULL
        ]  ,
             ]);

    }
}
