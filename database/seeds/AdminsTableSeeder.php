<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'Superadmin',
             'role_id' => 1,
             'title' => 'admin',
             'region_id' => null,
             'last_name' => 'admin',
             'phone' => '123456789',
             'email' => 'admin@palatix.com',
             'password' => Hash::make('123456'),
                'created_at' => now(),
                'updated_at' => now()],
        ]);
    }
}
