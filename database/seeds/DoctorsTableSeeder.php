<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DoctorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'=>3,
            'practice_id'=>1,
            'role_id'=>2,
            'title'=>'Test Doctor',
            'name'=>'Dr. John',
            'last_name'=>'Doe',
            'phone'=>'+9741258932',
            'email'=>'docotr@palatix.com',
            'password' => Hash::make('123456'),
            'doctor_id'=>0,
            'region_id' => null,
            'qualification_id'=>1,
            'status'=>1,
            'email_verified_at'=>'2019-10-18 18:00:00',
            'remember_token'=>NULL,
            'created_at'=>'2019-10-19 03:11:40',
            'updated_at'=>'2019-10-19 03:11:40',
            'deleted_at'=>NULL
        ] );
    }
}
