<?php

use Illuminate\Database\Seeder;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('devices')->insert([
            ['manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'B Futura',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'B Classic',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'S Classic 17',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'S Classic 22',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Millseal Rolling',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Millseal+Evo',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Millseal+Manual',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Tethys H10',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Tethys D60',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Tethys T60',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Tethys T45',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Vacuklav 41 B+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Vacuklav 43 B+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Vacuklav 40 B+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Vacuklav 44 B+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Vacuklav 31 B+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Vacuklav 23 B+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Vacuklav 30 B+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Vacuklav 24 B+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Vacuklav 24 BL+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Euroklav 23 VS+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Euroklav 29 VS+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'MELAtronic 23 EN',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'MELAtronic 15 EN+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'MELAtronic 23',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>2,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'MELAseal Pro',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>2,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'MELAseal 200',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>2,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'MELAseal 100+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>2,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Melatherm 10 Evolution',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>2,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Melatherm 10',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>3,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Quadro',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>3,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Getinge K3+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>3,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Getinge K5+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>3,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Getinge K7+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>3,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Getinge HS 100',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>3,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Claro WD 15',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>3,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Tablo WD 14',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>4,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'E10 B-Autoklav',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>4,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'E9 NEXT Klasse-B',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>4,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'E8 B-Autoklav',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>4,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'EXL B-Autoklav',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>4,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Euroseal Valida',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>4,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Euromatic Plus',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>4,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Euroseal',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>4,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Euroseal 2001 PL',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>4,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Eurosafe 60',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'CUBE PST 1710',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'CUBE PST 2210',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'CUBE X PST 1720',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'CUBE X PST 2220',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8536 [AD LFM SST]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8536 [AD OXI/ORTHOVARIO SST]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8536 [AD SST]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8582 CD [WW AD CM Set LAN]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8582 CD [WW AD CM Set WLAN]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8582 [WW AD LD Set LAN]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8582 [WW AD LD Set WLAN]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8582 [WW AD PD Set LAN]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8582 [WW AD PD Set WLAN]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8592 [WW AD CM Set LAN DOS]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8592 [WW AD CM Set WLAN DOS]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8592 [WW AD Set LAN DOS]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8592 [WW AD Set WLAN DOS]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PWD 8531 [WS DSN IMS DWC]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8581 [AD LD Set LAN]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8581 [AD LD Set WLAN]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8591 [WW AD Set LAN DOS]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PG 8591 [WW AD Set WLAN DOS]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'PWD 8532 [WS DSN IMS DWC]',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'G 7882 CD',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>5,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'G 7881',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>6,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'BRAVO 17',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>6,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'BRAVO 17V',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>6,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'BRAVO 21V',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>6,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'HYDRIM C61 WD G4',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>6,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'HYDRIM M2 G4',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>7,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Lina 17',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>7,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Lina 22',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>7,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Lara 17',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>7,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Lara 22',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>7,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Lisa 17',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>7,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Lisa 22',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>7,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Seal²',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>7,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Teon',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>7,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Teon +',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>8,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'Autoklav TT+ 23',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>1,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'RS200 Pro+',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>8,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'HD 450 Basic PRO',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>8,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'HD 450 Injection PRO',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>9,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'DAC Premium',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>9,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'DAC Premium Plus',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>9,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'DAC Professional',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>9,
            'type_id'=>1,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'DAC Professional Plus',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>9,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'SiroSeal Premium',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>9,
            'type_id'=>2,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'SiroSeal Professional',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>9,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'DAC Universal.',
            'created_at'=> now(),
            'updated_at'=> now()
        ], [
            'manufacturer_id'=>9,
            'type_id'=>3,
            'user_id'=>1,
            'val_duration'=>1,
            'name'=>'DAC Universal.Neu',
            'created_at'=> now(),
            'updated_at'=> now()]
        ] );
    }
}
