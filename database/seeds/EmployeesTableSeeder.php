<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'=>2,
            'practice_id'=>1,
            'role_id'=>3,
            'title'=>'Test Employee',
            'name'=>'John',
            'last_name'=>'Doe',
            'phone'=>'+9745896541',
            'email'=>'employee@palatix.com',
            'password' => Hash::make('123456'),
            'doctor_id'=>1,
            'region_id' => null,
            'qualification_id'=>1,
            'status'=>1,
            'email_verified_at'=>'2019-10-18 21:00:00',
            'remember_token'=>NULL,
            'created_at'=>'2019-10-19 06:11:40',
            'updated_at'=>'2019-10-19 06:11:40',
            'deleted_at'=>NULL
        ] );
    }
}
