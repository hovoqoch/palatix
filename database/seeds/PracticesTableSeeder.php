<?php

use Illuminate\Database\Seeder;

class PracticesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('practices')->insert([
            ['id' => '1',
                'name' => 'Practice 1',
                'specialization' => 'Specialization 1',
                'street_number' => 'Street 1',
                'postal_code' => '123456',
                'phone' => '123456',
                'practice_email' => 'email1@email.com',
                'city' => 'City 1',
                'region_id' => '5',
                'site' => 'site 1',
                'invoice_name' => 'invoice name 1',
                'invoice_street' => 'invoice street 1',
                'invoice_postal' => '123456',
                'invoice_city' => 'City 1',

                'created_at' => now(),
                'updated_at' => now()],

            ['id' => '2',
                'name' => 'Practice 2',
                'specialization' => 'Specialization 2',
                'street_number' => 'Street 2',
                'postal_code' => '112233',
                'phone' => '123456',
                'practice_email' => 'email2@email.com',
                'city' => 'City 2',
                'region_id' => '1',
                'site' => 'site 2',
                'invoice_name' => 'invoice name 2',
                'invoice_street' => 'invoice street 2',
                'invoice_postal' => '112233',
                'invoice_city' => 'City 2',

                'created_at' => now(),
                'updated_at' => now()],

            ['id' => '3',
                'name' => 'Practice 3',
                'specialization' => 'Specialization 3',
                'street_number' => 'Street 3',
                'postal_code' => '445566',
                'phone' => '123456',
                'practice_email' => 'email3@email.com',
                'city' => 'City 3',
                'region_id' => '2',
                'site' => 'site 3',
                'invoice_name' => 'invoice name 3',
                'invoice_street' => 'invoice street 3',
                'invoice_postal' => '445566',
                'invoice_city' => 'City 3',

                'created_at' => now(),
                'updated_at' => now()],
        ]);

    }
}
