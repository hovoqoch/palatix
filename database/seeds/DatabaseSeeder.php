<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RolesTableSeeder::class);
         $this->call(RegionsTableSeeder::class);
         $this->call(QualificationsTableSeeder::class);
         $this->call(SettingsTableSeeder::class);
         $this->call(AdminsTableSeeder::class);
         $this->call(TranslationsTableSeeder::class);
         $this->call(TypesTableSeeder::class);
         $this->call(ManufacturersTableSeeder::class);
         $this->call(EmployeesTableSeeder::class);
         $this->call(DoctorsTableSeeder::class);
         $this->call(DevicesTableSeeder::class);
         $this->call(PracticesTableSeeder::class);
    }
}
