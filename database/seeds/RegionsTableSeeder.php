<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([
            ['name' => 'Baden-Württemberg',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Bayern',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Berlin',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Brandenburg',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Bremen',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Hamburg',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Hessen',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Mecklenburg-Vorpommern',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Niedersachsen',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Nordrhein-Westfalen',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Rheinland-Pfalz',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Saarland',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Sachsen-Anhalt',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Sachsen',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Schleswig-Holstein',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Thüringen',
                'created_at' => now(),
                'updated_at' => now()],
        ]);
    }
}
