<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            ['name' => 'Registration email body',
             'key' => 'reg_email_body',
             'val' => 'Hello',
             'type' => '2',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Registration email subject',
                'key' => 'reg_email_subj',
                'val' => 'Welcome',
                'type' => '2',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Admin email',
                'key' => 'admin_email',
                'val' => 'admin@admin.com',
                'type' => '1',
                'created_at' => now(),
                'updated_at' => now()],
            ['name' => 'Website name',
                'key' => 'web_name',
                'val' => 'PERLEASE-MVP',
                'type' => '1',
                'created_at' => now(),
                'updated_at' => now()],
            ['name' => 'Recall 1',
                'key' => 'recall_1',
                'val' => '14',
                'type' => '1',
                'created_at' => now(),
                'updated_at' => now()],
            ['name' => 'Recall 2',
                'key' => 'recall_2',
                'val' => '30',
                'type' => '1',
                'created_at' => now(),
                'updated_at' => now()],
            ['name' => 'Recall 3',
                'key' => 'recall_3',
                'val' => '60',
                'type' => '1',
                'created_at' => now(),
                'updated_at' => now()],
        ]);
    }
}
