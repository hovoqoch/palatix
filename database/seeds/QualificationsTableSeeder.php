<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QualificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('qualifications')->insert([
            ['name' => 'Qualitätsbeauftrage',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Hygienebeauftragte',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Prozessbeauftragte',
                'created_at' => now(),
                'updated_at' => now()],
        ]);
    }
}
