<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            ['name' => 'Steri/Autoklaven',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'Siegelgerät',
                'created_at' => now(),
                'updated_at' => now()],

            ['name' => 'RDG',
                'created_at' => now(),
                'updated_at' => now()],
        ]);
    }
}
