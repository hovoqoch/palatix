<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElearningQuestionsAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elearning_questions_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('answer');
            $table->boolean('type')->default('0')->comment('If 1 answer is true, if 0 - false');
            $table->unsignedInteger('elearning_question_id')->unsigned();
            $table->foreign('elearning_question_id')->references('id')->on('elearning_questions');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elearning_questions_answers');
    }
}
