<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElearningUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elearning_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('elearning_id')->unsigned();
            $table->string('certification_report')->nullable();
            $table->double('percent_right_answers')->nullable();
            $table->integer('true_answers_count')->nullable();
            $table->integer('false_answers_count')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('admin_notified')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elearning_user');
    }
}
