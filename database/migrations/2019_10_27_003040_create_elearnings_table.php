<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElearningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elearnings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('summary');
            $table->text('youtube_link')->nullable();
            $table->text('vimeo_link')->nullable();
            $table->string('attach_file_name')->nullable();
            $table->unsignedInteger('device_id')->unsigned();
            $table->foreign('device_id')->references('id')->on('devices');
            $table->unsignedInteger('practice_id')->unsigned();
            $table->foreign('practice_id')->references('id')->on('practices');
            $table->date('deadline_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elearnings');
    }
}
