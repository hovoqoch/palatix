<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicePracticeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_practice', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('practice_id')->unsigned();
            $table->foreign('practice_id')->references('id')->on('practices');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('device_id')->unsigned();
            $table->foreign('device_id')->references('id')->on('devices');
            $table->string('place_in_practice');
            $table->string('serial_number');
            $table->string('software_version');
            $table->string('parameter_version');
            $table->boolean('maintenance_report_valid')->default('1')->comment('0-no, 1-yes');
            $table->integer('maintenance_report_date');
            $table->integer('first_setup_date');
            $table->string('setup_approval');
            $table->integer('manufacturing_year');
            $table->integer('last_validation_date');
            $table->integer('next_validation_date')->nullable();
            $table->integer('last_maintenance_date');
            $table->string('remarks');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_practice');
    }
}
