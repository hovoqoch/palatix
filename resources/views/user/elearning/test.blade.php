@extends('layouts.user')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['employee_elearning_test_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="container">
        <div class="kt-grid">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
            @if($elearning)
                <!--begin: Form Wizard Form-->
                    <form class="kt-form" id="kt_user_add_form" method="POST"
                          action="{{ url('/') }}/{{app()->getLocale()}}/user/elearning/{{$elearning->id}}/submit-test">
                        @csrf
                        <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                             data-ktwizard-state="current">
                            <div class="kt-section kt-section--first">
                                <div class="kt-wizard-v4__form">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="kt-section__body">
                                                <div class="kt-portlet">
                                                    <div class="kt-portlet__head">
                                                        <div class="kt-portlet__head-label">
                                                            <h3 class="kt-portlet__head-title">{{ $translations['questions'] ?? 'Questions'}}</h3>
                                                        </div>
                                                    </div>
                                                    <div class="kt-portlet__body">

                                                        <!--begin::Form-->
                                                        <div class="kt-form">
                                                            @foreach($elearning->questions as $question)

                                                            <div class="form-group">
                                                                <label><h6>{{$question->question}}</h6></label>
                                                                <div class="kt-checkbox-list">
                                                                    @foreach($question->answers as $answer)
                                                                        <label class="kt-checkbox">
                                                                            <input type="checkbox" name="answers[{{$answer->elearning_question_id}}][{{$answer->id}}][]" > {{$answer->answer}}
                                                                            <span></span>
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            </div>

                                                            @endforeach
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 ">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                @else
                    <h1>{{$translations['no_question_added'] ?? 'No question added'}}</h1>
                @endif
            </div>
        </div>
    </div>
@endsection
