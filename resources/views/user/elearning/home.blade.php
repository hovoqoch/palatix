@extends('layouts.user')
@section('meta_title', $meta_title)
@section('meta_title', $meta_title)@section('description')
    <div class="description">{{$translations['employee_elearning_desc'] ?? 'action'}}</div>
@endsection
@section('content')
        <div class="table-responsive">
            <table id="datatableUsersElearning" class="table">
                <thead class="thead-light">
                <tr>
                    <th>{{ $translations['device_name'] ?? 'device_name' }}</th>
                    <th>{{ $translations['valid_due'] ?? 'valid_due' }}</th>
                    <th>{{ $translations['action'] ?? 'Action'}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($elearnings as $item)
                    <tr>
                        <td>{{$item->elearnings_belongs->device->name}}</td>
                        <td>{{date('Y/m/d',strtotime($item->elearnings_belongs->deadline_date))}}</td>
                        <td class="my_action">
                            @if($item->status)
                                <a class="btn btn-primary" role="button"
                                   href="{{ route('user.generate_certificate',[app()->getLocale(),$item->elearnings_belongs->id] ) }}">{{ $translations['download_certificate'] ?? 'download_certificate'}}</a>
                            @else
                                <a class="btn btn-primary" role="button"
                                   href="{{ route('user.test.video',[app()->getLocale(),$item->elearnings_belongs->id] ) }}">{{ $translations['pass_test'] ?? 'Pass test'}}</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script>
        let url = "{{route('user.home',['locale'=>app()->getLocale()]).'/elearning'}}";
        $('#datatableUsersElearning').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": url,
            "language": {
                "emptyTable": "{{ $translations['no_elearnings'] ?? 'No Elearnings'}}"
            },
            "columns": [
                {
                    "name":"elearnings_belongs.device.name",
                    "data": "elearnings_belongs.device",
                    "render":function (device) {
                        return device ? device.name : "-";

                    }
                },
                {
                    "name":"elearnings_belongs.deadline_date",
                    "data": "elearnings_belongs",
                    "render":function (elearnings) {
                        return elearnings ? new Date(elearnings.deadline_date).toLocaleDateString() : "-";

                    }
                },
                {
                    "searchable":false,
                    "orderable":false,
                    "className":"my_action",
                    "data": "status",
                    "render": function (status,type,data) {
                        let a = document.createElement('a');
                        a.className = "btn btn-primary";
                        if(status){
                            a.href = "{{route('user.home',['locale'=>app()->getLocale()]).'/generate-certificate/'}}"+data.elearnings_belongs.id;
                            a.innerText = "{{ $translations['download_certificate'] ?? 'download_certificate' }}";
                        }else{
                            a.href = `${url}/${data.elearnings_belongs.id}/test`;
                            a.innerText ="{{ $translations['pass_test'] ?? 'Pass test'}}";
                        }
                        return a.outerHTML;
                    }
                },
            ],
        });
    </script>
@endpush

