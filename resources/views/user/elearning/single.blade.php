@extends('layouts.user')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['employee_single_elearning_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <?php
    function convertYoutube($string)
    {
        $urlParts = explode('/', $string);
        $vidid = explode('&', str_replace('watch?v=', '', end($urlParts)));
        return 'https://www.youtube.com/embed/' . $vidid[0];
    }

    function convertVimeo($url)
    {
        $regs = array();
        $id = '';
        if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url, $regs)) {
            $id = $regs[3];
        }
        return $id;
    }
    ?>
    <div class="container">


        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-notes"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        {{$elearning->name}}
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="pb-2 col-12">
                    {{$elearning->summary}}
                </div>
                <div class="container">
                    <div class="row">

                        @if($elearning)
                            @if($elearning->youtube)
                                <div class="col-md-6 col-12">
                                    <iframe src="{{convertYoutube($elearning->youtube)}}" allowfullscreen width="100%" height="240" ></iframe>
                                </div>
                            @endif
                            @if($elearning->vimeo)
                                <div class="col-md-6 col-12">
                                    <iframe src="https://player.vimeo.com/video/{{$elearning->vimeo}}" width="100%" height="240" webkitallowfullscreen
                                            mozallowfullscreen allowfullscreen></iframe>
                                </div>
                            @endif


                            <div class="pt-2 col-12">
                                @if($elearning->attach_file_name)
                                    <a class="btn btn-primary"
                                       href="{{ route('user.attached',[app()->getLocale(),$elearning->attach_file_name] ) }}">
                                        {{ $translations['download_attached_file'] ?? 'download_attached_file'}}
                                    </a>
                                @endif

                                <a class="btn btn-success"
                                   href="{{ route('user.test.questions',[app()->getLocale(),$elearning->id] ) }}">
                                    {{$translations['continue'] ?? 'Continue'}}
                                </a>

                                @else
                            </div>
                            <h1>{{$translations['no_file_attached'] ?? 'no_file_attached'}}</h1>

                            <a class="btn btn-brand"
                               href="{{ route('user.test.questions',[app()->getLocale(),$elearning->id] ) }}">
                                {{$translations['continue'] ?? 'Continue'}}
                            </a>
                        @endif

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
