@extends('layouts.user')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['employee_after_test_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="container">
        @if($elearning_users->status)
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ $translations['well_done'] ?? 'Well done!'}}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    @if($elearning_users->percent_right_answers)
                        <div class="kt-widget25">
                            <div class="kt-widget25__items">
                                <div class="kt-widget25__item">
                                    <span class="kt-widget25__number">
                                        {{ $elearning_users->percent_right_answers }} %
                                    </span>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar kt-bg-success" role="progressbar" style="width: {{ $elearning_users->percent_right_answers }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="kt-section__content kt-section__content--solid">
                        <p>
                            {{ $translations['true_ans'] ?? 'True answers'}} :
                            <span class="kt-badge kt-badge--success">{{ $elearning_users->true_answers_count }}</span>
                        </p>
                        <p>
                            {{ $translations['wrong_ans'] ?? 'Wrong answers'}}:
                            <span class="kt-badge kt-badge--danger">{{ $elearning_users->false_answers_count }}</span>
                        </p>
                    </div>
                        <p>{{ $translations['can_download'] ?? 'Aww yeah, Now you can download certificate'}}</p>
                    @if($elearning_users->certification_report)

                        <a href="{{ $elearning_users->certification_report }}" class="kt-widget__files">
                            <div class="kt-widget__media">
                                <img class="kt-widget__img kt-hidden-" src="{{ asset('assets/media/files/pdf.svg') }}" alt="image">
                            </div>
                            <span class="kt-widget__desc pt-1">Certificate.pdf</span>
                        </a>
                    @endif
                </div>
            </div>
        @else
            <div class="alert alert-danger" role="alert">
                <div class="alert-text">
                    <h4 class="alert-heading">{{ $translations['got_issues'] ?? 'Got Issues'}}!</h4>
                    <p>{{ $translations['important_alert'] ?? 'Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.'}}</p>
                    <hr>
                    <p class="mb-0">{{ $translations['use_utilities'] ?? 'Whenever you need to, be sure to use margin utilities to keep things nice and tidy.'}}</p>
                </div>
            </div>
        @endif
    </div>
@endsection