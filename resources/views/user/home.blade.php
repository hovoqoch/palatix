@extends('layouts.user')
@section('meta_title', $meta_title)
@section('breadcrump')
    <h3 class="kt-subheader__title">
        {{$meta_title}} </h3>
    <div class="kt-subheader__breadcrumbs">
        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
        <span class="kt-subheader__breadcrumbs-separator"></span>
        <a href="" class="kt-subheader__breadcrumbs-link">
            {{$meta_title}} </a>
    </div>
@endsection
@section('description')
    <div class="description">{{$translations['employee_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch"
                     id="kt_body">
                    <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                         id="kt_content">

                        <!-- end:: Subheader -->

                        <!-- begin:: Content -->
                        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

                            <!--Begin::Dashboard 3-->

                            <!--Begin::Row-->
                            <div class="row">
                                <div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">

                                    <!--begin:: Widgets/Trends-->
                                    <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Your Device List
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fluid kt-portlet__body--fit">
                                                <div class="kt-widget4 kt-widget4--sticky">
                                                    <div class="kt-widget4__chart">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-widget6">
                                                                <table class="table" id="userDataTable">
                                                                    <thead class="thead-light">
                                                                    <tr>
                                                                        <th>Type Name</th>
                                                                        <th>User</th>
                                                                        <th>Date</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>

                                    <!--end:: Widgets/Trends-->
                                </div>
                            </div>

                            <!--End::Row-->

                            <!--End::Dashboard 3-->
                        </div>

                        <!-- end:: Content -->
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('#userDataTable').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": "{{route('user.home',['locale'=>app()->getLocale()])}}",
            "language": {
                "emptyTable": "{{ $translations['no_devices'] ?? 'No Devices'}}"
            },
            "columns": [
                {"data": "name"},
                {
                    "name": "user.name",
                    "data": "user",
                    "render": function (user) {
                        return user ? user.name:"-";
                    }
                },
                {
                    "data": "created_at",
                    "render": function (created_at) {
                        return new Date(created_at).toLocaleDateString();
                    }
                },
            ],
        });
    </script>
    @endpush
