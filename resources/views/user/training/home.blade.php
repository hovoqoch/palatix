@extends('layouts.user')
@section('meta_title', $meta_title)@section('description')
    <div class="description">{{$translations['employee_training_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
<div class="table-responsive">
    <table id="datatableUsersTraining" class="table">
        <thead class="thead-light">
        <tr>
            <th>{{ $translations['name'] ?? 'name' }}</th>
            <th>{{ $translations['file_name'] ?? 'file_name' }}</th>
            <th>{{ $translations['device_name'] ?? 'device_name' }}</th>
            <th>{{ $translations['action'] ?? 'Action' }}</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

@endsection
@push('scripts')
    <script>
        let url = "{{route('user.home',['locale'=>app()->getLocale()]).'/training'}}";
        $('#datatableUsersTraining').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": url,
            "language": {
                "emptyTable": "{{ $translations['no_training_materials'] ?? 'No Training Materials'}}"
            },
            "columns": [
                {"data": "name"},
                {"data": "file"},
                {
                    "data": "device",
                    "name":"device.name",
                    "render": function (device) {
                        return device ? device.name : "-";
                    }
                },
                {
                     "searchable":false,
                     "orderable":false,
                    "data": "file",
                    "render": function (file) {
                        let a = document.createElement('a');
                        a.className = "btn btn-primary";
                        a.href = `${url}/${file}`;
                        a.innerText = "{{ $translations['download_file'] ?? 'download_file' }}";
                        return a.outerHTML;
                    }
                },
            ],
        });
    </script>
@endpush

