@extends('layouts.user')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/user/devices/create">
            {{$translations['add_new_device_to_practice'] ?? 'add_new_device_to_practice'}}
        </a>

    </div>
@endsection
@section('meta_title', $meta_title)@section('description')
    <div class="description">{{$translations['employee_pr_dev_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
        <div class="table-responsive">
            <table id="datatableUsersDevices" class="table">
                <thead class="thead-light">
                <tr>
                    <th>{{$translations['name'] ?? 'name'}}</th>
                    <th>{{$translations['manufacture'] ?? 'manufacture'}}</th>
                    <th>{{$translations['type'] ?? 'type'}}</th>

                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script>
        $('#datatableUsersDevices').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": "{{route('user.home',['locale'=>app()->getLocale()]).'/devices'}}",
            "language": {
                "emptyTable": "{{ $translations['no_device_added'] ?? 'No Device Added'}}"
            },
            "columns": [
                {"data": "device_name"},
                {
                    "data": "manufacturer",
                    "render": function (manufacturer) {
                        return manufacturer ? manufacturer : "-";
                    }
                },
                {
                    "data": "type",
                    "render": function (type) {
                        return type ? type : "-";
                    }
                },
            ],
        });
    </script>
@endpush
