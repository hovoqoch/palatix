@extends('layouts.user')
@section('meta_title', $meta_title)
@section('meta_title', $meta_title)@section('description')
    <div class="description">{{$translations['employee_pr_dev_create_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="container">
        <div class="kt-grid">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">

                <!--begin: Form Wizard Form-->
                <form class="kt-form" id="kt_user_add_form" method="POST"
                      action="{{ url('/') }}/{{app()->getLocale()}}/user/devices">
                @csrf
                <!--begin: Form Wizard Step 1-->
                    <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                         data-ktwizard-state="current">
                        <div class="kt-section kt-section--first">
                            <div class="kt-wizard-v4__form">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="kt-section__body">
                                            <div class="add__device__step--first">
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['manufacturer'] ?? 'manufacturer'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select id="manufacturer" class="form-control" name="manufacturer_id">
                                                            <option
                                                                value="0" disabled selected>{{ $translations['select_manufacturer'] ?? 'select_manufacturer'}}</option>
                                                            @foreach($manufactorers as $manufactorer)
                                                                <option
                                                                    value="{{$manufactorer->id}}">{{$manufactorer->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['type'] ?? 'type'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select id="type" class="form-control" name="type_id">
                                                            <option
                                                                value="0" disabled selected>{{ $translations['select_type'] ?? 'select_type'}}</option>
                                                            @foreach($types as $type)
                                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="add__device__step--second">
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_device'] ?? 'select_device'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select id="devices" class="form-control" name="device_id">

                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="device_name" class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['create_new_device'] ?? 'create_new_device'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" value="{{ old('device_name') }}"
                                                                   name="device_name">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="add__device__step--third">

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['place_in_practice'] ?? 'place_in_practice'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control"
                                                                   name="place_in_practice" value="{{ old('place_in_practice') }}"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['serial_number'] ?? 'serial_number'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="serial_number" value="{{ old('serial_number') }}"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['software_version'] ?? 'software_version'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control"
                                                                   name="software_version" value="{{ old('software_version') }}"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['parameter_version'] ?? 'parameter_version'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control"
                                                                   name="parameter_version" value="{{ old('parameter_version') }}"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['maintenance_report_valid'] ?? 'maintenance_report_valid'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <select class="form-control" name="maintenance_report_valid"
                                                                    required>
                                                                <option value="1">{{ $translations['yes'] ?? 'yes'}}</option>
                                                                <option value="0">{{ $translations['no'] ?? 'no'}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['maintenance_report_date'] ?? 'maintenance_report_date'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datatable"
                                                                   id="kt_datepicker_1" readonly value="{{ old('maintenance_report_date') }}"
                                                                   name="maintenance_report_date" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['first_setup_date'] ?? 'first_setup_date'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datatable"
                                                                   id="kt_datepicker_2" readonly name="first_setup_date" value="{{ old('first_setup_date') }}"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['setup_approval'] ?? 'setup_approval'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control"
                                                                   name="setup_approval" value="{{ old('setup_approval') }}"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['manufacturing_year'] ?? 'manufacturing_year'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datatable_year"
                                                                   readonly value="{{ old('manufacturing_year') }}"
                                                                   name="manufacturing_year" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['last_validation_year'] ?? 'last_validation_year'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datatable"
                                                                   id="kt_datepicker_3" readonly
                                                                   name="last_validation_date" value="{{ old('last_validation_date') }}"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['last_maintenance_date'] ?? 'last_maintenance_date'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datatable"
                                                                   id="kt_datepicker_4_1" readonly value="{{ old('last_maintenance_date') }}"
                                                                   name="last_maintenance_date" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['remarks'] ?? 'remarks'}}</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="remarks" value="{{ old('remarks') }}"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end: Form Wizard Step 1-->

                    <!--begin: Form Actions -->
                    <div class="kt-form__actions">
                        <button type="submit"
                                class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                            {{ $translations['create'] ?? 'Create' }}
                        </button>
                    </div>

                    <!--end: Form Actions -->
                </form>

                <!--end: Form Wizard Form-->
            </div>
        </div>
    </div>

@endsection
