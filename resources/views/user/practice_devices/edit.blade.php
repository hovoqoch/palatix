@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('meta_title', $meta_title)@section('description')
    <div class="description">{{$translations['employee_pr_dev_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="container">
        <div class="kt-grid">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">

                <form class="kt-form" id="kt_user_add_form" method="POST"
                      action="{{ url('/') }}/{{app()->getLocale()}}/user/devices/{{$device->id}}">
                    @csrf
                    {{method_field('PUT')}}
                    <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                         data-ktwizard-state="current">
                        <div class="kt-section kt-section--first">
                            <div class="kt-wizard-v4__form">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="kt-section__body">

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['place_in_practice'] ?? 'place_in_practice'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               name="place_in_practice"
                                                               value="{{$device->place_in_practice}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['serial_number'] ?? 'serial_number'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="serial_number"
                                                               value="{{$device->serial_number}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['software_version'] ?? 'software_version'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               name="software_version"
                                                               value="{{$device->software_version}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['parameter_version'] ?? 'parameter_version'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               name="parameter_version"
                                                               value="{{$device->parameter_version}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['maintenance_report_valid'] ?? 'maintenance_report_valid'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <select class="form-control" name="maintenance_report_valid"
                                                                required>
                                                            <option value="1" @if($device->maintenance_report_valid) selected @endif>Yes</option>
                                                            <option value="0" @if(!$device->maintenance_report_valid) selected @endif>No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['maintenance_report_date'] ?? 'maintenance_report_date'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datatable"
                                                               id="kt_datepicker_1" readonly
                                                               value="{{date("m/d/Y",$device->maintenance_report_date)}}"
                                                               name="maintenance_report_date" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['first_setup_date'] ?? 'first_setup_date'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datatable"
                                                               id="kt_datepicker_2" readonly name="first_setup_date"
                                                               value="{{date("m/d/Y",$device->first_setup_date)}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['setup_approval'] ?? 'setup_approval'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               name="setup_approval"
                                                               value="{{$device->setup_approval}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['manufacturing_year'] ?? 'manufacturing_year'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datatable_year"
                                                               readonly
                                                               value="{{$device->manufacturing_year}}"
                                                               name="manufacturing_year" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['last_validation_year'] ?? 'last_validation_year'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datatable"
                                                               id="kt_datepicker_3" readonly
                                                               name="last_validation_date"
                                                               value="{{date("m/d/Y",$device->last_validation_date)}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['last_maintenance_date'] ?? 'last_maintenance_date'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datatable"
                                                               id="kt_datepicker_4_1" readonly
                                                               value="{{date("m/d/Y",$device->last_maintenance_date)}}"
                                                               name="last_maintenance_date" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['remarks'] ?? 'remarks'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="remarks"
                                                               value="{{$device->remarks}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--begin: Form Actions -->
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                            {{ $translations['update'] ?? 'Update' }}
                        </button>
                    </div>

                    <!--end: Form Actions -->
                </form>


            </div>
        </div>
    </div>

@endsection
