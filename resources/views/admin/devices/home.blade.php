@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-primary" href="{{ url('/') }}/{{app()->getLocale()}}/download/devices">
            {{ $translations['export_list'] ?? 'export_list' }}
        </a>

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/devices/create">
            {{ $translations['add_new_device'] ?? 'add_new_device' }}
        </a>

    </div>
@endsection
@section('description')
    <div class="description">{{$translations['device_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

<div class="table-responsive">
    <table id="devicesList" class="table">
        <thead class="thead-light">
        <tr>
            <th>{{ $translations['name'] ?? 'name' }}</th>
            <th>{{ $translations['manufacturer'] ?? 'manufacturer' }}</th>
            <th>{{ $translations['type'] ?? 'type' }}</th>
            <th>{{ $translations['validation_duration'] ?? 'validation_duration' }}</th>
            <th>{{ $translations['status'] ?? 'status' }}</th>
            <th>{{ $translations['action'] ?? 'Action'}}</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#devicesList').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": "{{route('devices.index',['locale'=>app()->getLocale()])}}",
                "language": {
                    "emptyTable": "{{ $translations['no_devices'] ?? 'no_devices'}}"
                },
                "columns": [
                    {"data": "name"},
                    {
                        "name":"manufacturer.name",
                        "data": "manufacturer",
                        "render": function (manufacturer) {
                            return manufacturer ? manufacturer.name : '-';
                        }
                    },
                    {
                        "name":"type.name",
                        "data": "type",
                        "render": function (type) {
                            return type ? type.name : '-';
                        }
                    },
                    {
                        "data": "val_duration",
                        "searchable": false,
                        "render": function (duration) {
                            return duration + " " + "{{ $translations['year'] ?? 'year' }}";
                        }
                    },
                    {
                        "name":"user.name",
                        "data": "status",
                        "searchable": false,
                        "render": function (status, type, device) {
                            return status === 1 ? "{{($translations['public'] ?? 'public')}}" : device.user.name;
                        }
                    },
                    {
                        "data": "status",
                        "className": "my_action",
                        "searchable": false,
                        "orderable": false,
                        "render": function (status, type, device) {
                            let html;
                            let url = "{{url('/').'/'.app()->getLocale()}}/devices/" + device.id;
                            let edit = document.createElement("a");
                            let del = document.createElement("button");
                            edit.className = 'btn btn-success my_btn1';
                            edit.setAttribute('role', 'button');
                            edit.href = url + "/edit";
                            edit.innerText = "{{ $translations['edit'] ?? 'Edit' }}";
                            del.className = 'btn btn-danger delete_action';
                            del.type = 'button';
                            del.setAttribute('data-form_action', url);
                            del.setAttribute('data-toggle', 'modal');
                            del.setAttribute('data-target', '#deleteModal');
                            del.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            html = edit.outerHTML + " " + del.outerHTML;
                            if (parseInt(device.status) !== 1 && parseInt(device.user_id) !== 0) {
                                let text = "{{ $translations['are_you_sure'] ?? 'are_you_sure'}}";
                                let a1 = document.createElement("a");
                                let a2 = document.createElement("a");
                                a1.setAttribute('role', 'button');
                                a2.setAttribute('role', 'button');
                                a1.href = "{{url('/')}}/approveDevice/" + device.id;
                                a2.href = "{{url('/')}}/keepPrivet/" + device.id;
                                a1.className = 'btn btn-primary';
                                a2.className = 'btn btn-warning';
                                a1.innerText = "{{ $translations['approve'] ?? 'approve'}}";
                                a2.innerText = "{{ $translations['keep_private'] ?? 'keep_private'}}";
                                a1.setAttribute("onclick", "return confirm('" + text + "')");
                                a2.setAttribute("onclick", "return confirm('" + text + "')");
                                html += " " + a1.outerHTML + " " + a2.outerHTML;
                            }
                            return html;
                        }
                    }
                ],
            });
        })
    </script>
    @endpush
