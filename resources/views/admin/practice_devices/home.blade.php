@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">
        <a class="btn btn-primary" href="{{ url('/') }}/{{app()->getLocale()}}/download/practicedevices">
            {{ $translations['export_list'] ?? 'export_list' }}
        </a>

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/practice-devices/create">
            {{ $translations['add_new_device_to_practice'] ?? 'add_new_device_to_practice' }}
        </a>

    </div>
@endsection
@section('description')
    <div class="description">{{$translations['practice_devices_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
        <div class="table-responsive">
            <table id="practiceDevices" class="table">
                <thead class="thead-light">
                <tr>
                    <th>{{ $translations['practice'] ?? 'practice' }}</th>
                    <th>{{ $translations['device'] ?? 'device' }}</th>
                    <th>{{ $translations['next_validation'] ?? 'next_validation' }}</th>
                    <th>{{ $translations['action'] ?? 'action' }}</th>
                    <th style="display: none">{{ $translations['more_details'] ?? 'more_details' }}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

@endsection
@push('modal')
    @include('partials.delete', ['modal_title' =>  $translations['are_you_sure'] ?? 'are_you_sure' ])
@endpush
@push('scripts')
    <script>
        $(document).ready(function () {
            let url = "{{route('practice-devices.index',['locale'=>app()->getLocale()])}}";
            $('#practiceDevices').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": `${url}`,
                "language": {
                    "emptyTable": "{{ $translations['no_practice'] ?? 'no_practice' }}"
                },
                "columns": [
                    {
                        "name": "practice.name",
                        "data": "practice",
                        "render": function (practice) {
                            return practice ? practice.name : '-';
                        }
                    },
                    {
                        "name": "device.name",
                        "data": "device",
                        "render": function (device) {
                            return device ? device.name : '-';
                        }
                    },
                    {
                        "name": "next_validation_date",
                        "data": "next_validation_date",
                        "render": function (validation) {
                            return validation ? new Date(parseInt(validation)*1000).toLocaleDateString() : "{{$translations['not_set'] ?? 'not_set'}}";
                        }
                    },
                    {
                        "className": "my_action",
                        "name": "region.name",
                        "data": "id",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id) {
                            let a = document.createElement('a');
                            let button1 = document.createElement('button');
                            let button2 = document.createElement('button');
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute("role", "button");
                            a.href = `${url}/${id}/edit`;
                            a.innerText = "{{ $translations['update'] ?? 'Update' }}";
                            button1.type = "button";
                            button1.className = "btn btn-danger delete_action";
                            button1.setAttribute("data-form_action", `${url}/${id}`);
                            button1.setAttribute("data-toggle", "modal");
                            button1.setAttribute("data-target", "#deleteModal");
                            button1.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            button2.type = "button";
                            button2.className = "btn btn-info cd-showmore";
                            button2.innerText = "{{ $translations['show_details'] ?? 'show_details' }}";
                            return `${a.outerHTML} ${button1.outerHTML} ${button2.outerHTML}`;
                        }
                    },
                    {
                        "className": "cd-additional-info",
                        "name": "id",
                        "data": "id",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id, type, practice) {
                            return `<p><b>{{ $translations['place_in_practice'] ?? 'place_in_practice' }}:</b>${practice.place_in_practice}</p>` +
                                `<p><b>{{ $translations['serial_number'] ?? 'serial_number' }}:</b>${practice.serial_number}</p>` +
                                `<p><b>{{ $translations['software_version'] ?? 'software_version' }}:</b>${practice.software_version}</p>` +
                                `<p><b>{{ $translations['parameter_version'] ?? 'parameter_version' }}:</b>${practice.parameter_version}</p>` +
                                `<p><b>{{ $translations['maintenance_report_valid'] ?? 'maintenance_report_valid' }}:</b>${practice.maintenance_report_valid==='1'? 'Yes' : 'No'}</p>` +
                                `<p><b>{{ $translations['maintenance_report_date'] ?? 'maintenance_report_date' }}:</b>${new Date(parseInt(practice.maintenance_report_date)*1000).toLocaleDateString()}</p>` +
                                `<p><b>{{ $translations['first_setup_date'] ?? 'first_setup_date' }}:</b>${new Date(parseInt(practice.first_setup_date)*1000).toLocaleDateString()}</p>` +
                                `<p><b>{{ $translations['setup_approval'] ?? 'setup_approval' }}:</b>${practice.setup_approval}</p>` +
                                `<p><b>{{ $translations['manufacturing_year'] ?? 'manufacturing_year' }}:</b>${practice.manufacturing_year}</p>` +
                                `<p><b>{{ $translations['last_validation_year'] ?? 'last_validation_year' }}:</b>${new Date(parseInt(practice.last_validation_date)*1000).toLocaleDateString()}</p>` +
                                `<p><b>{{ $translations['next_validation_date'] ?? 'next_validation_date' }}:</b>${practice.next_validation_date?new Date(parseInt(practice.next_validation_date)*1000).toLocaleDateString():"{{$translations['not_set'] ?? 'not_set'}}"}</p>` +
                                `<p><b>{{ $translations['last_maintenance_date'] ?? 'last_maintenance_date' }}:</b>${new Date(parseInt(practice.last_maintenance_date)*1000).toLocaleDateString()}</p>` +
                                `<p><b>{{ $translations['remarks'] ?? 'remarks' }}:</b>${practice.remarks}</p>`;
                        }
                    }
                ], rowCallback: function (row) {
                    $(row).addClass('cd-info');
                },
            }).on('draw', function () {
                $(".cd-showmore").on("click", function () {
                    $(this).parent().parent().toggleClass("cd-info-open");
                    let text = $(this).text();
                    $(this).text(text === "Hide details" ? "Show details" : "Hide details");
                    $(".cd-additional-info:last").insertAfter($(this).parent().parent()).clone();
                });
            });
        });
    </script>
@endpush
