@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['practice_device_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="container">
        <div class="kt-grid">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">

                <!--begin: Form Wizard Form-->
                <form class="kt-form" id="kt_user_add_form" method="POST"
                      action="{{ url('/') }}/{{app()->getLocale()}}/practice-devices/{{$practiceDevices->id}}">
                @csrf
                {{method_field('PUT')}}
                <!--begin: Form Wizard Step 1-->
                    <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                         data-ktwizard-state="current">
                        <div class="kt-section kt-section--first">
                            <div class="kt-wizard-v4__form">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="kt-section__body">
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_practice'] ?? 'select_practice' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <select class="form-control" name="practice_id">
                                                        @foreach($practices as $item)
                                                            <option  {{(int)$item->id === (int)$practiceDevices->practice_id ? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_device'] ?? 'select_device' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <select class="form-control" name="device_id">
                                                        @foreach($devices as $item)
                                                            <option {{(int)$item->id === (int)$practiceDevices->device_id ? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['place_in_practice'] ?? 'place_in_practice' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{$practiceDevices->place_in_practice}}" type="text" class="form-control" name="place_in_practice" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['serial_number'] ?? 'serial_number' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{$practiceDevices->serial_number}}" type="text" class="form-control" name="serial_number" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['software_version'] ?? 'software_version' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{$practiceDevices->software_version}}" type="text" class="form-control" name="software_version" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['parameter_version'] ?? 'parameter_version' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{$practiceDevices->parameter_version}}" type="text" class="form-control" name="parameter_version" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['maintenance_report_valid'] ?? 'maintenance_report_valid' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <select class="form-control" name="maintenance_report_valid" required>
                                                            <option {{$practiceDevices->id === '1' ? 'selected' : ''}} value="1">{{ $translations['yes'] ?? 'yes' }}</option>
                                                            <option {{$practiceDevices->id === '0' ? 'selected' : ''}} value="0">{{ $translations['no'] ?? 'no' }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['maintenance_report_date'] ?? 'maintenance_report_date' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{date('m/d/Y',$practiceDevices->maintenance_report_date)}}" type="text" class="form-control datatable" id="kt_datepicker_1" readonly name="maintenance_report_date" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['first_setup_date'] ?? 'first_setup_date' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{date('m/d/Y',$practiceDevices->first_setup_date)}}" type="text" class="form-control datatable" id="kt_datepicker_2" readonly name="first_setup_date" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['setup_approval'] ?? 'setup_approval' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{$practiceDevices->setup_approval}}" type="text" class="form-control" name="setup_approval" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['manufacturing_year'] ?? 'manufacturing_year' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{$practiceDevices->manufacturing_year}}" type="text" class="form-control datatable_year" readonly name="manufacturing_year" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['last_validation_year'] ?? 'last_validation_year' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{date('m/d/Y',$practiceDevices->last_validation_date)}}" type="text" class="form-control datatable" id="kt_datepicker_3" readonly  name="last_validation_date" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['last_maintenance_date'] ?? 'last_maintenance_date' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{date('m/d/Y',$practiceDevices->last_maintenance_date)}}" type="text" class="form-control datatable" id="kt_datepicker_4_1" readonly  name="last_maintenance_date" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['remarks'] ?? 'remarks' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input value="{{$practiceDevices->remarks}}" type="text" class="form-control" name="remarks" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end: Form Wizard Step 1-->

                    <!--begin: Form Actions -->
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                            {{ $translations['update'] ?? 'Update'}}
                        </button>
                    </div>

                    <!--end: Form Actions -->
                </form>

                <!--end: Form Wizard Form-->
            </div>
        </div>
    </div>

@endsection