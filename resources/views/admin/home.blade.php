@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('breadcrump')
    <h3 class="kt-subheader__title">
        {{$meta_title}} </h3>
    <div class="kt-subheader__breadcrumbs">
        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
        <span class="kt-subheader__breadcrumbs-separator"></span>
        <a href="" class="kt-subheader__breadcrumbs-link">
            {{$meta_title}} </a>
    </div>
@endsection
@section('description')
    <div class="description">{{$translations['home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch"
                     id="kt_body">
                    <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                         id="kt_content">

                        <!-- end:: Subheader -->

                        <!-- begin:: Content -->
                        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

                            <!--Begin::Dashboard 3-->

                            <!--Begin::Row-->
                            <div class="row">
                                <div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">

                                    <!--begin:: Widgets/Trends-->
                                    <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    {{ $translations['custom_devices_to_be_reviewed'] ?? 'custom_devices_to_be_reviewed' }}
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fluid kt-portlet__body--fit">
                                            @php $count = 0 @endphp
                                            @foreach($devices as $item)
                                                @if(!Auth::user()->region_id || Auth::user()->region_id === $item->user->practice->region_id)
                                                    @php $count = 1 @endphp
                                                @endif
                                            @endforeach
                                            @if($count > 0 )
                                                <div class="kt-widget4 kt-widget4--sticky">
                                                    <div class="kt-widget4__chart">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-widget6">
                                                                <table class="table">
                                                                    <thead class="thead-light">
                                                                    <tr>
                                                                        <th>{{ $translations['name'] ?? 'name' }}</th>
                                                                        <th>{{ $translations['user'] ?? 'user' }}</th>
                                                                        <th>{{ $translations['date'] ?? 'date' }}</th>
                                                                        <th>{{ $translations['action'] ?? 'action' }}</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($devices as $item)
                                                                        @if(!Auth::user()->region_id || Auth::user()->region_id === $item->user->practice->region_id)

                                                                            <tr>
                                                                                <td>{{ $item->name }}</td>
                                                                                <td>
                                                                                    <a href="/doctor/{{ $item->user['id'] }}">{{ $item->user['name'] }}</a>
                                                                                </td>
                                                                                <td>{{ date('Y/m/d', strtotime($item->created_at)) }}</td>
                                                                                <td class="kt-font-success kt-font-bold">
                                                                                    <a onclick="return confirm('{{ $translations['are_you_sure'] ?? 'are_you_sure' }}')"
                                                                                       href="{{ route('approveDevice', ['id' => $item->id]) }}"
                                                                                       class="btn btn-primary btn-sm btn-bold">{{ $translations['approve'] ?? 'approve' }}</a>
                                                                                    <a onclick="return confirm('{{ $translations['are_you_sure'] ?? 'are_you_sure' }}')"
                                                                                       href="{{ route('keepPrivateDevice', ['id' => $item->id]) }}"
                                                                                       class="btn btn-warning btn-sm btn-bold">{{ $translations['keep_private'] ?? 'keep_private' }}</a>
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>

                                                                <div class="kt-widget6__foot">
                                                                    <a href="{{route('devices.index',[app()->getLocale()])}}"
                                                                       class="btn btn-label-brand btn-sm btn-bold">{{ $translations['see_all'] ?? 'see_all' }}</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <h3 class="pp-25">{{ $translations['no_device_to_show'] ?? 'no_device_to_show' }}</h3>
                                            @endif
                                        </div>
                                    </div>

                                    <!--end:: Widgets/Trends-->
                                </div>
                                <div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">

                                    <!--begin:: Widgets/Sales Stats-->
                                    <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    {{ $translations['practice_devices_for_validation'] ?? 'practice_devices_for_validation' }}
                                                </h3>
                                            </div>

                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fluid kt-portlet__body--fit">
                                            @php $count = 0 @endphp
                                            @foreach($practiceDevices as $item)
                                                @if(!Auth::user()->region_id || Auth::user()->region_id === $item->practice->region_id)
                                                    @php $count = 1 @endphp
                                                @endif
                                            @endforeach
                                            @if($count > 0)
                                                <div class="kt-widget4 kt-widget4--sticky">
                                                    <div class="kt-widget4__chart">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-widget6">
                                                                <table class="table">
                                                                    <thead class="thead-light">
                                                                    <tr>
                                                                        <th>{{ $translations['practice_name'] ?? 'practice_name' }}</th>
                                                                        <th>{{ $translations['device_name'] ?? 'device_name' }}</th>
                                                                        <th>{{ $translations['valid_until'] ?? 'valid_until' }}</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($practiceDevices as $item)
                                                                        @if(!Auth::user()->region_id || Auth::user()->region_id === $item->practice->region_id)
                                                                            <tr>
                                                                                <td>{{ $item->practice->name ?? '-' }}</td>
                                                                                <td>{{ $item->device->name ?? '-' }}</td>
                                                                                <td>{{ $item->next_validation_date ? date('Y/m/d', $item->next_validation_date) : $translations['not_set'] ?? 'not_set' }}</td>

                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                                <div class="kt-widget6__foot">
                                                                    <a href="{{route('practice-devices.index',[app()->getLocale()])}}"
                                                                       class="btn btn-label-brand btn-sm btn-bold">{{ $translations['see_all'] ?? 'see_all' }}</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <h3 class="pp-25">{{ $translations['no_device_to_show'] ?? 'no_device_to_show' }}</h3>
                                            @endif
                                        </div>
                                    </div>

                                    <!--end:: Widgets/Sales Stats-->
                                </div>
                            </div>

                            <!--End::Row-->

                            <!--End::Dashboard 3-->
                        </div>

                        <!-- end:: Content -->
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
