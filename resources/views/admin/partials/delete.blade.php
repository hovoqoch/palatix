<!-- The Modal -->
<div class="modal" id="deleteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" id="delete_form">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">{{ $modal_title }}</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    {{ $translations['are_you_sure'] ?? 'are_you_sure' }}
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-success" data-dismiss="modal">{{ $translations['no'] ?? 'no' }}</button>
                    <button type="submit" class="btn btn-outline-danger delete_submit">{{ $translations['yes'] ?? 'yes' }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

</div>

@push('scripts')
    <script>
        $(document).on('click', ".delete_action", function () {
            let form_action = $(this).data('form_action');
            $(".delete_submit").prop('disabled', true);
            if (form_action != '') {
                $("#delete_form").prop('action', form_action);
                $(".delete_submit").prop('disabled', false);
            }
        });
    </script>
@endpush
