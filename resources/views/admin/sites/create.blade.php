@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['sites_creat_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="container">
        <div class="kt-grid">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">

                <!--begin: Form Wizard Form-->
                <form class="kt-form" id="kt_user_add_form" method="POST"
                      action="{{ url('/') }}/{{app()->getLocale()}}/sites">
                @csrf
                <!--begin: Form Wizard Step 1-->
                    <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                         data-ktwizard-state="current">
                        <div class="kt-section kt-section--first">
                            <div class="kt-wizard-v4__form">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="kt-section__body">
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['name'] ?? 'name' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['Practice'] ?? 'Practice'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <select class="form-control" name="practice_id" required>
                                                        @foreach($practices as $practice)
                                                            <option value="{{$practice->id}}">{{$practice->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end: Form Wizard Step 1-->

                    <!--begin: Form Actions -->
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                            {{ $translations['create'] ?? 'Create' }}
                        </button>
                    </div>

                    <!--end: Form Actions -->
                </form>

                <!--end: Form Wizard Form-->
            </div>
        </div>
    </div>

@endsection