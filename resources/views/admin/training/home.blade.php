@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/training/create">
            {{ $translations['add_training'] ?? 'add_training' }}
        </a>

    </div>
@endsection
@section('description')
    <div class="description">{{$translations['trainings_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
<div class="table-responsive">
    <table id="datatableElearningTraining" class="table">
        <thead class="thead-light">
        <tr>
            <th>{{ $translations['name'] ?? 'name' }}</th>
            <th>{{ $translations['file_name'] ?? 'file_name' }}</th>
            <th>{{ $translations['device_name'] ?? 'device_name' }}</th>
            <th>{{ $translations['action'] ?? 'Action' }}</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure' ])
@endpush
@push('scripts')
    <script defer>
        $(document).ready(function () {
            let url = "{{route('training.index',['locale'=>app()->getLocale()])}}";
            $('#datatableElearningTraining').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": `${url}`,
                "language": {
                    "emptyTable": "{{ $translations['no_training_material'] ?? 'no_training_material' }}"
                },
                "columns": [
                    {"data": "name"},
                    {"data": "file"},
                    {
                        "name":"device.name",
                        "data": "device",
                        "render":function (device) {
                            return device ? device.name : "{{$translations['no_device'] ?? 'No device'}}";
                        }
                    },
                    {
                        "name": "id",
                        "data": "id",
                        "className": "my_action",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id,type,data) {
                            let a = document.createElement('a');
                            let download = document.createElement('a');
                            let button = document.createElement('button');
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute("role", "button");
                            a.href = `${url}/${id}/edit`;
                            a.innerText = "{{ $translations['edit'] ?? 'edit' }}";
                            button.type = "button";
                            button.className = "btn btn-danger delete_action";
                            button.setAttribute("data-form_action", `${url}/${id}`);
                            button.setAttribute("data-toggle", "modal");
                            button.setAttribute("data-target", "#deleteModal");
                            button.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            download.className = "btn btn-primary";
                            download.setAttribute("role", "button");
                            download.href = `${url}/download/${data.file}`;
                            download.innerText = "{{ $translations['download_file'] ?? 'download_file' }}";
                            return `${a.outerHTML} ${button.outerHTML} ${download.outerHTML}`;
                        }
                    },
                ]
            });
        });
    </script>
@endpush
