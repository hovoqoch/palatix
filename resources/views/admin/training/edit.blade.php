@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['training_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/training/{{$training->id}}"
                          enctype="multipart/form-data">
                        @csrf
                        {{method_field('PUT')}}

                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_device'] ?? 'select_device' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <select class="form-control" name="device_id" required>
                                    <option value="0" disabled selected>{{ $translations['select_device'] ?? 'select_device' }}</option>
                                    @foreach($devices as $device)
                                        <option
                                            {{(int)$device->id === (int)$training->device_id ? 'selected' : ''}} value="{{$device->id}}">{{$device->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['test_name'] ?? 'test_name' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control" placeholder="Enter Training name" id="name" type="text"
                                       name="name" value="{{$training->name}}" required>
                            </div>
                        </div>

                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['update'] ?? 'update' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
