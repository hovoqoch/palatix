@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['settings_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    @if(count($settings) > 0)

        <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/settings/updateBatch">
            @csrf
            {{method_field('PUT')}}
            <div class="col-md-1 pull-right mb-3">
                <button type="submit" class="btn btn-primary">
                    {{ $translations['update'] ?? 'Update' }}
                </button>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>{{ $translations['name'] ?? 'name' }}</th>
                            <th>{{ $translations['value'] ?? 'value' }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($settings as $setting)
                            <tr>
                                <td>{{$setting->name}}</td>
                                <td>
                                    @if($setting->type == 2 )
                                        <textarea name="settings[{{$setting->key}}]" class="form-control summary-ckeditor" cols="30" rows="10">{{$setting->val}}</textarea>
                                    @else
                                        <input type="text" class="form-control" value="{{$setting->val}}" name="settings[{{$setting->key}}]">
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-1 pull-right">
                <button type="submit" class="btn btn-primary">
                    {{ $translations['update'] ?? 'Update' }}
                </button>
            </div>
        </form>
    @else
        <h1>{{ $translations['no_settings'] ?? 'no_settings' }}</h1>
    @endif

@endsection