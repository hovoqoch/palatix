@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">
        <a href="{{ url('/') }}/{{app()->getLocale()}}/employees/create" class="btn btn-danger">
            {{ $translations['add_employee'] ?? 'add_employee' }}
        </a>
    </div>
@endsection
@section('description')
    <div class="description">{{$translations['employees_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    @if(count($employees) > 0)
    <div class="table-responsive">
        <table id="datatable" class="table">
            <thead class="thead-light">
                <tr>
                    <th>{{ $translations['practice_name'] ?? 'practice_name' }}</th>
                    <th>{{ $translations['first_name'] ?? 'first_name' }}</th>
                    <th>{{ $translations['last_name'] ?? 'last_name' }}</th>
                    <th>{{ $translations['email'] ?? 'email' }}</th>
                    <th>{{ $translations['phone'] ?? 'phone' }}</th>
                    <th>{{ $translations['job_title'] ?? 'job_title' }}</th>
                    <th>{{ $translations['qualification'] ?? 'qualification' }}</th>
                    <th>{{ $translations['action'] ?? 'action' }}</th>
                    <th>{{ $translations['status'] ?? 'status' }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <td>{{$employee->practice_id}}</td>
                        <td>{{$employee->name}}</td>
                        <td>{{$employee->first_name}}</td>
                        <td>{{$employee->email}}</td>
                        <td>{{$employee->phone}}</td>
                        <td>{{$employee->job_title}}</td>
                        <td>{{$employee->qualification_id}}</td>
                        <td class="my_action">
                                <a class="btn btn-success my_btn1" role="button" href="{{ url('/') }}/{{app()->getLocale()}}/employees/{{$employee->id.'/edit'}}">{{ $translations['edit'] ?? 'Edit' }}</a>
                                <button type="button" class="btn btn-danger delete_action"
                                        data-form_action="{{ route('employees.destroy',[app()->getLocale(),$employee->id] ) }}"
                                        data-toggle="modal" data-target="#deleteModal">{{ $translations['delete'] ?? 'Delete' }}
                                </button>
                        </td>
                        <td>
                            <span class="kt-switch">
                                <label>
                                    <input name="status" data-id="{{$employee->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $employee->status ? 'checked' : '' }}>
                                    <span></span>
                                </label>
                            </span>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

        @else
        <h1>{{ $translations['no_employees'] ?? 'no_employees' }}</h1>
    @endif
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure' ])
@endpush
