@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-primary" href="{{ url('/') }}/{{app()->getLocale()}}/download/practice">
            {{ $translations['export_list'] ?? 'export_list' }}
        </a>

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/practices/create">
            {{ $translations['add_practice'] ?? 'add_practice' }}
        </a>

    </div>
@endsection
@section('description')
    <div class="description">{{$translations['practices_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    <div class="table-responsive">
        <table id="practicesList" class="table">
            <thead class="thead-light">
                <tr>
                    <th>ID#</th>
                    <th>{{ $translations['fachrichtung'] ?? 'fachrichtung' }}</th>
                    <th>{{ $translations['name'] ?? 'name' }}</th>
                    <th>{{ $translations['stabe_and_hausnummer'] ?? 'stabe_and_hausnummer' }}</th>
                    <th>{{ $translations['plz'] ?? 'plz' }}</th>
                    <th>{{ $translations['ort'] ?? 'ORT' }}</th>
                    <th>{{ $translations['region'] ?? 'region' }}</th>
                    <th>{{ $translations['action'] ?? 'action' }}</th>
                    <th style="display: none">{{ $translations['more_details'] ?? 'more_details' }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($practices as $practice)
                    <tr class="cd-info">
                        <td>{{$practice->id}}</td>
                        <td>{{$practice->specialization}}</td>
                        <td>{{$practice->name}}</td>
                        <td>{{$practice->street_number}}</td>
                        <td>{{$practice->postal_code}}</td>
                        <td>{{$practice->city}}</td>
                        <td>{{$practice->region_id ? $practice->region->name : '-'}}</td>
                        <td class="my_action">
                                <a class="btn btn-success my_btn1" role="button" href="{{ url('/') }}/{{app()->getLocale()}}/practices/{{$practice->id.'/edit'}}">{{ $translations['update'] ?? 'Update' }}</a>
                                <button type="button" class="btn btn-danger delete_action"
                                        data-form_action="{{ route('practices.destroy',[app()->getLocale(),$practice->id] ) }}"
                                        data-toggle="modal" data-target="#deleteModal">{{ $translations['delete'] ?? 'Delete' }}
                                </button>
                                <button type="button" class="btn btn-info cd-showmore">
                                    {{ $translations['show_details'] ?? 'show_details' }}
                                </button>
                        </td>

                        <td class="cd-additional-info">
                            <p><b>{{ $translations['standort'] ?? 'standort' }}:</b> {{$practice->site}} </p>
                            <p><b>{{ $translations['rechung_name'] ?? 'rechung_name' }}:</b> {{$practice->invoice_name}}</p>
                            <p><b>{{ $translations['rechnung_strabe_and_hausnummer'] ?? 'rechnung_strabe_and_hausnummer' }}:</b> {{$practice->invoice_street}}</p>
                            <p><b>{{ $translations['rechnung_plz'] ?? 'rechnung_plz' }}:</b> {{$practice->invoice_postal}}</p>
                            <p><b>{{ $translations['rechnung_ort'] ?? 'rechnung_ort' }}:</b> {{$practice->invoice_city}}</p>
                            <p><b>{{ $translations['tel_ruckfragen'] ?? 'tel_ruckfragen' }}:</b> {{$practice->phone}}</p>
                            <p><b>{{ $translations['email'] ?? 'email' }}:</b> {{$practice->practice_email}}</p>
                        </td>
                    </tr>


                @endforeach
            </tbody>
        </table>
    </div>

@endsection

@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script>
        $(document).ready(function () {
            let url = "{{route('practices.index',['locale'=>app()->getLocale()])}}";
            $('#practicesList').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": `${url}`,
                "language": {
                    "emptyTable": "{{ $translations['no_practice'] ?? 'No Practice'}}"
                },
                "columns": [
                    {"data": "id"},
                    {"data": "specialization"},
                    {"data": "name"},
                    {"data": "street_number"},
                    {"data": "postal_code"},
                    {"data": "city"},
                    {
                        "name": "region.name",
                        "data": "region_id",
                        "render": function (region_id, type, device) {
                            return region_id ? device.region.name : '-';
                        }
                    },
                    {
                        "className": "my_action",
                        "name": "region.name",
                        "data": "id",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id) {
                            let a = document.createElement('a');
                            let button1 = document.createElement('button');
                            let button2 = document.createElement('button');
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute("role", "button");
                            a.href = `${url}/${id}/edit`;
                            a.innerText = "{{ $translations['update'] ?? 'Update' }}";
                            button1.type = "button";
                            button1.className = "btn btn-danger delete_action";
                            button1.setAttribute("data-form_action", `${url}/${id}`);
                            button1.setAttribute("data-toggle", "modal");
                            button1.setAttribute("data-target", "#deleteModal");
                            button1.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            button2.type = "button";
                            button2.className = "btn btn-info cd-showmore";
                            button2.innerText = "{{ $translations['show_details'] ?? 'show_details' }}";
                            return `${a.outerHTML} ${button1.outerHTML} ${button2.outerHTML}`;
                        }
                    },
                    {
                        "className": "cd-additional-info",
                        "name": "id",
                        "data": "id",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id, type, practice) {
                            return `<p><b>{{ $translations['standort'] ?? 'standort' }}:</b>${practice.site}</p>` +
                                `<p><b>{{ $translations['rechung_name'] ?? 'rechung_name' }}:</b>${practice.invoice_name}</p>` +
                                `<p><b>{{ $translations['rechnung_strabe_and_hausnummer'] ?? 'rechnung_strabe_and_hausnummer' }}:</b>${practice.invoice_street}</p>` +
                                `<p><b>{{ $translations['rechnung_plz'] ?? 'rechnung_plz' }}:</b>${practice.invoice_postal}</p>` +
                                `<p><b>{{ $translations['rechnung_ort'] ?? 'rechnung_ort' }}:</b>${practice.invoice_city}</p>` +
                                `<p><b>{{ $translations['tel_ruckfragen'] ?? 'tel_ruckfragen' }}:</b>${practice.phone}</p>` +
                                `<p><b>{{ $translations['email'] ?? 'email' }}:</b>${practice.practice_email}</p>`;
                        }
                    }
                ], rowCallback: function (row) {
                    $(row).addClass('cd-info');
                },
            }).on('draw', function () {
                $(".cd-showmore").on("click", function () {
                    $(this).parent().parent().toggleClass("cd-info-open");
                    let text = $(this).text();
                    $(this).text(text === "Hide details" ? "Show details" : "Hide details");
                    $(".cd-additional-info:last").insertAfter($(this).parent().parent()).clone();
                });
            });
        });
    </script>
    @endpush

