@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['practice_creat_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                <!-- end:: Header -->
                <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                    <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                        <!-- begin:: Content -->
                        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                            <div class="kt-portlet">
                                <div class="kt-portlet__body kt-portlet__body--fit">
                                    <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
                                        <div class="kt-grid__item">

                                            <!--begin: Form Wizard Nav -->
                                            <div class="kt-wizard-v3__nav">
                                                <div class="kt-wizard-v3__nav-items">
                                                    <!--doc: Replace A tag with SPAN tag to disable the step link click -->
                                                    <div class="kt-wizard-v3__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                                        <div class="kt-wizard-v3__nav-body">
                                                            <div class="kt-wizard-v3__nav-label">
                                                                <span>1</span>{{ $translations['setup_practice'] ?? 'setup_practice' }}
                                                            </div>
                                                            <div class="kt-wizard-v3__nav-bar"></div>
                                                        </div>
                                                    </div>
                                                    <div class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                                        <div class="kt-wizard-v3__nav-body">
                                                            <div class="kt-wizard-v3__nav-label">
                                                                <span>2</span>{{ $translations['invoice'] ?? 'invoice' }}
                                                            </div>
                                                            <div class="kt-wizard-v3__nav-bar"></div>
                                                        </div>
                                                    </div>
                                                    <div class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                                        <div class="kt-wizard-v3__nav-body">
                                                            <div class="kt-wizard-v3__nav-label">
                                                                <span>3</span>{{ $translations['review_and_submit'] ?? 'review_and_submit' }}
                                                            </div>
                                                            <div class="kt-wizard-v3__nav-bar"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--end: Form Wizard Nav -->
                                        </div>
                                        <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">

                                            <!--begin: Form Wizard Form-->
                                            <form method="POST" class="kt-form" id="kt_form" action="{{ url('/') }}/{{app()->getLocale()}}/practices">
                                             @csrf
                                                <fieldset class="field1">
                                            <!--begin: Form Wizard Step 1-->
                                                    <div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                        <div class="kt-heading kt-heading--md">{{ $translations['setup_you_location'] ?? 'setup_you_location' }}</div>
                                                        <div class="kt-form__section kt-form__section--first">
                                                            <div class="kt-wizard-v3__form">
                                                                <div class="col-xl-12">
                                                                    <div class="kt-section__body">
                                                                        <div class="form-group row">
                                                                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['name'] ?? 'name' }}</label>
                                                                            <div class="col-lg-9 col-xl-9">
                                                                                <input class="form-control" id="name" type="text" name="name_of_the_practice" value="{{ old('name_of_the_practice') }}" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['street_and_number'] ?? 'street_and_number' }}</label>
                                                                            <div class="col-lg-9 col-xl-9">
                                                                                <input class="form-control" id="street" type="text" name="street_and_number" value="{{ old('street_and_number') }}" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['practice_phone'] ?? 'practice_phone' }}</label>
                                                                            <div class="col-lg-9 col-xl-9">
                                                                                <input class="form-control" type="text" name="practice_phone_number" value="{{ old('practice_phone_number') }}" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['postal_code'] ?? 'postal_code' }}</label>
                                                                            <div class="col-lg-9 col-xl-9">
                                                                                <input class="form-control" id="phone" type="text" name="postel_code" value="{{ old('postel_code') }}" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['city'] ?? 'city' }}</label>
                                                                            <div class="col-lg-9 col-xl-9">
                                                                                <input class="form-control" id="city" type="text" name="city" value="{{ old('city') }}" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['specialization'] ?? 'specialization' }}</label>
                                                                            <div class="col-lg-9 col-xl-9">
                                                                                <input class="form-control" type="text" name="specialization" value="{{ old('specialization') }}" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['site'] ?? 'site' }}</label>
                                                                            <div class="col-lg-9 col-xl-9">
                                                                                <input class="form-control" type="text" name="site" value="{{ old('site') }}" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['email'] ?? 'email' }}</label>
                                                                            <div class="col-lg-9 col-xl-9">
                                                                                <input class="form-control" type="email" name="practice_email" value="{{ old('practice_email') }}" required>
                                                                            </div>
                                                                        </div>
                                                                        @if(!Auth::user()->region_id)
                                                                        <div class="form-group row">
                                                                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['region'] ?? 'region' }}</label>
                                                                            <div class="col-lg-9 col-xl-9">
                                                                                <select class="form-control" name="region_id">
                                                                                    @foreach($practice as $practic)
                                                                                        <option value="{{$practic->id}}">{{$practic->name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!--end: Form Wizard Step 1-->

                                                    <!--begin: Form Wizard Step 2-->
                                                    <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                                                        <div class="kt-heading kt-heading--md">{{ $translations['enter_invoice_data'] ?? 'enter_invoice_data' }}</div>
                                                        <div class="kt-form__section kt-form__section--first">
                                                            <div class="form-group row">
                                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['same_as_in_practice'] ?? 'same_as_in_practice' }}</label>
                                                                <div class="col-lg-9 col-xl-9">
                                                                    <div class="kt-checkbox-single">
                                                                        <label class="kt-checkbox">
                                                                            <input class="form-control" type="checkbox" value="{{ old('check') }}" name="check">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="kt-wizard-v3__form aaa">
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['invoice_name'] ?? 'invoice_name' }}</label>
                                                                    <div class="col-lg-9 col-xl-9">
                                                                        <input class="form-control" id="inv_name" type="text" name="invoice_name" value="{{ old('invoice_name') }}" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['invoice_street'] ?? 'invoice_street' }}</label>
                                                                    <div class="col-lg-9 col-xl-9">
                                                                        <input class="form-control" id="inv_street" type="text" name="invoice_street_and_number" value="{{ old('invoice_street_and_number') }}" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['invoice_postal'] ?? 'invoice_postal' }}</label>
                                                                    <div class="col-lg-9 col-xl-9">
                                                                        <input class="form-control" id="inv_postal" type="text" name="invoice_postal_code" value="{{ old('invoice_postal_code') }}" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['invoice_city'] ?? 'invoice_city' }}</label>
                                                                    <div class="col-lg-9 col-xl-9">
                                                                        <input class="form-control" id="inv_city" type="text" name="invoice_city" value="{{ old('invoice_city') }}" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end: Form Wizard Step 2-->
                                                </fieldset>
                                                 <!--begin: Form Wizard Step 2-->
                                                <fieldset class="field2">
                                                 <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                                                     <div class="kt-heading kt-heading--md">{{ $translations['review_and_submit'] ?? 'review_and_submit' }}</div>
                                                     <div class="kt-form__section kt-form__section--first">
                                                         <div class="kt-wizard-v3__form">
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['name'] ?? 'name' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['street_and_number'] ?? 'street_and_number' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['practice_phone'] ?? 'practice_phone' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['postal_code'] ?? 'postal_code' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['city'] ?? 'city' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['specialization'] ?? 'specialization' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['site'] ?? 'site' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['email'] ?? 'email' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['region'] ?? 'region' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <input class="form-control" type="hidden" readonly="readonly">
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['invoice_name'] ?? 'invoice_name' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['invoice_street'] ?? 'invoice_street' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['invoice_postal'] ?? 'invoice_postal' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                 <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['invoice_city'] ?? 'invoice_city' }}</label>
                                                                 <div class="col-lg-9 col-xl-9">
                                                                     <input class="form-control" type="text" readonly="readonly">
                                                                 </div>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                                </fieldset>
                                                 <!--end: Form Wizard Step 2-->

                                                <!--begin: Form Actions -->
                                                <div class="kt-form__actions">
                                                    <button class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
                                                        {{ $translations['previous'] ?? 'previous' }}
                                                    </button>
                                                    <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
                                                        {{ $translations['create'] ?? 'Create' }}
                                                    </button>
                                                    <button class="btn review btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next">
                                                        {{ $translations['next_step'] ?? 'next_step' }}
                                                    </button>
                                                </div>

                                                <!--end: Form Actions -->
                                            </form>

                                            <!--end: Form Wizard Form-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end:: Content -->
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
