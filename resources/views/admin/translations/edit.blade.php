@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['trans_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/translations/{{$translation->id}}">
                    @csrf
                    {{method_field('PUT')}}
                    <!--begin: Form Wizard Step 1-->
                        <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                             data-ktwizard-state="current">
                            <div class="kt-section kt-section--first">
                                <div class="kt-wizard-v4__form">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="kt-section__body">
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">En</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <input class="form-control" value="{{$translation->en}}" type="text" name="en" required>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">De</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <input class="form-control" value="{{$translation->de}}" type="text" name="de" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end: Form Wizard Step 1-->

                        <!--begin: Form Actions -->
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                                {{ $translations['update'] ?? 'update' }}
                            </button>
                        </div>

                        <!--end: Form Actions -->
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection