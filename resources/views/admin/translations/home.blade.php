@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">
        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/translations/create">
            {{ $translations['add_translation'] ?? 'add_translation' }}
        </a>
    </div>
@endsection
@section('description')
    <div class="description">{{$translations['trans_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
        <div class="table-responsive">
            <table id="datatableTranslations" class="table">
                <thead class="thead-light">
                    <tr>
                        <td>Code</td>
                        <td>en</td>
                        <td>de</td>
                        <td>{{ $translations['action'] ?? 'action' }}</td>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script defer>
        $(document).ready(function () {
            let url = "{{route('translations.index',['locale'=>app()->getLocale()])}}";
            $('#datatableTranslations').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": `${url}`,
                "language": {
                    "emptyTable": "{{ $translations['no_translations'] ?? 'no_translations' }}"
                },
                "columns": [
                    {"data": "short_code"},
                    {"data": "en"},
                    {"data": "de"},
                    {
                        "name": "id",
                        "data": "id",
                        "className": "my_action",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id) {
                            let a = document.createElement('a');
                            let button = document.createElement('button');
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute("role", "button");
                            a.href = `${url}/${id}/edit`;
                            a.innerText = "{{ $translations['edit'] ?? 'edit' }}";
                            button.type = "button";
                            button.className = "btn btn-danger delete_action";
                            button.setAttribute("data-form_action", `${url}/${id}`);
                            button.setAttribute("data-toggle", "modal");
                            button.setAttribute("data-target", "#deleteModal");
                            button.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            return `${a.outerHTML} ${button.outerHTML}`;
                        }
                    },
                ]
            });
        });
    </script>
@endpush
