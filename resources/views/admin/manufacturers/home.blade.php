@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-primary" href="{{ url('/') }}/{{app()->getLocale()}}/download/manufacturers">
            {{ $translations['export_list'] ?? 'export_list' }}
        </a>

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/manufacturers/create">
            {{ $translations['add_manufacturer'] ?? 'add_manufacturer' }}
        </a>

    </div>
@endsection
@section('description')
    <div class="description">{{$translations['manufacturers_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
        <div class="table-responsive">
            <table id="datatableManufacturers" class="table">
                <thead class="thead-light">
                <tr>
                    <th>{{ $translations['name'] ?? 'name' }}</th>
                    <th>{{ $translations['action'] }}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script defer>
        $(document).ready(function () {
            let url = "{{route('manufacturers.index',['locale'=>app()->getLocale()])}}";
            $('#datatableManufacturers').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": `${url}`,
                "language": {
                    "emptyTable": "{{ $translations['no_manufacturer'] ?? 'no_manufacturer' }}"
                },
                "columns": [
                    {
                        "data": "name",
                    },
                    {
                        "name": "id",
                        "data": "id",
                        "className": "my_action",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id) {
                            let a = document.createElement('a');
                            let button = document.createElement('button');
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute("role", "button");
                            a.href = `${url}/${id}/edit`;
                            a.innerText = "{{ $translations['edit'] ?? 'edit' }}";
                            button.type = "button";
                            button.className = "btn btn-danger delete_action";
                            button.setAttribute("data-form_action", `${url}/${id}`);
                            button.setAttribute("data-toggle", "modal");
                            button.setAttribute("data-target", "#deleteModal");
                            button.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            return `${a.outerHTML} ${button.outerHTML}`;
                        }
                    },
                ]
            });
        });
    </script>
@endpush
