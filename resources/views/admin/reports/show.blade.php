@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['reports_home_desc'] ?? 'reports'}}</div>
@endsection
@section('content')
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                <!-- end:: Header -->
                <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch"
                     id="kt_body">
                    <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                         id="kt_content">
                        <!-- begin:: Content -->
                        <div class="table-responsive">
                            <table id="reportTable" class="table">
                                <thead class="thead-light">
                                <tr>
                                    <th>{{ $translations['id'] ?? 'ID' }}</th>
                                    <th>{{ $translation['type']??'Type'}}</th>
                                    <th>{{ $translations['practice_name'] ?? 'Practice Name' }}</th>
                                    <th>{{ $translations['device_name'] ?? 'Device Name' }}</th>
                                    <th>{{ $translations['date'] ?? 'CreatedAt' }}</th>
                                    <th>{{ $translations['action'] ?? 'Action' }}</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('modal')
        @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
    @endpush
    @push('scripts')
        <script>
            //$('#reportTable').DataTable();
            $('#reportTable').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": "{{route('report.show',['locale'=>app()->getLocale()])}}",
                "language": {
                    "emptyTable": "{{ $translations['no_reports'] ?? 'No Reports'}}"
                },
                "columns": [
                    {"data": "id"},
                    {"data": "type"},
                    {
                        "name": "practice.name",
                        "data": "practice",
                        "render": function (practice) {
                            return practice ? practice.name:"-";
                        }
                    },
                    {
                        "name": "device.name",
                        "data": "device",
                        "render": function (device) {
                            return device ? device.name:"-";
                        }
                    },
                    {
                        "data": "created_at",
                    },
                    {
                        "name": "id",
                        "data": "id",
                        "searchable": false,
                        "orderable": false,
                        "className":"my_action",
                        "render": function (id) {
                            let url = "{{url('/')."/".app()->getLocale()}}";
                            let a = document.createElement('a');
                            let button = document.createElement('button');
                            let form = document.createElement('form');
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute('role', 'button');
                            a.href = url + "/report/" + id + "/edit";
                            a.innerText = "{{$translations['update'] ?? 'Update'}}";
                            button.type = "button";
                            button.className = "btn btn-danger delete_action";
                            button.setAttribute("data-form_action", url + "/report/" + id + "/delete");
                            button.setAttribute("data-toggle", "modal");
                            button.setAttribute("data-target", "#deleteModal");
                            button.innerText = "{{$translations['delete'] ?? 'Delete'}}";
                            form.action = url + "/report/" + id + "/pdf-download";
                            form.method = "POST";
                            form.className = "d-inline-block";
                            form.innerHTML = "<input type='hidden' name='_token' value='{{ @csrf_token() }}' />"
                                + "<button class='btn btn-primary' type='submit'>" + "{{ $translations['download_file'] ?? 'download_file' }}" + "</button>";
                            return a.outerHTML + " " + button.outerHTML + " " + form.outerHTML;
                        }
                    },
                ],
            });
        </script>
    @endpush
@endsection
