<div class="table-responsive">
    <table id="datatable" class="table">
        <thead class="thead-light">
        <tr>
            <th>{{ $translation['dev_types']??'Devices Types'}}</th>
            <th>{{ $translations['practice_name'] ?? 'Practice Name' }}</th>
            <th>{{ $translations['device_name'] ?? 'Device Name' }}</th>
            <th>{{ $translations['date'] ?? 'CreatedAt' }}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$report->deviceType()}}</td>
            <td>{{$report->practices()->name}}</td>
            <td>{{$report->devices()->name}}</td>
            <td>{{$report->created_at}}</td>
        </tr>
        </tbody>
    </table>
</div>
