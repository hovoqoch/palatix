<div class="kt-heading kt-heading--md">Angaben zu den
    Umgebungsbedingungen
</div>

<div class="kt-form__section kt-form__section--first">
    <div class="kt-wizard-v3__form">
        <div class="col-xl-12">
            <div class="kt-section__body">
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Umgebungstemperatur
                        des RDG gemäß Angaben des Herstellers
                        eingehalten?</label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control @error("report_data.$data.rdg") is-invalid @enderror" name="report_data[{{$data}}][rdg]">
                            <option @if((old('report_data.'.$data.'.rdg')==='yes')||(!empty($update) && $update['rdg']==='yes')) selected @endif value="yes">Yes</option>
                            <option @if((old('report_data.'.$data.'.rdg')==='no')||(!empty($update) && $update['rdg']==='no')) selected @endif value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Raumtemperatur
                        in °C:
                    </label>
                    <div class="col-lg-9 col-xl-9">
                                <textarea class="@error("report_data.$data.temperature") is-invalid border-danger @enderror" name="report_data[{{$data}}][temperature]" rows="7" style="width: 100%">{{$update['temperature']??old('report_data.'.$data.'.temperature')}}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Luftfeuchtigkeit
                        in %:</label>
                    <div class="col-lg-9 col-xl-9">
                                <textarea class="@error("report_data.$data.humidity") is-invalid border-danger @enderror" name="report_data[{{$data}}][humidity]" rows="7" style="width: 100%">{{$update['humidity']??old('report_data.'.$data.'.humidity')}}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">
                        Mechanische Raumbelüftung / räumliche
                        Trennung vorhanden?
                    </label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control @error("report_data.$data.ventilation") is-invalid @enderror"
                                name="report_data[{{$data}}][ventilation]">
                            <option @if((old('report_data.'.$data.'.ventilation')==='yes')||(!empty($update) && $update['ventilation']==='yes')) selected @endif value="yes">Yes</option>
                            <option @if((old('report_data.'.$data.'.ventilation')==='no')||(!empty($update) && $update['ventilation']==='no')) selected @endif value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">
                        Gibt es Prüfberichte des Raumlüftung?
                    </label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control @error("report_data.$data.test_report") is-invalid @enderror"
                                name="report_data[{{$data}}][test_report]">
                            <option @if((old('report_data.'.$data.'.test_report')==='yes')||(!empty($update) && $update['test_report']==='yes')) selected @endif value="yes">Yes</option>
                            <option @if((old('report_data.'.$data.'.test_report')==='no')||(!empty($update) && $update['test_report']==='no')) selected @endif value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label is-invalid">Bemerkungen</label>
                    <div class="col-lg-9 col-xl-9 ">
                        <textarea class="@error("report_data.$data.remarks_2") is-invalid border-danger @enderror" name="report_data[{{$data}}][remarks_2]" style="width: 100%;" rows="7">{{$update['remarks_2']??old('report_data.'.$data.'.remarks_2')}}</textarea>
                    </div>
                </div>
                <h3>Raumtemperatur- und Luftfeuchte-Messung</h3>
                <div class="mb-5">
                    <p>Information:</p>

                    „Bei Ankunft die Sensoren sofort aufstellen,
                    die Messung findet bei der Validierzeit
                    durchgehend statt.

                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Bemerkungen</label>
                    <div class="col-lg-9 col-xl-9">
                        <textarea class="@error("report_data.$data.analysis") is-invalid border-danger @enderror" name="report_data[{{$data}}][analysis]" style="width: 100%;" rows="7">{{$update['analysis']??old('report_data.'.$data.'.analysis')}}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Attach
                        document</label>

                    <div class="col-lg-9 col-xl-9">

                        <input type="file"
                               class="custom-file-input @error("report_data.$data.photo_1") is-invalid @enderror"
                               id="photo_1" name="report_data[{{$data}}][photo_1]"
                               value="" accept="image/*">
                        <label class="custom-file-label"
                               for="photo_1">Choose file</label>

                    </div>
                </div>

                <h3 class="mb-5">Raumtemperatur- und Luftfeuchte-Messung</h3>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Bemerkungen</label>
                    <div class="col-lg-9 col-xl-9">
                        <textarea class="@error("report_data.$data.evaluation") is-invalid border-danger @enderror" name="report_data[{{$data}}][evaluation]" style="width: 100%;" rows="7">{{$update['evaluation']??old('report_data.'.$data.'.evaluation')}}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Attach
                        document</label>

                    <div class="col-lg-9 col-xl-9">

                        <input type="file"
                               class="custom-file-input @error("report_data.$data.photo_2") is-invalid border-danger @enderror"
                               id="photo_2" name="report_data[{{$data}}][photo_2]"
                               value="" accept="image/*">
                        <label class="custom-file-label"
                               for="photo_2">Choose file</label>

                    </div>
                </div>
                <h3 class="mb-5">Raumtemperatur- und Luftfeuchte-Messung</h3>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Bemerkungen</label>
                    <div class="col-lg-9 col-xl-9">
                        <textarea class="@error("report_data.$data.measurement") is-invalid border-danger @enderror" name="report_data[{{$data}}][measurement]" style="width: 100%;" rows="7">{{$update['measurement']??old('report_data.'.$data.'.measurement')}}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Attach
                        document</label>

                    <div class="col-lg-9 col-xl-9">

                        <input type="file"
                               class="custom-file-input @error("report_data.$data.photo_3") is-invalid border-danger @enderror"
                               id="photo_3" name="report_data[{{$data}}][photo_3]"
                               value="" accept="image/*">
                        <label class="custom-file-label"
                               for="photo_3">Choose file</label>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

