<h3>Instandhaltung bei Beschädigungen an Gerät und
    Ausrüstung</h3>
<p class="mb-4">Beschädigungen an Geräten und Ausrüstung sind zu
    beheben.</p>
<p class="mb-4">Insbesondere bei Beschädigungen wie Risse in der
    Verkleidung besteht nicht nur Verletzungsgefahr sondern
    zusätzlich Infektionsgefahr,
    da eine Reinigung und Desinfektion an diesen Stellen nicht
    gewährleistet werden kann.</p>
<p class="mb-4">Daher muss eine Beseitigung dieses Mangels
    umgehend erfolgen:</p>
<div
    class="kt-heading kt-heading--md">Instandhaltung bei
    Beschädigungen an Gerät und Ausrüstung
</div>
<div class="kt-form__section kt-form__section--first">
    <div class="kt-wizard-v3__form">
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Sind
                Beschädigungen an den Aufbereitungsgeräten
                vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <select class="form-control @error("report_data.$data.damage_processing") is-invalid border-danger @enderror"
                        name="report_data[{{$data}}][damage_processing]">
                    <option @if((old('report_data.'.$data.'.damage_processing')==='yes')||(!empty($update) && $update['damage_processing']==='yes')) selected @endif value="yes">Yes</option>
                    <option @if((old('report_data.'.$data.'.damage_processing')==='no')||(!empty($update) && $update['damage_processing']==='no')) selected @endif value="no">No</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Sind
                Beschädigungen an den Ausrüstungen
                vorhanden (z.B. Arbeitsplatte, Schränke,
                Schubladen)?</label>
            <div class="col-lg-9 col-xl-9">
                <select class="form-control @error("report_data.$data.damages_equipment") is-invalid border-danger @enderror"
                        name="report_data[{{$data}}][damages_equipment]">
                    <option @if((old('report_data.'.$data.'.damages_equipment')==='yes')||(!empty($update) && $update['damages_equipment']==='yes')) selected @endif value="yes">Yes</option>
                    <option @if((old('report_data.'.$data.'.damages_equipment')==='no')||(!empty($update) && $update['damages_equipment']==='no')) selected @endif value="no">No</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label
                class="col-xl-3 col-lg-3 col-form-label"></label>
            <div class="col-lg-9 col-xl-9">
                <textarea class="@error("report_data.$data.damages_remark") is-invalid border-danger @enderror" name="report_data[{{$data}}][damages_remark]" style="width: 100%;" rows="7">{{$update['damages_remark']??old('report_data.'.$data.'.damages_remark')}}</textarea>
            </div>
        </div>
    </div>
</div>

