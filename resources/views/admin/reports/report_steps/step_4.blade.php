<div class="kt-heading kt-heading--md">Ausstattung</div>
<div class="kt-form__section kt-form__section--first">
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">Handwaschbecken
            unreiner Bereich vorhanden?</label>

        <div class="col-lg-9 col-xl-9">
            <select class="form-control @error("report_data.$data.washbasin") is-invalid border-danger @enderror" name="report_data[{{$data}}][washbasin]">
                <option @if((old('report_data.'.$data.'.washbasin')==='yes')||(!empty($update) && $update['washbasin']==='yes')) selected @endif value="yes">Yes</option>
                <option @if((old('report_data.'.$data.'.washbasin')==='no')||(!empty($update) && $update['washbasin']==='no')) selected @endif value="no">No</option>
            </select>
        </div>

    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">Wandspender
            für Seife und Desinfektion?</label>
        <div class="col-lg-9 col-xl-9">

            <select class="form-control @error("report_data.$data.dispensers") is-invalid border-danger @enderror" name="report_data[{{$data}}][dispensers]">
                <option @if((old('report_data.'.$data.'.dispensers')==='yes')||(!empty($update) && $update['dispensers']==='yes')) selected @endif value="yes">Yes</option>
                <option @if((old('report_data.'.$data.'.dispensers')==='no')||(!empty($update) && $update['dispensers']==='no')) selected @endif value="no">No</option>
            </select>

        </div>
    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">Ultraschallbad
            vorhanden?</label>

        <div class="col-lg-9 col-xl-9">
            <select class="form-control @error("report_data.$data.dispensers_manufacturer") is-invalid border-danger @enderror"
                    name="report_data[{{$data}}][dispensers_manufacturer]">
                @foreach($manufacturers as $manufacturer)
                    <option @if(((int)old('report_data.'.$data.'.dispensers_manufacturer')===$manufacturer->id)||(!empty($update) && (int)$update['dispensers_manufacturer']===$manufacturer->id)) selected @endif
                    value="{{$manufacturer->id}}">{{$manufacturer->name}}</option>
                @endforeach
            </select>
        </div>

    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label"></label>
        <div class="col-lg-9 col-xl-9">
            <select class="form-control @error("report_data.$data.dispensers_type") is-invalid border-danger @enderror"
                    name="report_data[{{$data}}][dispensers_type]">
                @foreach($types as $type)
                    <option @if(((int)old('report_data.'.$data.'.dispensers_type')===$type->id)||(!empty($update) && (int)$update['dispensers_type']===$type->id)) selected @endif
                        value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label"></label>
        <div class="col-lg-9 col-xl-9">
            <textarea class="@error("report_data.$data.ultrasonic") is-invalid border-danger @enderror" name="report_data[{{$data}}][ultrasonic]" style="width: 100%;" rows="7">{{$update['ultrasonic']??old('report_data.'.$data.'.ultrasonic')}}</textarea>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">Werden
            Routineprüfungen nach Arbeitsanweisung
            durchgeführt?</label>


        <div class="col-lg-9 col-xl-9">
            <select class="form-control @error("report_data.$data.instructions") is-invalid border-danger @enderror"
                    name="report_data[{{$data}}][instructions]">
                <option @if((old('report_data.'.$data.'.instructions')==='yes')||(!empty($update) && $update['instructions']==='yes')) selected @endif value="yes">Yes</option>
                <option @if((old('report_data.'.$data.'.instructions')==='no')||(!empty($update) && $update['instructions']==='no')) selected @endif value="no">No</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">Siegelgerät
            vorhanden?</label>

        <div class="col-lg-9 col-xl-9">
            <select class="form-control @error("report_data.$data.instructions_manufacturer") is-invalid border-danger @enderror"
                    name="report_data[{{$data}}][instructions_manufacturer]">
                @foreach($manufacturers as $manufacturer)
                    <option @if(((int)old('report_data.'.$data.'.instructions_manufacturer')===$manufacturer->id)||(!empty($update) && (int)$update['instructions_manufacturer']===$manufacturer->id)) selected @endif
                    value="{{$manufacturer->id}}">{{$manufacturer->name}}</option>
                @endforeach
            </select>
        </div>

    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label"></label>
        <div class="col-lg-9 col-xl-9">
            <select class="form-control @error("report_data.$data.instructions_type") is-invalid border-danger @enderror"
                    name="report_data[{{$data}}][instructions_type]">
                @foreach($types as $type)
                    <option @if(((int)old('report_data.'.$data.'.instructions_type')===$type->id)||(!empty($update) && (int)$update['instructions_type']===$type->id)) selected @endif
                        value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label"></label>
        <div class="col-lg-9 col-xl-9">
            <textarea class="@error("report_data.$data.instructions_text") is-invalid border-danger @enderror" name="report_data[{{$data}}][instructions_text]" style="width: 100%;" rows="7">{{$update['instructions_text']??old('report_data.'.$data.'.instructions_text')}}</textarea>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">Ist
            der Siegelprozess validiert?</label>
        <div class="col-lg-9 col-xl-9">
            <div class="input-group">
                <input value="{{$update['sealing_process']??old('report_data.'.$data.'.sealing_process')}}" type="text"
                       class="form-control datatable @error("report_data.$data.sealing_process") is-invalid border-danger @enderror"
                       id="kt_datepicker_2" readonly=""
                       name="report_data[{{$data}}][sealing_process]" >
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label
            class="col-xl-3 col-lg-3 col-form-label"></label>
        <div class="col-lg-9 col-xl-9">

            <textarea class="@error("report_data.$data.sealing_report") is-invalid border-danger @enderror" name="report_data[{{$data}}][sealing_report]" style="width: 100%;" rows="7">{{$update['sealing_report']??old('report_data.'.$data.'.sealing_report')}}</textarea>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">Werden
            Routinekontrollen durchgeführt z.B.
            Sealcheck)?</label>
        <div class="col-lg-9 col-xl-9">
            <div class="col-lg-9 col-xl-9">
                <select class="form-control @error("report_data.$data.performed") is-invalid border-danger @enderror"
                        name="report_data[{{$data}}][performed]">
                    <option @if((old('report_data.'.$data.'.performed')==='yes')||(!empty($update) && $update['performed']==='yes')) selected @endif value="yes">Yes</option>
                    <option @if((old('report_data.'.$data.'.performed')==='no')||(!empty($update) && $update['performed']==='no')) selected @endif value="no">No</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">DAC
            Universal vorhanden?</label>

        <div class="col-lg-9 col-xl-9">
            <select class="form-control @error("report_data.$data.dac_manufacturer") is-invalid border-danger @enderror"
                    name="report_data[{{$data}}][dac_manufacturer]">
                @foreach($manufacturers as $manufacturer)
                    <option @if(((int)old('report_data.'.$data.'.dac_manufacturer')===$manufacturer->id)||(!empty($update) && (int)$update['dac_manufacturer']===$manufacturer->id)) selected @endif
                    value="{{$manufacturer->id}}">{{$manufacturer->name}}</option>
                @endforeach
            </select>
        </div>

    </div>
    <div class="form-group row">
        <label
            class="col-xl-3 col-lg-3 col-form-label"></label>
        <div class="col-lg-9 col-xl-9">
            <select class="form-control @error("report_data.$data.dac_type") is-invalid border-danger @enderror"
                    name="report_data[{{$data}}][dac_type]">
                @foreach($types as $type)
                    <option @if(((int)old('report_data.'.$data.'.dac_type')===$type->id)||(!empty($update) && (int)$update['dac_type']===$type->id)) selected @endif
                        value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">Ist
            der Siegelprozess validiert?</label>
        <div class="col-lg-9 col-xl-9">
            <div class="input-group">
                <input value="{{$update['dac_date']??old('report_data.'.$data.'.dac_date')}}" type="text"
                       class="form-control datatable @error("report_data.$data.dac_date") is-invalid border-danger @enderror"
                       id="kt_datepicker_2" readonly=""
                       name="report_data[{{$data}}][dac_date]">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">DAC
            Universal vorhanden?</label>

        <div class="col-lg-9 col-xl-9">
            <select class="form-control @error("report_data.$data.sterilizer_manufacturer") is-invalid border-danger @enderror"
                    name="report_data[{{$data}}][sterilizer_manufacturer]">
                @foreach($manufacturers as $manufacturer)
                    <option @if(((int)old('report_data.'.$data.'.sterilizer_manufacturer')===$manufacturer->id)||(!empty($update) && (int)$update['sterilizer_manufacturer']===$manufacturer->id)) selected @endif
                        value="{{$manufacturer->id}}">{{$manufacturer->name}}</option>
                @endforeach
            </select>
        </div>

    </div>
    <div class="form-group row">
        <label
            class="col-xl-3 col-lg-3 col-form-label"></label>
        <div class="col-lg-9 col-xl-9">
            <select class="form-control @error("report_data.$data.sterilizer_sterilizer") is-invalid border-danger @enderror"
                    name="report_data[{{$data}}][sterilizer_sterilizer]">
                @foreach($types as $type)
                    <option @if(((int)old('report_data.'.$data.'.sterilizer_sterilizer')===$type->id)||(!empty($update) && (int)$update['sterilizer_sterilizer']===$type->id)) selected @endif
                        value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label">Ist
            der Sterilisationsprozess validiert?</label>
        <div class="col-lg-9 col-xl-9">
            <div class="input-group">
                <input value="{{$update['sterilization_process']??old('report_data.'.$data.'.sterilization_process')}}" type="text"
                       class="form-control datatable @error("report_data.$data.sterilizer_process") is-invalid border-danger @enderror"
                       id="kt_datepicker_2" readonly=""
                       name="report_data[{{$data}}][sterilization_process]">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label
            class="col-xl-3 col-lg-3 col-form-label"></label>
        <div class="col-lg-9 col-xl-9">
            <textarea class="@error("report_data.$data.sterilization_process_text") is-invalid border-danger @enderror" name="report_data[{{$data}}][sterilization_process_text]" rows="7" style="width: 100%;" >{{$update['sterilization_process_text']??old('report_data.'.$data.'.sterilization_process_text')}}</textarea>
        </div>
    </div>
    <div class="form-group row">
        <label
            class="col-xl-3 col-lg-3 col-form-label"></label>
        <div class="col-lg-9 col-xl-9">
            <textarea class="@error("report_data.$data.sterilization_process_remark") is-invalid border-danger @enderror" name="report_data[{{$data}}][sterilization_process_remark]" rows="7" style="width: 100%;">{{$update['sterilization_process_remark']??old('report_data.'.$data.'.sterilization_process_remark')}}</textarea>
        </div>
    </div>


</div>

