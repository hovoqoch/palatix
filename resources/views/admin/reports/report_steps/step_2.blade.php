<div class="kt-heading kt-heading--md">Baulich/räumliche
    Situation
</div>
<div class="kt-form__section kt-form__section--first">
    <div class="kt-wizard-v3__form">
        <div class="col-xl-12">
            <div class="kt-section__body">
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Aufbereitungsraum
                        vorhanden?</label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control @error("report_data.$data.room") is-invalid @enderror"
                                name="report_data[{{$data}}][room]" >
                            <option @if((old('report_data.'.$data.'.room')==='yes')||(!empty($update) && $update['room']==='yes')) selected @endif value="yes">Yes</option>
                            <option @if((old('report_data.'.$data.'.room')==='no')||(!empty($update) && $update['room']==='no')) selected @endif value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Räumliche
                        Trennung unrein/rein?</label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control @error("report_data.$data.spatial") is-invalid border-danger @enderror"
                                name="report_data[{{$data}}][spatial]">
                            <option @if((old('report_data.'.$data.'.spatial')==='yes')||(!empty($update) && $update['spatial']==='yes')) selected @endif value="yes">Yes</option>
                            <option @if((old('report_data.'.$data.'.spatial')==='no')||(!empty($update) && $update['spatial']==='no')) selected @endif value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Bauliche
                        Trennung unrein/rein?</label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control @error("report_data.$data.structural") is-invalid border-danger @enderror"
                                name="report_data[{{$data}}][structural]">

                            <option @if((old('report_data.'.$data.'.structural')==='yes')||(!empty($update) && $update['structural']==='yes')) selected @endif value="yes">Yes</option>
                            <option @if((old('report_data.'.$data.'.structural')==='no')||(!empty($update) && $update['structural']==='no')) selected @endif value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Schleuse
                        unrein/rein?</label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control @error("report_data.$data.lock") is-invalid border-danger @enderror"
                                name="report_data[{{$data}}][lock]">
                            <option @if((old('report_data.'.$data.'.lock')==='yes')||(!empty($update) && $update['lock']==='yes')) selected @endif value="yes">Yes</option>
                            <option @if((old('report_data.'.$data.'.lock')==='no')||(!empty($update) && $update['lock']==='no')) selected @endif value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label
                        class="col-xl-3 col-lg-3 col-form-label">Bemerkungen</label>
                    <div class="col-lg-9 col-xl-9">
                        <textarea class="@error("report_data.$data.remarks") is-invalid border-danger @enderror" name="report_data[{{$data}}][remarks]" rows="7" style="width: 100%;">{{$update['remarks'] ?? old('report_data.'.$data.'.remarks')}}</textarea>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

