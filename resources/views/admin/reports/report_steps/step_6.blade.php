<div class="kt-heading kt-heading--md">Installationsqualifikation (IQ)</div>
<div class="kt-form__section kt-form__section--first">
    <div class="kt-wizard-v3__form">
        <h3>Allgemeine Angaben</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Datum:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_1']??old("report_data.$data.field_1")}}" type="text"
                           class="form-control @error("report_data.$data.field_1") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_1]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Name und Anschrift des Betreibers:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_2']??old("report_data.$data.field_2")}}" type="text"
                           class="form-control @error("report_data.$data.field_2") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_2]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Fachrichtung::</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_3']??old("report_data.$data.field_3")}}" type="text"
                           class="form-control @error("report_data.$data.field_3") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_3]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Kundennummer::</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_4']??old("report_data.$data.field_4")}}" type="text"
                           class="form-control @error("report_data.$data.field_4") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_4]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Berichtsnummer:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_5']??old("report_data.$data.field_5")}}" type="text"
                           class="form-control @error("report_data.$data.field_5") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_5]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Personen die für die Aufbereitung von Medizinprodukten
                verantwortlich sind:</label>
            <div class="col-lg-9 col-xl-9">
                <select class="form-control @error("report_data.$data.field_6") is-invalid border-danger @enderror"
                        name="report_data[{{$data}}][field_6]">
                    <option @if((old('report_data.'.$data.'.field_6')==="yes")||(!empty($update) && $update['field_6']==="yes")) selected @endif value="yes">Yes</option>
                    <option @if((old('report_data.'.$data.'.field_6')==="no")||(!empty($update) && $update['field_6']==="no")) selected @endif value="no">No</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Anlass der Prüfung:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_7']??old("report_data.$data.field_7")}}" type="text"
                           class="form-control @error("report_data.$data.field_7") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_7]">
                </div>
            </div>
        </div>
        <h3>Identifikation Reinigungs-Desinfektions-Gerät</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Hersteller
                Anschrift des Herstellers:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_8']??old("report_data.$data.field_8")}}" type="text"
                           class="form-control @error("report_data.$data.field_8") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_8]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Typ:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_9']??old("report_data.$data.field_9")}}" type="text"
                           class="form-control @error("report_data.$data.field_9") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_9]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Gerätenummer:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_10']??old("report_data.$data.field_10")}}" type="text"
                           class="form-control @error("report_data.$data.field_10") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_10]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Baujahr:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_11']??old("report_data.$data.field_11")}}" type="text"
                           class="form-control @error("report_data.$data.field_11") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_11]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Software version:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_12']??old("report_data.$data.field_12")}}" type="text"
                           class="form-control @error("report_data.$data.field_12") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_12]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Software parameter:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_13']??old("report_data.$data.field_13")}}" type="text"
                           class="form-control @error("report_data.$data.field_13") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_13]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Standort des Geräts:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_14']??old("report_data.$data.field_14")}}" type="text"
                           class="form-control @error("report_data.$data.field_14") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_14]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Bezeichnung beim Betreiber:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_15']??old("report_data.$data.field_15")}}" type="text"
                           class="form-control @error("report_data.$data.field_15") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_15]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Erstinbetriebnahme:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_16']??old("report_data.$data.field_16")}}" type="text"
                           class="form-control @error("report_data.$data.field_16") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_16]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Datum der letzten Validierung:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_17']??old("report_data.$data.field_17")}}" type="text"
                           class="form-control @error("report_data.$data.field_17") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_17]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Gültiger Wartungsnachweis vorhanden::</label>
            <div class="col-lg-9 col-xl-9">
                        {{-- field_6 change to field_18  --}}
                <select class="form-control @error("report_data.$data.field_18") is-invalid border-danger @enderror"
                        name="report_data[{{$data}}][field_18]">
                    <option @if((old('report_data.'.$data.'.field_18')==='yes')||(isset($update['field_18']) && $update['field_18']==='yes')) selected @endif value="yes">Ja</option>
                    <option @if((old('report_data.'.$data.'.field_18')==='no')||(isset($update['field_18']) && $update['field_18']==='no')) selected @endif value="no">No</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Gültiger Wartungsnachweis vorhanden datum:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_19']??old("report_data.$data.field_19")}}" type="text"
                           class="form-control @error("report_data.$data.field_19") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_19]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Besonderheiten:</label>
            <div class="col-lg-9 col-xl-9">
                <textarea
                    class="@error("report_data.$data.field_20") is-invalid border-danger @enderror"
                name="report_data[{{$data}}][field_20]"
                style="width: 100%;"
                rows="7">{{$update['field_20']??old('report_data.'.$data.'.field_20')}}</textarea>
            </div>
        </div>
        <h3>Prüfung der Unterlagen</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Gebrauchsanweisung vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_21']??old("report_data.$data.field_21")}}" type="text"
                           class="form-control @error("report_data.$data.field_21") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_21]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Liegen die Beschreibungen über eine Typprüfung nach EN
                ISO 15883 vor?
                :</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_22']??old("report_data.$data.field_22")}}" type="text"
                           class="form-control @error("report_data.$data.field_22") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_22]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Wurde die Reinigungswirkung bei der Typprüfung nach der
                EN ISO 15883 geprüft?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_23']??old("report_data.$data.field_23")}}" type="text"
                           class="form-control @error("report_data.$data.field_23") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_23]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Konformitätserklärung
                (CE – Kennzeichnung) vorhanden?
            </label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_24']??old("report_data.$data.field_24")}}" type="text"
                           class="form-control @error("report_data.$data.field_24") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_24]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Werksprüfungsprotokoll vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_25']??old("report_data.$data.field_25")}}" type="text"
                           class="form-control @error("report_data.$data.field_25") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_25]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">GBA des Zubehörs vorhanden (z.B. Drucker)?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_26']??old("report_data.$data.field_26")}}" type="text"
                           class="form-control @error("report_data.$data.field_26") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_26]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Beschreibung der Installationsvoraussetzung
                vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_27']??old("report_data.$data.field_27")}}" type="text"
                           class="form-control @error("report_data.$data.field_27") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_27]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Einweisungsprotokoll / Installationsprotokoll?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_28']??old("report_data.$data.field_28")}}" type="text"
                           class="form-control @error("report_data.$data.field_28") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_28]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Wartungsplan / Gerätebuch vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_29']??old("report_data.$data.field_29")}}" type="text"
                           class="form-control @error("report_data.$data.field_29") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_29]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Gültige Sicherheits Technische Kontrolle nach VDE 0701-1
                vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_30']??old("report_data.$data.field_30")}}" type="text"
                           class="form-control @error("report_data.$data.field_30") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_30]">
                </div>
            </div>
        </div>
        <h3>Reinigungs – Desinfektions – Gerät Zubehör</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">VE-Wasser vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_31']??old("report_data.$data.field_31")}}" type="text"
                           class="form-control @error("report_data.$data.field_31") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_31]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Universalwagen mit Injektorschiene?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_32']??old("report_data.$data.field_32")}}" type="text"
                           class="form-control @error("report_data.$data.field_32") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_32]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Beladungsträger für dentale Instrumente vor Ort
                geeignet?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_33']??old("report_data.$data.field_33")}}" type="text"
                           class="form-control @error("report_data.$data.field_33") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_33]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Bemerkungen:</label>
            <div class="col-lg-9 col-xl-9">
                <textarea
                    name="report_data[{{$data}}][field_34]"
                    class="@error("report_data.$data.field_34") is-invalid border-danger @enderror"
                    style="width: 100%;"
                    rows="7">{{$update['field_34']??old("report_data.$data.field_34")}}</textarea>
            </div>
        </div>
        <h3>Programme</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Programm-Name:</label>
            <div class="col-lg-9 col-xl-9">
                <select class="form-control @error("report_data.$data.field_35") is-invalid border-danger @enderror"
                        name="report_data[{{$data}}][field_35]">
                    <option @if((old('report_data.'.$data.'.field_35')==='1')||(!empty($update) && $update['field_35']==='1')) selected @endif value="1">Universal Programm</option>
                    <option @if((old('report_data.'.$data.'.field_35')==='2')||(!empty($update) && $update['field_35']==='2')) selected @endif value="2">Universal Programm</option>
                </select>
            </div>
        </div>
        <h3>Verwendeter Prozess-Medien</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Prozess-Medien:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_36']??old("report_data.$data.field_36")}}" type="text"
                           class="form-control @error("report_data.$data.field_36") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_36]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Universal Programm:</label>

            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_37']??old("report_data_repeat.$data.field_37")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_37") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_37]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Neutralisator:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_38']??old("report_data_repeat.$data.field_38")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_38") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_38]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Klarspüler:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_39']??old("report_data.$data.field_39")}}" type="text"
                           class="form-control @error("report_data.$data.field_39") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_39]">
                </div>
            </div>
        </div>

        <div class="kt-heading kt-heading--md">Organisatorische Rahmenbedingungen</div>
        <h3>Standartarbeitsanweisungen</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Risikobewertung der MP nach RKI-Empfehlung oder
                DGSV?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_40']??old("report_data.$data.field_40")}}" type="text"
                           class="form-control @error("report_data.$data.field_40") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_40]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Sind Herstellerangaben zur Aufbereitung
                vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_41']??old("report_data_repeat.$data.field_41")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_41") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_41]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Entsorgung?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_42']??old("report_data_repeat.$data.field_42")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_42") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_42]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Entsorgungsart definiert?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_43']??old("report_data_repeat.$data.field_43")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_43") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_43]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Max. Antrocknungszeit definiert?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_44']??old("report_data_repeat.$data.field_44")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_44") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_44]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Vorbehaltung</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_45']??old("report_data_repeat.$data.field_45")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_45") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_45]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Transport kontaminierter MP?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_46']??old("report_data_repeat.$data.field_46")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_46") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_46]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Ultraschallreinigung?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_47']??old("report_data_repeat.$data.field_47")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_47") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_47]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Manuelle Vorreinigung?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_48']??old("report_data_repeat.$data.field_48")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_48") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_48]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">RD-Gerät?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_49']??old("report_data_repeat.$data.field_49")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_49") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_49]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Beladevorschriften RDG?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_50']??old("report_data_repeat.$data.field_50")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_50") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_50]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Programmwahl?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_51']??old("report_data_repeat.$data.field_51")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_51") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_51]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Kontrolle?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_52']??old("report_data_repeat.$data.field_52")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_52") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_52]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Freigabekriterien?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_53']??old("report_data.$data.field_53")}}" type="text"
                           class="form-control @error("report_data.$data.field_53") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_53]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Vorgangsweise bei Nichtkonformität mit
                Freigabekriterien?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_54']??old("report_data_repeat.$data.field_54")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_54") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_54]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Vorgangsweise bei Störungen?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_55']??old("report_data_repeat.$data.field_55")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_55") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_55]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Funktionskontrolle?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_56']??old("report_data_repeat.$data.field_56")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_56") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_56]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Pflege?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_57']??old("report_data_repeat.$data.field_57")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_57") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_57]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Verpackung(en)?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_58']??old("report_data_repeat.$data.field_58")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_58") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_58]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Sterilisator(en)?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_59']??old("report_data_repeat.$data.field_59")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_59") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_59]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Beladevorschriften Sterilisator?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_60']??old("report_data_repeat.$data.field_60")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_60") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_60]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Programmwahl?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_61']??old("report_data_repeat.$data.field_61")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_2") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_61]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Kontrolle?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_62']??old("report_data_repeat.$data.field_62")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_62") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_62]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Freigabekriterien?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_63']??old("report_data.$data.field_63")}}" type="text"
                           class="form-control @error("report_data.$data.field_63") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_63]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Vorgangsweise bei Nichtkonformität mit
                Freigabekriterien?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_64']??old("report_data_repeat.$data.field_64")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_64") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_64]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Vorgangsweise bei Störungen?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_65']??old("report_data_repeat.$data.field_65")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_65") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_65]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Transport steriler MP?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_66']??old("report_data_repeat.$data.field_66")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_66") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_66]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Lagerung?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_67']??old("report_data_repeat.$data.field_67")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_67") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_67]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Lagerfristen?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_68']??old("report_data_repeat.$data.field_68")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_68") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_68]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Behandlung von Sondergütern (Hep.)?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_69']??old("report_data.$data.field_69")}}" type="text"
                           class="form-control @error("report_data.$data.field_69") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_69]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Bei den aufgelisteten Arbeitsanweisungen wurde eine
                Sichtprüfung auf Vorhandensein durchgeführt.</label>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Bemerkungen:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_70']??old("report_data.$data.field_70")}}" type="text"
                           class="form-control @error("report_data.$data.field_70") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_70]">
                </div>
            </div>
        </div>
        <h3>Allgemeine Hygiene</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Hygieneplan (-ordner) vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_71']??old("report_data_repeat.$data.field_71")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_71") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_71]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Reinigungs- und Desinfektionsplan vorhanden??</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_72']??old("report_data_repeat.$data.field_72")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_72") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_72]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Ist dieser aktuell??</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_73']??old("report_data_repeat.$data.field_73")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_73") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_73]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Sind die Präparate vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_74']??old("report_data_repeat.$data.field_74")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_74") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_74]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Gelistete Präparate?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_75']??old("report_data_repeat.$data.field_75")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_75") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_75]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Anweisung zur persönlichen Hygiene??</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_76']??old("report_data_repeat.$data.field_76")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_76") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_76]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Anweisung zur Händehygiene?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_77']??old("report_data.$data.field_77")}}" type="text"
                           class="form-control @error("report_data.$data.field_77") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_77]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Bei dem aufgelisteten Hygieneplan mit Inhalt wurde eine
                Sichtprüfung auf Vorhandensein durchgeführt.</label>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Bemerkungen:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_78']??old("report_data.$data.field_78")}}" type="text"
                           class="form-control @error("report_data.$data.field_78") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_78]">
                </div>
            </div>
        </div>
        <h3>RG-Gerät</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Werden Routinekontrollen durchgeführt?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_79']??old("report_data_repeat.$data.field_79")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_79") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_79]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Hinsichtlich Reinigung?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_80']??old("report_data.$data.field_80")}}" type="text"
                           class="form-control @error("report_data.$data.field_80") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_80]">
                    <input value="{{$update['field_81']??old("report_data.$data.field_81")}}" type="text"
                           class="form-control @error("report_data.$data.field_81") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_81]">
                    <input value="{{$update['field_82']??old("report_data_repeat.$data.field_82")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_82") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_82]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Hinsichtlich Desinfektion?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_83']??old("report_data_repeat.$data.field_83")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_83") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_83]">
                </div>
                <div class="input-group">
                    <input value="{{$update['field_84']??old("report_data_repeat.$data.field_84")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_84") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_84]">
                </div>
                <div class="input-group">
                    <input value="{{$update['field_85']??old("report_data.$data.field_85")}}" type="text"
                           class="form-control @error("report_data.$data.field_85") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_85]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Aufzeigen der Übereinstimmung mit den Spezifikationen
                für den Desinfektionszyklus?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_86']??old("report_data_repeat.$data.field_86")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_86") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_86]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Wird eine Chargendokumentation geführt?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_87']??old("report_data_repeat.$data.field_87")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_87") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_87]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Bemerkungen:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_78']??old("report_data.$data.field_78")}}" type="text"
                           class="form-control @error("report_data.$data.field_78") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_78]">
                </div>
            </div>
        </div>
        <h3>Dokumentationen</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Flächendesinfektion?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_88']??old("report_data_repeat.$data.field_88")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_88") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_88]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Routinekontrolle RDG?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_89']??old("report_data_repeat.$data.field_89")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_89") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_89]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Störungen (Geräte)?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_90']??old("report_data_repeat.$data.field_90")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_90") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_90]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Wartungen (Benutzer)?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_91']??old("report_data_repeat.$data.field_91")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_91") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_91]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Wartungen extern (Servicetechniker)?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_92']??old("report_data_repeat.$data.field_92")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_92") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_92]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Bemerkungen:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_93']??old("report_data.$data.field_93")}}" type="text"
                           class="form-control @error("report_data.$data.field_93") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_93]">
                </div>
            </div>
        </div>
        <h3>Personalschutz</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Sind Sicherheitsdatenblätter der verwendeten Chemikalien
                vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_94']??old("report_data.$data.field_94")}}" type="text"
                           class="form-control @error("report_data.$data.field_94") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_94]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Existieren Merkblätter zum Verhalten bei Zwischenfällen
                mit biol. Arbeitsstoffen?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_95']??old("report_data_repeat.$data.field_95")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_95") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_95]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Ist persönliche Schutzausrichtung vorhanden?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_96']??old("report_data_repeat.$data.field_96")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_96") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_96]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Stichhemmende Handschuhe?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_97']??old("report_data_repeat.$data.field_97")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_97") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_97]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Übermäntel?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_98']??old("report_data_repeat.$data.field_98")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_98") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_98]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Schürzen?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_99']??old("report_data_repeat.$data.field_99")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_99") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_99]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Mund-Nasenschutz?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_100']??old("report_data_repeat.$data.field_100")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_100") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_100]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Schutzbrille?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_101']??old("report_data_repeat.$data.field_101")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_101") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_101]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Bemerkungen?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_102']??old("report_data.$data.field_102")}}" type="text"
                           class="form-control @error("report_data.$data.field_102") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_102]">
                </div>
            </div>
        </div>
        <h3>Qualifikation/Schulung des Personals</h3>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Existiert ein Schulungsplan?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_103']??old("report_data_repeat.$data.field_103")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_103") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_103]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Ist dieser aktuell?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_104']??old("report_data_repeat.$data.field_104")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_104") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_104]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Hygieneschulung?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_105']??old("report_data_repeat.$data.field_105")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_105") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_105]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Personalschutzschulung?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_106']??old("report_data_repeat.$data.field_106")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_106") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_106]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Geräteschulung / -einweisung?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_107']??old("report_data_repeat.$data.field_107")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_107") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_107]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Interne Schulungen zu den Arbeitsanweisungen?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_108']??old("report_data.$data.field_108")}}" type="text"
                           class="form-control @error("report_data.$data.field_108") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_108]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">• Teilnehmer?</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_109']??old("report_data_repeat.$data.field_109")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_109") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_109]">
                    <input value="{{$update['field_110']??old("report_data_repeat.$data.field_110")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_110") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_110]">
                    <input value="{{$update['field_111']??old("report_data_repeat.$data.field_111")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_111") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_111]">
                    <input value="{{$update['field_112']??old("report_data_repeat.$data.field_112")}}" type="text"
                           class="form-control @error("report_data_repeat.$data.field_112") is-invalid border-danger @enderror"
                           name="report_data_repeat[{{$data}}][field_112]">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Bemerkungen:</label>
            <div class="col-lg-9 col-xl-9">
                <div class="input-group">
                    <input value="{{$update['field_113']??old("report_data.$data.field_113")}}" type="text"
                           class="form-control @error("report_data.$data.field_113") is-invalid border-danger @enderror"
                           name="report_data[{{$data}}][field_113]">
                </div>
            </div>
        </div>
    </div>
</div>

