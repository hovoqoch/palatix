
    <div
        class="kt-heading kt-heading--md">{{ $translations['chose_practice_device'] ?? 'chose_practice_device' }}</div>
    <div class="kt-form__section kt-form__section--first">
        <div class="kt-wizard-v3__form">
            <div class="col-xl-12">
                <div class="kt-section__body">
                    <div class="form-group row">
                        <label
                            class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_practice'] ?? 'Select Practice'}}</label>
                        <div class="col-lg-9 col-xl-9">
                            <select class="form-control"
                                    id="practice" name="practice_id"
                                    required>
                                <option value="" disabled
                                        selected>{{ $translations['select_practice'] ?? 'Select Practice'}}</option>
                                @foreach($practices as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row select_type_block" style="display: none">
                        <label
                                class="col-xl-3 col-lg-3 col-form-label">{{ $translations['choose_type'] ?? 'Choose Type'}}</label>
                        <div class="col-lg-9 col-xl-9">
                            <select class="form-control"
                                    id="report_type" name="report_type"
                                    required>
                                <option value="" disabled
                                        selected>{{ $translations['select_type'] ?? 'Select Type'}}</option>
                                <option value="site">Site</option>
                                <option value="device">Device</option>
                            </select>
                        </div>
                    </div>
                    <div class="add__device__step--second">
                        <div class="form-group row">
                            <label
                                class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_device'] ?? 'Select Device'}}</label>
                            <div class="col-lg-9 col-xl-9">
                                <select required id="devices"
                                        multiple
                                        class="form-control"
                                        name="device_id[]">

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="add__site__step--second">
                        <div class="form-group row">
                            <label
                                    class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_site'] ?? 'Select Site'}}</label>
                            <div class="col-lg-9 col-xl-9">
                                <select required id="sites"
                                        multiple
                                        class="form-control"
                                        name="site_id[]">

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

