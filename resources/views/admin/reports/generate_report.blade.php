@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['generate_report_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    {{-- <div class="kt-grid kt-grid--hor kt-grid--root">
         <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
             <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                 <!-- end:: Header -->
                 <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch"
                      id="kt_body">
                     <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                          id="kt_content">
                         <!-- begin:: Content -->
                         <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                             <div class="kt-portlet">
                                 <div class="kt-portlet__body kt-portlet__body--fit">
                                     <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3"
                                          data-ktwizard-state="step-first">
                                         <div class="kt-grid__item">

                                             <!--begin: Form Wizard Nav -->
                                             <div class="kt-wizard-v3__nav">
                                                 <div class="kt-wizard-v3__nav-items">
                                                     <!--doc: Replace A tag with SPAN tag to disable the step link click -->
                                                     @php $i = 1 @endphp
                                                     @foreach($report_data as $data)
                                                         <div class="kt-wizard-v3__nav-item" data-ktwizard-type="step"
                                                         >
                                                             <div class="kt-wizard-v3__nav-body">
                                                                 <div class="kt-wizard-v3__nav-label">
                                                                     <span>{{$i}}</span>{{ $translations['step'] ?? 'Step' }} {{$i}}
                                                                 </div>
                                                                 <div class="kt-wizard-v3__nav-bar"></div>
                                                             </div>
                                                         </div>
                                                         @php $i ++ @endphp
                                                     @endforeach
                                                     <div class="kt-wizard-v3__nav-item" data-ktwizard-type="step"
                                                     >
                                                         <div class="kt-wizard-v3__nav-body">
                                                             <div class="kt-wizard-v3__nav-label">
                                                                 <span>{{$i}}</span>{{ $translations['step'] ?? 'Step' }} {{$i}}
                                                             </div>
                                                             <div class="kt-wizard-v3__nav-bar"></div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <!--end: Form Wizard Nav -->
                                         </div>
                                         <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">

<<<<<<< HEAD
                                            <!--begin: Form Wizard Form-->
                                            <form method="POST" class="kt-form" id="kt_form"
                                                  action="{{ route('report.store',app()->getLocale()) }}">
                                                @csrf
                                                @php $j = 1 @endphp
                                                @foreach($report_data as $data)
                                                    <fieldset class="field{{$j}}">
                                                        <div class="kt-wizard-v3__content"
                                                             data-ktwizard-type="step-content">
                                                             @include('admin.reports.report_steps.step_2',$data)
                                                             @include('admin.reports.report_steps.step_3',$data)
                                                             @include('admin.reports.report_steps.step_4',$data)
                                                             @include('admin.reports.report_steps.step_5',$data)
                                                         </div>
                                                     </fieldset>
                                                     @php $j++ @endphp
                                                 @endforeach
                                                 <fieldset class="field{{$j}}">
                                                     <div class="kt-wizard-v3__content"
                                                          data-ktwizard-type="step-content">
                                                         @include('admin.reports.report_steps.step_6')

                                                     </div>

                                                 </fieldset>

                                                 <!--begin: Form Actions -->
                                                 <div class="kt-form__actions">
                                                     <button class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
                                                         {{ $translations['create'] ?? 'Create' }}
                                                     </button>
                                                     <button
                                                         class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                                         data-ktwizard-type="action-prev">
                                                         {{ $translations['previous'] ?? 'previous' }}
                                                     </button>

                                                     <button
                                                         class="btn review btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                                         data-ktwizard-type="action-next">
                                                         {{ $translations['next_step'] ?? 'next_step' }}
                                                     </button>
                                                 </div>

                                                 <!--end: Form Actions -->
                                             </form>

                                             <!--end: Form Wizard Form-->
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <!-- end:: Content -->
                     </div>
                 </div>

             </div>
         </div>
     </div>--}}
    {{--{{ dd(Session::get('admin_id'), Session::get('device_ids')) }}--}}

    @php
        $check_type = 1; // if $check_type = 0 we are generating report for site(s) else for device(s)
        $check_type = Session::get('device_ids') ? 0 : 1;
    @endphp
    @if($check_type)
        @php $data_ids = Session::get('site_ids') @endphp
    @else
        @php $data_ids = Session::get('device_ids') @endphp
    @endif
    <div class="container">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                @php $i = 1 @endphp
                @foreach($data_ids as $data)

                    <div class="stepwizard-step">
                        <a href="#step-{{$i}}" type="button" class="btn btn-primary btn-circle">{{$i}}</a>
                        <p>Step {{$i}}</p>
                    </div>
                    @php $i ++ @endphp
                @endforeach
                {{--  <div class="stepwizard-step">
                      <a href="#step-{{$i}}" type="button" class="btn btn-primary btn-circle">{{$i}}</a>
                      <p>Step {{$i}}</p>
                  </div>--}}
            </div>
        </div>

        <form method="POST" action="{{url('/') }}/{{app()->getLocale()}}/reports/store" enctype="multipart/form-data">
            @empty($update)
                <input name="update" type="hidden" value="false">
            @else
                <input name="update" type="hidden" value="{{$update->id}}">
            @endempty
            @csrf
            @php $j = 1 @endphp
            @if(!$check_type)
                @foreach($data_ids as $data)
                    <div class="row setup-content" id="step-{{$j}}">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3> Step {{$j}}</h3>
                                @include('admin.reports.report_steps.step_2', ['data' => $data,'update' => empty($update)?[]:$update->getData()])
                                @include('admin.reports.report_steps.step_3',['data' => $data,'update' => empty($update)?[]:$update->getData()])
                                @include('admin.reports.report_steps.step_4',['data' => $data,'update' => empty($update)?[]:$update->getData()])
                                @include('admin.reports.report_steps.step_5',['data' => $data,'update' => empty($update)?[]:$update->getData()])
                                @if(!$loop->last)
                                    <button class="btn btn-primary nextBtn float-right"
                                            type="button">{{ $translations['next_step'] ?? 'next_step' }}</button>
                                @else
                                    <button class="btn btn-success float-right" type="submit">
                                        @empty($update)
                                            {{ $translations['create'] ?? 'Create' }}
                                        @else
                                            {{ $translations['update'] ?? 'Update' }}
                                        @endempty
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    @php $j++ @endphp
                @endforeach
            @else
                @foreach($data_ids as $data)
                    <div class="row setup-content" id="step-{{$j}}">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3> Step {{$j}}</h3>
                                @include('admin.reports.report_steps.step_6',['data' => $data,'update' => empty($update)?[]:$update->getData()])
                                @if (!$loop->last)
                                    <button class="btn btn-primary nextBtn float-right"
                                            type="button">{{ $translations['next_step'] ?? 'next_step' }}</button>
                                @else
                                    <button class="btn btn-success  float-right" type="submit">
                                        @empty($update)
                                            {{ $translations['create'] ?? 'Create' }}
                                        @else
                                            {{ $translations['update'] ?? 'Update' }}
                                        @endempty
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    @php $j++ @endphp
                @endforeach
            @endif
        </form>
    </div>
@endsection
