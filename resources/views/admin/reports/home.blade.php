@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['reports_home_desc'] ?? 'reports'}}</div>
@endsection
@section('content')
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                <!-- end:: Header -->
                <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch"
                     id="kt_body">
                    <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                         id="kt_content">
                        <!-- begin:: Content -->
                        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                            <div class="kt-portlet">
                                <div class="kt-portlet__body kt-portlet__body--fit">
                                    <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3"
                                         data-ktwizard-state="step-first">
                                        <div class="kt-grid__item">

                                            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                                                <form method="POST" class="kt-form"
                                                      action="{{ url('/') }}/{{app()->getLocale()}}/reports/generate">
                                                    @csrf
                                                @include('admin.reports.report_steps.step_1')
                                                <!--begin: Form Actions -->

                                                        <button type="submit"
                                                                class="btn btn-success  float-right"
                                                               >
                                                            {{ $translations['continue'] ?? 'Continue' }}
                                                        </button>

                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
