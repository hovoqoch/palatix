@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['admin_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/admin/{{$admin->id}}">
                        @csrf
                        {{method_field('PUT')}}
                        <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                            <div class="kt-heading kt-heading--md">{{ $translations['enter_details'] ?? 'enter_details' }}</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v3__form">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['region'] ?? 'region' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <select class="form-control" name="region_id">
                                                @foreach($regions as $region)
                                                    <option @if((int)$admin->region_id === (int)$region->id) {{ "selected" }} @endif value="{{$region->id}}">{{$region->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['first_name'] ?? 'first_name' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" value="{{$admin->name}}" name="name" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['last_name'] ?? 'last_name' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" value="{{$admin->last_name}}" name="last_name" required>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-last row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['email'] ?? 'email' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" value="{{$admin->email}}" name="email" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 mt-5 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['update'] ?? 'Update' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

