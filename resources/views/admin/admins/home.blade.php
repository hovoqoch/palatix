@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-primary" href="{{ url('/') }}/{{app()->getLocale()}}/download/{{$type}}">
            {{ $translations['export'] ?? 'export' }} {{$type}} {{ $translations['list'] ?? 'list' }}
        </a>

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/create{{$type}}">
            {{ $translations['add_new'] ?? 'add_new' }} {{$type}}
        </a>

    </div>
@endsection
@section('description')
    <div class="description">{{$translations['admin_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    <div class="table-responsive">
        <table id="datatableUsers" class="table">
            <thead class="thead-light">
            <tr>
                <th>ID#</th>
                <th>{{ $translations['name'] ?? 'name' }}</th>
                <th>{{ $translations['email'] ?? 'email' }}</th>
                <th>{{ $translations['region'] ?? 'region' }}</th>
                <th>{{ $translations['status'] ?? 'status' }}</th>
                <th>{{ $translations['action'] ?? 'action' }}</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script defer>
        $(document).ready(function () {
            let url = "{{route('admins.index',['locale'=>app()->getLocale()])}}";
            $('#datatableUsers').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": `${url}`,
                "language": {
                    "emptyTable": "{{ $translations['no_admin'] ?? 'No Admin' }}"
                },
                "columns": [
                    {
                        "data": "id",
                    },
                    {
                        "name": "name",
                        "data": "name",
                        "render": function (name, type, data) {
                            return `${name} ${data.last_name}`;
                        }
                    },
                    {
                        "data": "email",
                    },
                    {
                        "name":"region.name",
                        "data": "region",
                        "render": function (region) {
                            return region ? region.name : "-";
                        }
                    },
                    {
                        "name":"status",
                        "data": "status",
                        "render": function (status) {
                            return status ? "{{$translations['active'] ?? 'active'}}" : "{{$translations['passive'] ?? 'passive'}}";
                        }
                    },
                    {
                        "name": "id",
                        "data": "id",
                        "className": "my_action",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id) {
                            let a = document.createElement('a');
                            let button = document.createElement('button');
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute("role", "button");
                            a.href = `{{url('/',[app()->getLocale()])}}/{{$type}}/${id}/edit`;
                            a.innerText = "{{ $translations['update'] ?? 'Update' }}";
                            button.type = "button";
                            button.className = "btn btn-danger delete_action";
                            button.setAttribute("data-form_action", `{{url('/',[app()->getLocale()])}}/users/${id}`);
                            button.setAttribute("data-toggle", "modal");
                            button.setAttribute("data-target", "#deleteModal");
                            button.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            return `${a.outerHTML} ${button.outerHTML}`;
                        }
                    },
                ],rowCallback: function (row) {
                    $(row).addClass('cd-info');
                },
            });
        });
    </script>
@endpush
