@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/elearning/create">
            {{ $translations['add_new_test'] ?? 'Add new Test' }}
        </a>

    </div>
@endsection
@section('description')
    <div class="description">{{$translations['eleraning_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="table-responsive">
        <table id="datatableElearningTest" class="table">
            <thead class="thead-light">
            <tr>
                <th>{{ $translations['name'] ?? 'Name' }}</th>
                <th>{{ $translations['summary'] ?? 'Summary' }}</th>
                <th>{{ $translations['devices'] ?? 'Devices' }}</th>
                <th>{{ $translations['practice'] ?? 'Practice' }}</th>
                <th>{{ $translations['youtube_video'] ?? 'Youtube video' }}</th>
                <th>{{ $translations['vimeo_video'] ?? 'Vimeo video' }}</th>
                <th>{{ $translations['attached_document'] ?? 'Attached document' }}</th>
                <th>{{ $translations['action'] }}</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['del_tes'] ?? 'Delete Test'])
@endpush
@push('scripts')
    <script defer>
        $(document).ready(function () {
            let url = "{{route('elearning.index',['locale'=>app()->getLocale()])}}";
            $('#datatableElearningTest').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": `${url}`,
                "language": {
                    "emptyTable": "{{ $translations['no_test'] ?? 'No Tests Yet' }}"
                },
                "columns": [
                    {"data": "name",},
                    {"data": "summary",},
                    {"data": "device.name"},
                    {"data": "practice.name"},
                    {
                        "data": "youtube_link",
                        "render": function (youtube) {
                            let a = document.createElement("a");
                            a.href = youtube;
                            a.target = "_blank";
                            a.innerText = youtube;
                            return a.outerHTML;
                        }
                    },
                    {
                        "data": "vimeo_link",
                        "render": function (vimeo) {
                            let a = document.createElement("a");
                            a.href = vimeo;
                            a.target = "_blank";
                            a.innerText = vimeo;
                            return a.outerHTML;
                        }
                    },
                    {
                        "name": "attach_file_name",
                        "data": "attach_file_name",
                        "render": function (attach) {
                            let element;
                            if (attach) {
                                element = document.createElement('a');
                                element.className = "btn btn-primary";
                                element.href = `${url}/download/${attach}`;
                                element.innerText = "Download PDF";
                                element = element.outerHTML;
                            } else {
                                element = "{{$translations['no_attachment'] ?? 'no_attachment'}}";
                            }
                            return element;
                        }
                    },
                    {
                        "name": "id",
                        "data": "id",
                        "className": "my_action",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id) {
                            let a = document.createElement('a');
                            let questions = document.createElement('a');
                            let button = document.createElement('button');
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute("role", "button");
                            a.href = `${url}/${id}/edit`;
                            a.innerText = "{{ $translations['edit'] ?? 'edit' }}";
                            button.type = "button";
                            button.className = "btn btn-danger delete_action";
                            button.setAttribute("data-form_action", `${url}/${id}`);
                            button.setAttribute("data-toggle", "modal");
                            button.setAttribute("data-target", "#deleteModal");
                            button.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            questions.className = "btn btn-warning my_btn1";
                            questions.setAttribute("role", "button");
                            questions.href = `${url}/${id}/questions`;
                            questions.innerText = "{{ $translations['questions'] ?? 'Questions' }}";
                            return `${a.outerHTML} ${button.outerHTML} ${questions.outerHTML}`;
                        }
                    },
                ]
            });
        });
    </script>
@endpush
