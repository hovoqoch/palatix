@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['elernings_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST"
                          action="{{ url('/') }}/{{app()->getLocale()}}/elearning/{{$elearning->id}}"
                          enctype="multipart/form-data">
                        @csrf
                        {{method_field('PUT')}}

                        <div class="form-group row">

                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_practice'] ?? 'Select Practice'}}</label>

                            <div class="col-lg-9 col-xl-9">
                                <select class="form-control" id="practice" name="practice_id" required>
                                    @foreach($practices as $practice)
                                        <option
                                            {{(int)$practice->id === (int)$elearning->practice_id ? 'selected' : ''}} value="{{$practice->id}}">{{$practice->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">

                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_device'] ?? 'Select Device'}}</label>

                            <div class="col-lg-9 col-xl-9">
                                <select class="form-control" id="devices" name="device_id" required>
                                    @foreach($devices as $device)
                                        <option
                                            {{(int)$device->device_id === (int)$elearning->id ? 'selected' : ''}} value="{{$device->id}}">{{$device->device->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Select
                                User(s)</label>
                            <div class="col-lg-9 col-xl-9">
                                <select id="users" class="form-control" multiple="" name="user_id[]">
                                    @foreach($users as $user)

                                        <option
                                            {{$elearning->users->contains($user->id) ? 'selected' : ''}} value="{{$user->id}}">{{$user->name}}</option>

                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['test_name'] ?? 'Test Name'}}</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control" placeholder="Enter Test name" id="name" type="text"
                                       name="name" value="{{$elearning->name}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['test_summary'] ?? 'Test Summary'}}</label>
                            <div class="col-lg-9 col-xl-9">
                                <textarea placeholder="Enter Test summary" name="summary" class="form-control" cols="30"
                                          rows="4" required>{{$elearning->summary}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['youtube_video'] ?? 'Youtube video'}}</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control" id="youtube_link" type="url" name="youtube_link"
                                       value="{{$elearning->youtube_link}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['vimeo_video'] ?? 'Vimeo Video'}}</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control" id="vimeo_link" type="url" name="vimeo_link"
                                       value="{{$elearning->vimeo_link}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Deadline date</label>
                            <div class="col-lg-9 col-xl-9">
                                <div class="input-group">
                                    <input value="{{date("m/d/Y",strtotime($elearning->deadline_date))}}" type="text" class="form-control datatable" id="kt_datepicker_2" readonly="" name="deadline_date" required="">
                                </div>
                            </div>
                        </div>
                        @if(!$elearning->attach_file_name)
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['attach_document'] ?? 'Attach Document'}}</label>

                                <div class="col-lg-9 col-xl-9">
                                    <label class="custom-file-label" for="attach_file_name">{{ $translations['choose_file'] ?? 'Choose file'}}</label>
                                    <input type="file" class="custom-file-input" id="attach_file_name"
                                           name="attach_file_name" value="" accept="application/*">

                                </div>
                            </div>
                        @else
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['attached_document'] ?? 'Attached Document'}}</label>

                                <div class="col-lg-9 col-xl-9">

                                    <a class="btn btn-primary" target="_blank"
                                       href="{{route('elearning.file.view',['locale' => app()->getLocale(),'name' =>$elearning->attach_file_name ])}}">
                                        View File</a>
                                    <a class="btn btn-danger"

                                       href="{{route('elearning.file.delete',['locale' => app()->getLocale(),'name' =>$elearning->id ])}}">{{ $translations['delete_file'] ?? 'Delete File'}}</a>

                                </div>
                            </div>

                        @endif
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['update'] ?? 'Update' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
