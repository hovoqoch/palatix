@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-primary" href="{{ url('/') }}/{{app()->getLocale()}}/download/{{$type}}">
            {{ $translations['export'] ?? 'export' }} {{$type}} {{ $translations['list'] ?? 'list' }}
        </a>

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/create{{$type}}">
            {{ $translations['add_new'] ?? 'add_new' }} {{$type}}
        </a>

    </div>
@endsection
@section('description')
    <div class="description">{{$translations['users_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    <div class="table-responsive">
        <table id="employeesTable" class="table">
            <thead class="thead-light">
            <tr>
                <th>ID#</th>
                <th>{{ $translations['name'] ?? 'name' }}</th>
                <th>{{ $translations['email'] ?? 'email' }}</th>
                <th>{{ $translations['status'] ?? 'status' }}</th>
                <th>{{ $translations['action'] ?? 'action' }}</th>
                <th style="display: none">{{ $translations['more_details'] ?? 'more_details' }}</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script defer>
        $(document).ready(function () {
            let url = "{{route('employees.index',['locale'=>app()->getLocale()])}}";
            $('#employeesTable').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": `${url}`,
                "language": {
                    "emptyTable": "{{ $translations['no_practice'] ?? 'no_practice' }}"
                },
                "columns": [
                    {
                        "data": "id",
                    },
                    {
                        "name": "name",
                        "data": "name",
                        "render": function (name, type, data) {
                            return `${name} ${data.last_name}`;
                        }
                    },
                    {
                        "data": "email",
                    },
                    {
                        "data": "status",
                        "render": function (status) {
                            return status ? "{{$translations['active'] ?? 'active'}}" : "{{$translations['passive'] ?? 'passive'}}";
                        }
                    },
                    {
                        "name": "id",
                        "data": "id",
                        "className": "my_action",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id) {
                            let a = document.createElement('a');
                            let button1 = document.createElement('button');
                            let button2 = document.createElement('button');
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute("role", "button");
                            a.href = `{{url('/',[app()->getLocale()])}}/{{$type}}/${id}/edit`;
                            a.innerText = "{{ $translations['update'] ?? 'Update' }}";
                            button1.type = "button";
                            button1.className = "btn btn-danger delete_action";
                            button1.setAttribute("data-form_action", `{{url('/',[app()->getLocale()])}}/users/${id}`);
                            button1.setAttribute("data-toggle", "modal");
                            button1.setAttribute("data-target", "#deleteModal");
                            button1.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            button2.type = "button";
                            button2.className = "btn btn-info cd-showmore";
                            button2.innerText = "{{ $translations['show_details'] ?? 'show_details' }}";
                            return `${a.outerHTML} ${button1.outerHTML} ${button2.outerHTML}`;
                        }
                    },
                    {
                        "className": "cd-additional-info",
                        "name": "id",
                        "data": "id",
                        "searchable": false,
                        "orderable": false,
                        "render": function (id, type, user) {
                            let regionID = "{{is_null(auth()->user()->region_id)}}" !== "1";
                            let div = ``;
                            if (user.role_id === "3") {
                                div = `<p><b>{{ $translations['qualification'] ?? 'qualification' }} : </b>${regionID ? user.q_name : user.qualification ? user.qualification.name : '-'}</p>` +
                                    `<p><b>{{ $translations['doctor'] ?? 'doctor' }} : </b>${regionID ? user.d_name : user.doctor ? user.doctor.name : '-'}</p>`;
                            }
                            return `<p><b>{{ $translations['practice'] ?? 'practice' }} : </b>${regionID ? user.p_name : user.practice ? user.practice.name : '-'}</p>` +
                                `<p><b>{{ $translations['phone'] ?? 'phone' }} : </b>${user.phone}</p>` + div +
                                `<p><b>{{ $translations['last_login'] ?? 'last_login' }} : </b>${user.last_login_at ? new Date(user.last_login_at).toLocaleString() : "{{$translations['not_login_yet'] ?? 'not_login_yet'}}"}</p>`;
                        }
                    }
                ], rowCallback: function (row) {
                    $(row).addClass('cd-info');
                },
            }).on('draw', function () {
                $(".cd-showmore").on("click", function () {
                    $(this).parent().parent().toggleClass("cd-info-open");
                    let text = $(this).text();
                    $(this).text(text === "Hide details" ? "Show details" : "Hide details");
                    $(".cd-additional-info:last").insertAfter($(this).parent().parent()).clone();
                });
            });
        });
    </script>
@endpush
