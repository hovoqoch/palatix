@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['change_login_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/changelogin/store">
                        @csrf
                        {{method_field('PUT')}}
                        <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                            <div class="kt-heading kt-heading--md">{{ $translations['change_login_or_password'] ?? 'change_login_or_password' }}</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v3__form">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['login_email'] ?? 'login_email' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" value="{{$user->email}}" type="email" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['old_password'] ?? 'old_password' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="password"  name="password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['password'] ?? 'password' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" id="pwd"  type="password"  name="new_password">
                                            <div id="pwd_strength_wrap" class="pwd2">
                                                <div id="passwordDescription">{{ $translations['pass_not_ent'] ?? 'Password not entered' }}</div>
                                                <div id="passwordStrength" class="strength0"></div>
                                                <div id="pswd_info">
                                                    <strong>{{ $translations['strong_pass'] ?? 'Strong Password Tips' }}:</strong>
                                                    <ul>
                                                        <li class="invalid" id="length">{{ $translations['min_six'] ?? 'At least 6 characters' }}</li>
                                                        <li class="invalid" id="pnum">{{ $translations['one_num'] ?? 'At least one number' }}</li>
                                                        <li class="invalid" id="capital">{{ $translations['one_low_one_up'] ?? 'At least one lowercase & one uppercase letter' }}</li>
                                                        <li class="invalid" id="spchar">{{ $translations['min_one_char'] ?? 'At least one special character' }}</li>
                                                    </ul>
                                                </div><!-- END pswd_info -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['repeat_password'] ?? 'repeat_password' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" id="password" type="password"  name="new_password_confirmation">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['update'] ?? 'Update' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection