@extends('layouts.admin')
@section('meta_title', $meta_title)
{{--@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/elearning/create">
            Add new Test
        </a>

    </div>
@endsection--}}
@section('description')
    <div class="description">{{$translations['eleraning_quest_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST"
                          action="{{ url('/') }}/{{app()->getLocale()}}/elearning/{{$elearning->id}}/questions"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="form-container">
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['question'] ?? 'question' }} </label>
                                <div class="col-lg-9 col-xl-9">
                                    <input class="form-control" placeholder="Enter question" id="name" type="text"
                                           name="question" value="{{ old('question') }}" required>
                                </div>
                            </div>

                            <div class="form-group row form-answers">
                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['answer'] ?? 'answer' }} <span
                                        class="answer-number">1</span></label>
                                <div class="col-lg-6 col-xl-6">
                                <textarea placeholder="{{ $translations['enter_answer'] ?? 'enter_answer' }}" name="answer[]" class="form-control" cols="30"
                                          rows="4" required value="{{ old('answer') }}"></textarea>
                                </div>
                                <div class="col-lg-3 col-xl-3 question-section">
                                    <select class="form-control" name="type[]" required>
                                            <option value="0">{{ $translations['no'] ?? 'no' }}</option>
                                            <option value="1">{{ $translations['yes'] ?? 'yes' }}</option>
                                    </select>
                                    <div class="question-actions">
                                        <span class="font-action add-question" onclick="addSection(this)"><i class="far fa-plus-square"
                                                                                                             aria-hidden="true"></i></span>
                                        <span class="font-action remove-question" onclick="removeSection(this)"><i class="far fa-minus-square"
                                                                                                                   aria-hidden="true"></i></span>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['create'] ?? 'create' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
        <div class="table-responsive">
            <table id="tableAdminElearningQuestions" class="table">
                <thead class="thead-light">
                <tr>
                    <th>{{ $translations['question'] ?? 'question' }}</th>
                    <th>{{ $translations['answer'] ?? 'answer' }}</th>
                    <th>{{ $translations['action'] ?? 'Action' }}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    <style>
        .right_answer{
            background: greenyellow;
        }
    </style>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script>
        $(document).ready(function () {
            let url = "{{route('questions.index', ['locale' => app()->getLocale(), 'id' => $elearning->id])}}";
            $('#tableAdminElearningQuestions').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": url,
                "language": {
                    "emptyTable": "{{ $translations['no_questions'] ?? 'No Questions'}}"
                },
                "columns": [
                    {"data": "question"},
                    {
                        "searchable": false,
                        "orderable": false,
                        "data": "answers",
                        "render":function (answers) {
                            let answer = [];
                            let span = document.createElement('span');
                            $.each( answers, function( key, value ) {
                                span.className = value.type ? "{{$translations['correct_answer'] ?? 'correct_answer'}}":"{{$translations['wrong_answer'] ?? 'wrong_answer'}}";
                                span.innerText = value.answer;
                                answer.push(span.outerHTML);
                            });
                            return answer.join(' , ');
                        }
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "data": "id",
                        "className": "my_action",
                        "render": function (id) {
                            let a = document.createElement('a');
                            let button = document.createElement("button");
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute("role", "button");
                            a.href = `${url}/${id}/edit`;
                            a.innerText = "{{ $translations['edit'] ?? 'edit' }}";
                            button.type = "button";
                            button.className = "btn btn-danger delete_action";
                            button.setAttribute("data-form_action", `${url}/${id}`);
                            button.setAttribute("data-toggle", "modal");
                            button.setAttribute("data-target", "#deleteModal");
                            button.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            return `${a.outerHTML} ${button.outerHTML}`;
                        }
                    },
                ],
            });
        });


    </script>
@endpush
