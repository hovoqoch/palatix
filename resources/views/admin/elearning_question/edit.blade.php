@extends('layouts.admin')
@section('meta_title', $meta_title)
{{--@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/elearning/create">
            Add new Test
        </a>

    </div>
@endsection--}}
@section('description')
    <div class="description">{{$translations['elernings_questions_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                    <form method="POST"
                          action="{{ url('/') }}/{{app()->getLocale()}}/elearning/{{$question->elearning_id}}/questions/{{$question->id}}"
                          enctype="multipart/form-data">
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="form-container">
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['question'] ?? 'question' }} </label>
                                <div class="col-lg-9 col-xl-9">
                                    <input class="form-control" placeholder="Enter question" id="name" type="text"
                                           name="question" value="{{ $question->question }}" required>
                                </div>
                            </div>
                            @if(count($question->answers))
                                @foreach($question->answers as $key=>$answer)
                                    <div class="form-group row form-answers">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['answer'] ?? 'answer' }} <span
                                                    class="answer-number">{{$key+1}}</span></label>
                                        <div class="col-lg-6 col-xl-6">
                                        <textarea placeholder="{{ $translations['enter_answer'] ?? 'enter_answer' }}" name="answer[]" class="form-control" cols="30"
                                              rows="4" required >{{ $answer->answer }}</textarea>
                                        </div>
                                        <div class="col-lg-3 col-xl-3 question-section">
                                            <select class="form-control" name="type[]" required>
                                                <option {{ $answer->type ? '' : 'selected'}} value="0">{{ $translations['no'] ?? 'no' }}</option>
                                                <option {{ $answer->type ? 'selected' : ''}} value="1">{{ $translations['yes'] ?? 'yes' }}</option>
                                            </select>
                                            <div class="question-actions">
                                            <span class="font-action add-question" onclick="addSection(this)"><i class="far fa-plus-square"
                                                                                                             aria-hidden="true"></i></span>
                                                <span class="font-action remove-question" onclick="removeSection(this)"><i class="far fa-minus-square"
                                                                                                                           aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="form-group row form-answers">
                                    <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['answer'] ?? 'answer' }} <span
                                                class="answer-number">1</span></label>
                                    <div class="col-lg-6 col-xl-6">
                                        <textarea placeholder="{{ $translations['enter_answer'] ?? 'enter_answer' }}" name="answer[]" class="form-control" cols="30"
                                                  rows="4" required ></textarea>
                                    </div>
                                    <div class="col-lg-3 col-xl-3 question-section">
                                        <select class="form-control" name="type[]" required>
                                            <option  value="0">{{ $translations['no'] ?? 'no' }}</option>
                                            <option  value="1">{{ $translations['yes'] ?? 'yes' }}</option>
                                        </select>
                                        <div class="question-actions">
                                            <span class="font-action add-question" onclick="addSection(this)"><i class="far fa-plus-square"
                                                                                                                 aria-hidden="true"></i></span>
                                            <span class="font-action remove-question" onclick="removeSection(this)"><i class="far fa-minus-square"
                                                                                                                       aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['edit'] ?? 'edit' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
