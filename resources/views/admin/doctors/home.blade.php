@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-primary" href="{{ url('/') }}/{{app()->getLocale()}}/download/{{$type}}">
            {{ $translations['export'] ?? 'export' }} {{$type}} {{ $translations['list'] ?? 'list' }}
        </a>

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/doctors/createdoctor">
            {{ $translations['add_new_doctor'] ?? 'add_new_doctor' }}
        </a>

    </div>
@endsection
@section('description')
    <div class="description">{{$translations['doctors_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

        <div class="table-responsive">
            <table id="datatableDoctor" class="table">
                <thead class="thead-light">
                <tr>
                    <th>{{ $translations['name'] ?? 'name' }}</th>
                    <th>{{ $translations['practice'] ?? 'practice' }}</th>
                    <th>{{ $translations['action'] ?? 'Action'}}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script>
        $(document).ready(function () {
            let url = "{{route('doctors.home',['locale'=>app()->getLocale()])}}";
            $('#datatableDoctor').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "ajax": `${url}`,
                "language": {
                    "emptyTable": "{{ $translations['no_doctors'] ?? 'no_doctors' }}"
                },
                "columns": [
                    {
                        "name":"title",
                        "data": "title",
                        "render": function (title,type,data) {
                            return `${data.title} ${data.name} ${data.last_name} `;
                        }
                    },
                    {
                        "name":"p_name",
                        "data": "p_name",
                        "searchable": false,
                        "orderable": false,
                        "render": function (p_name,type,data) {
                            let practiceName = "";
                            let arr = [];
                            if ("{{is_null(auth()->user()->region_id)}}" !== "1"){
                                practiceName = p_name;
                            }else {
                                $.each(data.practices_many,function(index, value){
                                    arr.push(value.name);
                                });
                                practiceName = arr.join(',');
                            }
                            return practiceName;
                        }
                    },
                    {
                        "name":"id",
                        "data": "id",
                        "className":"my_action",
                        "searchable": false,
                        "orderable": false,
                        "render":function (id) {
                            let a =document.createElement('a');
                            let button =document.createElement('button');
                            a.className = "btn btn-success my_btn1";
                            a.setAttribute("role","button");
                            a.href =`${url}/${id}/editDoctor`;
                            a.innerText ="{{ $translations['edit'] ?? 'Edit' }}";
                            button.className = "btn btn-danger delete_action";
                            button.type = "button";
                            button.setAttribute("data-form_action",`{{ url('/',app()->getLocale())}}/users/${id}`);
                            button.setAttribute("data-toggle","modal");
                            button.setAttribute("data-target","#deleteModal");
                            button.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                            return `${a.outerHTML} ${button.outerHTML}`;
                        }
                    },
                ],
            })
        });
    </script>
@endpush
