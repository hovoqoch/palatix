@extends('layouts.login')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['create_pass_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="kt-login__body">

        <!--begin::Signin-->
        <div class="kt-login__form">
            <div class="kt-login__title">
                <h3>{{ $translations['create_password'] ?? 'create_password' }}</h3>
            </div>

            <!--begin::Form-->
            <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/doctors/createpassword/{{$user->remember_token}}/store">
                @csrf
                {{method_field('PUT')}}
                <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                    <div class="kt-form__section kt-form__section--first">
                        <div class="kt-wizard-v3__form">
                            @if(count($errors)>0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error => $message)
                                            <li>{{$message}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['password'] ?? 'Password' }}</label>
                                <div class="col-lg-9 col-xl-9">
                                    <input class="form-control" id="pwd" type="password" value="" name="password" required>
                                    <div id="pwd_strength_wrap">
                                        <div id="passwordDescription">{{ $translations['pass_not_ent'] ?? 'Password not entered' }}</div>
                                        <div id="passwordStrength" class="strength0"></div>
                                        <div id="pswd_info">
                                            <strong>{{ $translations['strong_pass'] ?? 'Strong Password Tips' }}:</strong>
                                            <ul>
                                                <li class="invalid" id="length">{{ $translations['min_six'] ?? 'At least 6 characters' }}</li>
                                                <li class="invalid" id="pnum">{{ $translations['one_num'] ?? 'At least one number' }}</li>
                                                <li class="invalid" id="capital">{{ $translations['one_low_one_up'] ?? 'At least one lowercase & one uppercase letter' }}</li>
                                                <li class="invalid" id="spchar">{{ $translations['min_one_char'] ?? 'At least one special character' }}</li>
                                            </ul>
                                        </div><!-- END pswd_info -->
                                    </div>
                                </div>
                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['confirm'] ?? 'Confirm' }}</label>
                                <div class="col-lg-9 col-xl-9">
                                    <input class="form-control"  type="password" value="" name="password_confirmation" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary btn-elevate kt-login__btn-primary">
                            {{ $translations['create'] ?? 'Create' }}
                        </button>
                    </div>
                </div>
            </form>

            <!--end::Form-->


        </div>

        <!--end::Signin-->
    </div>

@endsection