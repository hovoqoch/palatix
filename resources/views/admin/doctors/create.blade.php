@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['doctors_creat_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/doctors/storedoctor">
                        @csrf

                        <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                            <div class="kt-heading kt-heading--md">{{ $translations['enter_details'] ?? 'enter_details' }}</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v3__form">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_practice'] ?? 'select_practice' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <select class="form-control" name="practice_id[]" multiple>
                                                @foreach($practices as $practice)
                                                    <option value="{{$practice->id}}">{{$practice->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['doctors_title'] ?? 'doctors_title' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" name="title" value="{{ old('title') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['first_name'] ?? 'first_name' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" name="name" value="{{ old('name') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['last_name'] ?? 'last_name' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" name="last_name" value="{{ old('last_name') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['services'] ?? 'services' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <select class="form-control" multiple name="services[]">
                                                @foreach($services as $service)
                                                    <option value="{{$service->id}}">{{$service->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['phone'] ?? 'phone' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" name="phone" value="{{ old('phone') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['email'] ?? 'email' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" name="email" value="{{ old('email') }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['create'] ?? 'Create' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection