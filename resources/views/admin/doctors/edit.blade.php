@extends('layouts.admin')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['doctors_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('/') }}/doctors/{{$user->id}}">
                        @csrf
                        {{method_field('PUT')}}
                        <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                            <div class="kt-heading kt-heading--md">{{ $translations['enter_details'] ?? 'Enter details' }}</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v3__form">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_practice'] ?? 'Select practice' }}</label>
                                        <div class="col-lg-9 col-xl-9">

                                            @php $practice_ids = $user->practicesMany->pluck('id') @endphp
                                            <select class="form-control" name="practice_id[]" multiple>

                                                @foreach($practices as $practice)
                                                    <option {{$practice_ids->contains($practice->id) ? 'selected' : ''}}  value="{{$practice->id}}">{{$practice->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['doctors_title'] ?? 'Doctors title' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" value="{{$user->title}}" type="text" name="title" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['first_name'] ?? 'First name' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" value="{{$user->name}}" name="name" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['last_name'] ?? 'Last name' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" value="{{$user->last_name}}" name="last_name" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['services'] ?? 'services' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <?php $services_data = array(); ?>
                                            @foreach($user->services as $service)
                                                <?php array_push($services_data,$service->id) ?>
                                            @endforeach
                                            <select class="form-control" multiple name="services[]">

                                                @foreach($services as $service)
                                                    <option {{in_array($service->id,$services_data) ? 'selected' : ''}} value="{{$service->id}}">{{$service->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['phone'] ?? 'phone' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" value="{{$user->phone}}" name="phone" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['email'] ?? 'email' }}</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" type="text" value="{{$user->email}}" name="email" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['update'] ?? 'Update' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
