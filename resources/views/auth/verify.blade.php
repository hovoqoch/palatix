@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $translations['verify_your_email_address'] ?? 'verify_your_email_address' }}</div>
                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ $translations['fresh_verification_link_is_sent'] ?? 'fresh_verification_link_is_sent' }}
                        </div>
                    @endif
                        {{ $translations['before_proceeding'] ?? 'before_proceeding' }}
                        {{ $translations['if_not_receive'] ?? 'if_not_receive' }}, <a href="{{ route('verification.resend') }}">{{ $translations['click_to_request_again'] ?? 'click_to_request_again' }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
