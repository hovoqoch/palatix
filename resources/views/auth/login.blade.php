@extends('layouts.login')
@section('content')
    <div class="kt-login__body">
        <!--begin::Signin-->
        <div class="kt-login__form">
            <div class="kt-login__title">
                <h3>{{ $translations['sign_in'] ?? 'sign_in' }}</h3>
            </div>
            <!--begin::Form-->
            <form  method="POST" action="{{ route('login' , app()->getLocale()) }}">
                @csrf
                <div class="form-group">
                    <input id="email" class="form-control @error('email') is-invalid @enderror" type="email" placeholder="{{ $translations['username'] ?? 'username' }}" name="email" value="{{ old('email') }}" required autocomplete="off">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input class="form-control @error('password') is-invalid @enderror" id="password" type="password" placeholder="{{ $translations['password'] ?? 'password' }}" name="password" required autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <!--begin::Action-->
                <div class="kt-login__actions">
                    @if (Route::has('password.request'))
                        <a class="kt-link kt-login__link-forgot" href="{{ route('password.request', app()->getLocale()) }}">
                            {{ $translations['forgot_password'] ?? 'forgot_password' }}
                        </a>
                    @endif
                        <div class="kt-header__topbar kt-grid__item">

                            <!--begin: Language bar -->
                            <div class="kt-header__topbar-item kt-header__topbar-item--langs">
                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                <span class="kt-header__topbar-icon">
                                    @if(app()->getLocale() == 'en')
                                        <img class="" src="{{ url('/') }}/assets/media/flags/020-flag.svg" alt=""/>
                                    @elseif(app()->getLocale() == 'de')
                                        <img src="{{ url('/') }}/assets/media/flags/017-germany.svg" alt=""/>
                                    @endif
                                </span>
                                </div>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim">
                                    <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
                                        <li class="kt-nav__item">
                                            <a href="{{ app('App\Http\Controllers\LanguageController')->language_switcher('en') }}"
                                               class="kt-nav__link">
                                            <span class="kt-nav__link-icon"><img
                                                    src="{{ url('/') }}/assets/media/flags/020-flag.svg"
                                                    alt=""/></span>
                                                <span class="kt-nav__link-text">English</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="{{ app('App\Http\Controllers\LanguageController')->language_switcher('de') }}"
                                               class="kt-nav__link">
                                            <span class="kt-nav__link-icon"><img
                                                    src="{{ url('/') }}/assets/media/flags/017-germany.svg"
                                                    alt=""/></span>
                                                <span class="kt-nav__link-text">German</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!--end: Language bar -->

                            <!--begin: User bar -->
                            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                    <span class="kt-header__topbar-welcome">{{ $translations['hi'] ?? 'hi' }},</span>
                                    <img alt="Pic" src="{{ url('/') }}/assets/media/users/300_21.jpg" class="kt-hidden"/>
                                </div>
                                <div
                                    class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                                    <!--begin: Head -->
                                    <div class="kt-notification">
                                        <a href="{{ url('/') }}/{{app()->getLocale()}}/changelogin"
                                           class="kt-notification__item">
                                            <div class="kt-notification__item-icon">
                                                <i class="flaticon2-calendar-3 kt-font-success"></i>
                                            </div>
                                            <div class="kt-notification__item-details">
                                                <div class="kt-notification__item-title kt-font-bold">
                                                    {{ $translations['my_profile'] ?? 'my_profile' }}
                                                </div>
                                                <div class="kt-notification__item-time">
                                                    {{ $translations['account_email_password'] ?? 'account_email_password' }}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <!--end: Head -->
                                </div>
                            </div>

                            <!--end: User bar -->
                        </div>
                    <button type="submit" class="btn btn-primary btn-elevate kt-login__btn-primary">
                        {{ $translations['sign_in'] ?? 'sign_in' }}
                    </button>
                </div>
                <!--end::Action-->
            </form>
            <!--end::Form-->
        </div>
        <!--end::Signin-->
    </div>
@endsection
