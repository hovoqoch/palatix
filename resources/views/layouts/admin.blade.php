<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <base href="">
    <meta charset="utf-8"/>
    <title>{{$website_settings['web_name'] ?? 'PERLEASE MPV PORTAL'}} | @yield('meta_title')</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <link href="{{ asset('/css/admin.css') }}" rel="stylesheet"
          type="text/css"/>

    <!--end::Fonts -->

    <!--begin::Global Theme Styles(used by all pages) -->

    <!--begin:: Vendor Plugins -->
    <link href="{{ asset('/assets/plugins/general/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/tether/dist/css/tether.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/bootstrap-timepicker/css/bootstrap-timepicker.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/bootstrap-select/dist/css/bootstrap-select.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/ion-rangeslider/css/ion.rangeSlider.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/nouislider/distribute/nouislider.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/owl.carousel/dist/assets/owl.carousel.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/owl.carousel/dist/assets/owl.theme.default.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/dropzone/dist/dropzone.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/quill/dist/quill.snow.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/@yaireo/tagify/dist/tagify.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/summernote/dist/summernote.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/animate.css/animate.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/dual-listbox/dist/dual-listbox.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/morris.js/morris.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/sweetalert2/dist/sweetalert2.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/socicon/css/socicon.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/plugins/line-awesome/css/line-awesome.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/plugins/flaticon/flaticon.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/plugins/flaticon2/flaticon.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/css/pages/wizard/wizard-3.css') }}" rel="stylesheet"
          type="text/css"/>

    <!--end:: Vendor Plugins -->
    <link href="{{ asset('/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>

    <!--begin:: Vendor Plugins for custom pages -->
    <link href="{{ asset('/assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/@fullcalendar/core/main.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/@fullcalendar/daygrid/main.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/@fullcalendar/list/main.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/@fullcalendar/timegrid/main.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-autofill-bs4/css/autoFill.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-colreorder-bs4/css/colReorder.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link
        href="{{ asset('/assets/plugins/custom/datatables.net-fixedcolumns-bs4/css/fixedColumns.bootstrap4.min.css') }}"
        rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-keytable-bs4/css/keyTable.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-rowgroup-bs4/css/rowGroup.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-rowreorder-bs4/css/rowReorder.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-scroller-bs4/css/scroller.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/jstree/dist/themes/default/style.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/jqvmap/dist/jqvmap.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/custom/uppy/dist/uppy.min.css') }}" rel="stylesheet" type="text/css"/>

    <!--end:: Vendor Plugins for custom pages -->

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{{ url('/') }}/favicon.ico"/>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body
    class="kt-admin-dash kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">
<div class="kt-bg-overlay"></div>
<!-- begin::Page loader -->

<!-- end::Page Loader -->

<!-- begin:: Page -->

<!-- begin:: Header Mobile -->


<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        <a href="{{ url('/') }}/{{app()->getLocale()}}">
            <img alt="Logo" src="{{ url('/') }}/assets/media/logos/logo-gen-white.png"/>
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                class="flaticon-more-1"></i></button>
    </div>
</div>

<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            <div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
                <div class="kt-container ">

                    <!-- begin:: Brand -->
                    <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                        <a class="kt-header__brand-logo" href="{{ url('/') }}/{{app()->getLocale()}}">
                            <img alt="Logo" src="{{ url('/') }}/assets/media/logos/logo-gen-white.png"
                                 class="kt-header__brand-logo-default"/>
                            <img alt="Logo" src="{{ url('/') }}/assets/media/logos/logo-gen.png"
                                 class="kt-header__brand-logo-sticky"/>
                        </a>
                    </div>

                    <!-- end:: Brand -->

                    <!-- begin: Header Menu -->
                    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
                            class="la la-close"></i></button>
                    <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
                        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                            <ul class="kt-menu__nav ">
                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel @if(\Request::route()->getName() == "admin.home") {{ "kt-menu__item--open " }} @endif">
                                    <a href="{{ url('/') }}/{{app()->getLocale()}}"
                                       class="kt-menu__link"><span
                                            class="kt-menu__link-text">{{ $translations['dashboard'] ?? 'Dashboard' }}</span><i
                                            class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel @if(\Request::route()->getName() == "practices.index") {{ "kt-menu__item--open " }} @endif">
                                    <a href="{{ url('/') }}/{{app()->getLocale()}}/practices"
                                       class="kt-menu__link"><span
                                            class="kt-menu__link-text">{{ $translations['practice'] ?? 'Practice' }}</span><i
                                            class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel @if(array_search(request()->route()->getName(),["doctors.home","employees.index","admins.index"]) !== false) {{ "kt-menu__item--open " }} @endif"
                                    data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span
                                            class="kt-menu__link-text">{{ $translations['users'] ?? 'Users' }}</span><i
                                            class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                    <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                        <ul class="kt-menu__subnav">
                                            <li class="kt-menu__item " aria-haspopup="true">
                                                <a href="{{ url('/') }}/{{app()->getLocale()}}/doctors"
                                                   class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['doctors'] ?? 'Doctors' }}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                            <li class="kt-menu__item " aria-haspopup="true">
                                                <a href="{{ url('/') }}/{{app()->getLocale()}}/employees"
                                                   class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['employees'] ?? 'Employees' }}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                            @if(!Auth::user()->region_id)
                                                <li class="kt-menu__item " aria-haspopup="true">
                                                    <a href="{{ url('/') }}/{{app()->getLocale()}}/admins"
                                                       class="kt-menu__link"><span
                                                            class="kt-menu__link-text">{{ $translations['regional_admins'] ?? 'Regional admins' }}</span><i
                                                            class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </li>
                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel @if(\Request::route()->getName() == "services.index") {{ "kt-menu__item--open " }} @endif">
                                    <a href="{{ url('/') }}/{{app()->getLocale()}}/services"
                                       class="kt-menu__link"><span
                                            class="kt-menu__link-text">{{ $translations['services'] ?? 'Services'}}</span><i
                                            class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel @if(\Request::route()->getName() == "qualifications.index") {{ "kt-menu__item--open " }} @endif">
                                    <a href="{{ url('/') }}/{{app()->getLocale()}}/qualifications"
                                       class="kt-menu__link"><span
                                            class="kt-menu__link-text">{{ $translations['qualif'] ?? 'Qualifications' }}</span><i
                                            class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel @if(array_search(request()->route()->getName(),["practice-devices.index","devices.index","types.index","manufacturers.index"]) !== false) {{ "kt-menu__item--open " }} @endif"
                                    data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span
                                            class="kt-menu__link-text">{{ $translations['devices'] ?? 'Devices' }}</span><i
                                            class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                    <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                        <ul class="kt-menu__subnav">
                                            <li class="kt-menu__item " aria-haspopup="true">
                                                <a href="{{ url('/') }}/{{app()->getLocale()}}/practice-devices"
                                                   class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['practice_devices'] ?? 'Practice Devices' }}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                            <li class="kt-menu__item " aria-haspopup="true">
                                                <a href="{{ url('/') }}/{{app()->getLocale()}}/devices"
                                                   class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['device_list'] ?? 'Device list' }}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                            <li class="kt-menu__item " aria-haspopup="true"><a
                                                    href="{{ url('/') }}/{{app()->getLocale()}}/types"
                                                    class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['dev_types'] ?? 'Device types'}}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                            <li class="kt-menu__item " aria-haspopup="true"><a
                                                    href="{{ url('/') }}/{{app()->getLocale()}}/manufacturers"
                                                    class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['manufacturer'] ?? 'Manufacturer' }}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel @if(array_search(request()->route()->getName(),["elearning.index","training.index"]) !== false) {{ "kt-menu__item--open " }} @endif"
                                    data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span
                                            class="kt-menu__link-text">{{ $translations['elearning'] ?? 'elearning' }}</span><i
                                            class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                    <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                        <ul class="kt-menu__subnav">
                                            <li class="kt-menu__item " aria-haspopup="true">
                                                <a href="{{ url('/') }}/{{app()->getLocale()}}/elearning"
                                                   class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['elearning_tests'] ?? 'elearning_tests' }}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                            <li class="kt-menu__item " aria-haspopup="true">
                                                <a href="{{ url('/') }}/{{app()->getLocale()}}/training"
                                                   class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['elearning_materials'] ?? 'elearning_materials' }}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                            <li class="kt-menu__item " aria-haspopup="true"><a
                                                    href="{{ url('/') }}/{{app()->getLocale()}}/elearning-certificates"
                                                    class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['elearning_certificates'] ?? 'elearning-certificates' }}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                        </ul>
                                    </div>
                                </li>
                                @if(!Auth::user()->region_id)
                                    <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel @if(\Request::route()->getName() == "translations.index") {{ "kt-menu__item--open " }} @endif">
                                        <a href="{{ url('/') }}/{{app()->getLocale()}}/translations"
                                           class="kt-menu__link"><span
                                                class="kt-menu__link-text">{{ $translations['translations'] }}</span><i
                                                class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                    <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel @if(\Request::route()->getName() == "settings.index") {{ "kt-menu__item--open " }} @endif">
                                        <a href="{{ url('/') }}/{{app()->getLocale()}}/settings"
                                           class="kt-menu__link"><span
                                                class="kt-menu__link-text">{{ $translations['settings'] ?? 'Settings' }}</span><i
                                                class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                    </li>
                                @endif
                                <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel @if(\request()->is(app()->getLocale()."/reports")  || \request()->is(app()->getLocale()."/show/reports")) {{ "kt-menu__item--open " }} @endif"
                                    data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                    <a href="javascript:void(0)" class="kt-menu__link kt-menu__toggle"><span
                                            class="kt-menu__link-text">{{ $translations['report'] ?? 'Report' }}</span><i
                                            class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                    <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                        <ul class="kt-menu__subnav">
                                            <li class="kt-menu__item" aria-haspopup="true">
                                                <a href="{{ route('report.show',[app()->getLocale()]) }}"
                                                   class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['show_reports'] ?? 'Show reports' }}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                            <li class="kt-menu__item" aria-haspopup="true">
                                                <a href="{{ url('/') }}/{{app()->getLocale()}}/reports"
                                                   class="kt-menu__link"><span
                                                        class="kt-menu__link-text">{{ $translations['generate_report'] ?? 'Generate Report' }}</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel @if(\Request::route()->getName() == "sites.index") {{ "kt-menu__item--open " }} @endif">
                                    <a href="{{ url('/') }}/{{app()->getLocale()}}/sites"
                                       class="kt-menu__link"><span
                                                class="kt-menu__link-text">{{ $translations['sites'] ?? 'Sites' }}</span><i
                                                class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!-- end: Header Menu -->

                    <!-- begin:: Header Topbar -->
                    <div class="kt-header__topbar kt-grid__item">

                        <!--begin: Language bar -->
                        <div class="kt-header__topbar-item kt-header__topbar-item--langs">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                <span class="kt-header__topbar-icon">
                                    @if(app()->getLocale() == 'en')
                                        <img class="" src="{{ url('/') }}/assets/media/flags/020-flag.svg" alt=""/>
                                    @elseif(app()->getLocale() == 'de')
                                        <img src="{{ url('/') }}/assets/media/flags/017-germany.svg" alt=""/>
                                    @endif
                                </span>
                            </div>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim">
                                <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
                                    <li class="kt-nav__item">
                                        <a href="{{ app('App\Http\Controllers\LanguageController')->language_switcher('en') }}"
                                           class="kt-nav__link">
                                            <span class="kt-nav__link-icon"><img
                                                    src="{{ url('/') }}/assets/media/flags/020-flag.svg"
                                                    alt=""/></span>
                                            <span class="kt-nav__link-text">English</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="{{ app('App\Http\Controllers\LanguageController')->language_switcher('de') }}"
                                           class="kt-nav__link">
                                            <span class="kt-nav__link-icon"><img
                                                    src="{{ url('/') }}/assets/media/flags/017-germany.svg"
                                                    alt=""/></span>
                                            <span class="kt-nav__link-text">German</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!--end: Language bar -->

                        <!--begin: User bar -->
                        <div class="kt-header__topbar-item kt-header__topbar-item--user">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                <span class="kt-header__topbar-welcome">{{ $translations['hi'] ?? 'hi' }},</span>
                                <span class="kt-header__topbar-username">{{ Auth::user()->name }}</span>
                                <img alt="Pic" src="{{ url('/') }}/assets/media/users/300_21.jpg" class="kt-hidden"/>
                            </div>
                            <div
                                class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                                <!--begin: Head -->
                                <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                                     style="background-image: url({{ url('/') }}/assets/media/misc/bg-1.jpg)">
                                    <div class="kt-user-card__avatar">
                                        <img class="kt-hidden" alt="Pic"
                                             src="{{ url('/') }}/assets/media/users/300_25.jpg"/>

                                        <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                        <span
                                            class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{ ucfirst(substr(Auth::user()->name,0,1)) }}</span>
                                    </div>
                                    <div class="kt-user-card__name">
                                        {{ Auth::user()->name }}
                                    </div>
                                    <div class="kt-user-card__badge">
                                        <span>
                                            <div class="kt-notification__custom kt-space-between">
                                        <div class="c-pointer btn btn-success btn-sm btn-bold btn-font-md"
                                             onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ $translations['logout'] ?? 'logout' }}
                                        </div>

                                        <form id="logout-form" action="{{ route('logout', app()->getLocale()) }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                        </span>
                                    </div>

                                </div>
                                <div class="kt-notification">
                                    <a href="{{ url('/') }}/{{app()->getLocale()}}/changelogin"
                                       class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-calendar-3 kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title kt-font-bold">
                                                {{ $translations['my_profile'] ?? 'my_profile' }}
                                            </div>
                                            <div class="kt-notification__item-time">
                                                {{ $translations['account_email_password'] ?? 'account_email_password' }}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!--end: Head -->
                            </div>
                        </div>

                        <!--end: User bar -->
                    </div>

                    <!-- end:: Header Topbar -->
                </div>
            </div>

            <!-- end:: Header -->
            <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                     id="kt_content">

                    <!-- begin:: Subheader -->
                    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                        <div class="kt-container ">
                            <div class="kt-subheader__main">
                                <h3 class="kt-subheader__title">
                                    {{isset($meta_parent) ? $meta_parent : $meta_title}} </h3>
                                <div class="kt-subheader__breadcrumbs">
                                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                                    <span class="kt-subheader__breadcrumbs-separator"></span>
                                    <a href="{{$parent_link}}" class="kt-subheader__breadcrumbs-link">
                                        {{$meta_title}} </a>
                                    @if(isset($meta_parent))
                                        <span class="kt-subheader__breadcrumbs-separator"></span>
                                        <span class="kt-subheader__breadcrumbs-link">{{$meta_parent}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="kt-subheader__toolbar">
                                @yield('actionbutton')
                            </div>
                        </div>
                    </div>
                    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                        <div class="alert alert-light alert-elevate fade show">@yield('description')</div>
                    </div>
                    <!-- end:: Subheader -->
                    <!-- begin:: Content -->
                    <div class="kt-container  kt-grid__item kt-grid__item--fluid bg-cont-grey">
                        <div class="kt-portlet no-box-shadow no-bg">
                            <div class="kt-portlet__body">
                                <!--Begin::Row-->
                            @include('partials.messages')
                            @yield('content')
                            <!--End::Row-->
                            </div>
                        </div>
                    </div>
                    <!-- end:: Content -->
                </div>
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer">
                <div class="kt-footer__bottom">
                    <div class="kt-container ">
                        <div class="kt-footer__wrapper">
                            <div class="kt-footer__logo">
                                <div class="kt-footer__copyright">
                                    <?= date('Y') ?>
                                    &nbsp;&copy; {{$website_settings['web_name'] ?? 'PERLEASE MPV PORTAL'}} Made with
                                    <i class="fas fa-heart"></i> &amp; <i
                                        class="fas fa-coffee"></i> by <a href="http://tomsolutions.de"
                                                                         target="_blank">tomsolutions</a> in
                                    Mannheim
                                </div>
                            </div>
                            <div class="kt-footer__menu">
                                <a href="https://www.perlease-mpv.de/impressum" class="kt-link"
                                   target="_blank">{{ $translations['impressum'] ?? 'impressum' }}</a>
                                <a href="https://www.perlease-mpv.de/datenschutz" class="kt-link"
                                   target="_blank">{{ $translations['datenschutzerklarung'] ?? 'datenschutzerklarung' }}</a>
                                <a href="https://www.perlease-mpv.de/kontakt" class="kt-link"
                                   target="_blank">{{ $translations['kontakt'] ?? 'kontakt' }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @stack('modal')

        <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->

<!--begin::Global Theme Bundle(used by all pages) -->

<!--begin:: Vendor Plugins -->
<script src="{{ asset('/assets/plugins/general/jquery/dist/jquery.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/popper.js/dist/umd/popper.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/js-cookie/src/js.cookie.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/moment/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/tooltip.js/dist/umd/tooltip.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/perfect-scrollbar/dist/perfect-scrollbar.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/sticky-js/dist/sticky.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/wnumb/wNumb.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/jquery-form/dist/jquery.form.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/block-ui/jquery.blockUI.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/js/global/integration/plugins/bootstrap-datepicker.init.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/js/global/integration/plugins/bootstrap-timepicker.init.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap-daterangepicker/daterangepicker.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap-maxlength/src/bootstrap-maxlength.js') }}"
        type="text/javascript"></script>
<script
    src="{{ asset('/assets/plugins/general/plugins/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js') }}"
    type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap-select/dist/js/bootstrap-select.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap-switch/dist/js/bootstrap-switch.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/js/global/integration/plugins/bootstrap-switch.init.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/select2/dist/js/select2.full.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/ion-rangeslider/js/ion.rangeSlider.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/typeahead.js/dist/typeahead.bundle.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/handlebars/dist/handlebars.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/inputmask/dist/jquery.inputmask.bundle.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/inputmask/dist/inputmask/inputmask.date.extensions.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/nouislider/distribute/nouislider.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/owl.carousel/dist/owl.carousel.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/autosize/dist/autosize.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/clipboard/dist/clipboard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/dropzone/dist/dropzone.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/js/global/integration/plugins/dropzone.init.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/quill/dist/quill.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/@yaireo/tagify/dist/tagify.polyfills.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/@yaireo/tagify/dist/tagify.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/summernote/dist/summernote.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap-markdown/js/bootstrap-markdown.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/js/global/integration/plugins/bootstrap-markdown.init.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/bootstrap-notify/bootstrap-notify.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/js/global/integration/plugins/bootstrap-notify.init.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/jquery-validation/dist/jquery.validate.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/jquery-validation/dist/additional-methods.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/js/global/integration/plugins/jquery-validation.init.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/toastr/build/toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/dual-listbox/dist/dual-listbox.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/raphael/raphael.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/morris.js/morris.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/chart.js/dist/Chart.bundle.js') }}" type="text/javascript"></script>
<script
    src="{{ asset('/assets/plugins/general/plugins/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js') }}"
    type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/plugins/jquery-idletimer/idle-timer.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/waypoints/lib/jquery.waypoints.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/counterup/jquery.counterup.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/es6-promise-polyfill/promise.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/sweetalert2/dist/sweetalert2.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/js/global/integration/plugins/sweetalert2.init.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/jquery.repeater/src/lib.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/jquery.repeater/src/jquery.input.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/jquery.repeater/src/repeater.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/general/dompurify/dist/purify.js') }}" type="text/javascript"></script>

<!--end:: Vendor Plugins -->
<script src="{{ asset('/assets/js/scripts.bundle.js') }}" type="text/javascript"></script>

<!--begin:: Vendor Plugins for custom pages -->
<script src="{{ asset('/assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/@fullcalendar/core/main.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/@fullcalendar/daygrid/main.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/@fullcalendar/google-calendar/main.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/@fullcalendar/interaction/main.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/@fullcalendar/list/main.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/@fullcalendar/timegrid/main.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/gmaps/gmaps.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/flot/dist/es5/jquery.flot.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/flot/source/jquery.flot.resize.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/flot/source/jquery.flot.categories.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/flot/source/jquery.flot.pie.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/flot/source/jquery.flot.stack.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/flot/source/jquery.flot.crosshair.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/flot/source/jquery.flot.axislabels.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net/js/jquery.dataTables.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/js/global/integration/plugins/datatables.init.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-autofill/js/dataTables.autoFill.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/jszip/dist/jszip.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/pdfmake/build/pdfmake.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/pdfmake/build/vfs_fonts.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-buttons/js/buttons.colVis.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-buttons/js/buttons.flash.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-buttons/js/buttons.html5.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-buttons/js/buttons.print.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-colreorder/js/dataTables.colReorder.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-responsive/js/dataTables.responsive.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-rowgroup/js/dataTables.rowGroup.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-rowreorder/js/dataTables.rowReorder.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-scroller/js/dataTables.scroller.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/datatables.net-select/js/dataTables.select.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/jstree/dist/jstree.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/jqvmap/dist/jquery.vmap.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.world.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.russia.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.usa.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.germany.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.europe.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/custom/uppy/dist/uppy.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/pages/custom/wizard/wizard-3.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/pages/crud/forms/widgets/form-repeater.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/question.js') }}" type="text/javascript"></script>

<!--end:: Vendor Plugins for custom pages -->

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('/assets/js/pages/dashboard.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/pages/custom/wizard/wizard-custom.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
    CKEDITOR.replaceClass = "summary-ckeditor";
</script>
<!--begin::Page Scripts(used by this page) -->
<!-- Datepicker Practice devices create blade -->
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $.fn.dataTable.ext.errMode = 'throw';
    $(".datatable_year").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });
</script>
<!-- Datatable -->
<script type="text/javascript" defer>
    // Datatable release
    // $(document).ready(function () {
    //     $('#datatable').DataTable();
    // });
    // Datatable show more
    $(document).ready(function () {
        $(".cd-showmore").on("click", function () {
            //Toggle
            $(this).parent().parent().toggleClass("cd-info-open");
            var text = $(this).text();
            // Text changing
            $(this).text(text == "Hide details" ? "Show details" : "Hide details");
            //Clonning
            $(".cd-additional-info:last").insertAfter($(this).parent().parent()).clone();
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#checkbox').on('change', function () {
            $('#password').attr('type', $('#checkbox').prop('checked') == true ? "text" : "password");
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.toggle-class').change(function () {
            var status = $(this).prop('checked') === true ? 1 : 0;
            var id = $(this).attr('data-id');

            $.ajax({
                type: "GET",
                dataType: "json",
                url: '/changeStatus',
                data: {'status': status, 'id': id},
                success: function (data) {
                    /*console.log(data.success)*/
                }
            });
        })
    })
</script>

<script type="text/javascript">
    $(document).ready(function () {

        $('.review').click(function () {
            var formValues = [];
            // get values from inputs in first fieldset
            $('.field1 :input').each(function () {
                formValues.push($(this).val());
            });

            // formValues.pop(); //remove the button from input values
            // console.log(formValues);

            // set values in second fieldset
            $('.field2 :input').each(function (index) {
                if (formValues[index]) {
                    $(this).val(formValues[index]);
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.aaa :input').prop("disabled", false);
        $('input:checkbox').click(function () {
            if ($(this).is(':checked')) {
                var a = document.getElementById("name").value;
                var b = document.getElementById("street").value;
                var c = document.getElementById("phone").value;
                var d = document.getElementById("city").value;
                document.getElementById("inv_name").value = a;
                document.getElementById("inv_street").value = b;
                document.getElementById("inv_postal").value = c;
                document.getElementById("inv_city").value = d;
                $('.aaa :input').prop("readonly", true);
            } else {
                if ($('.checks').filter(':checked').length < 1) {
                    $('.aaa :input').attr('disabled', false);
                }
            }
        });
    });
</script>


{{--  This script is for adding question with answers--}}
<script>


    function addSection(element) {

        var answerSection = $(element).closest($('.form-answers'))[0];
        $(answerSection).clone()
            .find("textarea").val('').end()
            .appendTo($('.form-container'))

        $(".answer-number").each(function (index) {
            $(this).text(index + 1);
        });

    }

    function removeSection(element) {

        if ($('.remove-question').length > 1) {
            var answerSection = $(element).closest($('.form-answers'))[0];
            $(answerSection).remove();
            $(".answer-number").each(function (index) {
                $(this).text(index + 1);
            });
        } else {

        }


    }


</script>

<script>
    $("input#pwd").on("focus keyup", function () {
        var score = 0;
        var a = $(this).val();
        var desc = new Array();

        // strength desc
        desc[0] = "Too short";
        desc[1] = "Weak";
        desc[2] = "Good";
        desc[3] = "Strong";
        desc[4] = "Best";

        $("#pwd_strength_wrap").fadeIn(400);

        // password length
        if (a.length >= 6) {
            $("#length").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#length").removeClass("valid").addClass("invalid");
        }

        // at least 1 digit in password
        if (a.match(/\d/)) {
            $("#pnum").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#pnum").removeClass("valid").addClass("invalid");
        }

        // at least 1 capital & lower letter in password
        if (a.match(/[A-Z]/) && a.match(/[a-z]/)) {
            $("#capital").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#capital").removeClass("valid").addClass("invalid");
        }


        {{--  This script is for doctor elerning create page  --}}

        // at least 1 special character in password {
        if (a.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) {
            $("#spchar").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#spchar").removeClass("valid").addClass("invalid");
        }


        if (a.length > 0) {
            //show strength text
            $("#passwordDescription").text(desc[score]);
            // show indicator
            $("#passwordStrength").removeClass().addClass("strength" + score);
        } else {
            $("#passwordDescription").text("Password not entered");
            $("#passwordStrength").removeClass().addClass("strength" + score);
        }
    });

    $("input#pwd").blur(function () {
        $("#pwd_strength_wrap").fadeOut(400);
    });
</script>

<script>
    $(document).ready(function () {
        const token = $('meta[name="csrf-token"]').attr('content');
        // getting filtered devices
        function getPracticedevices() {
            const url = '{{route('admin.filter',[app()->getLocale()])}}';
            const data = {
                practice_id: $('#practice').val(),
                "_token": token
            };
            if (data.practice_id) {
                $.ajax({type: "POST", url, data,})
                    .done(function (item) {
                        /*$('.add__device__step--second').addClass('active');*/
                        $('#devices').text('');
                        $('#users').text('');
                        $('#devices').append('<option value="" disabled selected> Select device</option>');
                        $.each(item.devices, function (index, data) {
                            $('#devices').append('<option value="' + data.id + '">' + data.device.name + '</option>')
                        });
                        $.each(item.users, function (index, data) {
                            $('#users').append('<option value="' + data.id + '">' + data.name + ' ' + data.last_name + '</option>')

                        });
                    })
                    .fail(function () {
                        alert("error");
                    });
            }
        }

        function getSites() {
            const url = '{{route('admin.getSitesByPractice',[app()->getLocale()])}}';
            const data = {
                practice_id: $('#practice').val(),
                "_token": token
            };
            if (data.practice_id) {
                $.ajax({type: "POST", url, data,})
                    .done(function (item) {
                        $('#sites').text('');
                        $('#sites').append('<option value="" disabled selected> Select site</option>');
                        $.each(item.sites, function (index, data) {
                            $('#sites').append('<option value="' + data.id + '">' + data.name + '</option>')
                        });
                    })
                    .fail(function () {
                        alert("error");
                    });
            }
        }

        $('#practice').on('change', function () {
            $(".select_type_block").show();
            getPracticedevices();
            getSites();
        });
        $('#report_type').on('change', function () {
            if($(this).val() == "device"){
                $('.add__site__step--second').removeClass('active')
                $('.add__device__step--second').addClass('active');
            }else{

                $('.add__device__step--second').removeClass('active');
                $('.add__site__step--second').addClass('active')
            }
        });

    });

</script>
<script>
    $('.create__report').on('click',function () {
        $('form').submit();
    })
</script>
@stack('scripts')

<!--end::Page Scripts -->

</body>

<!-- end::Body -->
</html>
