<!-- begin:: Alert Message block -->
@if(Session::has('type'))
    <div class="alert alert-{{ session('type') }}"  role="alert">
        <div class="alert-text">{{ session('message') }}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-close"></i></span>
            </button>
        </div>
    </div>
@endif
<!-- end:: Alert Message block -->

@if(count($errors)>0)
    <div class="alert alert-danger">
{{--        @if(in_array(request()->segment(2), ["report", "reports"]))--}}
{{--        <ul>--}}
{{--            <li>{{__("Fill in all the fields")}}</li>--}}
{{--        </ul>--}}
{{--        @else--}}
            <ul>
                @foreach($errors->all() as $error => $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
{{--        @endif--}}

    </div>
@endif
