@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['doc_elerning_questions_creat_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/elearning" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_device'] ?? 'select_device' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <select class="form-control" name="device_id" required>
                                    @foreach($devices as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['test_name'] ?? 'test_name' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control" placeholder="{{ $translations['enter_test_name'] ?? 'enter_test_name' }}" id="name" type="text" name="name" value="{{ old('name') }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['test_summary'] ?? 'test_summaryw' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <textarea placeholder="Enter Test summary" name="summary" class="form-control" cols="30" rows="4" required value="{{ old('summary') }}"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['youtube_video'] ?? 'youtube_video' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control" id="youtube_link" type="url" name="youtube_link" value="{{ old('youtube_link') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['vimeo_video'] ?? 'vimeo_video' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control" id="vimeo_link" type="url" name="vimeo_link" value="{{ old('vimeo_link') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['attach_document'] ?? 'attach_document' }}</label>

                            <div class="col-lg-9 col-xl-9">
                                <input type="file" class="custom-file-input" id="attach_file_name" name="attach_file_name" value="{{ old('attach_file_name') }}" accept="application/*">
                                <label class="custom-file-label" for="attach_file_name">{{ $translations['choose_file'] ?? 'choose_file' }}</label>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['create'] ?? 'create' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
