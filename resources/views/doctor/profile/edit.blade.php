@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['doc_profile_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST"
                          action="{{ route('doctor.profile.update',['locale'=>app()->getLocale(),'id'=>$user->id] ) }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name"
                                   class="col-md-4 col-form-label text-md-right">{{$translations['first_name'] ?? 'First Name'}}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                       name="name" value="{{$user->name}}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{$translations['last_name'] ?? 'last_name'}}</label>
                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                       name="last_name" value="{{$user->last_name}}" required>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{$translations['email'] ?? 'email'}}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                       name="email" value="{{$user->email}}" required>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{$translations['title'] ?? 'title'}}</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                       name="title" value="{{$user->title}}" required>

                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{$translations['phone'] ?? 'phone'}}</label>

                            <div class="col-md-6">
                                <input id="email" type="text"
                                       class="form-control @error('email') is-invalid @enderror"
                                       name="phone" value="{{$user->phone}}" required>

                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['update'] ?? 'Update' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8 mt-4">
            <div class="card">
                <div class="card-body">
                    <form method="POST"
                          action="{{ route('doctor.profile.update.password',['locale'=>app()->getLocale()] ) }}">
                        @csrf
                        <div class="form-group row">
                            <label for="old_password"
                                   class="col-md-4 col-form-label text-md-right">{{$translations['old_password'] ?? 'old_password'}}</label>

                            <div class="col-md-6">
                                <input id="old_password" type="password"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="old_password" value="" required autocomplete="password" autofocus>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password"
                                   class="col-md-4 col-form-label text-md-right">{{$translations['new_password'] ?? 'new_password'}}</label>

                            <div class="col-md-6">
                                <input id="pwd" type="password"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="password" value="" required autofocus>
                                <div id="pwd_strength_wrap" class="pwd3">
                                    <div id="passwordDescription">{{ $translations['pass_not_ent'] ?? 'Password not entered' }}</div>
                                    <div id="passwordStrength" class="strength0"></div>
                                    <div id="pswd_info">
                                        <strong>{{ $translations['strong_pass'] ?? 'Strong Password Tips' }}:</strong>
                                        <ul>
                                            <li class="invalid" id="length">{{ $translations['min_six'] ?? 'At least 6 characters' }}</li>
                                            <li class="invalid" id="pnum">{{ $translations['one_num'] ?? 'At least one number' }}</li>
                                            <li class="invalid" id="capital">{{ $translations['one_low_one_up'] ?? 'At least one lowercase & one uppercase letter' }}</li>
                                            <li class="invalid" id="spchar">{{ $translations['min_one_char'] ?? 'At least one special character' }}</li>
                                        </ul>
                                    </div><!-- END pswd_info -->
                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password_confirmation"
                                   class="col-md-4 col-form-label text-md-right">{{$translations['confirm_new_password'] ?? 'confirm_new_password'}}</label>

                            <div class="col-md-6">
                                <input id="password_confirmation" type="password"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="password_confirmation" value="" required autofocus>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['update'] ?? 'Update' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
