@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['doc_employee_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                    <!--begin: Form Wizard Form-->
                    <form method="POST" class="kt-form" id="kt_form"
                          action="{{ url('/') }}/{{app()->getLocale()}}/account/employees/{{$employee->id}}">
                    @csrf
                    {{--{{dd($employee)}}--}}
                    {{method_field('PUT')}}
                    <!--begin: Form Wizard Step 1-->
                        <div class="kt-wizard-v3__content" data-ktwizard-type="step-content"
                             data-ktwizard-state="current">
                                <div class="kt-heading kt-heading--md">{{$translations['details'] ?? 'details'}}</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v3__form">
                                    <div class="col-xl-12">
                                        <div class="kt-section__body">
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_practice'] ?? 'select_practice' }}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <select class="form-control" name="practice_id">
                                                        @foreach($practices as $item)
                                                            <option @if($item->practice->id == $employee->practice_id) selected @endif value="{{$item->practice->id}}">{{$item->practice->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['first_name'] ?? 'first_name'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <input class="form-control" type="text" value="{{$employee->name}}"
                                                           name="name" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['last_name'] ?? 'last_name'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <input class="form-control" type="text"
                                                           value="{{$employee->last_name}}" name="last_name" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['phone'] ?? 'phone'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <input class="form-control" type="text" value="{{$employee->phone}}"
                                                           name="phone" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['job_title'] ?? 'job_title'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <input class="form-control" type="text" value="{{$employee->title}}"
                                                           name="title" required>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['qualification'] ?? 'qualification'}}</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <select class="form-control" name="qualification_id">
                                                            @foreach($qualifications as $qualification)
                                                                <option
                                                                    @if($employee->qualification_id == $qualification->id) {{ "selected" }} @endif value="{{$qualification->id}}">{{$qualification->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!--begin: Form Actions -->
                        <div class="form-group row mt-3 mb-0">
                            <div class="col-md-5 offset-md-3">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['update'] ?? 'Update'}}
                                </button>
                            </div>
                        </div>
                        <!--end: Form Actions -->
                    </form>

                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
    </div>

@endsection
