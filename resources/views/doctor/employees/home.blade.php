@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">
        <a href="{{ url('/') }}/{{app()->getLocale()}}/account/employees/create" class="btn btn-danger">
            {{$translations['add_new_employee'] ?? 'add_new_employee'}}
        </a>
    </div>
@endsection
@section('description')
    <div class="description">{{$translations['doc_employees_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
        <div class="table-responsive">
            <table id="tableDoctorEmployees" class="table">
                <thead class="thead-light">
                <tr>
                    <th>{{$translations['practice_name'] ?? 'practice_name'}}</th>
                    <th>{{$translations['first_name'] ?? 'first_name'}}</th>
                    <th>{{$translations['last_name'] ?? 'last_name'}}</th>
                    <th>{{$translations['email'] ?? 'email'}}</th>
                    <th>{{$translations['phone'] ?? 'phone'}}</th>
                    <th>{{$translations['job_title'] ?? 'job_title'}}</th>
                    <th>{{$translations['qualif'] ?? 'qualif'}}</th>
                    <th>{{$translations['action'] ?? 'action'}}</th>
                    <th>{{$translations['action'] ?? 'action'}}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script>
        let url = "{{route("doctor.employees.index",["local"=>app()->getLocale()])}}";
        $('#tableDoctorEmployees').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": url,
            "language": {
                "emptyTable": "{{ $translations['no_employees_registered'] ?? 'No Employees Registered'}}"
            },
            "columns": [
                {
                    "name": "practice.name",
                    "data": "practice",
                    "render": function (practice) {
                        return practice ? practice.name : '-';
                    }
                },
                {"data": "name"},
                {"data": "last_name",},
                {"data": "email",},
                {"data": "phone",},
                {"data": "title",},
                {
                    "name": "qualification.name",
                    "data": "qualification",
                    "render": function (qualification) {
                        return qualification ? qualification.name : '-';
                    }
                },
                {
                    "searchable":false,
                    "orderable":false,
                    "data": "id",
                    "className":"my_action",
                    "render": function (id) {
                        let a = document.createElement('a');
                        let button = document.createElement("button");
                        a.className = "btn btn-success my_btn1";
                        a.setAttribute("role", "button");
                        a.href = `${url}/${id}/edit`;
                        a.innerText = "{{ $translations['edit'] ?? 'edit' }}";
                        button.type = "button";
                        button.className = "btn btn-danger delete_action";
                        button.setAttribute("data-form_action",`${url}/${id}`);
                        button.setAttribute("data-toggle","modal");
                        button.setAttribute("data-target","#deleteModal");
                        button.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                        return `${a.outerHTML} ${button.outerHTML}`;
                    }
                },
                {
                    "searchable":false,
                    "name":"status",
                    "data": "status",
                    "render": function (status,type,data) {
                        let span = document.createElement('span');
                        let label = document.createElement('label');
                        let input = document.createElement('input');
                        let spanNew = document.createElement('span');
                        input.name = "status";
                        input.setAttribute("data-id",data.id);
                        input.className = "toggle-class";
                        input.type = "checkbox";
                        input.setAttribute("data-onstyle","success");
                        input.setAttribute("data-toggle","toggle");
                        input.setAttribute("data-on","Active");
                        input.setAttribute("data-off","InActive");
                        if (status) input.setAttribute("checked","checked");
                        span.className ="kt-switch";
                        label.appendChild(input);
                        label.appendChild(spanNew);
                        span.appendChild(label);
                        return span.outerHTML;
                    }
                },
            ],
        });
    </script>
@endpush
