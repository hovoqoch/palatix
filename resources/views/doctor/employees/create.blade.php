@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['doc_employees_creat_desc'] ?? 'action'}}</div>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                    <form method="POST" class="kt-form" id="kt_form"
                          action="{{ url('/') }}/{{app()->getLocale()}}/account/employee/add">
                        @csrf
                        <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                            <div class="kt-heading kt-heading--md">{{$translations['details'] ?? 'details'}}</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v3__form">

                                    <div class="kt-section__body">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_practice'] ?? 'select_practice' }}</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <select class="form-control" name="practice_id">
                                                    @foreach($practices as $item)
                                                        <option value="{{$item->practice->id}}">{{$item->practice->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['first_name'] ?? 'first_name'}}</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input class="form-control" type="text"
                                                       name="name" value="{{ old('name') }}"
                                                       required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['last_name'] ?? 'last_name'}}</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input class="form-control" type="text"
                                                       name="first_name" value="{{ old('first_name') }}"
                                                       required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label
                                                class="col-xl-3 col-lg-3 col-form-label">{{$translations['email'] ?? 'email'}}</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input class="form-control" type="text"
                                                       name="email" value="{{ old('email') }}"
                                                       required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['phone'] ?? 'phone'}}</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input class="form-control" type="text"
                                                       name="phone" value="{{ old('phone') }}"
                                                       required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">{{$translations['job_title'] ?? 'job_title'}}</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input class="form-control" type="text"
                                                       name="job_title" value="{{ old('job_title') }}"
                                                       required>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-last row">
                                            <label
                                                class="col-xl-3 col-lg-3 col-form-label">{{$translations['qualification'] ?? 'qualification'}}</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <div class="input-group">
                                                    <select class="form-control"
                                                            name="qualification_id">
                                                        @foreach($qualifications as $qualification)
                                                            <option
                                                                value="{{$qualification->id}}">{{$qualification->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--begin: Form Actions -->
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['create'] ?? 'Create' }}
                                </button>
                            </div>
                        </div>
                        <!--end: Form Actions -->
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
