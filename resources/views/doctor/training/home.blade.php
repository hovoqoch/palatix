@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">
        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/account/training/create">
            {{ $translations['add_training'] ?? 'add_training' }}
        </a>
    </div>
@endsection
@section('description')
    <div class="description">{{$translations['doc_training_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
<div class="table-responsive">
    <table id="tableDoctorTraining" class="table">
        <thead class="thead-light">
        <tr>
            <th>{{$translations['name'] ?? 'name'}}</th>
            <th>{{$translations['file_name'] ?? 'file_name'}}</th>
            <th>{{$translations['device_name'] ?? 'device_name'}}</th>
            <th>{{ $translations['action'] ?? 'Action'}}</th>

        </tr>
        </thead>
        <tbody>
        @foreach($trainings as $training)
            <tr>
                <td>{{$training->name }}</td>
                <td>{{$training->file }}</td>
                <td>{{$training->device->name ?? '' }}</td>
                <td>
                    <a class="btn btn-success my_btn1" role="button" href="{{ route('doctor.training.edit',[app()->getLocale(),$training->id] ) }}">{{ $translations['edit'] ?? 'Edit' }}</a>
                    <button type="button" class="btn btn-danger delete_action"
                            data-form_action="{{ route('doctor.training.destroy',[app()->getLocale(),$training->id] ) }}"
                            data-toggle="modal" data-target="#deleteModal">{{ $translations['delete'] ?? 'Delete' }}
                    </button>
                    <a class="btn btn-primary"  href="{{url(app()->getLocale().'/account/training/download/'.$training->file)}}">{{$translations['download_file'] ?? 'download_file'}}</a>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure' ])
@endpush
@push('scripts')
    <script>
        let url = "{{route("doctor.training.index",["local"=>app()->getLocale()])}}";
        $('#tableDoctorTraining').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": url,
            "language": {
                "emptyTable": "{{ $translations['no_training_materials'] ?? 'no_training_materials'}}"
            },
            "columns": [
                {"data": "name",},
                {"data": "file",},
                {
                    "name":"device.name",
                    "data": "device",
                    "render": function (device) {
                        return device ? device.name : '-';
                    }
                },
                {
                    "searchable": false,
                    "orderable": false,
                    "data": "id",
                    "className": "my_action",
                    "render": function (id,type,data) {
                        let a = document.createElement('a');
                        let button = document.createElement("button");
                        let a2 = document.createElement('a');
                        a.className = "btn btn-success my_btn1";
                        a.setAttribute("role", "button");
                        a.href = `${url}/${id}/edit`;
                        a.innerText = "{{ $translations['edit'] ?? 'edit' }}";
                        a2.className = "btn btn-primary";
                        a2.href = `${url}/download/${data.file}`;
                        a2.innerText ="{{$translations['download_file'] ?? 'download_file'}}";
                        button.type = "button";
                        button.className = "btn btn-danger delete_action";
                        button.setAttribute("data-form_action", `${url}/${id}`);
                        button.setAttribute("data-toggle", "modal");
                        button.setAttribute("data-target", "#deleteModal");
                        button.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                        return `${a.outerHTML} ${button.outerHTML} ${a2.outerHTML}`;
                    }
                },
            ],
        });
    </script>
@endpush

