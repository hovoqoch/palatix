@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['doc_training_edit_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/account/training/{{$training->id}}"
                          enctype="multipart/form-data">
                        @csrf
                        {{method_field('PUT')}}

                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_practice'] ?? 'select_practice' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <select class="form-control"  id="devices_by_practice" required>
                                    @foreach($practices as $practice)
                                        <option @if($practice->practice_id == $chosen_prectice_id->practice_id ) selected @endif  value="{{$practice->practice->id}}">{{$practice->practice->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_device'] ?? 'select_device' }}</label>
                            <div class="col-lg-9 col-xl-9">

                                <select class="form-control" name="device_id" id="devices" required>
                                    @foreach($devices as $device)
                                        <option
                                            {{(int)$device->device_id === (int)$training->device_id ? 'selected' : ''}} value="{{$device->device_id}}">{{$device->device->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['test_name'] ?? 'test_name' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control" placeholder="Enter Training name" id="name" type="text"
                                       name="name" value="{{$training->name}}" required>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['update'] ?? 'update' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
