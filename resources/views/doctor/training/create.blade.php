@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['doc_training_creat_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/account/training" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_practice'] ?? 'select_practice' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <select class="form-control" id="devices_by_practice" name="practice_id" required>
                                    <option value="0"  selected>{{ $translations['select_practice'] ?? 'select_practice' }}</option>
                                    @foreach($practices as $practice)
                                        <option value="{{$practice->practice->id}}">{{$practice->practice->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['select_device'] ?? 'Select Device'}}</label>
                            <div class="col-lg-9 col-xl-9">
                                <select required id="devices" class="form-control" name="device_id">

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['name'] ?? 'name' }}</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control" placeholder="Enter Training name" id="name" type="text" name="name" value="{{ old('name') }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">{{ $translations['attach_document'] ?? 'attach_document' }}</label>

                            <div class="col-lg-9 col-xl-9">
                                <input type="file" class="custom-file-input" id="attach_file_name" name="file" value="{{ old('file') }}" accept="application/*">
                                <label class="custom-file-label" for="attach_file_name">{{ $translations['choose_file'] ?? 'choose_file' }}</label>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $translations['create'] ?? 'create' }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
