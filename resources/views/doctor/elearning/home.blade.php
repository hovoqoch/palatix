@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">
        <a href="{{ url('/') }}/{{app()->getLocale()}}/account/elearning/create" class="btn btn-danger">
            {{$translations['add_new_test'] ?? 'add_new_test'}}
        </a>
    </div>
@endsection
@section('description')
    <div class="description">{{$translations['doc_elearning_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

<div class="table-responsive">
    <table id="tableDoctorElearning" class="table table-hover table-sm">
        <thead class="thead-light">
        <tr>
            <th>{{ $translations['name'] ?? 'Name' }}</th>
            <th>{{ $translations['summary'] ?? 'Summary' }}</th>
            <th>{{ $translations['devices'] ?? 'Devices' }}</th>
            <th>{{ $translations['youtube_video'] ?? 'Youtube video' }}</th>
            <th>{{ $translations['vimeo_video'] ?? 'Vimeo video' }}</th>
            <th>{{ $translations['attached_document'] ?? 'Attached document' }}</th>
            <th>{{ $translations['action'] ?? 'Action'}}</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['del_tes'] ?? 'Delete Test' ])
@endpush
@push('scripts')
    <script>
        let url = "{{route("doctor.elearning.index",["local"=>app()->getLocale()])}}";
        $('#tableDoctorElearning').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": url,
            "language": {
                "emptyTable": "{{ $translations['no_test'] ?? 'No Tests Yet'}}"
            },
            "columns": [
                {"data": "name"},
                {"data": "summary"},
                {
                    "name":"device.name",
                    "data": "device",
                    "render": function (device) {
                        return device ? device.name : '-';
                    }
                },
                {
                    "name":"youtube_link",
                    "data": "youtube_link",
                    "render": function (youtube) {
                        return youtube?`<a href="${youtube}" target="_blank">${youtube}</a>`:'';
                    }
                },
                {
                    "name":"vimeo_link",
                    "data": "vimeo_link",
                    "render": function (vimeo) {
                        return vimeo?`<a href="${vimeo}" target="_blank">${vimeo}</a>`:'';
                    }
                },
                {
                    "name":"attach_file_name",
                    "data": "attach_file_name",
                    "searchable": false,
                    "render": function (attach) {
                        return attach?`<a class="btn btn-primary" href="${url}/download/${attach}">Download PDF</a>`:"{{$translations['no_attachment'] ?? 'No Attachment'}}";
                    }
                },

                {
                    "searchable": false,
                    "orderable": false,
                    "data": "id",
                    "className": "my_action",
                    "render": function (id) {
                        let edit = document.createElement('a');
                        let del = document.createElement("button");
                        let questions = document.createElement('a');
                        edit.className = "btn btn-success my_btn1";
                        edit.setAttribute("role", "button");
                        edit.href = `${url}/${id}/edit`;
                        edit.innerText = "{{ $translations['edit'] ?? 'edit' }}";
                        questions.className = "btn btn-warning my_btn1";
                        questions.setAttribute("role","button");
                        questions.href = `${url}/${id}/questions`;
                        questions.innerText ="{{$translations['questions'] ?? 'Questions'}}";
                        del.type = "button";
                        del.className = "btn btn-danger delete_action";
                        del.setAttribute("data-form_action", `${url}/${id}`);
                        del.setAttribute("data-toggle", "modal");
                        del.setAttribute("data-target", "#deleteModal");
                        del.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                        return `${edit.outerHTML} ${del.outerHTML} ${questions.outerHTML}`;
                    }
                },
            ],
        });
    </script>
@endpush
