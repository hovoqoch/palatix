@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('breadcrump')
    <h3 class="kt-subheader__title">
        {{$meta_title}} </h3>
    <div class="kt-subheader__breadcrumbs">
        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
        <span class="kt-subheader__breadcrumbs-separator"></span>
        <a href="" class="kt-subheader__breadcrumbs-link">
            {{$meta_title}} </a>
    </div>
@endsection
@section('description')
    <div class="description">{{$translations['doc_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')

    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch"
                     id="kt_body">
                    <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                         id="kt_content">

                        <!-- end:: Subheader -->

                        <!-- begin:: Content -->
                        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

                            <!--Begin::Dashboard 3-->

                            <!--Begin::Row-->
                            <div class="row">
                                <div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">

                                    <!--begin:: Widgets/Trends-->
                                    <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    {{$translations['your_device_lists'] ?? 'Your Device List'}}
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fluid kt-portlet__body--fit">

                                                <div class="table-responsive pp-25">
                                                    <table id="tableDoctor" class="table">
                                                        <thead class="thead-light">
                                                        <tr>
                                                            <th> {{$translations['name'] ?? 'name'}}</th>
                                                            <th> {{$translations['serial_number'] ?? 'serial_number'}}</th>
                                                            <th> {{$translations['software_version'] ?? 'software_version'}}</th>
                                                            <th> {{$translations['parameter_version'] ?? 'parameter_version'}}</th>
                                                            <th> {{$translations['last_validation'] ?? 'last_validation'}}</th>
                                                            <th> {{$translations['next_validation'] ?? 'next_validation'}}</th>
                                                            <th> {{$translations['action'] ?? 'action'}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>

                                                </div>
                                        </div>

                                    </div>
                                    <!--end:: Widgets/Trends-->
                                </div>
                                {{-- <div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">

                                     <!--begin:: Widgets/Sales Stats-->
                                     <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                                         <div class="kt-portlet__head kt-portlet__head--noborder">
                                             <div class="kt-portlet__head-label">
                                                 <h3 class="kt-portlet__head-title">
                                                     Your Device List
                                                 </h3>
                                             </div>

                                         </div>
                                         <div class="kt-portlet__body kt-portlet__body--fluid kt-portlet__body--fit">
                                             @if(count($devices) > 0)
                                                 <div class="kt-widget4 kt-widget4--sticky">
                                                     <div class="kt-widget4__chart">
                                                         <div class="kt-portlet__body">
                                                             <div class="kt-widget6">
                                                                 <div class="kt-widget6__head">
                                                                     <div class="kt-widget6__item">
                                                                         <span>Practice Name</span>
                                                                         <span>Device Name</span>
                                                                         <span>Valid until</span>
                                                                         <span>Actions</span>
                                                                     </div>
                                                                 </div>
                                                                 <div class="kt-widget6__body">

                                                                 </div>

                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             @else
                                                 <h3 class="p-25">No Devices to show</h3>
                                             @endif
                                         </div>
                                     </div>

                                     <!--end:: Widgets/Sales Stats-->
                                 </div>--}}
                            </div>

                            <!--End::Row-->

                            <!--End::Dashboard 3-->
                        </div>

                        <!-- end:: Content -->
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        let url = "{{route("doctor.home",["local"=>app()->getLocale()])}}";
        $('#tableDoctor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": url,
            "language": {
                "emptyTable": "{{ $translations['no_devices'] ?? 'No Devices'}}"
            },
            "columns": [
                {
                    "name":"device.name",
                    "data": "device",
                    "render":function (device) {
                        return device ? device.name : "-";

                    }
                },
                {"data": "serial_number",},
                {"data": "software_version",},
                {"data":"parameter_version"},
                {
                    "data":"last_validation_date",
                    "render":function (date) {
                        return new Date(parseInt(date)*1000).toLocaleDateString();
                    }
                },
                {
                    "data":"next_validation_date",
                    "render":function (date) {
                        return new Date(parseInt(date)*1000).toLocaleDateString();
                    }
                },
                {
                    "searchable":false,
                    "orderable":false,
                    "data": "id",
                    "render": function (id) {
                        let a = document.createElement('a');
                        a.className = "btn btn-success my_btn1";
                        a.setAttribute("role", "button");
                        a.href = `${url}/devices/${id}/edit`;
                        a.innerText = "{{ $translations['edit'] ?? 'edit' }}";
                        return a.outerHTML;
                    }
                },
            ],
        });
    </script>
@endpush
