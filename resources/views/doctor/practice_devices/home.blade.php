@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('actionbutton')
    <div class="kt-subheader__wrapper">

        <a class="btn btn-danger" href="{{ url('/') }}/{{app()->getLocale()}}/account/devices/create">
            {{ $translations['add_new_device_to_practice'] ?? 'add_new_device_to_practice'}}
        </a>

    </div>
@endsection
@section('description')
    <div class="description">{{$translations['doc_practice_devices_home_desc'] ?? 'action'}}</div>
@endsection
@section('content')
        <div class="table-responsive">
            <table id="tableDoctorDevices" class="table">
                <thead class="thead-light">
                <tr>
                    <th>{{ $translations['name'] ?? 'name'}}</th>
                    <th>{{ $translations['manufacturer'] ?? 'manufacturer'}}</th>
                    <th>{{ $translations['type'] ?? 'type'}}</th>
                    <th>{{ $translations['action'] ?? 'action' }}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
@endsection
@push('modal')
    @include('partials.delete', ['modal_title' => $translations['are_you_sure'] ?? 'are_you_sure'])
@endpush
@push('scripts')
    <script>
        let url = "{{route("doctor.devices.index",["local"=>app()->getLocale()])}}";
        $('#tableDoctorDevices').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": url,
            "language": {
                "emptyTable": "{{ $translations['no_device_to_show'] ?? 'No Device To Show'}}"
            },
            "columns": [
                {
                    "name":"device.name",
                    "data": "device",
                    "render": function (device) {
                        return device ? device.name : '-';
                    }
                },
                {
                    "name":"device.manufacturer.name",
                    "data": "device.manufacturer",
                    "render": function (manufacturer) {
                        return manufacturer ? manufacturer.name : '-';
                    }
                },
                {
                    "name":"device.type.name",
                    "data": "device.type",
                    "render": function (type) {
                        return type ? type.name : '-';
                    }
                },
                {
                    "searchable": false,
                    "orderable": false,
                    "data": "id",
                    "className": "my_action",
                    "render": function (id) {
                        let a = document.createElement('a');
                        let button = document.createElement("button");
                        let a2 = document.createElement('a');
                        a.className = "btn btn-success my_btn1";
                        a.setAttribute("role", "button");
                        a.href = `${url}/${id}/edit`;
                        a.innerText = "{{ $translations['edit'] ?? 'edit' }}";
                        a2.className = "btn btn-info";
                        a2.href = `{{ url('/') }}/{{app()->getLocale()}}/account/report/${id}`;
                        a2.innerText ="Generate validation report";
                        button.type = "button";
                        button.className = "btn btn-danger delete_action";
                        button.setAttribute("data-form_action", `${url}/${id}`);
                        button.setAttribute("data-toggle", "modal");
                        button.setAttribute("data-target", "#deleteModal");
                        button.innerText = "{{ $translations['delete'] ?? 'Delete' }}";
                        return `${a.outerHTML} ${button.outerHTML}`;
                    }
                },
            ],
        });
    </script>
@endpush
