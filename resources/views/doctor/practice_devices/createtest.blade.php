@extends('layouts.doctor')
@section('meta_title', $meta_title)
@section('description')
    <div class="description">{{$translations['doc_test_creat_desc'] ?? 'action'}}</div>
@endsection
@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ url('/') }}/{{app()->getLocale()}}/account/createtest/store" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Test Name</label>
                        <div class="col-lg-9 col-xl-9">
                            <input class="form-control" placeholder="Enter Test name" id="name" type="text" name="name" value="{{ old('name') }}" required>
                        </div>
                    </div>
                    <input type="hidden" value="{{$device->id}}" name="device_id">
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Test Summary</label>
                        <div class="col-lg-9 col-xl-9">
                            <textarea placeholder="Enter Test summary" name="summary" class="form-control" cols="30" rows="4" required value="{{ old('summary') }}"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Youtube video</label>
                        <div class="col-lg-9 col-xl-9">
                            <input class="form-control" id="youtube_link" type="url" name="youtube_link" value="{{ old('youtube_link') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Vimeo Video</label>
                        <div class="col-lg-9 col-xl-9">
                            <input class="form-control" id="vimeo_link" type="url" name="vimeo_link" value="{{ old('vimeo_link') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Attach Document</label>

                        <div class="col-lg-9 col-xl-9">
                            <input type="file" class="custom-file-input" id="attach_file_name" name="attach_file_name" value="{{ old('attach_file_name') }}" accept="application/*">
                            <label class="custom-file-label" for="attach_file_name">Choose file</label>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ $translations['create'] ?? 'Create' }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
