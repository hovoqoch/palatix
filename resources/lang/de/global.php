<?php

return [
    'doctor' => 'Доктор',
    'dashboard' => 'Главная',
    'employee' => 'Работник',
    'devices' => 'Оборудывание',
    'services' => 'Сервиси',
    'recall' => 'Рекол',
    'add new doc' => 'Добавить нового доктора',
    'edit' => 'Редактировать',
    'delete' => 'Удалить',
    'name' => 'Имя',
    'role' => 'Роль',
    'action' => 'Действие',
    'translations' => 'Переводы',
    'settings' => 'Settings',
    'status' => 'Статус',
    'add new dev' => 'Add new device',
    'no dev' => 'No devices',
];
