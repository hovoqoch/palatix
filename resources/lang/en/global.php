<?php

return [
    'doctor' => 'Doctors',
    'dashboard' => 'Dashboard',
    'employee' => 'Employees',
    'devices' => 'Devices',
    'services' => 'Services',
    'recall' => 'Recall rhythms',
    'add new doc' => 'Add new doctor',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'name' => 'Name',
    'role' => 'Role',
    'action' => 'Action',
    'translations' => 'Translations',
    'settings' => 'Settings',
    'status' => 'Settings',
    'add new dev' => 'Add new device',
    'no dev' => 'No devices',
];
